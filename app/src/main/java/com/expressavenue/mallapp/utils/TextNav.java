package com.expressavenue.mallapp.utils;

import android.location.Location;
import android.util.Log;

import com.expressavenue.mallapp.pojos.Store;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by nivedith on 1/8/2016.
 */
public class TextNav {

    private Location srcLoc;
    private Location destLoc;
    private ArrayList<String> routePathNodes;
    private Hashtable<String, Location> nodeHashTable;
    private ArrayList<Location> nodeLocations;
    private ArrayList<Store> storeList;

    private float fromStartBearing = 0;
    private float toStartBearing = 0;
    private float fromEndBearing = 0;
    private float toEndBearing = 0;

    private String floorName = "";
    private String storeName = "";

    private String nearestSource = "";
    private String nearestDestination = "";

    private ArrayList<String> directionList;

    public TextNav(Location srcLoc, Location destLoc, ArrayList<String> routePathNodes, Hashtable<String, Location> nodeHashTable, ArrayList<Store> storeList, String floorName){

        this.srcLoc = srcLoc;
        this.destLoc = destLoc;
        this.routePathNodes = routePathNodes;
        this.nodeHashTable = nodeHashTable;
        this.storeList = storeList;
        this.floorName = floorName;

        nodeLocations = new ArrayList<Location>();
        directionList = new ArrayList<String>();

        getNodeLocations();
    }

    private void getNodeLocations(){

        if(!nodeLocations.isEmpty())
            nodeLocations.clear();

        for(String node: routePathNodes){
            nodeLocations.add(nodeHashTable.get(node));
        }
    }

    public ArrayList getTextNav(){

        Location sLoc = srcLoc;
        Location dLoc = destLoc;
        ArrayList<Location> nodeLocs = nodeLocations;

        float fromBearing = 0;
        float toBearing = 0;
        String nearestStore = " ";

        if(!directionList.isEmpty())
            directionList.clear();

        fromStartBearing = getBearing(sLoc, nodeLocs.get(0));
        toStartBearing = getBearing(nodeLocs.get(0), nodeLocs.get(1));
        nearestSource = getNearestStore(nodeLocs.get(1));
        getNavForSrc(fromStartBearing, toStartBearing);

        for(int i = 0; i < nodeLocs.size(); i++){
            if(i > 0 && i < nodeLocs.size()-3){
                fromBearing = getBearing(nodeLocs.get(i), nodeLocs.get(i+1));
                toBearing = getBearing(nodeLocs.get(i+1), nodeLocs.get(i+2));
                nearestStore = getNearestStore(nodeLocs.get(i+2));

                if(0 <= fromBearing && fromBearing <= 180){
                    if(fromBearing >= 90 && fromBearing <= 180){
                        if(fromBearing >= 90 && fromBearing <= 120){
                            if(0 <= toBearing && toBearing <= 90){
                                if(toBearing > 20 && toBearing < 70){
                                    directionList.add("MSL* "+nearestStore);
                                }else if(toBearing >= 70 && toBearing <= 90){
                                    directionList.add("MF* "+nearestStore);
                                }else {
                                    directionList.add("ML* "+nearestStore);
                                }
                            }else if(toBearing > -90){
                                directionList.add("ML* "+nearestStore);
                            }else if(toBearing > 90 && toBearing < 180){
                                if(toBearing >= 110 &&toBearing <= 140){
                                    directionList.add("MSR* "+nearestStore);
                                }else if(toBearing > 140){
                                    directionList.add("MR* "+nearestStore);
                                }else {
                                    directionList.add("MF* "+nearestStore);
                                }
                            }else if(toBearing <= -90 && toBearing >= 180){
                                directionList.add("MR* "+nearestStore);
                            }
                        }else if(fromBearing > 120){
                            if(toBearing >= 0 && toBearing <= 90){
                                directionList.add("ML* "+nearestStore);
                            }else if(toBearing >= -90){
                                directionList.add("MR* "+nearestStore);
                            }else if(toBearing > 90 && toBearing < 180){
                                if(toBearing >= 110 &&toBearing <= 160){
                                    directionList.add("MSL* "+nearestStore);
                                }else if(toBearing>160){
                                    directionList.add("MF* "+nearestStore);
                                }else {
                                    directionList.add("ML* "+nearestStore);
                                }
                            }else if(toBearing <= -90 && toBearing > -180){
                                if(toBearing >= -180 && toBearing > 130){
                                    directionList.add("MF* "+nearestStore);
                                }else {
                                    directionList.add("MR* "+nearestStore);
                                }
                            }
                        }
                    }else {
                        if(fromBearing >= 80 && fromBearing <= 90){
                            if(toBearing>=0 && toBearing<=90){
                                if(toBearing>=0 && toBearing<=20){
                                    directionList.add("ML* "+nearestStore);
                                }else if(toBearing > 20 && toBearing <= 70){
                                    directionList.add("MSL* "+nearestStore);
                                }else {
                                    directionList.add("MF* "+nearestStore);
                                }
                            }else if (toBearing >= 270){
                                directionList.add("ML* "+nearestStore);
                            }else if (toBearing > 90 && toBearing < 180){
                                if(toBearing >= 110 &&toBearing <= 160){
                                    directionList.add("MSR* "+nearestStore);
                                }else if(toBearing > 160){
                                    directionList.add("MR* "+nearestStore);
                                }else{
                                    directionList.add("MF* "+nearestStore);
                                }
                            }else if(toBearing <= -90 && toBearing < -180){
                                directionList.add("MR* "+nearestStore);
                            }
                        }else if(fromBearing < 80 && fromBearing > 20){
                            if(toBearing >= 0 && toBearing < 90){
                                if(toBearing >= 0 && toBearing <= 20){
                                    directionList.add("ML* "+nearestStore);
                                }else if (toBearing > 20 && toBearing <= 80){
                                    directionList.add("MSL* "+nearestStore);
                                }else{
                                    directionList.add("MR* "+nearestStore);
                                }
                            }else if (toBearing <= -90){
                                directionList.add("ML* "+nearestStore);
                            }else if (toBearing >= 90 && toBearing < 180){
                                directionList.add("MR* "+nearestStore);
                            }else if(toBearing >= -180 && toBearing < -90){
                                directionList.add("MR* "+nearestStore);
                            }
                        }else{
                            if(toBearing >= 0 && toBearing < 90){
                                if(toBearing >= 0 && toBearing <= 20){
                                    directionList.add("MF* "+nearestStore);
                                }else if (toBearing > 20 && toBearing <= 80){
                                    directionList.add("MSR* "+nearestStore);
                                }else{
                                    directionList.add("MR* "+nearestStore);
                                }
                            }else if (toBearing >= -90){
                                directionList.add("ML* "+nearestStore);
                            }else if (toBearing >= 90 && toBearing < 180){
                                directionList.add("MR* "+nearestStore);
                            }else if(toBearing >= -180 && toBearing < -90)
                            {
                                directionList.add("ML* "+nearestStore);
                            }
                        }

                    }
                }else if (fromBearing > -180 && fromBearing <= 0){
                    if (fromBearing <= -90 && fromBearing >= -110){
                        if(0 <= toBearing && toBearing > -90){
                            if(toBearing <= -100 && toBearing >= -150){
                                directionList.add("MSL* "+nearestStore);
                            }else if(toBearing < -150){
                                directionList.add("MR* "+nearestStore);
                            }else{
                                directionList.add("MF* "+nearestStore);
                            }
                        }else if (toBearing <= -90 && toBearing > -180){
                            if(toBearing >= -80 && toBearing <= -40){
                                directionList.add("MSL* "+nearestStore);
                            }else if (toBearing <= -70 &&toBearing >= -90){
                                directionList.add("MF* "+nearestStore);
                            }else{
                                directionList.add("ML* "+nearestStore);
                            }
                        }else if (toBearing >= 0 && toBearing < 90){
                            directionList.add("MR* "+nearestStore);
                        }else if(toBearing <= 180 && toBearing > 90){
                            directionList.add("ML* "+nearestStore);
                        }
                    }else if (fromBearing >= -70 && fromBearing <= -10){
                        if( 0 >= toBearing && toBearing < -90){
                            if(toBearing >= -70 &&toBearing <= -30){
                                directionList.add("MSR* "+nearestStore);
                            }else if(toBearing > -30){
                                directionList.add("MR* "+nearestStore);
                            }else{
                                directionList.add("MSL* "+nearestStore);
                            }
                        }
                    }
                }
            }

        }
        fromEndBearing = getBearing(nodeLocs.get(nodeLocs.size()-3), nodeLocs.get(nodeLocs.size()-2));
        toEndBearing = getBearing(nodeLocs.get(nodeLocs.size()-2), dLoc);
        nearestDestination = getNearestStore(nodeLocs.get(nodeLocs.size()-1));
        getNavForDest(fromEndBearing, toEndBearing);

        for (int i = 0; i < directionList.size(); i++)
            Log.i("TextNav", directionList.get(i));

        return directionList;
    }

    private void getNavForSrc(float fromStartBearing, float toStartBearing ){
        if(0 <= fromStartBearing && fromStartBearing <= 180){
            if(fromStartBearing >= 90 && fromStartBearing <= 180){
                if(fromStartBearing >= 90 && fromStartBearing <= 120){
                    if(0 <= toStartBearing && toStartBearing <= 90){
                        if(toStartBearing > 20 && toStartBearing < 70){
                            directionList.add("MSL* " + nearestSource);
                        }else if(toStartBearing >= 70 && toStartBearing <= 90){
                            directionList.add("MF* " + nearestSource);
                        }else {
                            directionList.add("ML* " + nearestSource);
                        }
                    }else if(toStartBearing > -90){
                        directionList.add("ML* " + nearestSource);
                    }else if(toStartBearing > 90 && toStartBearing < 180){
                        if(toStartBearing >= 110 &&toStartBearing <= 140){
                            directionList.add("MSR* " + nearestSource);
                        }else if(toStartBearing > 140){
                            directionList.add("MR* " + nearestSource);
                        }else {
                            directionList.add("MF* " + nearestSource);
                        }
                    }else if(toStartBearing <= -90 && toStartBearing >= 180){
                        directionList.add("MR* " + nearestSource);
                    }
                }else if(fromStartBearing > 120){
                    if(toStartBearing >= 0 && toStartBearing <= 90){
                        directionList.add("ML* " + nearestSource);
                    }else if(toStartBearing >= -90){
                        directionList.add("MR* " + nearestSource);
                    }else if(toStartBearing > 90 && toStartBearing < 180){
                        if(toStartBearing >= 110 &&toStartBearing <= 160){
                            directionList.add("MSL* " + nearestSource);
                        }else if(toStartBearing>160){
                            directionList.add("MF* " + nearestSource);
                        }else {
                            directionList.add("ML* " + nearestSource);
                        }
                    }else if(toStartBearing <= -90 && toStartBearing > -180){
                        if(toStartBearing >= -180 && toStartBearing > 130){
                            directionList.add("MF* " + nearestSource);
                        }else {
                            directionList.add("MR* " + nearestSource);
                        }
                    }
                }
            }else {
                if(fromStartBearing >= 80 && fromStartBearing <= 90){
                    if(toStartBearing>=0 && toStartBearing <=90){
                        if(toStartBearing>=0 && toStartBearing<=20){
                            directionList.add("ML* " + nearestSource);
                        }else if(toStartBearing > 20 && toStartBearing <= 70){
                            directionList.add("MSL* " + nearestSource);
                        }else {
                            directionList.add("MF* " + nearestSource);
                        }
                    }else if (toStartBearing >= 270){
                        directionList.add("ML* " + nearestSource);
                    }else if (toStartBearing > 90 && toStartBearing < 180){
                        if(toStartBearing >= 110 && toStartBearing <= 160){
                            directionList.add("MSR* " + nearestSource);
                        }else if(toStartBearing > 160){
                            directionList.add("MR* " + nearestSource);
                        }else{
                            directionList.add("MF* " + nearestSource);
                        }
                    }else if(toStartBearing <= -90 && toStartBearing < -180){
                        directionList.add("MR* " + nearestSource);
                    }
                }else if(fromStartBearing < 80 && fromStartBearing > 20){
                    if(toStartBearing >= 0 && toStartBearing < 90){
                        if(toStartBearing >= 0 && toStartBearing <= 20){
                            directionList.add("ML* " + nearestSource);
                        }else if (toStartBearing > 20 && toStartBearing <= 80){
                            directionList.add("MSL* " + nearestSource);
                        }else{
                            directionList.add("MR* " + nearestSource);
                        }
                    }else if (toStartBearing <= -90){
                        directionList.add("ML* " + nearestSource);
                    }else if (toStartBearing >= 90 && toStartBearing < 180){
                        directionList.add("MR* " + nearestSource);
                    }else if(toStartBearing >= -180 && toStartBearing < -90){
                        directionList.add("MR* " + nearestSource);
                    }
                }else{
                    if(toStartBearing >= 0 && toStartBearing < 90){
                        if(toStartBearing >= 0 && toStartBearing <= 20){
                            directionList.add("MF* " + nearestSource);
                        }else if (toStartBearing > 20 && toStartBearing <= 80){
                            directionList.add("MSR* " + nearestSource);
                        }else{
                            directionList.add("MR* " + nearestSource);
                        }
                    }else if (toStartBearing >= -90){
                        directionList.add("ML* " + nearestSource);
                    }else if (toStartBearing >= 90 && toStartBearing < 180){
                        directionList.add("MR* " + nearestSource);
                    }else if(toStartBearing >= -180 && toStartBearing < -90)
                    {
                        directionList.add("ML* " + nearestSource);
                    }
                }

            }
        }else if (fromStartBearing > -180 && fromStartBearing <= 0){
            if (fromStartBearing <= -90 && fromStartBearing >= -110){
                if(0 <= toStartBearing && toStartBearing > -90){
                    if(toStartBearing <= -100 && toStartBearing >= -150){
                        directionList.add("MSL* " + nearestSource);
                    }else if(toStartBearing < -150){
                        directionList.add("MR* " + nearestSource);
                    }else{
                        directionList.add("MF* " + nearestSource);
                    }
                }else if (toStartBearing <= -90 && toStartBearing > -180){
                    if(toStartBearing >= -80 && toStartBearing <= -40){
                        directionList.add("MSL* " + nearestSource);
                    }else if (toStartBearing <= -70 &&toStartBearing >= -90){
                        directionList.add("MF* " + nearestSource);
                    }else{
                        directionList.add("ML* " + nearestSource);
                    }
                }else if (toStartBearing >= 0 && toStartBearing < 90){
                    directionList.add("MR* " + nearestSource);
                }else if(toStartBearing <= 180 && toStartBearing > 90){
                    directionList.add("ML* " + nearestSource);
                }
            }else if (fromStartBearing >= -70 && fromStartBearing <= -10){
                if( 0 >= toStartBearing && toStartBearing < -90){
                    if(toStartBearing >= -70 &&toStartBearing <= -30){
                        directionList.add("MSR* " + nearestSource);
                    }else if(toStartBearing > -30){
                        directionList.add("MR* " + nearestSource);
                    }else{
                        directionList.add("MSL* " + nearestSource);
                    }
                }
            }
        }
    }

    private void getNavForDest(float fromStartBearing, float toStartBearing ){
        if(0 <= fromStartBearing && fromStartBearing <= 180){
            if(fromStartBearing >= 90 && fromStartBearing <= 180){
                if(fromStartBearing >= 90 && fromStartBearing <= 120){
                    if(0 <= toStartBearing && toStartBearing <= 90){
                        if(toStartBearing > 20 && toStartBearing < 70){
                            directionList.add("MSL* " + nearestDestination);
                        }else if(toStartBearing >= 70 && toStartBearing <= 90){
                            directionList.add("MF* " + nearestDestination);
                        }else {
                            directionList.add("ML* " + nearestDestination);
                        }
                    }else if(toStartBearing > -90){
                        directionList.add("ML* " + nearestDestination);
                    }else if(toStartBearing > 90 && toStartBearing < 180){
                        if(toStartBearing >= 110 &&toStartBearing <= 140){
                            directionList.add("MSR* " + nearestDestination);
                        }else if(toStartBearing > 140){
                            directionList.add("MR* " + nearestDestination);
                        }else {
                            directionList.add("MF* " + nearestDestination);
                        }
                    }else if(toStartBearing <= -90 && toStartBearing >= 180){
                        directionList.add("MR* " + nearestDestination);
                    }
                }else if(fromStartBearing > 120){
                    if(toStartBearing >= 0 && toStartBearing <= 90){
                        directionList.add("ML* " + nearestDestination);
                    }else if(toStartBearing >= -90){
                        directionList.add("MR* " + nearestDestination);
                    }else if(toStartBearing > 90 && toStartBearing < 180){
                        if(toStartBearing >= 110 &&toStartBearing <= 160){
                            directionList.add("MSL* " + nearestDestination);
                        }else if(toStartBearing>160){
                            directionList.add("MF* " + nearestDestination);
                        }else {
                            directionList.add("ML* " + nearestDestination);
                        }
                    }else if(toStartBearing <= -90 && toStartBearing > -180){
                        if(toStartBearing >= -180 && toStartBearing > 130){
                            directionList.add("MF* " + nearestDestination);
                        }else {
                            directionList.add("MR* " + nearestDestination);
                        }
                    }
                }
            }else {
                if(fromStartBearing >= 80 && fromStartBearing <= 90){
                    if(toStartBearing>=0 && toStartBearing <=90){
                        if(toStartBearing>=0 && toStartBearing<=20){
                            directionList.add("ML* " + nearestDestination);
                        }else if(toStartBearing > 20 && toStartBearing <= 70){
                            directionList.add("MSL* " + nearestDestination);
                        }else {
                            directionList.add("MF* " + nearestDestination);
                        }
                    }else if (toStartBearing >= 270){
                        directionList.add("ML* " + nearestDestination);
                    }else if (toStartBearing > 90 && toStartBearing < 180){
                        if(toStartBearing >= 110 && toStartBearing <= 160){
                            directionList.add("MSR* " + nearestDestination);
                        }else if(toStartBearing > 160){
                            directionList.add("MR* " + nearestDestination);
                        }else{
                            directionList.add("MF* " + nearestDestination);
                        }
                    }else if(toStartBearing <= -90 && toStartBearing < -180){
                        directionList.add("MR* " + nearestDestination);
                    }
                }else if(fromStartBearing < 80 && fromStartBearing > 20){
                    if(toStartBearing >= 0 && toStartBearing < 90){
                        if(toStartBearing >= 0 && toStartBearing <= 20){
                            directionList.add("ML* " + nearestDestination);
                        }else if (toStartBearing > 20 && toStartBearing <= 80){
                            directionList.add("MSL* " + nearestDestination);
                        }else{
                            directionList.add("MR* " + nearestDestination);
                        }
                    }else if (toStartBearing <= -90){
                        directionList.add("ML* " + nearestDestination);
                    }else if (toStartBearing >= 90 && toStartBearing < 180){
                        directionList.add("MR* " + nearestDestination);
                    }else if(toStartBearing >= -180 && toStartBearing < -90){
                        directionList.add("MR* " + nearestDestination);
                    }
                }else{
                    if(toStartBearing >= 0 && toStartBearing < 90){
                        if(toStartBearing >= 0 && toStartBearing <= 20){
                            directionList.add("MF* " + nearestDestination);
                        }else if (toStartBearing > 20 && toStartBearing <= 80){
                            directionList.add("MSR* " + nearestDestination);
                        }else{
                            directionList.add("MR* " + nearestDestination);
                        }
                    }else if (toStartBearing >= -90){
                        directionList.add("ML* " + nearestDestination);
                    }else if (toStartBearing >= 90 && toStartBearing < 180){
                        directionList.add("MR* " + nearestDestination);
                    }else if(toStartBearing >= -180 && toStartBearing < -90)
                    {
                        directionList.add("ML* " + nearestDestination);
                    }
                }

            }
        }else if (fromStartBearing > -180 && fromStartBearing <= 0){
            if (fromStartBearing <= -90 && fromStartBearing >= -110){
                if(0 <= toStartBearing && toStartBearing > -90){
                    if(toStartBearing <= -100 && toStartBearing >= -150){
                        directionList.add("MSL* " + nearestDestination);
                    }else if(toStartBearing < -150){
                        directionList.add("MR* " + nearestDestination);
                    }else{
                        directionList.add("MF* " + nearestDestination);
                    }
                }else if (toStartBearing <= -90 && toStartBearing > -180){
                    if(toStartBearing >= -80 && toStartBearing <= -40){
                        directionList.add("MSL* " + nearestDestination);
                    }else if (toStartBearing <= -70 &&toStartBearing >= -90){
                        directionList.add("MF* " + nearestDestination);
                    }else{
                        directionList.add("ML* " + nearestDestination);
                    }
                }else if (toStartBearing >= 0 && toStartBearing < 90){
                    directionList.add("MR* " + nearestDestination);
                }else if(toStartBearing <= 180 && toStartBearing > 90){
                    directionList.add("ML* " + nearestDestination);
                }
            }else if (fromStartBearing >= -70 && fromStartBearing <= -10){
                if( 0 >= toStartBearing && toStartBearing < -90){
                    if(toStartBearing >= -70 &&toStartBearing <= -30){
                        directionList.add("MSR* " + nearestDestination);
                    }else if(toStartBearing > -30){
                        directionList.add("MR* " + nearestDestination);
                    }else{
                        directionList.add("MSL* " + nearestDestination);
                    }
                }
            }
        }
    }
    private Float getBearing(Location srcLoc, Location destLoc){
        return srcLoc.bearingTo(destLoc);
    }
    public String getNearestStore(Location jLocation){

        String storeName = "";
        ArrayList<Double> distValues = new ArrayList<Double>();
        Hashtable<Double, String> distStoreHash = new Hashtable<Double, String>();
        Location nodeLocation = jLocation;

        for(Store store : storeList){
            if(store.getStoreFloor().equals(floorName)){
                String lat = store.getStoreLatitude();
                String lon = store.getStoreLongitude();
                if(!lat.equals("NA") && !lon.equals("NA")){

                    Location location = new Location("");
                    location.setLatitude(Double.parseDouble(lat));
                    location.setLongitude(Double.parseDouble(lon));
                    double dist = location.distanceTo(nodeLocation);
                    distValues.add(dist);
                    distStoreHash.put(dist, store.getStoreName());
                }
            }


        }

        double smallest = Double.MAX_VALUE;
        for(int i = 0; i < distValues.size(); i++) {
            if(smallest > distValues.get(i)) {
                smallest = distValues.get(i);
            }
        }

        storeName = distStoreHash.get(smallest);
        if(storeName == null)
            storeName = " ";
        return storeName;
    }
}
