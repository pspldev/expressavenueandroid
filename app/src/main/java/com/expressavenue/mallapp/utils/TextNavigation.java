package com.expressavenue.mallapp.utils;

import android.location.Location;

import com.expressavenue.mallapp.pojos.Store;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by nivedith on 1/8/2016.
 */
public class TextNavigation {

    private Location srcLoc = null;
    private Location destLoc = null;
    private ArrayList<Location> junctionNodeLocations;
    private ArrayList<String> junctionNodes;
    private ArrayList<Store> storeList;

    private Hashtable<String, Location> nodeDistTable;

    private ArrayList<String> textualDesc;
    private String storeName = "";
    private String floorName = "";
    private Location nearestToDestLoc= null;

    public TextNavigation(Location srcLoc, Location destLoc, ArrayList<Location> junctionNodeLocations,
                          ArrayList<String> junctionNodes, ArrayList<Store> storeList, Hashtable<String, Location> nodeDistTable, String floorName){

        this.srcLoc = srcLoc;
        this.destLoc = destLoc;
        this.junctionNodeLocations = junctionNodeLocations;
        this.junctionNodes = junctionNodes;
        this.storeList = storeList;
        this.nodeDistTable = nodeDistTable;
        this.floorName = floorName;

        textualDesc = new ArrayList<String>();
        if(!textualDesc.isEmpty())
            textualDesc.clear();

    }

    public ArrayList getNavText(){

        if(!textualDesc.isEmpty())
            textualDesc.clear();

        String directionText = "";
        String nearestStore = "";

        float fromBearing = 0;
        float toBearing = 0;

        if(junctionNodeLocations.size() > 2){

            fromBearing = getBearing(srcLoc, junctionNodeLocations.get(0));
            toBearing = getBearing(junctionNodeLocations.get(0), junctionNodeLocations.get(1));

            nearestStore = getNearestStore(junctionNodeLocations.get(1));

            if(fromBearing <= 0 && fromBearing >= -45){
                directionText =  getDirectionTo_NNW(toBearing, nearestStore);
            }else if (fromBearing < -45 && fromBearing >= -90 ){
                directionText = getDirectionTo_WNW(toBearing, nearestStore);
            }else if (fromBearing < -90 && fromBearing >= -135){
                directionText = getDirectionTo_WSW(toBearing, nearestStore);
            }else if (fromBearing < -135 && fromBearing >= -180 ){
                directionText = getDirectionTo_SSW(toBearing, nearestStore);
            }else if (fromBearing <= 180 && fromBearing >= 135){
                directionText = getDirectionTo_SSE(toBearing, nearestStore);
            }else if (fromBearing < 135 && fromBearing >= 90){
                directionText = getDirectionTo_ESE(toBearing, nearestStore);
            }else if (fromBearing < 90 && fromBearing >= 45){
                directionText = getDirectionTo_ENE(toBearing, nearestStore);
            }else if (fromBearing < 45 && fromBearing >= 0){
                directionText = getDirectionTo_NNE(toBearing, nearestStore);
            }

            textualDesc.add(directionText);

            for(int i = 0; i < junctionNodeLocations.size() - 2; i++){

                fromBearing = getBearing(junctionNodeLocations.get(i), junctionNodeLocations.get(i+1));
                toBearing = getBearing(junctionNodeLocations.get(i+1), junctionNodeLocations.get(i+2));

                nearestStore = getNearestStore(junctionNodeLocations.get(i + 2));

                if(fromBearing <= 0 && fromBearing >= -45){
                    directionText =  getDirectionTo_NNW(toBearing, nearestStore);
                }else if (fromBearing < -45 && fromBearing >= -90 ){
                    directionText = getDirectionTo_WNW(toBearing, nearestStore);
                }else if (fromBearing < -90 && fromBearing >= -135){
                    directionText = getDirectionTo_WSW(toBearing, nearestStore);
                }else if (fromBearing < -135 && fromBearing >= -180 ){
                    directionText = getDirectionTo_SSW(toBearing, nearestStore);
                }else if (fromBearing <= 180 && fromBearing >= 135){
                    directionText = getDirectionTo_SSE(toBearing, nearestStore);
                }else if (fromBearing < 135 && fromBearing >= 90){
                    directionText = getDirectionTo_ESE(toBearing, nearestStore);
                }else if (fromBearing < 90 && fromBearing >= 45){
                    directionText = getDirectionTo_ENE(toBearing, nearestStore);
                }else if (fromBearing < 45 && fromBearing >= 0){
                    directionText = getDirectionTo_NNE(toBearing, nearestStore);
                }

                textualDesc.add(directionText);

            }

            int lastNodeIndex = junctionNodeLocations.size() - 1;
            int lastSecondNodeIndex = junctionNodeLocations.size() - 2;

            fromBearing = getBearing(junctionNodeLocations.get(lastSecondNodeIndex), junctionNodeLocations.get(lastNodeIndex));
            toBearing = getBearing(junctionNodeLocations.get(lastNodeIndex), destLoc);

            if(fromBearing <= 0 && fromBearing >= -45){
                directionText =  getDirectionTo_NNW(toBearing,"");
            }else if (fromBearing < -45 && fromBearing >= -90 ){
                directionText = getDirectionTo_WNW(toBearing, "");
            }else if (fromBearing < -90 && fromBearing >= -135){
                directionText = getDirectionTo_WSW(toBearing, "");
            }else if (fromBearing < -135 && fromBearing >= -180 ){
                directionText = getDirectionTo_SSW(toBearing, "");
            }else if (fromBearing <= 180 && fromBearing >= 135){
                directionText = getDirectionTo_SSE(toBearing, "");
            }else if (fromBearing < 135 && fromBearing >= 90){
                directionText = getDirectionTo_ESE(toBearing, "");
            }else if (fromBearing < 90 && fromBearing >= 45){
                directionText = getDirectionTo_ENE(toBearing, "");
            }else if (fromBearing < 45 && fromBearing >= 0){
                directionText = getDirectionTo_NNE(toBearing, "");
            }

            textualDesc.add(directionText);

        }else{

            textualDesc.add("*You are now adjacent to the Destination");
        }

        return textualDesc;
    }

    private String getDirectionTo_NNW(float toBearing, String storeName){

        String text = "";

        if (toBearing < 20 && toBearing >= -20)
            text = "MF";
        else if (toBearing < -25 && toBearing >= -45)
            text = "MSL";
        else if (toBearing < -45 && toBearing >= -90)
            text = "ML";
        else if (toBearing < -90 && toBearing >= -135)
            text = "ML";
        else if (toBearing < -135 && toBearing >= -180)
            text = "ML";
        else if (toBearing < 180 && toBearing >= 135)
            text = "MB";
        else if (toBearing < 135 && toBearing >= 90)
            text = "MR";
        else if (toBearing < 90 && toBearing >= 45)
            text = "MR";
        else if (toBearing < 45 && toBearing >= 20)
            text = "MSR";

        return text+"*"+storeName;
    }

    private String getDirectionTo_WNW(float toBearing, String storeName){

        String text = "";

        if (toBearing < 20 && toBearing >= -20)
            text = "MF";
        else if (toBearing < -25 && toBearing >= -45)
            text = "MSL";
        else if (toBearing < -45 && toBearing >= -90)
            text = "ML";
        else if (toBearing < -90 && toBearing >= -135)
            text = "ML";
        else if (toBearing < -135 && toBearing >= -180)
            text = "ML";
        else if (toBearing < 180 && toBearing >= 135)
            text = "MR";
        else if (toBearing < 135 && toBearing >= 90)
            text = "MB";
        else if (toBearing < 90 && toBearing >= 45)
            text = "MR";
        else if (toBearing < 45 && toBearing >= 20)
            text = "MSR";

        return  text+"*"+storeName;
    }

    private String getDirectionTo_WSW(float toBearing, String storeName) {

        String text = "";

        if (toBearing <= 0 && toBearing >= -45)
            text = "MR";
        else if (toBearing < -45 && toBearing >= -70)
            text = "MSR";
        else if (toBearing < -70 && toBearing >= -110)
            text = "MF";
        else if (toBearing < -110 && toBearing >= -135)
            text = "MSL";
        else if (toBearing < -135 && toBearing >= -180)
            text = "ML";
        else if (toBearing <= 180 && toBearing >= 90)
            text = "ML";
        else if (toBearing < 90 && toBearing >= 45)
            text = "MB";
        else if (toBearing < 45 && toBearing >= 0)
            text = "MR";

        return  text+"*"+storeName;
    }

    private String getDirectionTo_SSW(float toBearing, String storeName){

        String text = "";

        if(toBearing <= 0 && toBearing >= -45)
            text = "MR";
        else if (toBearing < -45 && toBearing >= -90)
            text = "MR";
        else if (toBearing < -90 && toBearing >= -135)
            text = "MSR";
        else if (toBearing < -135 && toBearing >= -160)
            text = "MSR";
        else if (toBearing < -160 && toBearing >= 160)
            text = "MF";
        else if (toBearing < 160 && toBearing >= 135)
            text = "MSL";
        else if (toBearing < 135 && toBearing >= 45)
            text = "ML";
        else if (toBearing < 45 && toBearing >= 0)
            text = "MB";

        return  text+"*"+storeName;
    }

    private String getDirectionTo_SSE(float toBearing, String storeName){

        String text = "";

        if (toBearing <= 0 && toBearing >= -45)
            text = "MB";
        else if (toBearing < -45 && toBearing >= -135)
            text = "MR";
        else if (toBearing < -135 && toBearing >= -160)
            text = "MSR";
        else if (toBearing < -160 && toBearing >= 160)
            text = "MF";
        else if (toBearing < 160 && toBearing >= 135)
            text = "MSL";
        else if (toBearing < 135 && toBearing >= 0)
            text = "ML";

        return  text+"*"+storeName;
    }

    private String getDirectionTo_ESE(float toBearing, String storeName){

        String text = "";

        if(toBearing <= 110 && toBearing >= 70)
            text = "MF";
        else if (toBearing < 70 && toBearing >= 0)
            text = "ML";
        else if (toBearing <= 0 && toBearing >= -45)
            text = "ML";
        else if (toBearing < -45 && toBearing >= -90)
            text = "MB";
        else if (toBearing < -90 && toBearing >= 135)
            text = "MR";
        else if (toBearing < 135 && toBearing >= 110)
            text = "MSR";
        else if (toBearing < 70 && toBearing >= 45)
            text = "MSL";

        return  text+"*"+storeName;
    }

    private String getDirectionTo_ENE(float toBearing, String storeName){

        String text = "";

        if (toBearing <= 110 && toBearing >= 70)
            text = "MF";
        else if (toBearing < 70 && toBearing > 45)
            text = "MSL";
        else if (toBearing < 45 && toBearing >= -90)
            text = "ML";
        else if (toBearing < -90 && toBearing >= -135)
            text = "MB";
        else if (toBearing < -135 && toBearing >= 90)
            text = "MR";
        else if (toBearing < 90 && toBearing >= 70)
            text = "MSR";

        return  text+"*"+storeName;
    }

    private String getDirectionTo_NNE(float toBearing, String storeName){

        String text = "";

        if (toBearing >= -20 && toBearing <= 20)
            text = "MF";
        else if (toBearing < -20 && toBearing >= -45)
            text = "MSL";
        else if (toBearing < -45 && toBearing >= -135)
            text = "ML";
        else if (toBearing < -135 && toBearing >= -180)
            text = "MB";
        else if (toBearing <= 180 && toBearing >= 45)
            text = "MR";
        else if (toBearing < 45 && toBearing >= 20)
            text = "MSR";

        return  text+"*"+storeName;
    }

    private Float getBearing(Location srcLoc, Location destLoc){
        return srcLoc.bearingTo(destLoc);
    }

    public String getNearestStore(Location jLocation){

        String storeName = "";
        ArrayList<Double> distValues = new ArrayList<Double>();
        Hashtable<Double, String> distStoreHash = new Hashtable<Double, String>();
        Location nodeLocation = jLocation;

        for(Store store : storeList){
            if(store.getStoreFloor().equals(floorName)){
                String lat = store.getStoreLatitude();
                String lon = store.getStoreLongitude();
                if(!lat.equals("NA") && !lon.equals("NA")){

                    Location location = new Location("");
                    location.setLatitude(Double.parseDouble(lat));
                    location.setLongitude(Double.parseDouble(lon));
                    double dist = location.distanceTo(nodeLocation);
                    distValues.add(dist);
                    distStoreHash.put(dist, store.getStoreName());
                }
            }


        }

        double smallest = Double.MAX_VALUE;
        for(int i = 0; i < distValues.size(); i++) {
            if(smallest > distValues.get(i)) {
                smallest = distValues.get(i);
            }
        }

        storeName = distStoreHash.get(smallest);

        return storeName;
    }
}
