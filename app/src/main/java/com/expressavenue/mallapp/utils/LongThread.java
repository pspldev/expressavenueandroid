package com.expressavenue.mallapp.utils;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.expressavenue.mallapp.database.SimpleDatabaseHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

public class LongThread implements Runnable {

    int threadNo;
    Handler handler;
    String imageUrl;
    String storeId;
    String storeName;
    String updatedAt;
    String todo;
    Context context;
    SimpleDatabaseHelper simpleDatabaseHelper;
    public static final String TAG = "LongThread";

    public LongThread() {
    }

    public LongThread(Context context, int threadNo, String storeId, String storeName, String imageUrl,
                      String updatedAt, String todo, SimpleDatabaseHelper simpleDatabaseHelper, Handler handler) {
        this.threadNo = threadNo;
        this.storeId = storeId;
        this.storeName = storeName;
        this.updatedAt = updatedAt;
        this.todo = todo;
        this.handler = handler;
        this.imageUrl = imageUrl;
        this.context = context;
        this.simpleDatabaseHelper = simpleDatabaseHelper;
    }

    @Override
    public void run() {
        getBitmap(imageUrl, storeName);
        sendMessage(threadNo, "Thread Completed");
    }


    public void sendMessage(int what, String msg) {
        Message message = handler.obtainMessage(what, msg);
        message.sendToTarget();
    }

    private Bitmap getBitmap(String url, String name) {
        Bitmap bitmap = null;
        try {
            InputStream input = new java.net.URL(url).openStream();
            bitmap = BitmapFactory.decodeStream(input);

            ContextWrapper cw = new ContextWrapper(context);
            File directory = cw.getDir("ealogos", Context.MODE_PRIVATE);
            if (!directory.exists()) {
                directory.mkdir();
            }
            File mypath = new File(directory, name + ".png");
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(mypath);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                fos.close();
            } catch (Exception e) {
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }
}
