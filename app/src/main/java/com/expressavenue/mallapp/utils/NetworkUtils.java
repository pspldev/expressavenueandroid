package com.expressavenue.mallapp.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.expressavenue.mallapp.R;


/**
 * Created by dhanil on 13-12-2016.
 */

public class NetworkUtils {

    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;


    public static int getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;

            if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        }
        return TYPE_NOT_CONNECTED;
    }

    public static String getConnectivityStatusString(Context context) {
        int conn = NetworkUtils.getConnectivityStatus(context);
        String status = null;
        if (conn == NetworkUtils.TYPE_WIFI) {
            status = context.getResources().getString(R.string.app_name) + " "+
                    context.getResources().getString(R.string.wifi_enabled);
        } else if (conn == NetworkUtils.TYPE_MOBILE) {
            status = context.getResources().getString(R.string.app_name) + " "+
                    context.getResources().getString(R.string.mobile_data_enabled);
        } else if (conn == NetworkUtils.TYPE_NOT_CONNECTED) {
            status = context.getResources().getString(R.string.app_name) + " "+
                    context.getResources().getString(R.string.no_internet);
        }
        return status;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
