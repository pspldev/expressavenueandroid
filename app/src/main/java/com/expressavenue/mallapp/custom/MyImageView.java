package com.expressavenue.mallapp.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by anisha on 6/3/17.
 */

public class MyImageView extends ImageView {

    public MyImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

    }

    public MyImageView(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    public MyImageView(Context context) {
        super(context);

    }

    @Override
    public void requestLayout() {
        /*
         * Do nothing here
         */
    }
}
