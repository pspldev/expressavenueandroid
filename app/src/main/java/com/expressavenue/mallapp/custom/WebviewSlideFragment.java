package com.expressavenue.mallapp.custom;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.TextView;

import com.expressavenue.mallapp.R;
import com.expressavenue.mallapp.global.GlobalState;


public class WebviewSlideFragment extends Fragment {

    public static final String ARG_PAGE = "map";
    public static final String ARG_TITLE = "title";
    public static final String ARG_SOURCE = "source";
    public static final String ARG_DEST = "dest";
    public static final String ARG_POS = "pos";
    private String mUrl;
    private String mTitle;
    private String mSource;
    private String mDest;
    private int mPosition;
    private WebView myWebView;

    private TextView txtMapTitle;
    private TextView txtSourceFloor;
    private TextView txtDestFloor;

    private Typeface robotoFont;


    public static WebviewSlideFragment create(String mUrl, String mTitle, String mSource, String mDest, int position) {

        WebviewSlideFragment fragment = new WebviewSlideFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PAGE, mUrl);
        args.putString(ARG_TITLE, mTitle);
        args.putString(ARG_SOURCE, mSource);
        args.putString(ARG_DEST, mDest);
        args.putInt(ARG_POS, position);
        fragment.setArguments(args);
        return fragment;
    }

    public WebviewSlideFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUrl = getArguments().getString(ARG_PAGE);
        mTitle = getArguments().getString(ARG_TITLE);
        mSource = getArguments().getString(ARG_SOURCE);
        mDest = getArguments().getString(ARG_DEST);
        mPosition = getArguments().getInt(ARG_POS);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ViewGroup rootView = (ViewGroup) inflater
                .inflate(R.layout.webview_slide, container, false);

        robotoFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Regular.ttf");

        txtMapTitle = (TextView) rootView.findViewById(R.id.txtFloorTitle);
        txtSourceFloor = (TextView) rootView.findViewById(R.id.txtSourceFloor);
        txtDestFloor = (TextView) rootView.findViewById(R.id.txtDestFloor);

        txtMapTitle.setText(geTitle());
        txtSourceFloor.setText(getSource());
        txtDestFloor.setText(getDestination());

        txtMapTitle.setTypeface(Typeface.DEFAULT_BOLD);
        txtSourceFloor.setTypeface(robotoFont);
        txtDestFloor.setTypeface(robotoFont);

        myWebView = (WebView) rootView.findViewById(R.id.webViewslide);
        final MyJavaScriptInterface myJavaScriptInterface = new MyJavaScriptInterface(getActivity());
        myWebView.addJavascriptInterface(myJavaScriptInterface, "webAppInterfaceObject");
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.loadUrl(getUrl());

        return rootView;
    }

    public int getPosition(){
        return mPosition;
    }

    public String getUrl() {
        return mUrl;
    }

    public String geTitle(){
        return mTitle;
    }

    public String getSource(){
        return mSource;
    }

    public String getDestination(){
        return mDest;
    }

    public class MyJavaScriptInterface {
        Context mContext;

        MyJavaScriptInterface(Context c) {
            mContext = c;
        }
        @JavascriptInterface
        public String getDrawPath() {
            return GlobalState.drawPath + "#" + GlobalState.source + "#" + GlobalState.destination;

        }
        @JavascriptInterface
        public String getDrawPaths() {
            return GlobalState._drawPath + "#" + GlobalState._source + "#" + GlobalState._destination;

        }


    }
}

