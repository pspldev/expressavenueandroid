package com.expressavenue.mallapp.custom;

import com.expressavenue.mallapp.pojos.Store;

import java.util.ArrayList;

/**
 * Created by dhanil on 29-12-2015.
 */
public class ProcessQRText {

    private String qrText;

    private String qrFloorName;
    private String qrLatitude;
    private String qrLongitude;
    private String qrStoreName;
    private String qrStoreNumber;

    private String srcStoreName = "";
    private String srcStoreLatitude = "";
    private String srcStoreLongitude = "";
    private String srcStoreNumber = "";
    private String srcStoreFloor = "";

    private String sourcePoint = "";

    private ArrayList<Store> storeArrayList;
    private ArrayList<Store> sortedStoreList;

    private final String INVALID_MESSAGE = "INVALID";

    public ProcessQRText(String qrText){
        this.qrText = qrText;
    }

    public String processText(){

        String[] qrOutputArray = qrText.split("\\*");

        if(qrOutputArray.length == 6){

            qrFloorName = qrOutputArray[1];
            qrLatitude = qrOutputArray[2];
            qrLongitude = qrOutputArray[3];
            qrStoreName = qrOutputArray[4];
            qrStoreNumber = qrOutputArray[5];

            if(!qrFloorName.equals("") && !qrLatitude.equals("") && !qrLongitude.equals("") &&
                    !qrStoreName.equals("") && !qrStoreNumber.equals("")){

                if(!qrOutputArray[5].equals("0")){

                    srcStoreName = qrStoreName;
                    srcStoreLatitude = qrLatitude;
                    srcStoreLongitude = qrLongitude;
                    srcStoreNumber = qrStoreNumber;
                    srcStoreFloor = qrFloorName;

                    sourcePoint = srcStoreLatitude + "*" + srcStoreLongitude + "*" + srcStoreName + "*" + srcStoreFloor + "*" + srcStoreNumber;
                    return sourcePoint;

                }else{
                    return INVALID_MESSAGE;
                }
            }else{
                return INVALID_MESSAGE;
            }
        }else{
             return INVALID_MESSAGE;
        }
    }
}
