package com.expressavenue.mallapp.custom;

import com.expressavenue.mallapp.pojos.Store;
import java.util.ArrayList;
/**
 * Created by dhanil on 29-12-2015.
 */
public class ProcessQRTextWMI {

    private String qrText;

    private String qrFloorName;
    private String qrLatitude;
    private String qrLongitude;
    private String qrStoreName;
    private String qrStoreNumber;

    private String srcStoreName = "";
    private String srcStoreLatitude = "";
    private String srcStoreLongitude = "";
    private String srcStoreNumber = "";
    private String srcStoreFloor = "";

    private String sourcePoint = "";

    private ArrayList<Store> storeArrayList;
    private ArrayList<Store> sortedStoreList;

    private final String INVALID_MESSAGE = "INVALID";

    public ProcessQRTextWMI(String qrText){
        this.qrText = qrText;
    }

    public String processText(){

        String[] qrOutputArray = qrText.split("\\*");

        if(qrOutputArray.length == 6){

            qrFloorName = qrOutputArray[1];
            qrLatitude = qrOutputArray[2];
            qrLongitude = qrOutputArray[3];
            qrStoreName = qrOutputArray[4];
            qrStoreNumber = qrOutputArray[5];

            if(!qrFloorName.equals("") && !qrLatitude.equals("") && !qrLongitude.equals("") &&
                    !qrStoreName.equals("") && !qrStoreNumber.equals("")){

                srcStoreName = qrStoreName;
                srcStoreLatitude = qrLatitude;
                srcStoreLongitude = qrLongitude;
                srcStoreNumber = qrStoreNumber;
                srcStoreFloor = qrFloorName;

                sourcePoint = srcStoreLatitude + "*" + srcStoreLongitude + "*" + srcStoreName + "*" + srcStoreFloor + "*" + srcStoreNumber;
                return sourcePoint;

            }else{
                return INVALID_MESSAGE;
            }
        }else{
            return INVALID_MESSAGE;
        }
    }
}
