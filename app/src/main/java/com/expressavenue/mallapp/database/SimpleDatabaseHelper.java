package com.expressavenue.mallapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by anisha on 2/3/17.
 */

public class SimpleDatabaseHelper {
    private SQLiteOpenHelper _openHelper;
    private String DB_NAME = "db_eamall";

    /**
     * Construct a new database helper object
     * @param context The current context for the application or activity
     */
    public SimpleDatabaseHelper(Context context) {
        _openHelper = new SimpleSQLiteOpenHelper(context);
    }

    /**
     * This is an internal class that handles the creation of all database tables
     */
    class SimpleSQLiteOpenHelper extends SQLiteOpenHelper {
        SimpleSQLiteOpenHelper(Context context) {
            super(context, DB_NAME, null, 1);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("create table tb_store_details (store_id text, " +
                    "store_name text, " +
                    "store_logo_url text," +
            "store_updated_at text)");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }

    /**
     * Return a cursor object with all rows in the table.
     * @return A cursor suitable for use in a SimpleCursorAdapter
     */
    public Cursor getStoreDetails(String store_id) {
        SQLiteDatabase db = _openHelper.getReadableDatabase();
        if (db == null) {
            return null;
        }
        String query = "select * from tb_store_details where store_id='" + store_id + "'";
        return db.rawQuery(query, null);
    }

    public Cursor getAllStores() {
        SQLiteDatabase db = _openHelper.getReadableDatabase();
        if (db == null) {
            return null;
        }
        String query = "select * from tb_store_details";
        return db.rawQuery(query, null);
    }

    public long addAStore(String storeId, String storeName, String storeLogoUrl, String updatedAt) {
        SQLiteDatabase db = _openHelper.getWritableDatabase();
        if (db == null) {
            return 0;
        }
        ContentValues row = new ContentValues();
        row.put("store_id", storeId);
        row.put("store_name", storeName);
        row.put("store_logo_url", storeLogoUrl);
        row.put("store_updated_at", updatedAt);
        long id = db.insert("tb_store_details", null, row);
        db.close();
        return id;
    }


    public void update(String storeId, String storeName, String storeLogoUrl, String updatedAt) {
        SQLiteDatabase db = _openHelper.getWritableDatabase();
        if (db == null) {
            return;
        }
        ContentValues row = new ContentValues();
        row.put("store_id", storeId);
        row.put("store_name", storeName);
        row.put("store_logo_url", storeLogoUrl);
        row.put("store_updated_at", updatedAt);
        db.update("tb_store_details", row, "store_id = ?" , new String[] { storeId } );
        db.close();
    }
}
