package com.expressavenue.mallapp.adapters;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.expressavenue.mallapp.R;
import com.expressavenue.mallapp.activity.LocateStoreActivity;
import com.expressavenue.mallapp.database.SimpleDatabaseHelper;
import com.expressavenue.mallapp.global.GlobalState;
import com.expressavenue.mallapp.pojos.Store;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by nivedith on 1/5/2016.
 */
public class FoodCourtListAdapter extends RecyclerView.Adapter<FoodCourtListAdapter.FoodCourtViewHolder> {

    private List<Store> foodCourtList;
    private Context context;
    private SimpleDatabaseHelper simpleDatabaseHelper;
    private Cursor cursor;

    public FoodCourtListAdapter(List<Store> foodCourtList, Context context) {
        this.foodCourtList = foodCourtList;
        this.context = context;
        simpleDatabaseHelper = new SimpleDatabaseHelper(GlobalState.appContext);
        cursor = simpleDatabaseHelper.getAllStores();
    }
    @Override
    public int getItemCount() {
        return foodCourtList.size();
    }

    @Override
    public void onBindViewHolder(FoodCourtViewHolder FoodCourtViewHolder, int i) {
        final Store foodCourt = foodCourtList.get(i);
        Bitmap storeLogoBitmap = null;

        if(cursor != null){
            if (cursor.moveToFirst()) {
                do {
                    String storeId = cursor.getString(1);
                    if(storeId.equals(foodCourt.getStoreName())){
                        storeLogoBitmap = loadImageFromStorage(storeId);
                    }
                } while (cursor.moveToNext());
            }
        }

        if(storeLogoBitmap != null){
            FoodCourtViewHolder.foodCourtLogo.setImageBitmap(storeLogoBitmap);
        } else {
            Bitmap icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ea_logo_whitebg);
            FoodCourtViewHolder.foodCourtLogo.setImageBitmap(icon);
        }
        FoodCourtViewHolder.foodCourtName.setText(foodCourt.getStoreName());
        FoodCourtViewHolder.foodCourtCategory.setText(foodCourt.getStoreCategory());

        String floorName = foodCourt.getStoreFloor();
        switch (floorName){
            case "LGF" : floorName = "Lower Ground Floor";break;
            case "UGF" : floorName = "Upper Ground Floor";break;
            case "FF"  : floorName = "First Floor";break;
            case "SF"  : floorName = "Second Floor";break;
            case "TF"  : floorName = "Third Floor";break;
        }

        FoodCourtViewHolder.foodCourtFloor.setText(floorName);

        FoodCourtViewHolder.foodCourtLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                context.startActivity(new Intent(context, LocateStoreActivity.class)
                        .putExtra("storeName", foodCourt.getStoreName())
                        .putExtra("storeNumber", foodCourt.getStoreNumber())
                        .putExtra("storeCategory", foodCourt.getStoreCategory())
                        .putExtra("storeFloor", foodCourt.getStoreFloor())
                        .putExtra("storeLatitude", foodCourt.getStoreLatitude())
                        .putExtra("storeLongitude", foodCourt.getStoreLongitude())
                        .putExtra("storeDesc", foodCourt.getStoreDesc())
                        .putExtra("storeLogo", foodCourt.getStoreLogo()));

            }
        });

    }

    @Override
    public FoodCourtViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.item_card_food_courts, viewGroup, false);

        return new FoodCourtViewHolder(itemView);
    }

    public static class FoodCourtViewHolder extends RecyclerView.ViewHolder {

        protected RelativeLayout foodCourtLayout;
        protected ImageView foodCourtLogo;
        protected TextView foodCourtName;
        protected TextView foodCourtCategory;
        protected TextView foodCourtFloor;

        public FoodCourtViewHolder(View v) {
            super(v);

            foodCourtLayout = (RelativeLayout) v.findViewById(R.id.foodCourts_carditem);
            foodCourtLogo = (ImageView) v.findViewById(R.id.foodCourt_logo);
            foodCourtName =  (TextView) v.findViewById(R.id.foodCourts_name);
            foodCourtCategory = (TextView)  v.findViewById(R.id.foodCourts_category);
            foodCourtFloor = (TextView)  v.findViewById(R.id.foodCourts_floor);
        }
    }

    private Bitmap loadImageFromStorage(String storeId)
    {
        try {
            ContextWrapper cw = new ContextWrapper(context);
            File directory = cw.getDir("ealogos", Context.MODE_PRIVATE);
            File f = new File(directory.getAbsolutePath(), storeId + ".png");
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            return b;
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        return null;
    }
}