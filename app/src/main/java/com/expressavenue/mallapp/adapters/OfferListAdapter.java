package com.expressavenue.mallapp.adapters;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.expressavenue.mallapp.R;
import com.expressavenue.mallapp.activity.OfferDetailsActivity;
import com.expressavenue.mallapp.database.SimpleDatabaseHelper;
import com.expressavenue.mallapp.global.GlobalState;
import com.expressavenue.mallapp.pojos.Offer;
import com.expressavenue.mallapp.pojos.Store;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dhanil on 22-12-2015.
 */
public class OfferListAdapter extends RecyclerView.Adapter<OfferListAdapter.OffersViewHolder> {

    private List<Offer> offerList;
    private Context context;

    private String startingDate = "";
    private String endingDate = "";

    private ArrayList<Store> storeArrayList;
    private SimpleDatabaseHelper simpleDatabaseHelper;
    private Cursor cursor;

    public OfferListAdapter(List<Offer> offerList, Context context, ArrayList<Store> storeArrayList ) {
        this.offerList = offerList;
        this.context = context;
        this.storeArrayList = storeArrayList;
        simpleDatabaseHelper = new SimpleDatabaseHelper(GlobalState.appContext);
        cursor = simpleDatabaseHelper.getAllStores();
    }
    @Override
    public int getItemCount() {
        return offerList.size();
    }

    @Override
    public void onBindViewHolder(OffersViewHolder offersViewHolder, int i) {

        final Offer offer = offerList.get(i);
        offersViewHolder.offerName.setText(offer.getOfferName());
        String storeId = offer.getStoreId();
        for(Store store : storeArrayList){
            String id = store.getStoreObjectId();
            if(storeId.equals(id)){
                String logo = store.getStoreLogo();
                String name = store.getStoreName();
                offersViewHolder.storeName.setText(name);

                Bitmap storeLogoBitmap = null;

                if(cursor != null){
                    if (cursor.moveToFirst()) {
                        do {
                            String storeName = cursor.getString(1);
                            if(storeName.equals(store.getStoreName())){
                                storeLogoBitmap = loadImageFromStorage(storeName);
                            }
                        } while (cursor.moveToNext());
                    }
                }
                if(storeLogoBitmap != null){
                    offersViewHolder.offerLogo.setImageBitmap(storeLogoBitmap);
                } else {
                    Bitmap icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ea_logo_whitebg);
                    offersViewHolder.offerLogo.setImageBitmap(icon);
                }
            }
        }

        if(offer.getOfferStartDate() != null){
            String[] startDate = offer.getOfferStartDate().toString().split("\\s+");
            startingDate = "Starting on: "+startDate[0] +" "+ startDate[1] +" "+ startDate[2] +" "+ startDate[5];
            offersViewHolder.offerstartdate.setText(startingDate);
        }
        if(offer.getOfferEndDate() != null){
            String[] endDate = offer.getOfferEndDate().toString().split("\\s+");
            endingDate = "Ending on: "+endDate[0] +" "+ endDate[1] +" "+ endDate[2] +" "+ endDate[5];
            offersViewHolder.offerenddate.setText(endingDate);
        }

        offersViewHolder.singleItemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                context.startActivity(new Intent(context, OfferDetailsActivity.class)
                        .putExtra("FROM", "OFFERLIST")
                        .putExtra("offerName", offer.getOfferName())
                        .putExtra("offerStartDate", offer.getOfferStartDate().toString())
                        .putExtra("offerEndDate", offer.getOfferEndDate().toString())
                        .putExtra("offerStartTime", offer.getOfferStartTime())
                        .putExtra("offerEndTime", offer.getOfferEndTime())
                        .putExtra("offerDesc", offer.getOfferDesc())
                        .putExtra("offerStoreId", offer.getStoreId())
                        .putParcelableArrayListExtra("offerStoreArrayList", storeArrayList));

            }
        });
    }

    @Override
    public OffersViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.item_card_offers, viewGroup, false);

        return new OffersViewHolder(itemView);
    }

    public static class OffersViewHolder extends RecyclerView.ViewHolder {

        protected ImageView offerLogo;
        protected TextView offerName;
        protected TextView offerstartdate;
        protected TextView offerenddate;
        protected RelativeLayout singleItemLayout;
        protected TextView storeName;

        public OffersViewHolder(View v) {
            super(v);

            offerLogo = (ImageView) v.findViewById(R.id.offer_logo);
            offerName =  (TextView) v.findViewById(R.id.offer_name);
            offerstartdate = (TextView)  v.findViewById(R.id.offer_startdate);
            offerenddate = (TextView)  v.findViewById(R.id.offer_enddate);
            singleItemLayout = (RelativeLayout) v.findViewById(R.id.offer_carditem);
            storeName = (TextView) v.findViewById(R.id.store_name);
        }
    }

    private Bitmap loadImageFromStorage(String storeId)
    {
        try {
            ContextWrapper cw = new ContextWrapper(context);
            File directory = cw.getDir("ealogos", Context.MODE_PRIVATE);
            File f = new File(directory.getAbsolutePath(), storeId + ".png");
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            return b;
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        return null;
    }
}
