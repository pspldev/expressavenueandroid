package com.expressavenue.mallapp.adapters;

/**
 * Created by dhanil on 20-12-2015.
 */

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.expressavenue.mallapp.R;
import com.expressavenue.mallapp.activity.LocateStoreActivity;
import com.expressavenue.mallapp.database.SimpleDatabaseHelper;
import com.expressavenue.mallapp.global.GlobalState;
import com.expressavenue.mallapp.pojos.Store;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class StoreListAdapter extends RecyclerView.Adapter<StoreListAdapter.StoreViewHolder> {

    private List<Store> storeList;
    private Context context;
    private SimpleDatabaseHelper simpleDatabaseHelper;
    private Cursor cursor;

    public StoreListAdapter(List<Store> contactList, Context context) {
        this.storeList = contactList;
        this.context = context;
        simpleDatabaseHelper = new SimpleDatabaseHelper(GlobalState.appContext);
        cursor = simpleDatabaseHelper.getAllStores();
    }
    @Override
    public int getItemCount() {
        return storeList.size();
    }

    @Override
    public void onBindViewHolder(StoreViewHolder storeViewHolder, int i) {

        final Store store = storeList.get(i);
        Bitmap storeLogoBitmap = null;

        if(cursor != null){
            if (cursor.moveToFirst()) {
                do {
                    String storeId = cursor.getString(1);
                    if(storeId.equals(store.getStoreName())){
                        storeLogoBitmap = loadImageFromStorage(storeId);
                    }
                } while (cursor.moveToNext());
            }
        }

        if(storeLogoBitmap != null){
            storeViewHolder.storeLogo.setImageBitmap(storeLogoBitmap);
        } else {
            Bitmap icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ea_logo_whitebg);
            storeViewHolder.storeLogo.setImageBitmap(icon);
        }
        storeViewHolder.storeName.setText(store.getStoreName());
        storeViewHolder.storeCategory.setText(store.getStoreCategory());

        String floorName = store.getStoreFloor();
        switch (floorName){
            case "LGF" : floorName = "Lower Ground Floor";break;
            case "UGF" : floorName = "Upper Ground Floor";break;
            case "FF"  : floorName = "First Floor";break;
            case "SF"  : floorName = "Second Floor";break;
            case "TF"  : floorName = "Third Floor";break;
        }

        storeViewHolder.storeFloor.setText(floorName);

        storeViewHolder.storeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                context.startActivity(new Intent(context, LocateStoreActivity.class)
                        .putExtra("storeName", store.getStoreName())
                        .putExtra("storeNumber", store.getStoreNumber())
                        .putExtra("storeCategory", store.getStoreCategory())
                        .putExtra("storeFloor", store.getStoreFloor())
                        .putExtra("storeLatitude", store.getStoreLatitude())
                        .putExtra("storeLongitude", store.getStoreLongitude())
                        .putExtra("storeDesc", store.getStoreDesc())
                        .putExtra("storeLogo", store.getStoreLogo()));
            }
        });
    }

    @Override
    public StoreViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.item_card_stores, viewGroup, false);

        return new StoreViewHolder(itemView);
    }

    public static class StoreViewHolder extends RecyclerView.ViewHolder {

        protected RelativeLayout storeLayout;
        protected ImageView storeLogo;
        protected TextView storeName;
        protected TextView storeCategory;
        protected TextView storeFloor;

        public StoreViewHolder(View v) {
            super(v);

            storeLayout = (RelativeLayout) v.findViewById(R.id.store_carditem);
            storeLogo = (ImageView) v.findViewById(R.id.store_logo);
            storeName =  (TextView) v.findViewById(R.id.store_name);
            storeCategory = (TextView)  v.findViewById(R.id.store_category);
            storeFloor = (TextView)  v.findViewById(R.id.store_floor);
        }
    }


    private Bitmap loadImageFromStorage(String storeId)
    {
        try {
            ContextWrapper cw = new ContextWrapper(context);
            File directory = cw.getDir("ealogos", Context.MODE_PRIVATE);
            File f = new File(directory.getAbsolutePath(), storeId + ".png");
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            return b;
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        return null;
    }
}
