package com.expressavenue.mallapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.expressavenue.mallapp.R;
import com.expressavenue.mallapp.activity.OfferListActivity;
import com.expressavenue.mallapp.global.GlobalState;
import com.expressavenue.mallapp.pojos.Category;

import java.util.List;

/**
 * Created by nivedith on 12/29/2015.
 */
public class OfferCategoryListAdapter extends RecyclerView.Adapter<OfferCategoryListAdapter.OfferViewHolder> {

    private List<Category> categoriesList;
    private Context context;

    public OfferCategoryListAdapter(List<Category> contactList, Context context) {
        this.categoriesList = contactList;
        this.context = context;
    }
    @Override
    public int getItemCount() {
        return categoriesList.size();
    }

    @Override
    public OfferViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.item_card_offer_categories, viewGroup, false);

        return new OfferViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(OfferCategoryListAdapter.OfferViewHolder offerViewHolder, int position) {
        final Category category = categoriesList.get(position);
        offerViewHolder.categoryName.setText(category.getCategoryName());
        offerViewHolder.offerCount.setText(String.valueOf(category.getOfferCount()));

        offerViewHolder.singleItemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GlobalState.editor.putString("categoryGlobal", category.getCategoryName());
                GlobalState.editor.commit();
                context.startActivity(new Intent(context, OfferListActivity.class)
                        .putExtra("category", "category"));
            }
        });
    }

    public static class OfferViewHolder extends RecyclerView.ViewHolder {

        protected TextView categoryName;
        protected TextView offerCount;
        private RelativeLayout singleItemLayout;

        public OfferViewHolder(View v) {
            super(v);

            singleItemLayout = (RelativeLayout) v.findViewById(R.id.categories_carditem);
            categoryName =  (TextView) v.findViewById(R.id.category_name);
            offerCount = (TextView) v.findViewById(R.id.offer_count);
        }
    }


}

