package com.expressavenue.mallapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.expressavenue.mallapp.R;

import java.util.ArrayList;

/**
 * Created by nivedith on 1/11/2016.
 */
public class TextNavAdapter extends BaseAdapter
{
    private ArrayList<String> textNavArray;
    private Context context;

    String srcName;
    String destName;


    public TextNavAdapter(ArrayList<String> textNavArray, Context context, String srcName, String destName){
        this.textNavArray = textNavArray;
        this.context = context;
        this.srcName = srcName;
        this.destName = destName;
    }

    @Override
    public int getCount()
    {
        return textNavArray.size();
    }

    @Override
    public String getItem(int arg0)
    {
        return textNavArray.get(arg0);
    }

    @Override
    public long getItemId(int arg0)
    {
        return arg0;
    }

    @Override
    public View getView(int arg0, View v, ViewGroup arg2)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (v == null)
            v = inflater.inflate(R.layout.textnavitem, null);

        String item = getItem(arg0);

        String navItems[] = item.split("\\*");
        String direction = "";
        String place = "";
        if(navItems.length == 2){
            direction = navItems[0];
            place = navItems[1];
        }

        String navText = "";
        int imageid = R.drawable.uparrow;

        switch (direction.trim()){
            case "MR":
                navText = "Move to the right to reach the point near ";
                imageid = R.drawable.rightarrow;
                break;
            case "ML":
                navText = "Move to the left to reach the point near ";
                imageid = R.drawable.leftarrow;
                break;
            case "MSR":
                navText = "Move slight right to reach the point near ";
                imageid = R.drawable.rightarrow;
                break;
            case "MSL":
                navText = "Move slight left to reach the point near ";
                imageid = R.drawable.leftarrow;
                break;
            case "MF":
                navText = "Move forward to reach the point near ";
                imageid = R.drawable.uparrow;
                break;
            case "MB":
                navText = "Move backward to reach the point near ";
                break;
        }

        TextView directionText = (TextView) v.findViewById(R.id.directiontext);
        ImageView img = (ImageView) v.findViewById(R.id.directionimage);

        String finalText = "";
        if(direction.trim().equals("S")){
            finalText = srcName;
            directionText.setText(finalText);
            img.setImageResource(R.drawable.greenmarker);
        }
        else if(direction.trim().equals("D")){
            finalText = destName;
            directionText.setText(finalText);
            img.setImageResource(R.drawable.redmarker);
        }
        else{
            finalText = navText + place;
            if(finalText.length()>0){
                directionText.setText(finalText);
                img.setImageResource(imageid);
            }
        }

        return v;
    }

}
