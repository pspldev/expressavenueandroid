package com.expressavenue.mallapp.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.expressavenue.mallapp.custom.WebviewSlideFragment;
import com.expressavenue.mallapp.global.GlobalState;

/**
 * Created by nivedith on 12/30/2015.
 */
public class WebViewSlidePagerAdapter extends FragmentStatePagerAdapter {
    public WebViewSlidePagerAdapter(FragmentManager fm) {
        super(fm);
    }
    @Override
    public Fragment getItem(int position) {
        String url = "";
        String floor = "";
        String source = "";
        String dest = "";
        if(position == 0){

            url = GlobalState.liftSource;
            floor = GlobalState.mapTitle;
            source = GlobalState.sourceStore;
            dest = GlobalState.destStore;

        } else if(position == 1){

            url = GlobalState.liftDest;
            floor = GlobalState._mapTitle;
            source = GlobalState._sourceStore;
            dest = GlobalState._destStore;
        }
        return WebviewSlideFragment.create(url, floor, source, dest, position);
    }
    @Override
    public int getCount() {
        return 2;
    }
}