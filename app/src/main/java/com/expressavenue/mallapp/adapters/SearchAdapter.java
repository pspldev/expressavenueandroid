package com.expressavenue.mallapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.expressavenue.mallapp.R;

import java.util.ArrayList;

public class SearchAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<String> mStoreArray;
    private LayoutInflater mLayoutInflater;
    private boolean mIsFilterList;

    private String storeName = "";

    public SearchAdapter(Context context, ArrayList<String> storeWithoutFloor, boolean isFilterList) {
        this.mContext = context;
        this.mIsFilterList = isFilterList;
        this.mStoreArray = storeWithoutFloor;
    }

    public void updateList(ArrayList<String> filterList, boolean isFilterList) {
        this.mStoreArray = filterList;
        this.mIsFilterList = isFilterList;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mStoreArray.size();
    }

    @Override
    public String getItem(int position) {
        return mStoreArray.get(position);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder = null;

        if(v == null){
            holder = new ViewHolder();
            mLayoutInflater = (LayoutInflater)mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
            v = mLayoutInflater.inflate(R.layout.list_item_search, parent, false);
            holder.txtStore = (TextView)v.findViewById(R.id.txt_store);
            holder.txtFloor = (TextView)v.findViewById(R.id.txt_floor);
            v.setTag(holder);
        }else{
            holder = (ViewHolder) v.getTag();
        }
        String item = mStoreArray.get(position);
        String[] itemArray = item.split("\\*");
        holder.txtStore.setText(itemArray[0]);

        switch (itemArray[1]){
            case "LGF" : itemArray[1] = "Lower Ground Floor";break;
            case "UGF" : itemArray[1] = "Upper Ground Floor";break;
            case "FF"  : itemArray[1] = "First Floor";break;
            case "SF"  : itemArray[1] = "Second Floor";break;
            case "TF"  : itemArray[1] = "Third Floor";break;
        }
        holder.txtFloor.setText(itemArray[1]);
        holder.txtStore.setTag(itemArray[2]);
        return v;
    }

    class ViewHolder{
        protected TextView txtStore;
        protected TextView txtFloor;
    }
}






