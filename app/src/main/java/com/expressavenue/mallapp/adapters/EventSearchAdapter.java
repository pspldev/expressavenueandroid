package com.expressavenue.mallapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.expressavenue.mallapp.R;

import java.util.ArrayList;

/**
 * Created by nivedith on 1/15/2016.
 */

public class EventSearchAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<String> mEventsArray;
    private LayoutInflater mLayoutInflater;
    private boolean mIsFilterList;


    public EventSearchAdapter(Context context, ArrayList<String> mEventsArray, boolean isFilterList) {
        this.mContext = context;
        this.mIsFilterList = isFilterList;
        this.mEventsArray = mEventsArray;
    }

    public void updateList(ArrayList<String> filterList, boolean isFilterList) {
        this.mEventsArray = filterList;
        this.mIsFilterList = isFilterList;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mEventsArray.size();
    }

    @Override
    public String getItem(int position) {
        return mEventsArray.get(position);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder = null;

        if(v == null){
            holder = new ViewHolder();
            mLayoutInflater = (LayoutInflater)mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
            v = mLayoutInflater.inflate(R.layout.list_item_search_event, parent, false);
            holder.txtCategory = (TextView)v.findViewById(R.id.txt_event_name);
            v.setTag(holder);
        }else{
            holder = (ViewHolder) v.getTag();
        }
        String item = mEventsArray.get(position);
        String[] itemArray = item.split("\\*");
        holder.txtCategory.setText(itemArray[0]);
        holder.txtCategory.setTag(itemArray[1]);

        return v;
    }

    class ViewHolder{
        protected TextView txtCategory;
    }
}