package com.expressavenue.mallapp.adapters;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.expressavenue.mallapp.R;
import com.expressavenue.mallapp.activity.LocateStoreActivity;
import com.expressavenue.mallapp.pojos.Store;

import java.util.List;

public class RestroomListAdapter extends RecyclerView.Adapter<RestroomListAdapter.RestroomViewHolder> {

    private List<Store> restroomList;
    private Context context;

    public RestroomListAdapter(List<Store> restroomList, Context context) {
        this.restroomList = restroomList;
        this.context = context;
    }
    @Override
    public int getItemCount() {
        return restroomList.size();
    }

    @Override
    public void onBindViewHolder(RestroomViewHolder RestroomViewHolder, int i) {
        final Store restroom = restroomList.get(i);
        RestroomViewHolder.restroomName.setText(restroom.getStoreName());
        RestroomViewHolder.restroomCategory.setText(restroom.getStoreCategory());

        String floorName = restroom.getStoreFloor();
        switch (floorName){
            case "LGF" : floorName = "Lower Ground Floor";break;
            case "UGF" : floorName = "Upper Ground Floor";break;
            case "FF"  : floorName = "First Floor";break;
            case "SF"  : floorName = "Second Floor";break;
            case "TF"  : floorName = "Third Floor";break;
        }

        RestroomViewHolder.restroomFloor.setText(floorName);

        RestroomViewHolder.restroomLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                context.startActivity(new Intent(context, LocateStoreActivity.class)
                        .putExtra("storeName", restroom.getStoreName())
                        .putExtra("storeNumber", restroom.getStoreNumber())
                        .putExtra("storeCategory", restroom.getStoreCategory())
                        .putExtra("storeFloor", restroom.getStoreFloor())
                        .putExtra("storeLatitude", restroom.getStoreLatitude())
                        .putExtra("storeLongitude", restroom.getStoreLongitude())
                        .putExtra("storeDesc", restroom.getStoreDesc())
                        .putExtra("storeLogo", restroom.getStoreLogo()));

            }
        });

    }

    @Override
    public RestroomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.item_card_restrooms, viewGroup, false);

        return new RestroomViewHolder(itemView);
    }

    public static class RestroomViewHolder extends RecyclerView.ViewHolder {

        protected RelativeLayout restroomLayout;
        protected TextView restroomName;
        protected TextView restroomCategory;
        protected TextView restroomFloor;

        public RestroomViewHolder(View v) {
            super(v);

            restroomLayout = (RelativeLayout) v.findViewById(R.id.restroom_carditem);
            restroomName =  (TextView) v.findViewById(R.id.restroom_name);
            restroomCategory = (TextView)  v.findViewById(R.id.restroom_category);
            restroomFloor = (TextView)  v.findViewById(R.id.restroom_floor);
        }
    }
}
