package com.expressavenue.mallapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.expressavenue.mallapp.R;
import com.expressavenue.mallapp.activity.EventDetailsActivity;
import com.expressavenue.mallapp.pojos.Offer;

import java.util.List;

/**
 * Created by nivedith on 1/15/2016.
 */
    public class EventListAdapter extends RecyclerView.Adapter<EventListAdapter.EventsViewHolder> {

        private List<Offer> eventList;
        private Context context;

        private String startingDate = "";
        private String endingDate = "";

        public EventListAdapter(List<Offer> eventList, Context context) {
            this.eventList = eventList;
            this.context = context;
        }
        @Override
        public int getItemCount() {
            return eventList.size();
        }

        @Override
        public void onBindViewHolder(EventsViewHolder eventsViewHolder, int i) {

            final Offer event = eventList.get(i);

            eventsViewHolder.eventName.setText(event.getOfferName());

            if(event.getOfferStartDate() != null){
                String[] startDate = event.getOfferStartDate().toString().split("\\s+");
                startingDate = "Starting on: "+startDate[0] +" "+ startDate[1] +" "+ startDate[2] +" "+ startDate[5];
                eventsViewHolder.eventstartdate.setText(startingDate);
            }
            if(event.getOfferEndDate() != null){
                String[] endDate = event.getOfferEndDate().toString().split("\\s+");
                endingDate = "Ending on: "+endDate[0] +" "+ endDate[1] +" "+ endDate[2] +" "+ endDate[5];
                eventsViewHolder.eventenddate.setText(endingDate);
            }

            eventsViewHolder.singleItemLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    context.startActivity(new Intent(context, EventDetailsActivity.class)
                            .putExtra("FROM", "EVENTLIST")
                            .putExtra("offerName", event.getOfferName())
                            .putExtra("offerStartDate", event.getOfferStartDate().toString())
                            .putExtra("offerEndDate", event.getOfferEndDate().toString())
                            .putExtra("offerStartTime", event.getOfferStartTime())
                            .putExtra("offerEndTime", event.getOfferEndTime())
                            .putExtra("offerDesc", event.getOfferDesc())
                            .putExtra("offerStoreId", event.getStoreId()));

                }
            });
        }

        @Override
        public EventsViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View itemView = LayoutInflater.
                    from(viewGroup.getContext()).
                    inflate(R.layout.item_card_events, viewGroup, false);

            return new EventsViewHolder(itemView);
        }

        public static class EventsViewHolder extends RecyclerView.ViewHolder {

            protected ImageView eventLogo;
            protected TextView eventName;
            protected TextView eventstartdate;
            protected TextView eventenddate;
            protected RelativeLayout singleItemLayout;

            public EventsViewHolder(View v) {
                super(v);

                eventLogo = (ImageView) v.findViewById(R.id.event_logo);
                eventName =  (TextView) v.findViewById(R.id.event_name);
                eventstartdate = (TextView)  v.findViewById(R.id.event_startdate);
                eventenddate = (TextView)  v.findViewById(R.id.event_enddate);
                singleItemLayout = (RelativeLayout) v.findViewById(R.id.event_carditem);
            }
        }
    }
