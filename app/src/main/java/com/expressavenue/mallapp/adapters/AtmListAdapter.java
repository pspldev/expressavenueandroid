package com.expressavenue.mallapp.adapters;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.expressavenue.mallapp.R;
import com.expressavenue.mallapp.activity.LocateStoreActivity;
import com.expressavenue.mallapp.database.SimpleDatabaseHelper;
import com.expressavenue.mallapp.global.GlobalState;
import com.expressavenue.mallapp.pojos.Store;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class AtmListAdapter extends RecyclerView.Adapter<AtmListAdapter.AtmViewHolder> {

    private List<Store> atmList;
    private Context context;
    private SimpleDatabaseHelper simpleDatabaseHelper;
    private Cursor cursor;

    public AtmListAdapter(List<Store> atmList, Context context) {
        this.atmList = atmList;
        this.context = context;
        simpleDatabaseHelper = new SimpleDatabaseHelper(GlobalState.appContext);
        cursor = simpleDatabaseHelper.getAllStores();
    }
    @Override
    public int getItemCount() {
        return atmList.size();
    }

    @Override
    public void onBindViewHolder(AtmViewHolder AtmViewHolder, int i) {
        final Store atm = atmList.get(i);
        Bitmap storeLogoBitmap = null;
        if(cursor != null){
            if (cursor.moveToFirst()) {
                do {
                    String storeId = cursor.getString(1);
                    if(storeId.equals(atm.getStoreName())){
                        storeLogoBitmap = loadImageFromStorage(storeId);
                    }
                } while (cursor.moveToNext());
            }
        }

        if(storeLogoBitmap != null){
            AtmViewHolder.atmLogo.setImageBitmap(storeLogoBitmap);
        } else {
            Bitmap icon = BitmapFactory.decodeResource(context.getResources(), R.mipmap.atm);
            AtmViewHolder.atmLogo.setImageBitmap(icon);
        }
        AtmViewHolder.atmName.setText(atm.getStoreName());
        AtmViewHolder.atmCategory.setText(atm.getStoreCategory());

        String floorName = atm.getStoreFloor();
        switch (floorName){
            case "LGF" : floorName = "Lower Ground Floor";break;
            case "UGF" : floorName = "Upper Ground Floor";break;
            case "FF"  : floorName = "First Floor";break;
            case "SF"  : floorName = "Second Floor";break;
            case "TF"  : floorName = "Third Floor";break;
        }

        AtmViewHolder.atmFloor.setText(floorName);

        AtmViewHolder.atmLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                context.startActivity(new Intent(context, LocateStoreActivity.class)
                        .putExtra("storeName", atm.getStoreName())
                        .putExtra("storeNumber", atm.getStoreNumber())
                        .putExtra("storeCategory", atm.getStoreCategory())
                        .putExtra("storeFloor", atm.getStoreFloor())
                        .putExtra("storeLatitude", atm.getStoreLatitude())
                        .putExtra("storeLongitude", atm.getStoreLongitude())
                        .putExtra("storeDesc", atm.getStoreDesc())
                        .putExtra("storeLogo", atm.getStoreLogo()));

            }
        });

    }

    @Override
    public AtmViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.item_card_atms, viewGroup, false);

        return new AtmViewHolder(itemView);
    }

    public static class AtmViewHolder extends RecyclerView.ViewHolder {

        protected RelativeLayout atmLayout;
        protected TextView atmName;
        protected TextView atmCategory;
        protected TextView atmFloor;
        protected ImageView atmLogo;

        public AtmViewHolder(View v) {
            super(v);

            atmLayout = (RelativeLayout) v.findViewById(R.id.atm_carditem);
            atmName =  (TextView) v.findViewById(R.id.atm_name);
            atmCategory = (TextView)  v.findViewById(R.id.atm_category);
            atmFloor = (TextView)  v.findViewById(R.id.atm_floor);
            atmLogo = (ImageView) v.findViewById(R.id.img_atm_logo);
        }
    }

    private Bitmap loadImageFromStorage(String storeId)
    {
        try {
            ContextWrapper cw = new ContextWrapper(context);
            File directory = cw.getDir("ealogos", Context.MODE_PRIVATE);
            File f = new File(directory.getAbsolutePath(), storeId + ".png");
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            return b;
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        return null;
    }
}
