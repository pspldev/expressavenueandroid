package com.expressavenue.mallapp.adapters;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.expressavenue.mallapp.R;
import com.expressavenue.mallapp.activity.LocateStoreActivity;
import com.expressavenue.mallapp.database.SimpleDatabaseHelper;
import com.expressavenue.mallapp.pojos.Store;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by nivedith on 1/14/2016.
 */

public class RestaurantListAdapter extends RecyclerView.Adapter<RestaurantListAdapter.RestaurantViewHolder> {

    private List<Store> restaurantList;
    private Context context;
    private SimpleDatabaseHelper simpleDatabaseHelper;
    private Cursor cursor;

    public RestaurantListAdapter(List<Store> restaurantList, Context context) {
        this.restaurantList = restaurantList;
        this.context = context;
        simpleDatabaseHelper = new SimpleDatabaseHelper(context);
        cursor = simpleDatabaseHelper.getAllStores();
    }
    @Override
    public int getItemCount() {
        return restaurantList.size();
    }

    @Override
    public void onBindViewHolder(RestaurantViewHolder RestaurantViewHolder, int i) {
        final Store restaurant = restaurantList.get(i);
        Bitmap storeLogoBitmap = null;

        if(cursor != null){
            if (cursor.moveToFirst()) {
                do {
                    String restaurantId = cursor.getString(1);
                    if(restaurantId.equals(restaurant.getStoreName())){
                        storeLogoBitmap = loadImageFromStorage(restaurantId);
                    }
                } while (cursor.moveToNext());
            }
        }

        if(storeLogoBitmap != null){
            RestaurantViewHolder.restaurantLogo.setImageBitmap(storeLogoBitmap);
        } else {
            Bitmap icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ea_logo_whitebg);
            RestaurantViewHolder.restaurantLogo.setImageBitmap(icon);
        }
        RestaurantViewHolder.restaurantName.setText(restaurant.getStoreName());
        RestaurantViewHolder.restaurantCategory.setText(restaurant.getStoreCategory());

        String floorName = restaurant.getStoreFloor();
        switch (floorName){
            case "LGF" : floorName = "Lower Ground Floor";break;
            case "UGF" : floorName = "Upper Ground Floor";break;
            case "FF"  : floorName = "First Floor";break;
            case "SF"  : floorName = "Second Floor";break;
            case "TF"  : floorName = "Third Floor";break;
        }

        RestaurantViewHolder.restaurantFloor.setText(floorName);

        RestaurantViewHolder.restaurantLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                context.startActivity(new Intent(context, LocateStoreActivity.class)
                        .putExtra("storeName", restaurant.getStoreName())
                        .putExtra("storeNumber", restaurant.getStoreNumber())
                        .putExtra("storeCategory", restaurant.getStoreCategory())
                        .putExtra("storeFloor", restaurant.getStoreFloor())
                        .putExtra("storeLatitude", restaurant.getStoreLatitude())
                        .putExtra("storeLongitude", restaurant.getStoreLongitude())
                        .putExtra("storeDesc", restaurant.getStoreDesc())
                        .putExtra("storeLogo", restaurant.getStoreLogo()));

            }
        });

    }

    @Override
    public RestaurantViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.item_card_restaurants, viewGroup, false);

        return new RestaurantViewHolder(itemView);
    }

    public static class RestaurantViewHolder extends RecyclerView.ViewHolder {

        protected RelativeLayout restaurantLayout;
        protected TextView restaurantName;
        protected TextView restaurantCategory;
        protected TextView restaurantFloor;
        protected ImageView restaurantLogo;

        public RestaurantViewHolder(View v) {
            super(v);

            restaurantLayout = (RelativeLayout) v.findViewById(R.id.restaurants_carditem);
            restaurantLogo = (ImageView) v.findViewById(R.id.restaurants_logo);
            restaurantName =  (TextView) v.findViewById(R.id.restaurants_name);
            restaurantCategory = (TextView)  v.findViewById(R.id.restaurants_category);
            restaurantFloor = (TextView)  v.findViewById(R.id.restaurants_floor);
        }
    }

    private Bitmap loadImageFromStorage(String storeId)
    {
        try {
            ContextWrapper cw = new ContextWrapper(context);
            File directory = cw.getDir("ealogos", Context.MODE_PRIVATE);
            File f = new File(directory.getAbsolutePath(), storeId + ".png");
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            return b;
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        return null;
    }
}
