package com.expressavenue.mallapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.expressavenue.mallapp.R;
import com.expressavenue.mallapp.activity.CategoryStoresListActivity;
import com.expressavenue.mallapp.global.GlobalState;
import com.expressavenue.mallapp.pojos.Category;

import java.util.List;

/**
 * Created by dhanil on 20-12-2015.
 */
public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.CategoryViewHolder> {

    private List<Category> categoryList;
    private Context context;

    public CategoryListAdapter(List<Category> categoryList, Context context) {
        this.categoryList = categoryList;
        this.context = context;
    }
    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    @Override
    public void onBindViewHolder(CategoryViewHolder categoryViewHolder, int i) {
        final Category category = categoryList.get(i);

        categoryViewHolder.categoryName.setText(category.getCategoryName());

        categoryViewHolder.categorylayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                GlobalState.editor.putString("categoryGlobal", category.getCategoryName());
                GlobalState.editor.commit();
                context.startActivity(new Intent(context, CategoryStoresListActivity.class)
                        .putExtra("category", "category"));
            }
        });
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.item_card_categories, viewGroup, false);

        return new CategoryViewHolder(itemView);
    }

    public static class CategoryViewHolder extends RecyclerView.ViewHolder {

        protected TextView categoryName;
        protected RelativeLayout categorylayout;

        public CategoryViewHolder(View v) {
            super(v);

            categoryName =  (TextView) v.findViewById(R.id.category_name);
            categorylayout = (RelativeLayout) v.findViewById(R.id.categories_carditem);
        }
    }
}
