package com.expressavenue.mallapp.interfaces;

/**
 * Created by dhanil on 20-12-2016.
 */

public interface RestCallBack<T> {

    void onSuccess(T object);

    void onFailure(Throwable t);
}