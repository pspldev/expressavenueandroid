package com.expressavenue.mallapp.services;

import android.text.TextUtils;
import android.util.Log;

import com.expressavenue.mallapp.rest.RestServiceFactory;
import com.expressavenue.mallapp.rest.request.InstallationRequestDTO;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by dhanil on 16-03-2017.
 */

public class MyService extends FirebaseInstanceIdService {

    private static final String TAG = "MyService";

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String refreshedToken){
        if(refreshedToken != null && !TextUtils.isEmpty(refreshedToken)){
            InstallationRequestDTO installationRequestDTO = new InstallationRequestDTO("android", refreshedToken);
            doInstallation(installationRequestDTO);
        }
    }

    private void doInstallation(InstallationRequestDTO installationRequestDTO){
        RestServiceFactory.getInstance().doInstallation(getApplicationContext(), installationRequestDTO);
    }
}
