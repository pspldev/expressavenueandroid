package com.expressavenue.mallapp.services;

import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;

import com.expressavenue.mallapp.R;
import com.expressavenue.mallapp.activity.MainActivity;
import com.expressavenue.mallapp.database.SimpleDatabaseHelper;
import com.expressavenue.mallapp.fragments.SettingsFragment;
import com.expressavenue.mallapp.global.GlobalState;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Created by anisha on 2/3/17.
 */

public class UpdateStoresService extends Service {

    private SimpleDatabaseHelper simpleDatabaseHelper;
    private HashMap<String, String> hashIdUpdated;
    private int totalStores = 0;
    private int countedStores = 0;
    private int storeCount = 0;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        hashIdUpdated = new HashMap<>();
        simpleDatabaseHelper = new SimpleDatabaseHelper(GlobalState.appContext);

        Cursor cursor = simpleDatabaseHelper.getAllStores();
        if(cursor != null){
            if (cursor.moveToFirst()) {
                do {
                    hashIdUpdated.put(cursor.getString(0), cursor.getString(3));
                } while (cursor.moveToNext());
            }
        }

        getStoresFromParse();
        return Service.START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        //TODO for communication return IBinder implementation
        return null;
    }


    private void getStoresFromParse(){
        new CHECKINTERNETCONNECTION().execute();
    }

    private void storeToLocalDb(String storeId, String storeName, String storeLogoUrl, String updatedAt){
        countedStores = countedStores + 1;
        boolean isExists = false;
        boolean isUpdated = false;

        Set<String> idSet = hashIdUpdated.keySet();
        for(int i = 0; i < idSet.size(); i++){
            if(idSet.contains(storeId)){
                isExists = true;
                String _updatedAt = hashIdUpdated.get(storeId);
                if(!TextUtils.isEmpty(_updatedAt) && _updatedAt != null &&
                        !TextUtils.isEmpty(updatedAt) && updatedAt != null){
                    if(!_updatedAt.equals(updatedAt)){
                        isUpdated = true;
                        break;
                    }
                }
            }
        }
        if(!isExists){
            if(!TextUtils.isEmpty(storeLogoUrl) && storeLogoUrl != null)
                new DownloadImage().execute(storeId, storeName, storeLogoUrl, updatedAt, "add");
            else{
                Bitmap icon = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.ea_logo_whitebg);
                byte[] dummyImage = getBitmapAsByteArray(icon);
                simpleDatabaseHelper.addAStore(storeId, storeName, "", updatedAt);
            }
        } else if(isExists && isUpdated){
            if(!TextUtils.isEmpty(storeLogoUrl) && storeLogoUrl != null)
                new DownloadImage().execute(storeId, storeName, storeLogoUrl, updatedAt, "update");
            else{
                Bitmap icon = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.ea_logo_whitebg);
                byte[] dummyImage = getBitmapAsByteArray(icon);
                simpleDatabaseHelper.update(storeId, storeName, "", updatedAt);
            }
        }
    }

    // DownloadImage AsyncTask
    private class DownloadImage extends AsyncTask<String, Void, Bitmap> {

        String id = "";
        String name = "";
        String url = "";
        String updatedAt = "";
        String todo = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected Bitmap doInBackground(String... URL) {

            id = URL[0];
            name = URL[1];
            url = URL[2];
            updatedAt = URL[3];
            todo = URL[4];

            Bitmap bitmap = null;
            try {
                InputStream input = new java.net.URL(url).openStream();
                bitmap = BitmapFactory.decodeStream(input);

                ContextWrapper cw = new ContextWrapper(getApplicationContext());
                File directory = cw.getDir("ealogos", Context.MODE_PRIVATE);
                if (!directory.exists()) {
                    directory.mkdir();
                }
                File mypath = new File(directory, name + ".png");

                FileOutputStream fos = null;
                try {
                    fos = new FileOutputStream(mypath);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                    fos.close();
                } catch (Exception e) {
                    Log.e("SAVE_IMAGE", e.getMessage(), e);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap result) {

            byte[] imagedata = null;

            if(result == null){
                Bitmap icon = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.ea_logo_whitebg);
                imagedata = getBitmapAsByteArray(icon);
            } else {
                imagedata = getBitmapAsByteArray(result);
            }

            if(todo.equals("add"))
                simpleDatabaseHelper.addAStore(id, name, url, updatedAt);
            else if (todo.equals("update"))
                simpleDatabaseHelper.update(id, name, url, updatedAt);

            //manageAlerts();
        }
    }


    public static byte[] getBitmapAsByteArray(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, outputStream);
        return outputStream.toByteArray();
    }

    private class CHECKINTERNETCONNECTION extends AsyncTask<String, Integer, String> {

        String status = "null";
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected String doInBackground(String... f_url) {
            try {
                if (isNetworkAvailable()) {
                    try {
                        HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
                        urlc.setRequestProperty("User-Agent", "Test");
                        urlc.setRequestProperty("Connection", "close");
                        urlc.setConnectTimeout(1500);
                        urlc.connect();
                        if((urlc.getResponseCode() == 200)){
                            status = "connected";
                        }else{
                            status = "notConnected";
                        }
                    } catch (IOException e) {
                    }
                } else {
                }

            } catch (Exception e) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(String url) {
            if(status.equals("connected")){

                ParseQuery<ParseObject> storeQuery = ParseQuery.getQuery(getResources().getString(R.string.store_class_name));
                storeQuery.setLimit(1000);
                storeQuery.findInBackground(new FindCallback<ParseObject>() {
                    public void done(List<ParseObject> storeList, ParseException e) {
                        if (e == null) {
                            ParseObject.pinAllInBackground(storeList);
                            storeCount = storeList.size();
                            totalStores = storeList.size();
                            for (ParseObject store : storeList) {
                                String storeName = "";
                                String floorName = "";
                                if (store.has("sName")) {
                                    storeName = store.getString("sName");
                                    floorName = store.getString("sFloor");
                                    String storeId = "";
                                    String storeLogoUrl = "";
                                    String updatedAt = "";

                                    storeId = store.getString("sId");
                                    storeLogoUrl = store.getString("sLogo");
                                    updatedAt = store.getString("sUpdated_at");

                                    storeToLocalDb(storeId, storeName, storeLogoUrl, updatedAt);
                                }
                            }
                        }
                    }
                });


                ParseQuery<ParseObject> offerQuery = ParseQuery.getQuery("Offer");
                offerQuery.setLimit(1000);
                offerQuery.findInBackground(new FindCallback<ParseObject>() {
                    public void done(List<ParseObject> offerList, ParseException e) {
                        if (e == null) {
                            ParseObject.pinAllInBackground(offerList);
                        }
                    }
                });

                ParseQuery<ParseObject> categoryQuery = ParseQuery.getQuery("Categories");
                categoryQuery.setLimit(500);
                categoryQuery.findInBackground(new FindCallback<ParseObject>() {
                    public void done(List<ParseObject> categoryList, ParseException e) {
                        if (e == null) {

                            ParseObject.pinAllInBackground(categoryList);

                            long date = System.currentTimeMillis();
                            SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy h:mm a");
                            String dateString = sdf.format(date);

                            GlobalState.editor.putString("lastUpdatedOn", dateString);
                            GlobalState.editor.commit();

                            GlobalState.editor.putString("storeDownload", "exists");
                            GlobalState.editor.commit();
                        }
                    }
                });

            }
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }
}
