package com.expressavenue.mallapp.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.expressavenue.mallapp.R;
import com.expressavenue.mallapp.activity.EventDetailsActivity;
import com.expressavenue.mallapp.activity.MainActivity;
import com.expressavenue.mallapp.activity.OfferByCategoryActivity;
import com.expressavenue.mallapp.activity.OfferDetailsActivity;
import com.expressavenue.mallapp.global.GlobalState;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by dhanil on 16-03-2017.
 */

public class MyMessageService extends FirebaseMessagingService {

    private static final String TAG = "MyMessageService";
    private JSONObject notificationJsonObject = new JSONObject();
    private String[] notificationContentArray ;
    private String notificationTitle = "";
    private String notificationBody = "";
    private String alert = "";
    private String storeName = "";
    private String storeCategory = "";
    private String storeFloor = "";
    private String storeLatitude = "";
    private String storeLongitude = "";
    private String storeLogo = "";
    private String storeDesc = "";
    private String storeNumber = "";

    private String offerType = "";
    private String offerName = "";
    private String storeId = "";
    private String offerDesc = "";
    private String offerStartDate = "";
    private String offerEndDate = "";
    private String offerStartTime = "";
    private String offerEndTime = "";

    private boolean storeStatus = false;
    private boolean isStoreDeleted = true;

    final private int NOTIFICATION_ID = 1;
    private int pendingNotificationsCount = 0;

    private SharedPreferences eapref = GlobalState.eaPref;;
    private SharedPreferences.Editor editor = GlobalState.editor;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        if (remoteMessage.getData().size() > 0) {
            pendingNotificationsCount = eapref.getInt("NOTIFICATION_COUNT", 0) + 1;
            editor.putInt("NOTIFICATION_COUNT", pendingNotificationsCount);
            editor.commit();
            try {
                notificationJsonObject = new JSONObject(remoteMessage.getData().get("data"));
                if(notificationJsonObject != null)
                    sendNotification(notificationJsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void sendNotification(JSONObject notificationJsonObject) {
        try {
            try {
                if(notificationJsonObject.has("alert"))
                    alert = notificationJsonObject.getString("alert");
                if(notificationJsonObject.has("offer_type"))
                    offerType = notificationJsonObject.getString("offer_type");

            } catch (Throwable t) {
                Log.e("Exception", "Could not parse malformed JSON:");
            }

        } catch (Exception e) {
            Log.i("Exception: " , e.getMessage());
        }

        if(offerType.equals("Offer")){

            try {
                if(notificationJsonObject.has("offer_name"))
                    offerName = notificationJsonObject.getString("offer_name");
                if(notificationJsonObject.has("storeId"))
                    storeId = notificationJsonObject.getString("storeId");
                if(notificationJsonObject.has("offerDesc"))
                    offerDesc = notificationJsonObject.getString("offerDesc");
                if(notificationJsonObject.has("start_date_string"))
                    offerStartDate = notificationJsonObject.getString("start_date_string");
                if(notificationJsonObject.has("start_time"))
                    offerStartTime = notificationJsonObject.getString("start_time");
                if(notificationJsonObject.has("end_date_string"))
                    offerEndDate = notificationJsonObject.getString("end_date_string");
                if(notificationJsonObject.has("end_time"))
                    offerEndTime = notificationJsonObject.getString("end_time");

                if(notificationJsonObject.has("sName"))
                    storeName = notificationJsonObject.getString("sName");
                if(notificationJsonObject.has("sCategory"))
                    storeCategory = notificationJsonObject.getString("sCategory");
                if(notificationJsonObject.has("sFloor"))
                    storeFloor = notificationJsonObject.getString("sFloor");
                if(notificationJsonObject.has("sLatitude"))
                    storeLatitude = notificationJsonObject.getString("sLatitude");
                if(notificationJsonObject.has("sLongitude"))
                    storeLongitude = notificationJsonObject.getString("sLongitude");
                storeDesc = "NA";
                if(notificationJsonObject.has("sNumber"))
                    storeNumber = notificationJsonObject.getString("sNumber");
                if(notificationJsonObject.has("deleted"))
                    isStoreDeleted = notificationJsonObject.getBoolean("deleted");
                if(notificationJsonObject.has("sLogoName"))
                    storeLogo = notificationJsonObject.getString("sLogoName");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            notificationContentArray = alert.split("!!");
            notificationTitle = notificationContentArray[0];
            notificationBody = notificationContentArray[1];
            Intent resultIntent = new Intent(getApplicationContext(), OfferDetailsActivity.class);
            resultIntent.putExtra("FROM", "PUSH");
            resultIntent.putExtra("offerType",offerType);
            resultIntent.putExtra("offerName",offerName);
            resultIntent.putExtra("storeId",storeId);
            resultIntent.putExtra("offerDesc",offerDesc);
            resultIntent.putExtra("offerStartDate",offerStartDate);
            resultIntent.putExtra("offerStartTime",offerStartTime);
            resultIntent.putExtra("offerEndDate",offerEndDate);
            resultIntent.putExtra("offerEndTime",offerEndTime);
            resultIntent.putExtra("storeName",storeName);
            resultIntent.putExtra("storeNumber",storeNumber);
            resultIntent.putExtra("storeCategory",storeCategory);
            resultIntent.putExtra("storeFloor",storeFloor);
            resultIntent.putExtra("storeLatitude",storeLatitude);
            resultIntent.putExtra("storeLongitude",storeLongitude);
            resultIntent.putExtra("storeDesc",storeDesc);
            resultIntent.putExtra("storeLogo", storeLogo);
            resultIntent.putExtra("storeStatus",storeStatus);
            resultIntent.putExtra("isStoreDeleted",isStoreDeleted);
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplicationContext());
            stackBuilder.addParentStack(OfferDetailsActivity.class);
            stackBuilder.addNextIntent(resultIntent);
            PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

            Notification.InboxStyle inboxStyle = new Notification.InboxStyle();
            NotificationManager nManager = (NotificationManager) getApplicationContext().getSystemService(getApplicationContext().NOTIFICATION_SERVICE);
            Notification.Builder builder = new Notification.Builder(getApplicationContext());
            builder.setContentIntent(resultPendingIntent);
            builder.setAutoCancel(true);
            if(pendingNotificationsCount == 1){
                builder.setContentTitle(notificationTitle);
                builder.setContentText(notificationBody);
            }else {
                builder.setContentTitle("You have " + pendingNotificationsCount + " notifications.");
                builder.setContentText(alert);
            }
            builder.setSmallIcon(R.mipmap.ic_launchericon);
            builder.setStyle(inboxStyle);
            builder.setVibrate(new long[]{500,1000});
            Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            builder.setSound(uri);
            Notification notification = new Notification.BigTextStyle(builder)
                    .bigText(alert).build();
            nManager.notify(NOTIFICATION_ID, notification);
        }else if(offerType.equals("Event")){

            try {
                if(notificationJsonObject.has("offer_name"))
                    offerName = notificationJsonObject.getString("offer_name");
                if(notificationJsonObject.has("offerDesc"))
                    offerDesc = notificationJsonObject.getString("offerDesc");
                if(notificationJsonObject.has("start_date_string"))
                    offerStartDate = notificationJsonObject.getString("start_date_string");
                if(notificationJsonObject.has("start_time"))
                    offerStartTime = notificationJsonObject.getString("start_time");
                if(notificationJsonObject.has("end_date_string"))
                    offerEndDate = notificationJsonObject.getString("end_date_string");
                if(notificationJsonObject.has("end_time"))
                    offerEndTime = notificationJsonObject.getString("end_time");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            notificationContentArray = alert.split("!!");
            notificationTitle = notificationContentArray[0];
            notificationBody = notificationContentArray[1];
            Intent resultIntent = new Intent(getApplicationContext(), EventDetailsActivity.class);
            resultIntent.putExtra("FROM", "PUSH");
            resultIntent.putExtra("offerName",offerName);
            resultIntent.putExtra("offerDesc",offerDesc);
            resultIntent.putExtra("offerStartDate",offerStartDate);
            resultIntent.putExtra("offerStartTime",offerStartTime);
            resultIntent.putExtra("offerEndDate",offerEndDate);
            resultIntent.putExtra("offerEndTime",offerEndTime);
            resultIntent.putExtra("storeId",storeId);
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplicationContext());
            stackBuilder.addParentStack(OfferByCategoryActivity.class);
            stackBuilder.addNextIntent(resultIntent);
            PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

            Notification.InboxStyle inboxStyle = new Notification.InboxStyle();
            NotificationManager nManager = (NotificationManager) getApplicationContext().getSystemService(getApplicationContext().NOTIFICATION_SERVICE);
            Notification.Builder builder = new Notification.Builder(getApplicationContext());
            builder.setContentIntent(resultPendingIntent);
            builder.setAutoCancel(true);
            if(pendingNotificationsCount == 1){
                builder.setContentTitle(notificationTitle);
                builder.setContentText(notificationBody);
            }else {
                builder.setContentTitle("You have " + pendingNotificationsCount + " notifications.");
                builder.setContentText(alert);
            }
            builder.setSmallIcon(R.mipmap.ic_launchericon);
            builder.setStyle(inboxStyle);
            builder.setVibrate(new long[]{500,1000});
            Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            builder.setSound(uri);
            Notification notification = new Notification.BigTextStyle(builder)
                    .bigText(alert).build();
            nManager.notify(NOTIFICATION_ID, notification);
        }
    }
}
