package com.expressavenue.mallapp.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.expressavenue.mallapp.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.expressavenue.mallapp.pojos.Offer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nivedith on 3/11/2016.
 */
public class OfflineDataSyncService extends Service{

    private ArrayList storeArrayList;
    private ArrayList sortedStoreList;

    private ArrayList<Offer> sortedOfferList;
    private ArrayList<Offer> offerArrayList;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        getStores();
        getOffers();
        return super.onStartCommand(intent, flags, startId);
    }

    public void getOffers() {
        ParseQuery<ParseObject> offerQuery = ParseQuery.getQuery("Offer");
        offerQuery.setLimit(500);
        offerQuery.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> offerList, ParseException e) {
                if (e == null) {
                    ParseObject.pinAllInBackground(offerList);
                } else {
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void getStores(){
        ParseQuery<ParseObject> storeQuery = ParseQuery.getQuery(getResources().getString(R.string.store_class_name));
        storeQuery.setLimit(500);
        storeQuery.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> storeList, ParseException e) {
                if (e == null) {
                    ParseObject.pinAllInBackground(storeList);
                } else {
                }
            }
        });
    }
}
