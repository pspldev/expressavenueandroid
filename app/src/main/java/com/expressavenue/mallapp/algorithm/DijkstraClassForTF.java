package com.expressavenue.mallapp.algorithm;

import android.location.Location;

import com.expressavenue.mallapp.global.GlobalState;
import com.expressavenue.mallapp.pojos.Store;
import com.expressavenue.mallapp.utils.TextNav;

import org.psjava.algo.graph.shortestpath.DijkstraAlgorithm;
import org.psjava.algo.graph.shortestpath.SingleSourceShortestPathResult;
import org.psjava.ds.graph.DirectedWeightedEdge;
import org.psjava.ds.graph.MutableDirectedWeightedGraph;
import org.psjava.ds.numbersystrem.IntegerNumberSystem;
import org.psjava.goods.GoodDijkstraAlgorithm;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;


public class DijkstraClassForTF {

    private String srcNumber ="";
    private String srcLat ="";
    private String srcLong ="";

    private String destNumber ="";
    private String destLat ="";
    private String destLong ="";

    private String nearestToSource;
    private String nearestToDest;
    private double sWeight;
    private double dWeight;
    private String nearestToS[];
    private String nearestToD[];

    private Location srcBearingLoc;
    private Location destBearingLoc;

    private ArrayList<String> tnForTF;

    private String junctionNodes[]= {"ctf3","ctf13","ctf14","ctf15","ctf17", "ctf19",
            "ctf4","ctf7","ctf10","ctf11","ctf33","ctf25","ctf34","ctf35", "ctf39",
            "ctf41","ctf42","ctf45","ctf54","ctf55","ctf96","ctf84","ctf91","ctf90","ctf59","ctf73","ctf71","ctf67","ctf65","ctf64"};

    private ArrayList<String> resultantJunctionNodes;
    private ArrayList<Float> bearingsList;
    private Hashtable<String, Float> resultantBearingHash;

    private Hashtable<Float, String> bearingRJNHash;

    private  Location lcff1;

    private Hashtable<Location, String> nodeHash;
    private Hashtable<String, Location> nodeDistTable;

    private ArrayList<Store> sortedStoreList;

    private String srcStoreName = "NA";
    private String destStoreName = "NA";

    private String floorName = "";

    private ArrayList<Location> juctionNodeLocation;

    public DijkstraClassForTF(String srcNumber, String srcLat, String srcLong,
                              String destNumber, String destLat, String destLong, ArrayList<Store> sortedStoreList, String floorName){

        juctionNodeLocation = new ArrayList<Location>();

        for(Store store : sortedStoreList){

            if(store.getStoreNumber().equals(srcNumber)){
                srcStoreName = store.getStoreName();
            }
            if(store.getStoreNumber().equals(destNumber)){
                destStoreName = store.getStoreName();
            }
        }

        if(srcNumber.equals("LT120"))
            srcStoreName = "Main Lift";
        if(srcNumber.equals("LT120LGF") || srcNumber.equals("LT120UGF") || srcNumber.equals("LT120FF") || srcNumber.equals("LT120SF")
                || srcNumber.equals("LT120TF") || srcNumber.equals("LT120B1")|| srcNumber.equals("LT120B2")|| srcNumber.equals("LT120B3") )
            srcStoreName = "Main Lift";
        else if(srcNumber.equals("ESC"))
            srcStoreName = "Escalator";
        else if(srcNumber.equals("ESC"))
            srcStoreName = "Escalator";
        else if(srcNumber.equals("ESC-Left"))
            srcStoreName = "Escalator";
        else if(srcNumber.equals("ESC-Right"))
            srcStoreName = "Escalator";

        if(destNumber.equals("LT120"))
            destStoreName = "Main Lift";
        if(destNumber.equals("LT120LGF") || destNumber.equals("LT120UGF") || destNumber.equals("LT120FF") || destNumber.equals("LT120SF")
                || destNumber.equals("LT120TF") || destNumber.equals("LT120B1")|| destNumber.equals("LT120B2")|| destNumber.equals("LT120B3") )
            destStoreName = "Main Lift";
        else if(destNumber.equals("ESC"))
            destStoreName = "Escalator";
        else if(destNumber.equals("ESC"))
            destStoreName = "Escalator";
        else if(destNumber.equals("ESC-Left"))
            destStoreName = "Escalator";
        else if(destNumber.equals("ESC-Right"))
            destStoreName = "Escalator";

        nodeHash = new Hashtable<Location, String>();
        nodeDistTable = new Hashtable<String, Location>();

        resultantJunctionNodes = new ArrayList<String >();
        resultantBearingHash = new Hashtable<String, Float>();
        bearingsList = new ArrayList<Float>();

        bearingRJNHash = new Hashtable<Float, String>();

        if(!bearingRJNHash.isEmpty())
            bearingRJNHash.clear();


        this.srcNumber = srcNumber;
        this.srcLat = srcLat;
        this.srcLong = srcLong;

        this.destNumber = destNumber;
        this.destLat = destLat;
        this.destLong = destLong;

        this.sortedStoreList = sortedStoreList;

        this.floorName = floorName;

        tnForTF = new ArrayList<String>();

        setNodeHash(13.058668,80.263083, "ctf1");
        setNodeHash(13.058667,80.263125, "ctf2");
        setNodeHash(13.058667,80.263167, "ctf3");
        setNodeHash(13.05862,80.263209, "ctf4");
        setNodeHash(13.058529,80.26321, "ctf5");
        setNodeHash(13.058457,80.263209, "ctf6");
        setNodeHash(13.058612,80.263294, "ctf7");
        setNodeHash(13.058533,80.263294, "ctf8");
        setNodeHash(13.058457,80.263294, "ctf9");
        setNodeHash(13.058637,80.263358, "ctf10");
        setNodeHash(13.05869,80.263358, "ctf11");
        setNodeHash(13.058699,80.263196, "ctf12");
        setNodeHash(13.058719,80.263266, "ctf13");
        setNodeHash(13.058743,80.263267, "ctf14");
        setNodeHash(13.058743,80.263167, "ctf15");
        setNodeHash(13.058807,80.263168, "ctf16");
        setNodeHash(13.058842,80.263168, "ctf17");
        setNodeHash(13.058842,80.263125, "ctf18");
        setNodeHash(13.058743,80.263379, "ctf19");
        setNodeHash(13.058766,80.263379, "ctf20");
        setNodeHash(13.058637,80.263379, "ctf21");
        setNodeHash(13.05869,80.263449, "ctf22");
        setNodeHash(13.05869,80.263379, "ctf23");
        setNodeHash(13.05869,80.263619, "ctf24");
        setNodeHash(13.058637,80.263453, "ctf25");
        setNodeHash(13.05857,80.263454, "ctf26");
        setNodeHash(13.058637,80.263496, "ctf27");
        setNodeHash(13.058637,80.263552, "ctf28");
        setNodeHash(13.058637,80.263623, "ctf29");
        setNodeHash(13.058637,80.263704, "ctf30");
        setNodeHash(13.058638,80.263746, "ctf31");
        setNodeHash(13.058638,80.263781, "ctf32");
        setNodeHash(13.05869,80.263781, "ctf33");
        setNodeHash(13.058637,80.263816, "ctf34");
        setNodeHash(13.05862, 80.263842, "ctf35");
        setNodeHash(13.058584, 80.263842, "ctf36");
        setNodeHash(13.058584, 80.263788, "ctf37");
        setNodeHash(13.0586,80.263901, "ctf38");
        setNodeHash(13.058592,80.263979, "ctf39");
        setNodeHash(13.058592,80.264042, "ctf40");
        setNodeHash(13.058592,80.264105, "ctf41");
        setNodeHash(13.058517,80.264105, "ctf42");
        setNodeHash(13.058517,80.263979, "ctf43");
        setNodeHash(13.058517,80.264056, "ctf44");
        setNodeHash(13.058592,80.264134, "ctf45");
        setNodeHash(13.05858,80.264134, "ctf46");
        setNodeHash(13.058564,80.264134, "ctf47");
        setNodeHash(13.058549,80.264134, "ctf48");
        setNodeHash(13.058529,80.264134, "ctf49");
        setNodeHash(13.058505,80.264134, "ctf50");
        setNodeHash(13.0586,80.264176, "ctf51");
        setNodeHash(13.0586,80.264226, "ctf52");
        setNodeHash(13.058569,80.264254, "ctf53");
        setNodeHash(13.058637,80.264253, "ctf54");
        setNodeHash(13.058692,80.264254, "ctf55");
        setNodeHash(13.058683,80.264225, "ctf56");
        setNodeHash(13.058633,80.264225, "ctf58");
        setNodeHash(13.058644,80.264519, "ctf59");
        setNodeHash(13.058644,80.26455, "ctf60");
        setNodeHash(13.058644,80.264592, "ctf61");
        setNodeHash(13.058644,80.264631, "ctf62");
        setNodeHash(13.058644,80.26466, "ctf63");
        setNodeHash(13.058633,80.264722, "ctf64");
        setNodeHash(13.058527,80.264722, "ctf65");
        setNodeHash(13.058527,80.264673, "ctf66");
        setNodeHash(13.058528,80.264635, "ctf67");
        setNodeHash(13.058567,80.264635, "ctf69");
        setNodeHash(13.058567,80.264586, "ctf70");
        setNodeHash(13.058527,80.264586, "ctf71");
        setNodeHash(13.058528, 80.264558, "ctf72");
        setNodeHash(13.058528, 80.264527, "ctf73");
        setNodeHash(13.058551, 80.264527, "ctf74");
        setNodeHash(13.058599, 80.264527, "ctf75");
        setNodeHash(13.058527, 80.264499, "ctf76");
        setNodeHash(13.058528, 80.264443, "ctf77");
        setNodeHash(13.058496,80.264442, "ctf78");
        setNodeHash(13.058547,80.264442, "ctf79");
        setNodeHash(13.058563,80.264442, "ctf80");
        setNodeHash(13.058598,80.264442, "ctf81");
        setNodeHash(13.058614,80.264442, "ctf82");
        setNodeHash(13.058626,80.264442, "ctf83");
        setNodeHash(13.058533,80.264442, "ctf84");
        setNodeHash(13.058533,80.26442, "ctf85");
        setNodeHash(13.058531,80.26437, "ctf86");
        setNodeHash(13.058529,80.264328, "ctf87");
        setNodeHash(13.058529,80.264258, "ctf88");
        setNodeHash(13.058529,80.264222, "ctf89");
        setNodeHash(13.058693,80.264519, "ctf90");
        setNodeHash(13.058693,80.264547, "ctf91");
        setNodeHash(13.058748, 80.264547, "ctf92");
        setNodeHash(13.058637, 80.264352, "ctf93");
        setNodeHash(13.058582, 80.264225, "ctf94");
        setNodeHash(13.058582, 80.26425, "ctf95");
        setNodeHash(13.058637, 80.264442, "ctf96");
        setDistHash(13.058668, 80.263083, "ctf1");
        setDistHash(13.058667, 80.263125, "ctf2");
        setDistHash(13.058667, 80.263167, "ctf3");
        setDistHash(13.05862, 80.263209, "ctf4");
        setDistHash(13.058529, 80.26321, "ctf5");
        setDistHash(13.058457, 80.263209, "ctf6");
        setDistHash(13.058612, 80.263294, "ctf7");
        setDistHash(13.058533, 80.263294, "ctf8");
        setDistHash(13.058457, 80.263294, "ctf9");
        setDistHash(13.058637, 80.263358, "ctf10");
        setDistHash(13.05869, 80.263358, "ctf11");
        setDistHash(13.058699, 80.263196, "ctf12");
        setDistHash(13.058719, 80.263266, "ctf13");
        setDistHash(13.058743, 80.263267, "ctf14");
        setDistHash(13.058743, 80.263167, "ctf15");
        setDistHash(13.058807, 80.263168, "ctf16");
        setDistHash(13.058842, 80.263168, "ctf17");
        setDistHash(13.058842, 80.263125, "ctf18");
        setDistHash(13.058743, 80.263379, "ctf19");
        setDistHash(13.058766, 80.263379, "ctf20");
        setDistHash(13.058637, 80.263379, "ctf21");
        setDistHash(13.05869, 80.263449, "ctf22");
        setDistHash(13.05869, 80.263379, "ctf23");
        setDistHash(13.05869, 80.263619, "ctf24");
        setDistHash(13.058637, 80.263453, "ctf25");
        setDistHash(13.05857, 80.263454, "ctf26");
        setDistHash(13.058637, 80.263496, "ctf27");
        setDistHash(13.058637, 80.263552, "ctf28");
        setDistHash(13.058637, 80.263623, "ctf29");
        setDistHash(13.058637, 80.263704, "ctf30");
        setDistHash(13.058638, 80.263746, "ctf31");
        setDistHash(13.058638, 80.263781, "ctf32");
        setDistHash(13.05869, 80.263781, "ctf33");
        setDistHash(13.058637, 80.263816, "ctf34");
        setDistHash(13.05862, 80.263842, "ctf35");
        setDistHash(13.058584, 80.263842, "ctf36");
        setDistHash(13.058584, 80.263788, "ctf37");
        setDistHash(13.0586, 80.263901, "ctf38");
        setDistHash(13.058592, 80.263979, "ctf39");
        setDistHash(13.058592, 80.264042, "ctf40");
        setDistHash(13.058592, 80.264105, "ctf41");
        setDistHash(13.058517, 80.264105, "ctf42");
        setDistHash(13.058517, 80.263979, "ctf43");
        setDistHash(13.058517, 80.264056, "ctf44");
        setDistHash(13.058592, 80.264134, "ctf45");
        setDistHash(13.05858, 80.264134, "ctf46");
        setDistHash(13.058564, 80.264134, "ctf47");
        setDistHash(13.058549, 80.264134, "ctf48");
        setDistHash(13.058529, 80.264134, "ctf49");
        setDistHash(13.058505, 80.264134, "ctf50");
        setDistHash(13.0586, 80.264176, "ctf51");
        setDistHash(13.0586, 80.264226, "ctf52");
        setDistHash(13.058569, 80.264254, "ctf53");
        setDistHash(13.058637, 80.264253, "ctf54");
        setDistHash(13.058692, 80.264254, "ctf55");
        setDistHash(13.058683, 80.264225, "ctf56");
        setDistHash(13.058633, 80.264225, "ctf58");
        setDistHash(13.058644, 80.264519, "ctf59");
        setDistHash(13.058644, 80.26455, "ctf60");
        setDistHash(13.058644, 80.264592, "ctf61");
        setDistHash(13.058644, 80.264631, "ctf62");
        setDistHash(13.058644, 80.26466, "ctf63");
        setDistHash(13.058633, 80.264722, "ctf64");
        setDistHash(13.058527, 80.264722, "ctf65");
        setDistHash(13.058527, 80.264673, "ctf66");
        setDistHash(13.058528, 80.264635, "ctf67");
        setDistHash(13.058567, 80.264635, "ctf69");
        setDistHash(13.058567, 80.264586, "ctf70");
        setDistHash(13.058527, 80.264586, "ctf71");
        setDistHash(13.058528, 80.264558, "ctf72");
        setDistHash(13.058528, 80.264527, "ctf73");
        setDistHash(13.058551, 80.264527, "ctf74");
        setDistHash(13.058599, 80.264527, "ctf75");
        setDistHash(13.058527, 80.264499, "ctf76");
        setDistHash(13.058528, 80.264443, "ctf77");
        setDistHash(13.058496, 80.264442, "ctf78");
        setDistHash(13.058547, 80.264442, "ctf79");
        setDistHash(13.058563, 80.264442, "ctf80");
        setDistHash(13.058598, 80.264442, "ctf81");
        setDistHash(13.058614, 80.264442, "ctf82");
        setDistHash(13.058626, 80.264442, "ctf83");
        setDistHash(13.058533, 80.264442, "ctf84");
        setDistHash(13.058533, 80.26442, "ctf85");
        setDistHash(13.058531, 80.26437, "ctf86");
        setDistHash(13.058529, 80.264328, "ctf87");
        setDistHash(13.058529, 80.264258, "ctf88");
        setDistHash(13.058529, 80.264222, "ctf89");
        setDistHash(13.058693, 80.264519, "ctf90");
        setDistHash(13.058693, 80.264547, "ctf91");
        setDistHash(13.058748, 80.264547, "ctf92");
        setDistHash(13.058637, 80.264352, "ctf93");
        setDistHash(13.058582, 80.264225, "ctf94");
        setDistHash(13.058582, 80.26425, "ctf95");
        setDistHash(13.058637, 80.264442, "ctf96");

        setDistHash(Double.parseDouble(srcLat), Double.parseDouble(srcLong), this.srcNumber);
        setDistHash(Double.parseDouble(destLat), Double.parseDouble(destLong), this.destNumber);

        Location srcLocation = new Location("");
        srcLocation.setLatitude(Double.parseDouble(this.srcLat));
        srcLocation.setLongitude(Double.parseDouble(this.srcLong));
        srcBearingLoc = srcLocation;
        nearestToS = getNearesLocation(srcLocation, nodeHash);
        nearestToSource = nearestToS[0];
        sWeight = Double.parseDouble(nearestToS[1]);

        Location destLocation = new Location("");
        destLocation.setLatitude(Double.parseDouble(this.destLat));
        destLocation.setLongitude(Double.parseDouble(this.destLong));
        destBearingLoc = destLocation;
        nearestToD = getNearesLocation(destLocation, nodeHash);
        nearestToDest = nearestToD[0];
        dWeight = Double.parseDouble(nearestToD[1]);
    }

    private void setNodeHash(double lat, double lon, String storeNo)
    {
        lcff1 = new Location(storeNo);
        lcff1.setLatitude(lat);
        lcff1.setLongitude(lon);
        nodeHash.put(lcff1,storeNo);
    }
    private void setDistHash(double lat, double lon, String storeNo)
    {
        lcff1 = new Location(storeNo);
        lcff1.setLatitude(lat);
        lcff1.setLongitude(lon);
        nodeDistTable.put(storeNo, lcff1);
    }
    public String[] getNearesLocation(Location srcLoc, Hashtable<Location, String> srcHash){

        String nearestStore[] = new String[2];
        ArrayList<Double> distValues = new ArrayList<Double>();
        Hashtable<Double, Location> distHash = new Hashtable<Double, Location>();
        Set<Location> locSet = srcHash.keySet();
        for(Location location : locSet){

            double dist = location.distanceTo(srcLoc);
            distValues.add(dist);
            distHash.put(dist,location);
        }

        double smallest = Double.MAX_VALUE;
        for(int i =0;i<distValues.size();i++) {
            if(smallest > distValues.get(i)) {
                smallest = distValues.get(i);
            }
        }

        Location nearLoc = distHash.get(smallest);
        nearestStore[0] = srcHash.get(nearLoc);
        nearestStore[1] = String.valueOf(smallest);

        return nearestStore;
    }

    public ArrayList<String> algorithmImplementation() {

        ArrayList<String> pathVertices = new ArrayList<>();
        pathVertices.clear();

        IntegerNumberSystem NS = IntegerNumberSystem.getInstance();

        MutableDirectedWeightedGraph<String, Integer> graph = MutableDirectedWeightedGraph.create();

        graph.insertVertex("ctf1");
        graph.insertVertex("ctf2");
        graph.insertVertex("ctf3");
        graph.insertVertex("ctf4");
        graph.insertVertex("ctf5");
        graph.insertVertex("ctf6");
        graph.insertVertex("ctf7");
        graph.insertVertex("ctf8");
        graph.insertVertex("ctf9");
        graph.insertVertex("ctf10");
        graph.insertVertex("ctf11");
        graph.insertVertex("ctf12");
        graph.insertVertex("ctf13");
        graph.insertVertex("ctf14");
        graph.insertVertex("ctf15");
        graph.insertVertex("ctf16");
        graph.insertVertex("ctf17");
        graph.insertVertex("ctf18");
        graph.insertVertex("ctf19");
        graph.insertVertex("ctf20");
        graph.insertVertex("ctf21");
        graph.insertVertex("ctf22");
        graph.insertVertex("ctf23");
        graph.insertVertex("ctf24");
        graph.insertVertex("ctf25");
        graph.insertVertex("ctf26");
        graph.insertVertex("ctf27");
        graph.insertVertex("ctf28");
        graph.insertVertex("ctf29");
        graph.insertVertex("ctf30");
        graph.insertVertex("ctf31");
        graph.insertVertex("ctf32");
        graph.insertVertex("ctf33");
        graph.insertVertex("ctf34");
        graph.insertVertex("ctf35");
        graph.insertVertex("ctf36");
        graph.insertVertex("ctf37");
        graph.insertVertex("ctf38");
        graph.insertVertex("ctf39");
        graph.insertVertex("ctf40");
        graph.insertVertex("ctf41");
        graph.insertVertex("ctf42");
        graph.insertVertex("ctf43");
        graph.insertVertex("ctf44");
        graph.insertVertex("ctf45");
        graph.insertVertex("ctf46");
        graph.insertVertex("ctf47");
        graph.insertVertex("ctf48");
        graph.insertVertex("ctf49");
        graph.insertVertex("ctf50");
        graph.insertVertex("ctf51");
        graph.insertVertex("ctf52");
        graph.insertVertex("ctf53");
        graph.insertVertex("ctf54");
        graph.insertVertex("ctf55");
        graph.insertVertex("ctf56");
        graph.insertVertex("ctf57");
        graph.insertVertex("ctf58");
        graph.insertVertex("ctf59");
        graph.insertVertex("ctf60");
        graph.insertVertex("ctf61");
        graph.insertVertex("ctf62");
        graph.insertVertex("ctf63");
        graph.insertVertex("ctf64");
        graph.insertVertex("ctf65");
        graph.insertVertex("ctf66");
        graph.insertVertex("ctf67");
        graph.insertVertex("ctf68");
        graph.insertVertex("ctf69");
        graph.insertVertex("ctf70");
        graph.insertVertex("ctf71");
        graph.insertVertex("ctf72");
        graph.insertVertex("ctf73");
        graph.insertVertex("ctf74");
        graph.insertVertex("ctf75");
        graph.insertVertex("ctf76");
        graph.insertVertex("ctf77");
        graph.insertVertex("ctf78");
        graph.insertVertex("ctf79");
        graph.insertVertex("ctf80");
        graph.insertVertex("ctf81");
        graph.insertVertex("ctf82");
        graph.insertVertex("ctf83");
        graph.insertVertex("ctf84");
        graph.insertVertex("ctf85");
        graph.insertVertex("ctf86");
        graph.insertVertex("ctf87");
        graph.insertVertex("ctf88");
        graph.insertVertex("ctf89");
        graph.insertVertex("ctf90");
        graph.insertVertex("ctf91");
        graph.insertVertex("ctf92");
        graph.insertVertex("ctf93");
        graph.insertVertex("ctf94");
        graph.insertVertex("ctf95");
        graph.insertVertex("ctf96");

        graph.insertVertex(srcNumber);
        graph.insertVertex(destNumber);

        graph.addEdge(srcNumber, nearestToSource, (int) sWeight);
        graph.addEdge(nearestToDest, destNumber, (int)dWeight);

        graph.addEdge(nearestToSource, srcNumber, (int)sWeight);
        graph.addEdge(destNumber, nearestToDest, (int)dWeight);

        graph.addEdge("ctf1", "ctf2", (int)nodeDistTable.get("ctf1").distanceTo(nodeDistTable.get("ctf2")));
        graph.addEdge("ctf2", "ctf3", (int)nodeDistTable.get("ctf1").distanceTo(nodeDistTable.get("ctf2")));
        graph.addEdge("ctf3", "ctf4", (int)nodeDistTable.get("ctf3").distanceTo(nodeDistTable.get("ctf4")));
        graph.addEdge("ctf4", "ctf5", (int)nodeDistTable.get("ctf4").distanceTo(nodeDistTable.get("ctf5")));
        graph.addEdge("ctf5", "ctf6", (int)nodeDistTable.get("ctf5").distanceTo(nodeDistTable.get("ctf6")));
        graph.addEdge("ctf6", "ctf9", (int)nodeDistTable.get("ctf6").distanceTo(nodeDistTable.get("ctf9")));
        graph.addEdge("ctf9", "ctf8", (int)nodeDistTable.get("ctf9").distanceTo(nodeDistTable.get("ctf8")));
        graph.addEdge("ctf8", "ctf7", (int)nodeDistTable.get("ctf8").distanceTo(nodeDistTable.get("ctf7")));
        graph.addEdge("ctf7", "ctf4", (int)nodeDistTable.get("ctf7").distanceTo(nodeDistTable.get("ctf4")));
        graph.addEdge("ctf7", "ctf10", (int)nodeDistTable.get("ctf7").distanceTo(nodeDistTable.get("ctf10")));
        graph.addEdge("ctf10", "ctf21",(int)nodeDistTable.get("ctf10").distanceTo(nodeDistTable.get("ctf21")) );
        graph.addEdge("ctf21", "ctf25", (int)nodeDistTable.get("ctf21").distanceTo(nodeDistTable.get("ctf25")));
        graph.addEdge("ctf25", "ctf26", (int)nodeDistTable.get("ctf25").distanceTo(nodeDistTable.get("ctf26")));
        graph.addEdge("ctf25", "ctf27",(int)nodeDistTable.get("ctf25").distanceTo(nodeDistTable.get("ctf27")) );
        graph.addEdge("ctf27", "ctf28", (int)nodeDistTable.get("ctf27").distanceTo(nodeDistTable.get("ctf28")));
        graph.addEdge("ctf28", "ctf29",(int)nodeDistTable.get("ctf28").distanceTo(nodeDistTable.get("ctf29")) );
        graph.addEdge("ctf29", "ctf30", (int)nodeDistTable.get("ctf29").distanceTo(nodeDistTable.get("ctf30")));
        graph.addEdge("ctf30", "ctf31", (int)nodeDistTable.get("ctf30").distanceTo(nodeDistTable.get("ctf31")));
        graph.addEdge("ctf31", "ctf32",(int)nodeDistTable.get("ctf31").distanceTo(nodeDistTable.get("ctf32")) );
        graph.addEdge("ctf32", "ctf34", (int)nodeDistTable.get("ctf32").distanceTo(nodeDistTable.get("ctf34")));
        graph.addEdge("ctf34", "ctf35", (int)nodeDistTable.get("ctf34").distanceTo(nodeDistTable.get("ctf35")));
        graph.addEdge("ctf35", "ctf36",(int)nodeDistTable.get("ctf35").distanceTo(nodeDistTable.get("ctf36")) );
        graph.addEdge("ctf36", "ctf37",(int)nodeDistTable.get("ctf36").distanceTo(nodeDistTable.get("ctf37")) );
        graph.addEdge("ctf35", "ctf38",(int)nodeDistTable.get("ctf35").distanceTo(nodeDistTable.get("ctf38")) );
        graph.addEdge("ctf38", "ctf39",(int)nodeDistTable.get("ctf38").distanceTo(nodeDistTable.get("ctf39")) );
        graph.addEdge("ctf39", "ctf40",(int)nodeDistTable.get("ctf39").distanceTo(nodeDistTable.get("ctf40")) );
        graph.addEdge("ctf40", "ctf41",(int)nodeDistTable.get("ctf40").distanceTo(nodeDistTable.get("ctf41")) );
        graph.addEdge("ctf41", "ctf42",(int)nodeDistTable.get("ctf41").distanceTo(nodeDistTable.get("ctf42")) );
        graph.addEdge("ctf42", "ctf44",(int)nodeDistTable.get("ctf42").distanceTo(nodeDistTable.get("ctf44")) );
        graph.addEdge("ctf44", "ctf43",(int)nodeDistTable.get("ctf44").distanceTo(nodeDistTable.get("ctf43")) );
        graph.addEdge("ctf42", "ctf50",(int)nodeDistTable.get("ctf42").distanceTo(nodeDistTable.get("ctf50")) );
        graph.addEdge("ctf50", "ctf49",(int)nodeDistTable.get("ctf50").distanceTo(nodeDistTable.get("ctf49")) );
        graph.addEdge("ctf49", "ctf42", (int)nodeDistTable.get("ctf49").distanceTo(nodeDistTable.get("ctf42")));
        graph.addEdge("ctf49", "ctf48", (int)nodeDistTable.get("ctf49").distanceTo(nodeDistTable.get("ctf48")));
        graph.addEdge("ctf48", "ctf47",(int)nodeDistTable.get("ctf48").distanceTo(nodeDistTable.get("ctf47")) );
        graph.addEdge("ctf47", "ctf46",(int)nodeDistTable.get("ctf47").distanceTo(nodeDistTable.get("ctf46")) );
        graph.addEdge("ctf46", "ctf45",(int)nodeDistTable.get("ctf46").distanceTo(nodeDistTable.get("ctf45")) );
        graph.addEdge("ctf45", "ctf41",(int)nodeDistTable.get("ctf45").distanceTo(nodeDistTable.get("ctf41")) );
        graph.addEdge("ctf45", "ctf51",(int)nodeDistTable.get("ctf45").distanceTo(nodeDistTable.get("ctf51")) );
        graph.addEdge("ctf51", "ctf52", (int)nodeDistTable.get("ctf51").distanceTo(nodeDistTable.get("ctf52")));
        graph.addEdge("ctf94", "ctf95",(int)nodeDistTable.get("ctf94").distanceTo(nodeDistTable.get("ctf95")) );
        graph.addEdge("ctf95", "ctf53",(int)nodeDistTable.get("ctf95").distanceTo(nodeDistTable.get("ctf53")) );
        graph.addEdge("ctf51", "ctf58",(int)nodeDistTable.get("ctf51").distanceTo(nodeDistTable.get("ctf58")) );
        graph.addEdge("ctf58", "ctf52",(int)nodeDistTable.get("ctf58").distanceTo(nodeDistTable.get("ctf52")) );
        graph.addEdge("ctf58", "ctf54", (int)nodeDistTable.get("ctf58").distanceTo(nodeDistTable.get("ctf54")));
        graph.addEdge("ctf54", "ctf93",(int)nodeDistTable.get("ctf54").distanceTo(nodeDistTable.get("ctf93")) );
        graph.addEdge("ctf93", "ctf96",(int)nodeDistTable.get("ctf93").distanceTo(nodeDistTable.get("ctf96")) );
        graph.addEdge("ctf96", "ctf83", (int)nodeDistTable.get("ctf96").distanceTo(nodeDistTable.get("ctf83")));
        graph.addEdge("ctf83", "ctf82",(int)nodeDistTable.get("ctf83").distanceTo(nodeDistTable.get("ctf82")) );
        graph.addEdge("ctf82", "ctf81",(int)nodeDistTable.get("ctf82").distanceTo(nodeDistTable.get("ctf81")) );
        graph.addEdge("ctf81", "ctf80", (int)nodeDistTable.get("ctf81").distanceTo(nodeDistTable.get("ctf80")));
        graph.addEdge("ctf80", "ctf79",(int)nodeDistTable.get("ctf80").distanceTo(nodeDistTable.get("ctf79")) );
        graph.addEdge("ctf79", "ctf84",(int)nodeDistTable.get("ctf79").distanceTo(nodeDistTable.get("ctf84")) );
        graph.addEdge("ctf79", "ctf85",(int)nodeDistTable.get("ctf79").distanceTo(nodeDistTable.get("ctf85")) );
        graph.addEdge("ctf24", "ctf22",(int)nodeDistTable.get("ctf24").distanceTo(nodeDistTable.get("ctf22")) );
        graph.addEdge("ctf22", "ctf23", (int)nodeDistTable.get("ctf22").distanceTo(nodeDistTable.get("ctf23")));
        graph.addEdge("ctf23", "ctf11",(int)nodeDistTable.get("ctf23").distanceTo(nodeDistTable.get("ctf11")) );
        graph.addEdge("ctf23", "ctf10",(int)nodeDistTable.get("ctf23").distanceTo(nodeDistTable.get("ctf10")) );
        graph.addEdge("ctf21", "ctf11", (int)nodeDistTable.get("ctf21").distanceTo(nodeDistTable.get("ctf11")));
        graph.addEdge("ctf23", "ctf21",(int)nodeDistTable.get("ctf23").distanceTo(nodeDistTable.get("ctf21")) );
        graph.addEdge("ctf11", "ctf10", (int)nodeDistTable.get("ctf11").distanceTo(nodeDistTable.get("ctf10")));
        graph.addEdge("ctf11", "ctf13",(int)nodeDistTable.get("ctf11").distanceTo(nodeDistTable.get("ctf13")) );
        graph.addEdge("ctf13", "ctf14",(int)nodeDistTable.get("ctf13").distanceTo(nodeDistTable.get("ctf14")) );
        graph.addEdge("ctf14", "ctf15", (int)nodeDistTable.get("ctf14").distanceTo(nodeDistTable.get("ctf15")));
        graph.addEdge("ctf15", "ctf16",(int)nodeDistTable.get("ctf15").distanceTo(nodeDistTable.get("ctf16")) );
        graph.addEdge("ctf16", "ctf17",(int)nodeDistTable.get("ctf16").distanceTo(nodeDistTable.get("ctf17")) );
        graph.addEdge("ctf17", "ctf18",(int)nodeDistTable.get("ctf17").distanceTo(nodeDistTable.get("ctf18")) );
        graph.addEdge("ctf17", "ctf20",(int)nodeDistTable.get("ctf17").distanceTo(nodeDistTable.get("ctf20")) );
        graph.addEdge("ctf20", "ctf19",(int)nodeDistTable.get("ctf20").distanceTo(nodeDistTable.get("ctf19")) );
        graph.addEdge("ctf19", "ctf14",(int)nodeDistTable.get("ctf19").distanceTo(nodeDistTable.get("ctf14")) );
        graph.addEdge("ctf16", "ctf20",(int)nodeDistTable.get("ctf16").distanceTo(nodeDistTable.get("ctf20")) );
        graph.addEdge("ctf16", "ctf19",(int)nodeDistTable.get("ctf16").distanceTo(nodeDistTable.get("ctf19")) );
        graph.addEdge("ctf14", "ctf16",(int)nodeDistTable.get("ctf14").distanceTo(nodeDistTable.get("ctf16")) );
        graph.addEdge("ctf14", "ctf17",(int)nodeDistTable.get("ctf14").distanceTo(nodeDistTable.get("ctf17")) );
        graph.addEdge("ctf15", "ctf20",(int)nodeDistTable.get("ctf15").distanceTo(nodeDistTable.get("ctf20")) );
        graph.addEdge("ctf19", "ctf17",(int)nodeDistTable.get("ctf19").distanceTo(nodeDistTable.get("ctf17")) );
        graph.addEdge("ctf13", "ctf12",(int)nodeDistTable.get("ctf13").distanceTo(nodeDistTable.get("ctf12")) );
        graph.addEdge("ctf12", "ctf3",(int)nodeDistTable.get("ctf12").distanceTo(nodeDistTable.get("ctf3")) );
        graph.addEdge("ctf85", "ctf86",(int)nodeDistTable.get("ctf85").distanceTo(nodeDistTable.get("ctf86")) );
        graph.addEdge("ctf86", "ctf87",(int)nodeDistTable.get("ctf86").distanceTo(nodeDistTable.get("ctf87")) );
        graph.addEdge("ctf87", "ctf88",(int)nodeDistTable.get("ctf87").distanceTo(nodeDistTable.get("ctf88")) );
        graph.addEdge("ctf88", "ctf89",(int)nodeDistTable.get("ctf88").distanceTo(nodeDistTable.get("ctf89")) );
        graph.addEdge("ctf84", "ctf77",(int)nodeDistTable.get("ctf84").distanceTo(nodeDistTable.get("ctf77")) );
        graph.addEdge("ctf77", "ctf78",(int)nodeDistTable.get("ctf77").distanceTo(nodeDistTable.get("ctf78")) );
        graph.addEdge("ctf77", "ctf76", (int)nodeDistTable.get("ctf77").distanceTo(nodeDistTable.get("ctf76")));
        graph.addEdge("ctf76", "ctf73", (int)nodeDistTable.get("ctf76").distanceTo(nodeDistTable.get("ctf73")));
        graph.addEdge("ctf73", "ctf74",(int)nodeDistTable.get("ctf73").distanceTo(nodeDistTable.get("ctf74")) );
        graph.addEdge("ctf74", "ctf75",(int)nodeDistTable.get("ctf74").distanceTo(nodeDistTable.get("ctf75")) );
        graph.addEdge("ctf73", "ctf72",(int)nodeDistTable.get("ctf73").distanceTo(nodeDistTable.get("ctf72")) );
        graph.addEdge("ctf72", "ctf71",(int)nodeDistTable.get("ctf72").distanceTo(nodeDistTable.get("ctf71")) );
        graph.addEdge("ctf71", "ctf70", (int)nodeDistTable.get("ctf71").distanceTo(nodeDistTable.get("ctf70")));
        graph.addEdge("ctf71", "ctf67",(int)nodeDistTable.get("ctf71").distanceTo(nodeDistTable.get("ctf67")) );
        graph.addEdge("ctf67", "ctf66",(int)nodeDistTable.get("ctf67").distanceTo(nodeDistTable.get("ctf66")) );
        graph.addEdge("ctf67", "ctf69",(int)nodeDistTable.get("ctf67").distanceTo(nodeDistTable.get("ctf69")) );
        graph.addEdge("ctf66", "ctf65", (int)nodeDistTable.get("ctf66").distanceTo(nodeDistTable.get("ctf65")));
        graph.addEdge("ctf65", "ctf64", (int)nodeDistTable.get("ctf65").distanceTo(nodeDistTable.get("ctf64")));
        graph.addEdge("ctf64", "ctf63",(int)nodeDistTable.get("ctf64").distanceTo(nodeDistTable.get("ctf63")) );
        graph.addEdge("ctf63", "ctf62",(int)nodeDistTable.get("ctf63").distanceTo(nodeDistTable.get("ctf62")) );
        graph.addEdge("ctf62", "ctf61", (int)nodeDistTable.get("ctf62").distanceTo(nodeDistTable.get("ctf61")));
        graph.addEdge("ctf61", "ctf60",(int)nodeDistTable.get("ctf61").distanceTo(nodeDistTable.get("ctf60")) );
        graph.addEdge("ctf60", "ctf59",(int)nodeDistTable.get("ctf60").distanceTo(nodeDistTable.get("ctf59")) );
        graph.addEdge("ctf59", "ctf96",(int)nodeDistTable.get("ctf59").distanceTo(nodeDistTable.get("ctf96")) );
        graph.addEdge("ctf59", "ctf90",(int)nodeDistTable.get("ctf59").distanceTo(nodeDistTable.get("ctf90")) );
        graph.addEdge("ctf90", "ctf91",(int)nodeDistTable.get("ctf90").distanceTo(nodeDistTable.get("ctf91")) );
        graph.addEdge("ctf91", "ctf92",(int)nodeDistTable.get("ctf91").distanceTo(nodeDistTable.get("ctf92")) );
        graph.addEdge("ctf90", "ctf55",(int)nodeDistTable.get("ctf90").distanceTo(nodeDistTable.get("ctf55")) );
        graph.addEdge("ctf55", "ctf54",(int)nodeDistTable.get("ctf55").distanceTo(nodeDistTable.get("ctf54")) );
        graph.addEdge("ctf55", "ctf58",(int)nodeDistTable.get("ctf55").distanceTo(nodeDistTable.get("ctf58")) );
        graph.addEdge("ctf55", "ctf56",(int)nodeDistTable.get("ctf55").distanceTo(nodeDistTable.get("ctf56")) );
        graph.addEdge("ctf56", "ctf54",(int)nodeDistTable.get("ctf56").distanceTo(nodeDistTable.get("ctf54")) );
        graph.addEdge("ctf33", "ctf34",(int)nodeDistTable.get("ctf33").distanceTo(nodeDistTable.get("ctf34")) );
        graph.addEdge("ctf33", "ctf24",(int)nodeDistTable.get("ctf33").distanceTo(nodeDistTable.get("ctf24")) );
        graph.addEdge("ctf2", "ctf1", (int)nodeDistTable.get("ctf1").distanceTo(nodeDistTable.get("ctf2")));
        graph.addEdge("ctf3", "ctf2", (int)nodeDistTable.get("ctf1").distanceTo(nodeDistTable.get("ctf2")));
        graph.addEdge("ctf4", "ctf3", (int)nodeDistTable.get("ctf3").distanceTo(nodeDistTable.get("ctf4")));
        graph.addEdge("ctf5", "ctf4", (int)nodeDistTable.get("ctf4").distanceTo(nodeDistTable.get("ctf5")));
        graph.addEdge("ctf6", "ctf5", (int)nodeDistTable.get("ctf5").distanceTo(nodeDistTable.get("ctf6")));
        graph.addEdge("ctf9", "ctf6", (int)nodeDistTable.get("ctf6").distanceTo(nodeDistTable.get("ctf9")));
        graph.addEdge("ctf8", "ctf9", (int)nodeDistTable.get("ctf9").distanceTo(nodeDistTable.get("ctf8")));
        graph.addEdge("ctf7", "ctf8", (int)nodeDistTable.get("ctf8").distanceTo(nodeDistTable.get("ctf7")));
        graph.addEdge("ctf4", "ctf7", (int)nodeDistTable.get("ctf7").distanceTo(nodeDistTable.get("ctf4")));
        graph.addEdge("ctf10", "ctf7", (int)nodeDistTable.get("ctf7").distanceTo(nodeDistTable.get("ctf10")));
        graph.addEdge("ctf21", "ctf10",(int)nodeDistTable.get("ctf10").distanceTo(nodeDistTable.get("ctf21")) );
        graph.addEdge("ctf25", "ctf21", (int)nodeDistTable.get("ctf21").distanceTo(nodeDistTable.get("ctf25")));
        graph.addEdge("ctf26", "ctf25", (int)nodeDistTable.get("ctf25").distanceTo(nodeDistTable.get("ctf26")));
        graph.addEdge("ctf27", "ctf25",(int)nodeDistTable.get("ctf25").distanceTo(nodeDistTable.get("ctf27")) );
        graph.addEdge("ctf28", "ctf27", (int)nodeDistTable.get("ctf27").distanceTo(nodeDistTable.get("ctf28")));
        graph.addEdge("ctf29", "ctf28",(int)nodeDistTable.get("ctf28").distanceTo(nodeDistTable.get("ctf29")) );
        graph.addEdge("ctf30", "ctf29", (int)nodeDistTable.get("ctf29").distanceTo(nodeDistTable.get("ctf30")));
        graph.addEdge("ctf31", "ctf30", (int)nodeDistTable.get("ctf30").distanceTo(nodeDistTable.get("ctf31")));
        graph.addEdge("ctf32", "ctf31",(int)nodeDistTable.get("ctf31").distanceTo(nodeDistTable.get("ctf32")) );
        graph.addEdge("ctf34", "ctf32", (int)nodeDistTable.get("ctf32").distanceTo(nodeDistTable.get("ctf34")));
        graph.addEdge("ctf35", "ctf34", (int)nodeDistTable.get("ctf34").distanceTo(nodeDistTable.get("ctf35")));
        graph.addEdge("ctf36", "ctf35",(int)nodeDistTable.get("ctf35").distanceTo(nodeDistTable.get("ctf36")) );
        graph.addEdge("ctf37", "ctf36",(int)nodeDistTable.get("ctf36").distanceTo(nodeDistTable.get("ctf37")) );
        graph.addEdge("ctf38", "ctf35",(int)nodeDistTable.get("ctf35").distanceTo(nodeDistTable.get("ctf38")) );
        graph.addEdge("ctf39", "ctf38",(int)nodeDistTable.get("ctf38").distanceTo(nodeDistTable.get("ctf39")) );
        graph.addEdge("ctf40", "ctf39",(int)nodeDistTable.get("ctf39").distanceTo(nodeDistTable.get("ctf40")) );
        graph.addEdge("ctf41", "ctf40",(int)nodeDistTable.get("ctf40").distanceTo(nodeDistTable.get("ctf41")) );
        graph.addEdge("ctf42", "ctf41",(int)nodeDistTable.get("ctf41").distanceTo(nodeDistTable.get("ctf42")) );
        graph.addEdge("ctf44", "ctf42",(int)nodeDistTable.get("ctf42").distanceTo(nodeDistTable.get("ctf44")) );
        graph.addEdge("ctf43", "ctf44",(int)nodeDistTable.get("ctf44").distanceTo(nodeDistTable.get("ctf43")) );
        graph.addEdge("ctf50", "ctf42",(int)nodeDistTable.get("ctf42").distanceTo(nodeDistTable.get("ctf50")) );
        graph.addEdge("ctf49", "ctf50",(int)nodeDistTable.get("ctf50").distanceTo(nodeDistTable.get("ctf49")) );
        graph.addEdge("ctf42", "ctf49", (int)nodeDistTable.get("ctf49").distanceTo(nodeDistTable.get("ctf42")));
        graph.addEdge("ctf48", "ctf49", (int)nodeDistTable.get("ctf49").distanceTo(nodeDistTable.get("ctf48")));
        graph.addEdge("ctf47", "ctf48",(int)nodeDistTable.get("ctf48").distanceTo(nodeDistTable.get("ctf47")) );
        graph.addEdge("ctf46", "ctf47",(int)nodeDistTable.get("ctf47").distanceTo(nodeDistTable.get("ctf46")) );
        graph.addEdge("ctf45", "ctf46",(int)nodeDistTable.get("ctf46").distanceTo(nodeDistTable.get("ctf45")) );
        graph.addEdge("ctf41", "ctf45",(int)nodeDistTable.get("ctf45").distanceTo(nodeDistTable.get("ctf41")) );
        graph.addEdge("ctf51", "ctf45",(int)nodeDistTable.get("ctf45").distanceTo(nodeDistTable.get("ctf51")) );
        graph.addEdge("ctf52", "ctf51", (int)nodeDistTable.get("ctf51").distanceTo(nodeDistTable.get("ctf52")));
        graph.addEdge("ctf95", "ctf94",(int)nodeDistTable.get("ctf94").distanceTo(nodeDistTable.get("ctf95")) );
        graph.addEdge("ctf53", "ctf95",(int)nodeDistTable.get("ctf95").distanceTo(nodeDistTable.get("ctf53")) );
        graph.addEdge("ctf58", "ctf51",(int)nodeDistTable.get("ctf51").distanceTo(nodeDistTable.get("ctf58")) );
        graph.addEdge("ctf52", "ctf58",(int)nodeDistTable.get("ctf58").distanceTo(nodeDistTable.get("ctf52")) );
        graph.addEdge("ctf54", "ctf58", (int)nodeDistTable.get("ctf58").distanceTo(nodeDistTable.get("ctf54")));
        graph.addEdge("ctf93", "ctf54",(int)nodeDistTable.get("ctf54").distanceTo(nodeDistTable.get("ctf93")) );
        graph.addEdge("ctf96", "ctf93",(int)nodeDistTable.get("ctf93").distanceTo(nodeDistTable.get("ctf96")) );
        graph.addEdge("ctf83", "ctf96", (int)nodeDistTable.get("ctf96").distanceTo(nodeDistTable.get("ctf83")));
        graph.addEdge("ctf82", "ctf83",(int)nodeDistTable.get("ctf83").distanceTo(nodeDistTable.get("ctf82")) );
        graph.addEdge("ctf81", "ctf82",(int)nodeDistTable.get("ctf82").distanceTo(nodeDistTable.get("ctf81")) );
        graph.addEdge("ctf80", "ctf81", (int)nodeDistTable.get("ctf81").distanceTo(nodeDistTable.get("ctf80")));
        graph.addEdge("ctf79", "ctf80",(int)nodeDistTable.get("ctf80").distanceTo(nodeDistTable.get("ctf79")) );
        graph.addEdge("ctf84", "ctf79",(int)nodeDistTable.get("ctf79").distanceTo(nodeDistTable.get("ctf84")) );
        graph.addEdge("ctf85", "ctf79",(int)nodeDistTable.get("ctf79").distanceTo(nodeDistTable.get("ctf85")) );
        graph.addEdge("ctf22", "ctf24",(int)nodeDistTable.get("ctf24").distanceTo(nodeDistTable.get("ctf22")) );
        graph.addEdge("ctf23", "ctf22", (int)nodeDistTable.get("ctf22").distanceTo(nodeDistTable.get("ctf23")));
        graph.addEdge("ctf11", "ctf23",(int)nodeDistTable.get("ctf23").distanceTo(nodeDistTable.get("ctf11")) );
        graph.addEdge("ctf10", "ctf23",(int)nodeDistTable.get("ctf23").distanceTo(nodeDistTable.get("ctf10")) );
        graph.addEdge("ctf11", "ctf21", (int)nodeDistTable.get("ctf21").distanceTo(nodeDistTable.get("ctf11")));
        graph.addEdge("ctf21", "ctf23",(int)nodeDistTable.get("ctf23").distanceTo(nodeDistTable.get("ctf21")) );
        graph.addEdge("ctf10", "ctf11", (int)nodeDistTable.get("ctf11").distanceTo(nodeDistTable.get("ctf10")));
        graph.addEdge("ctf13", "ctf11",(int)nodeDistTable.get("ctf11").distanceTo(nodeDistTable.get("ctf13")) );
        graph.addEdge("ctf14", "ctf13",(int)nodeDistTable.get("ctf13").distanceTo(nodeDistTable.get("ctf14")) );
        graph.addEdge("ctf15", "ctf14", (int)nodeDistTable.get("ctf14").distanceTo(nodeDistTable.get("ctf15")));
        graph.addEdge("ctf16", "ctf15",(int)nodeDistTable.get("ctf15").distanceTo(nodeDistTable.get("ctf16")) );
        graph.addEdge("ctf17", "ctf16",(int)nodeDistTable.get("ctf16").distanceTo(nodeDistTable.get("ctf17")) );
        graph.addEdge("ctf18", "ctf17",(int)nodeDistTable.get("ctf17").distanceTo(nodeDistTable.get("ctf18")) );
        graph.addEdge("ctf20", "ctf17",(int)nodeDistTable.get("ctf17").distanceTo(nodeDistTable.get("ctf20")) );
        graph.addEdge("ctf19", "ctf20",(int)nodeDistTable.get("ctf20").distanceTo(nodeDistTable.get("ctf19")) );
        graph.addEdge("ctf14", "ctf19",(int)nodeDistTable.get("ctf19").distanceTo(nodeDistTable.get("ctf14")) );
        graph.addEdge("ctf20", "ctf16",(int)nodeDistTable.get("ctf16").distanceTo(nodeDistTable.get("ctf20")) );
        graph.addEdge("ctf19", "ctf16",(int)nodeDistTable.get("ctf16").distanceTo(nodeDistTable.get("ctf19")) );
        graph.addEdge("ctf16", "ctf14",(int)nodeDistTable.get("ctf14").distanceTo(nodeDistTable.get("ctf16")) );
        graph.addEdge("ctf17", "ctf14",(int)nodeDistTable.get("ctf14").distanceTo(nodeDistTable.get("ctf17")) );
        graph.addEdge("ctf20", "ctf15",(int)nodeDistTable.get("ctf15").distanceTo(nodeDistTable.get("ctf20")) );
        graph.addEdge("ctf17", "ctf19",(int)nodeDistTable.get("ctf19").distanceTo(nodeDistTable.get("ctf17")) );
        graph.addEdge("ctf12", "ctf13",(int)nodeDistTable.get("ctf13").distanceTo(nodeDistTable.get("ctf12")) );
        graph.addEdge("ctf3", "ctf12",(int)nodeDistTable.get("ctf12").distanceTo(nodeDistTable.get("ctf3")) );
        graph.addEdge("ctf86", "ctf85",(int)nodeDistTable.get("ctf85").distanceTo(nodeDistTable.get("ctf86")) );
        graph.addEdge("ctf87", "ctf86",(int)nodeDistTable.get("ctf86").distanceTo(nodeDistTable.get("ctf87")) );
        graph.addEdge("ctf88", "ctf87",(int)nodeDistTable.get("ctf87").distanceTo(nodeDistTable.get("ctf88")) );
        graph.addEdge("ctf89", "ctf88",(int)nodeDistTable.get("ctf88").distanceTo(nodeDistTable.get("ctf89")) );
        graph.addEdge("ctf77", "ctf84",(int)nodeDistTable.get("ctf84").distanceTo(nodeDistTable.get("ctf77")) );
        graph.addEdge("ctf78", "ctf77",(int)nodeDistTable.get("ctf77").distanceTo(nodeDistTable.get("ctf78")) );
        graph.addEdge("ctf76", "ctf77", (int)nodeDistTable.get("ctf77").distanceTo(nodeDistTable.get("ctf76")));
        graph.addEdge("ctf73", "ctf76", (int)nodeDistTable.get("ctf76").distanceTo(nodeDistTable.get("ctf73")));
        graph.addEdge("ctf74", "ctf73",(int)nodeDistTable.get("ctf73").distanceTo(nodeDistTable.get("ctf74")) );
        graph.addEdge("ctf75", "ctf74",(int)nodeDistTable.get("ctf74").distanceTo(nodeDistTable.get("ctf75")) );
        graph.addEdge("ctf72", "ctf73",(int)nodeDistTable.get("ctf73").distanceTo(nodeDistTable.get("ctf72")) );
        graph.addEdge("ctf71", "ctf72",(int)nodeDistTable.get("ctf72").distanceTo(nodeDistTable.get("ctf71")) );
        graph.addEdge("ctf70", "ctf71", (int)nodeDistTable.get("ctf71").distanceTo(nodeDistTable.get("ctf70")));
        graph.addEdge("ctf67", "ctf71",(int)nodeDistTable.get("ctf71").distanceTo(nodeDistTable.get("ctf67")) );
        graph.addEdge("ctf66", "ctf67",(int)nodeDistTable.get("ctf67").distanceTo(nodeDistTable.get("ctf66")) );
        graph.addEdge("ctf69", "ctf67",(int)nodeDistTable.get("ctf67").distanceTo(nodeDistTable.get("ctf69")) );
        graph.addEdge("ctf65", "ctf66", (int)nodeDistTable.get("ctf66").distanceTo(nodeDistTable.get("ctf65")));
        graph.addEdge("ctf64", "ctf65", (int)nodeDistTable.get("ctf65").distanceTo(nodeDistTable.get("ctf64")));
        graph.addEdge("ctf63", "ctf64",(int)nodeDistTable.get("ctf64").distanceTo(nodeDistTable.get("ctf63")) );
        graph.addEdge("ctf62", "ctf63",(int)nodeDistTable.get("ctf63").distanceTo(nodeDistTable.get("ctf62")) );
        graph.addEdge("ctf61", "ctf62", (int)nodeDistTable.get("ctf62").distanceTo(nodeDistTable.get("ctf61")));
        graph.addEdge("ctf60", "ctf61",(int)nodeDistTable.get("ctf61").distanceTo(nodeDistTable.get("ctf60")) );
        graph.addEdge("ctf59", "ctf60",(int)nodeDistTable.get("ctf60").distanceTo(nodeDistTable.get("ctf59")) );
        graph.addEdge("ctf96", "ctf59",(int)nodeDistTable.get("ctf59").distanceTo(nodeDistTable.get("ctf96")) );
        graph.addEdge("ctf90", "ctf59",(int)nodeDistTable.get("ctf59").distanceTo(nodeDistTable.get("ctf90")) );
        graph.addEdge("ctf91", "ctf90",(int)nodeDistTable.get("ctf90").distanceTo(nodeDistTable.get("ctf91")) );
        graph.addEdge("ctf92", "ctf91",(int)nodeDistTable.get("ctf91").distanceTo(nodeDistTable.get("ctf92")) );
        graph.addEdge("ctf55", "ctf90",(int)nodeDistTable.get("ctf90").distanceTo(nodeDistTable.get("ctf55")) );
        graph.addEdge("ctf54", "ctf55",(int)nodeDistTable.get("ctf55").distanceTo(nodeDistTable.get("ctf54")) );
        graph.addEdge("ctf58", "ctf55",(int)nodeDistTable.get("ctf55").distanceTo(nodeDistTable.get("ctf58")) );
        graph.addEdge("ctf56", "ctf55",(int)nodeDistTable.get("ctf55").distanceTo(nodeDistTable.get("ctf56")) );
        graph.addEdge("ctf54", "ctf56",(int)nodeDistTable.get("ctf56").distanceTo(nodeDistTable.get("ctf54")) );
        graph.addEdge("ctf34", "ctf33",(int)nodeDistTable.get("ctf33").distanceTo(nodeDistTable.get("ctf34")) );
        graph.addEdge("ctf24", "ctf33",(int)nodeDistTable.get("ctf33").distanceTo(nodeDistTable.get("ctf24")) );

        DijkstraAlgorithm dijkstra = GoodDijkstraAlgorithm.getInstance();
        SingleSourceShortestPathResult<String, Integer, DirectedWeightedEdge<String, Integer>> result = dijkstra.calc(graph, srcNumber, NS);

        Iterable<DirectedWeightedEdge<String, Integer>> pathToD = result.getPath(destNumber);
        Iterator<DirectedWeightedEdge<String, Integer>> it = pathToD.iterator();

        int count = 0;
        while(it.hasNext()){
            DirectedWeightedEdge<String, Integer> item = it.next();
            if(count == 0){
                pathVertices.add(item.from());
                pathVertices.add(item.to());
            }if(count > 0){
                pathVertices.add(item.to());
            }
            count++;
        }

        String path = "";
        for(String node : pathVertices){
            path = path + node + "*";
        }

        GlobalState.drawPath = path;

        //Getting junction nodes for taking bearings
        resultantJunctionNodes.clear();
        for(int i=0; i < pathVertices.size(); i++){
            for(String node : junctionNodes){
                if(pathVertices.get(i).equals(node)){
                    resultantJunctionNodes.add(node);
                }
            }
        }

        if(!juctionNodeLocation.isEmpty())
            juctionNodeLocation.clear();

        for(int i = 0; i < resultantJunctionNodes.size(); i++){
            juctionNodeLocation.add(nodeDistTable.get(resultantJunctionNodes.get(i)));
        }
        for (int i=0; i < resultantJunctionNodes.size(); i++){
            if(nearestToSource.equals(resultantJunctionNodes.get(i))){
                resultantJunctionNodes.remove(i);
            }
        }

        float preSrcBearing =  getBearings(srcBearingLoc, nodeDistTable.get(nearestToSource));
        bearingRJNHash.put(preSrcBearing, "NA");
        Location dst = null;
        Location src = null;
        if(resultantJunctionNodes.size() == 0){
            src = nodeDistTable.get(nearestToSource);
            dst = nodeDistTable.get(nearestToDest);
        }else {
            src = nodeDistTable.get(nearestToSource);
            dst = nodeDistTable.get(resultantJunctionNodes.get(0));
        }

        float srcBearing =  getBearings(src, dst);

        if(resultantJunctionNodes.size() ==0)
            bearingRJNHash.put(srcBearing, nearestToDest);
        else
            bearingRJNHash.put(srcBearing, resultantJunctionNodes.get(0));

        bearingsList.add(preSrcBearing);
        bearingsList.add(srcBearing);

        for (int i=0; i < resultantJunctionNodes.size()-1; i++){

            Location firstLoc = nodeDistTable.get(resultantJunctionNodes.get(i));
            Location secondLoc = nodeDistTable.get(resultantJunctionNodes.get(i + 1));
            float resultantBearing =  getBearings(firstLoc, secondLoc);

            bearingRJNHash.put(resultantBearing,resultantJunctionNodes.get(i + 1));

            bearingsList.add(resultantBearing);
        }

        if(resultantJunctionNodes.size() > 2){
            for (int i=0; i < resultantJunctionNodes.size(); i++){
                if(nearestToDest.equals(resultantJunctionNodes.get(i))){
                    resultantJunctionNodes.remove(i);
                }
            }
        }

        float preDestBearing = 0;

        if(resultantJunctionNodes.size() == 0)
            preDestBearing =  getBearings(nodeDistTable.get(nearestToDest),nodeDistTable.get(nearestToDest) );
        else
            preDestBearing =  getBearings(nodeDistTable.get(resultantJunctionNodes.get(resultantJunctionNodes.size()-1)),nodeDistTable.get(nearestToDest) );

        bearingRJNHash.put(preDestBearing, nearestToDest);
        float destBearing =  getBearings(nodeDistTable.get(nearestToDest), destBearingLoc );
        bearingRJNHash.put(destBearing, destNumber);
        bearingsList.add(preDestBearing);
        bearingsList.add(destBearing);

        TextNav textNav = new TextNav(srcBearingLoc, destBearingLoc, pathVertices, nodeDistTable, sortedStoreList, floorName);
        if(!tnForTF.isEmpty())
            tnForTF.clear();

        GlobalState.tnPathTF = "";
        GlobalState.tnSrcStoreTF = "";
        GlobalState.tnDestStoreTF = "";

        tnForTF = textNav.getTextNav();
        for(int i =0; i < tnForTF.size(); i++){
            GlobalState.tnPathTF = GlobalState.tnPathTF + tnForTF.get(i) + "#";
        }
        GlobalState.tnSrcStoreTF = srcStoreName;
        GlobalState.tnDestStoreTF = destStoreName;

        return pathVertices;
    }
    private float getBearings(Location sLoc, Location dLoc){
        float bearing = 0;

        Location firstLoc = sLoc;
        Location secondLoc = dLoc;
        bearing = firstLoc.bearingTo(secondLoc);
        return bearing;
    }
}
