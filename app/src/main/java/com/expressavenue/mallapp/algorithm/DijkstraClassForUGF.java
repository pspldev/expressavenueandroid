package com.expressavenue.mallapp.algorithm;

import android.location.Location;

import com.expressavenue.mallapp.global.GlobalState;
import com.expressavenue.mallapp.pojos.Store;
import com.expressavenue.mallapp.utils.TextNav;

import org.psjava.algo.graph.shortestpath.DijkstraAlgorithm;
import org.psjava.algo.graph.shortestpath.SingleSourceShortestPathResult;
import org.psjava.ds.graph.DirectedWeightedEdge;
import org.psjava.ds.graph.MutableDirectedWeightedGraph;
import org.psjava.ds.numbersystrem.IntegerNumberSystem;
import org.psjava.goods.GoodDijkstraAlgorithm;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;

public class DijkstraClassForUGF {

    private String srcNumber ="";
    private String srcLat ="";
    private String srcLong ="";

    private String destNumber ="";
    private String destLat ="";
    private String destLong ="";

    private String nearestToSource;
    private String nearestToDest;
    private double sWeight;
    private double dWeight;
    private String nearestToS[];
    private String nearestToD[];

    private Location srcBearingLoc;
    private Location destBearingLoc;

    private ArrayList<String> tnForUGF;

    private String junctionNodes[]= {"cugf2","cugf6","cugf35","cugf36","cugf162","cugf60","cugf163","cugf62","cugf63",
            "cugf84","cugf68","cugf83","cugf71","cugf78","cugf91","cugf94","cugf96","cugf99","cugf103","cugf105","cugf107",
            "cugf110","cugf77","cugf137","cugf136","cugf140","cugf133","cugf131","cugf129","cugf117","cugf123","cugf126",
            "cugf127","cugf145","cugf147","cugf148","cugf149","cugf40","cugf41","cugf42","cugf44","cugf46","cugf47","cugf49",
            "cugf51","cugf52","cugf54"};

    private ArrayList<String> resultantJunctionNodes;
    private ArrayList<Float> bearingsList;
    private Hashtable<String, Float> resultantBearingHash;

    private Hashtable<Float, String> bearingRJNHash;

    private  Location lcff1;

    private Hashtable<Location, String> nodeHash;
    private Hashtable<String, Location> nodeDistTable;

    private ArrayList<Store> sortedStoreList;

    private String srcStoreName = "";
    private String destStoreName = "";

    private String floorName = "";

    private ArrayList<Location> juctionNodeLocation;

    public DijkstraClassForUGF(String srcNumber, String srcLat, String srcLong,
                              String destNumber, String destLat, String destLong, ArrayList<Store> sortedStoreList, String floorName){

        juctionNodeLocation = new ArrayList<Location>();

        for(Store store : sortedStoreList){

            if(store.getStoreNumber().equals(srcNumber)){
                srcStoreName = store.getStoreName();
            }
            if(store.getStoreNumber().equals(destNumber)){
                destStoreName = store.getStoreName();
            }
        }

        if(srcNumber.equals("LT120"))
            srcStoreName = "Main Lift";
        if(srcNumber.equals("LT120LGF") || srcNumber.equals("LT120UGF") || srcNumber.equals("LT120FF") || srcNumber.equals("LT120SF")
                || srcNumber.equals("LT120TF") || srcNumber.equals("LT120B1")|| srcNumber.equals("LT120B2")|| srcNumber.equals("LT120B3") )
            srcStoreName = "Main Lift";
        else if(srcNumber.equals("ESC"))
            srcStoreName = "Escalator";
        else if(srcNumber.equals("ESC"))
            srcStoreName = "Escalator";
        else if(srcNumber.equals("ESC-Left"))
            srcStoreName = "Escalator";
        else if(srcNumber.equals("ESC-Right"))
            srcStoreName = "Escalator";

        if(destNumber.equals("LT120"))
            destStoreName = "Main Lift";
        if(destNumber.equals("LT120LGF") || destNumber.equals("LT120UGF") || destNumber.equals("LT120FF") || destNumber.equals("LT120SF")
                || destNumber.equals("LT120TF") || destNumber.equals("LT120B1")|| destNumber.equals("LT120B2")|| destNumber.equals("LT120B3") )
            destStoreName = "Main Lift";
        else if(destNumber.equals("ESC"))
            destStoreName = "Escalator";
        else if(destNumber.equals("ESC"))
            destStoreName = "Escalator";
        else if(destNumber.equals("ESC-Left"))
            destStoreName = "Escalator";
        else if(destNumber.equals("ESC-Right"))
            destStoreName = "Escalator";

        nodeHash = new Hashtable<Location, String>();
        nodeDistTable = new Hashtable<String, Location>();

        resultantJunctionNodes = new ArrayList<String >();
        resultantBearingHash = new Hashtable<String, Float>();
        bearingsList = new ArrayList<Float>();

        bearingRJNHash = new Hashtable<Float, String>();

        if(!bearingRJNHash.isEmpty())
            bearingRJNHash.clear();


        this.srcNumber = srcNumber;
        this.srcLat = srcLat;
        this.srcLong = srcLong;

        this.destNumber = destNumber;
        this.destLat = destLat;
        this.destLong = destLong;

        this.sortedStoreList = sortedStoreList;

        this.floorName = floorName;

        tnForUGF = new ArrayList<String>();

        setNodeHash( 13.058868,80.262972,"cugf1");
        setNodeHash(13.058868,80.263009, "cugf2");
        setNodeHash( 13.058867,80.263113,"cugf3");
        setNodeHash(13.058868,80.263201, "cugf4");
        setNodeHash( 13.058868,80.263282,"cugf5");
        setNodeHash( 13.058868,80.263363,"cugf6");
        setNodeHash( 13.058813,80.263363,"cugf7");
        setNodeHash( 13.058813,80.263289,"cugf8");
        setNodeHash(13.058788,80.263289, "cugf9");
        setNodeHash( 13.058754,80.263289,"cugf10");
        setNodeHash( 13.058733,80.263289,"cugf11");
        setNodeHash(13.058733,80.263363, "cugf12");
        setNodeHash( 13.058733,80.263459,"cugf13");
        setNodeHash(13.05875,80.263459, "cugf14");
        setNodeHash( 13.058775,80.263459,"cugf15");
        setNodeHash(13.058813,80.263459, "cugf16");
        setNodeHash(13.058868,80.263444, "cugf17");
        setNodeHash( 13.058868,80.263518,"cugf18");
        setNodeHash( 13.058867,80.263607,"cugf19");
        setNodeHash(13.058867,80.263687, "cugf20");
        setNodeHash(13.058868,80.263762, "cugf21");
        setNodeHash(13.058867,80.263835, "cugf22");
        setNodeHash(13.058867,80.263917, "cugf23");
        setNodeHash(13.058868,80.26399, "cugf24");
        setNodeHash( 13.058868,80.264064,"cugf25");
        setNodeHash(13.058867,80.264138, "cugf26");
        setNodeHash( 13.058868,80.264212,"cugf27");
        setNodeHash( 13.058867,80.264293,"cugf28");
        setNodeHash(13.058868,80.264381, "cugf29");
        setNodeHash(13.058868,80.264455, "cugf30");
        setNodeHash(13.058868,80.264544, "cugf31");
        setNodeHash( 13.058868,80.264618,"cugf32");
        setNodeHash(13.058867,80.264691, "cugf33");
        setNodeHash( 13.058868,80.264765,"cugf34");
        setNodeHash( 13.058867,80.264854,"cugf35");
        setNodeHash(13.058867,80.264964, "cugf36");
        setNodeHash( 13.058868,80.265031,"cugf37");
        setNodeHash(13.058868,80.265031, "cugf38");
        setNodeHash(13.0588,80.263016, "cugf39");
        setNodeHash(13.058775,80.263016, "cugf40");
        setNodeHash( 13.058775,80.263061,"cugf41");
        setNodeHash( 13.058637,80.263061,"cugf42");
        setNodeHash(13.058637,80.263038, "cugf43");
        setNodeHash( 13.058406,80.263039,"cugf44");
        setNodeHash(13.058397,80.263067, "cugf45");
        setNodeHash( 13.058397,80.263171,"cugf46");
        setNodeHash( 13.058433,80.263171,"cugf47");
        setNodeHash(  13.058433,80.263238,"cugf48");
        setNodeHash(13.058433,80.263311, "cugf49");
        setNodeHash( 13.058397,80.263237,"cugf50");
        setNodeHash( 13.058397,80.263312,"cugf51");
        setNodeHash(13.058406,80.2645, "cugf52");
        setNodeHash(13.058431,80.264691,"cugf53");
        setNodeHash( 13.058448,80.264735,"cugf54");
        setNodeHash( 13.058482,80.26478,"cugf55");
        setNodeHash( 13.058515,80.264824,"cugf56");
        setNodeHash(13.058549,80.264854, "cugf57");
        setNodeHash( 13.058582,80.264869,"cugf58");
        setNodeHash( 13.058633, 80.264876,"cugf60");
        setNodeHash( 13.058632, 80.264758,"cugf61");
        setNodeHash(13.058633, 80.264632, "cugf62");
        setNodeHash(13.058675, 80.264632,"cugf63");
        setNodeHash( 13.058675, 80.264566, "cugf64");
        setNodeHash( 13.058675, 80.264522,"cugf65");
        setNodeHash(13.058674, 80.264492, "cugf66");
        setNodeHash(13.058675, 80.264462,"cugf67");
        setNodeHash(13.058674, 80.264441, "cugf68");
        setNodeHash( 13.058733, 80.26444,"cugf69");
        setNodeHash(13.05875, 80.264441, "cugf70");
        setNodeHash(13.058761, 80.26444,"cugf71");
        setNodeHash(13.05876, 80.264411, "cugf72");
        setNodeHash(13.058761, 80.264374, "cugf73");
        setNodeHash(13.058761, 80.264315,"cugf74");
        setNodeHash( 13.058761, 80.264256,"cugf75");
        setNodeHash( 13.058761, 80.264223,"cugf76");
        setNodeHash( 13.05876, 80.264194,"cugf77");
        setNodeHash(13.058677, 80.26419, "cugf78");
        setNodeHash(13.058677, 80.264223, "cugf79");
        setNodeHash(13.058677, 80.26426, "cugf80");
        setNodeHash(13.058677, 80.264319, "cugf81");
        setNodeHash(13.058677, 80.264371, "cugf82");
        setNodeHash(13.058677, 80.26441, "cugf83");
        setNodeHash(13.05863, 80.26441, "cugf84");
        setNodeHash( 13.05863, 80.264444,"cugf85");
        setNodeHash(13.05863, 80.264481,"cugf86");
        setNodeHash( 13.058632, 80.264588,"cugf87");
        setNodeHash(13.05863, 80.264371,"cugf88");
        setNodeHash( 13.05863, 80.264297,"cugf89");
        setNodeHash(13.058631, 80.264223,"cugf90");
        setNodeHash( 13.05863, 80.264194,"cugf91");
        setNodeHash(13.058567, 80.264193, "cugf92");
        setNodeHash( 13.05863, 80.264179,"cugf93");
        setNodeHash(13.058626, 80.264156,"cugf94");
        setNodeHash( 13.058601, 80.264098,"cugf95");
        setNodeHash( 13.058593, 80.264038,"cugf96");
        setNodeHash(13.058559, 80.264039, "cugf97");
        setNodeHash(13.058534, 80.264039, "cugf98");
        setNodeHash( 13.058593, 80.263957,"cugf99");
        setNodeHash( 13.058559, 80.263957,"cugf100");
        setNodeHash( 13.058534, 80.263957,"cugf101");
        setNodeHash(13.058601, 80.263898, "cugf102");
        setNodeHash(13.058626, 80.263839, "cugf103");
        setNodeHash(13.058631, 80.263817, "cugf104");
        setNodeHash( 13.058677, 80.263817,"cugf105");
        setNodeHash(13.058691, 80.263847, "cugf106");
        setNodeHash(13.058704, 80.263891, "cugf107");
        setNodeHash(  13.058712, 80.263921,"cugf108");
        setNodeHash( 13.058718, 80.263957, "cugf109");
        setNodeHash( 13.058719, 80.264002,"cugf110");
        setNodeHash(13.05874, 80.264002, "cugf111");
        setNodeHash( 13.058677, 80.264178,"cugf112");
        setNodeHash(13.058692, 80.264142, "cugf113");
        setNodeHash(13.058703, 80.264105, "cugf114");
        setNodeHash(13.05871, 80.264075, "cugf115");
        setNodeHash(13.058716, 80.264038, "cugf116");
        setNodeHash( 13.05863, 80.263795,"cugf117");
        setNodeHash( 13.058572, 80.263795,"cugf118");
        setNodeHash( 13.05863, 80.263765, "cugf119");
        setNodeHash( 13.05863, 80.263662,"cugf120");
        setNodeHash( 13.058631, 80.263581,"cugf121");
        setNodeHash(  13.05863, 80.263551,"cugf122");
        setNodeHash( 13.05863, 80.263527,"cugf123");
        setNodeHash(13.058572, 80.263527, "cugf124");
        setNodeHash( 13.05863, 80.263497, "cugf125");
        setNodeHash(13.05863, 80.263463, "cugf126");
        setNodeHash(  13.058677, 80.263463,"cugf127");
        setNodeHash( 13.058677, 80.263512,"cugf128");
        setNodeHash( 13.058676, 80.263553,"cugf129");
        setNodeHash( 13.058731, 80.263553,"cugf130");
        setNodeHash( 13.058758, 80.263553,"cugf131");
        setNodeHash( 13.058758, 80.263603,"cugf132");
        setNodeHash( 13.058758, 80.263677,"cugf133");
        setNodeHash( 13.058758, 80.263728,"cugf134");
        setNodeHash( 13.058758, 80.263758,"cugf135");
        setNodeHash( 13.058758, 80.263795,"cugf136");
        setNodeHash( 13.058677, 80.263795,"cugf137");
        setNodeHash( 13.058676, 80.26377,"cugf138");
        setNodeHash( 13.058677, 80.263733,"cugf139");
        setNodeHash( 13.058676, 80.263696,"cugf140");
        setNodeHash( 13.058676, 80.263659,"cugf141");
        setNodeHash(  13.058676, 80.263622,"cugf142");
        setNodeHash( 13.058676, 80.263581,"cugf143");
        setNodeHash(13.058691, 80.263423, "cugf144");
        setNodeHash(13.058691, 80.263335, "cugf145");
        setNodeHash(13.058676, 80.263305, "cugf146");
        setNodeHash( 13.058643, 80.263304,"cugf147");
        setNodeHash(13.058614, 80.263328, "cugf148");
        setNodeHash(  13.058612, 80.263396,"cugf149");
        setNodeHash(13.058622, 80.263438, "cugf150");
        setNodeHash(  13.058588, 80.263397,"cugf151");
        setNodeHash( 13.058563, 80.263397,"cugf152");
        setNodeHash(13.058529, 80.263397, "cugf153");
        setNodeHash(13.058513, 80.263397, "cugf154");
        setNodeHash(  13.058492, 80.263397,"cugf155");
        setNodeHash( 13.058591, 80.263328,"cugf156");
        setNodeHash(13.058574, 80.263327, "cugf157");
        setNodeHash(13.058551, 80.263327, "cugf158");
        setNodeHash( 13.058525, 80.263327,"cugf159");
        setNodeHash( 13.058691, 80.264913,"cugf160");
        setNodeHash( 13.058758, 80.264912,"cugf161");
        setNodeHash(13.058758, 80.264861, "cugf162");
        setNodeHash( 13.058775, 80.264942,"cugf163");
        setDistHash(13.058868, 80.262972, "cugf1");
        setDistHash(13.058868, 80.263009, "cugf2");
        setDistHash(13.058867, 80.263113, "cugf3");
        setDistHash(13.058868, 80.263201, "cugf4");
        setDistHash(13.058868, 80.263282, "cugf5");
        setDistHash(13.058868, 80.263363, "cugf6");
        setDistHash(13.058813, 80.263363, "cugf7");
        setDistHash(13.058813, 80.263289, "cugf8");
        setDistHash(13.058788, 80.263289, "cugf9");
        setDistHash(13.058754, 80.263289, "cugf10");
        setDistHash(13.058733, 80.263289, "cugf11");
        setDistHash(13.058733, 80.263363, "cugf12");
        setDistHash(13.058733, 80.263459, "cugf13");
        setDistHash(13.05875, 80.263459, "cugf14");
        setDistHash(13.058775, 80.263459, "cugf15");
        setDistHash(13.058813, 80.263459, "cugf16");
        setDistHash(13.058868, 80.263444, "cugf17");
        setDistHash(13.058868, 80.263518, "cugf18");
        setDistHash(13.058867, 80.263607, "cugf19");
        setDistHash(13.058867, 80.263687, "cugf20");
        setDistHash(13.058868, 80.263762, "cugf21");
        setDistHash(13.058867, 80.263835, "cugf22");
        setDistHash(13.058867, 80.263917, "cugf23");
        setDistHash(13.058868, 80.26399, "cugf24");
        setDistHash(13.058868, 80.264064, "cugf25");
        setDistHash(13.058867, 80.264138, "cugf26");
        setDistHash(13.058868, 80.264212, "cugf27");
        setDistHash(13.058867, 80.264293, "cugf28");
        setDistHash(13.058868, 80.264381, "cugf29");
        setDistHash(13.058868, 80.264455, "cugf30");
        setDistHash(13.058868, 80.264544, "cugf31");
        setDistHash(13.058868, 80.264618, "cugf32");
        setDistHash(13.058867, 80.264691, "cugf33");
        setDistHash(13.058868, 80.264765, "cugf34");
        setDistHash(13.058867, 80.264854, "cugf35");
        setDistHash(13.058867, 80.264964, "cugf36");
        setDistHash(13.058868, 80.265031, "cugf37");
        setDistHash(13.058868, 80.265031, "cugf38");
        setDistHash(13.0588, 80.263016, "cugf39");
        setDistHash(13.058775, 80.263016, "cugf40");
        setDistHash(13.058775, 80.263061, "cugf41");
        setDistHash(13.058637, 80.263061, "cugf42");
        setDistHash(13.058637, 80.263038, "cugf43");
        setDistHash(13.058406, 80.263039, "cugf44");
        setDistHash(13.058397, 80.263067, "cugf45");
        setDistHash(13.058397, 80.263171, "cugf46");
        setDistHash(13.058433, 80.263171, "cugf47");
        setDistHash(13.058433, 80.263238, "cugf48");
        setDistHash(13.058433, 80.263311, "cugf49");
        setDistHash(13.058397, 80.263237, "cugf50");
        setDistHash(13.058397, 80.263312, "cugf51");
        setDistHash(13.058406, 80.2645, "cugf52");
        setDistHash(13.058431, 80.264691, "cugf53");
        setDistHash(13.058448, 80.264735, "cugf54");
        setDistHash(13.058482, 80.26478, "cugf55");
        setDistHash(13.058515, 80.264824, "cugf56");
        setDistHash(13.058549, 80.264854, "cugf57");
        setDistHash(13.058582, 80.264869, "cugf58");
        setDistHash(13.058633, 80.264876, "cugf60");
        setDistHash(13.058632, 80.264758, "cugf61");
        setDistHash(13.058633, 80.264632, "cugf62");
        setDistHash(13.058675, 80.264632, "cugf63");
        setDistHash(13.058675, 80.264566, "cugf64");
        setDistHash(13.058675, 80.264522, "cugf65");
        setDistHash(13.058674, 80.264492, "cugf66");
        setDistHash(13.058675, 80.264462, "cugf67");
        setDistHash(13.058674, 80.264441, "cugf68");
        setDistHash(13.058733, 80.26444, "cugf69");
        setDistHash(13.05875, 80.264441, "cugf70");
        setDistHash(13.058761, 80.26444, "cugf71");
        setDistHash(13.05876, 80.264411, "cugf72");
        setDistHash(13.058761, 80.264374, "cugf73");
        setDistHash(13.058761, 80.264315, "cugf74");
        setDistHash(13.058761, 80.264256, "cugf75");
        setDistHash(13.058761, 80.264223, "cugf76");
        setDistHash(13.05876, 80.264194, "cugf77");
        setDistHash(13.058677, 80.26419, "cugf78");
        setDistHash(13.058677, 80.264223, "cugf79");
        setDistHash(13.058677, 80.26426, "cugf80");
        setDistHash(13.058677, 80.264319, "cugf81");
        setDistHash(13.058677, 80.264371, "cugf82");
        setDistHash(13.058677, 80.26441, "cugf83");
        setDistHash(13.05863, 80.26441, "cugf84");
        setDistHash(13.05863, 80.264444, "cugf85");
        setDistHash(13.05863, 80.264481, "cugf86");
        setDistHash(13.058632, 80.264588, "cugf87");
        setDistHash(13.05863, 80.264371, "cugf88");
        setDistHash(13.05863, 80.264297, "cugf89");
        setDistHash(13.058631, 80.264223, "cugf90");
        setDistHash(13.05863, 80.264194, "cugf91");
        setDistHash(13.058567, 80.264193, "cugf92");
        setDistHash(13.05863, 80.264179, "cugf93");
        setDistHash(13.058626, 80.264156, "cugf94");
        setDistHash(13.058601, 80.264098, "cugf95");
        setDistHash(13.058593, 80.264038, "cugf96");
        setDistHash(13.058559, 80.264039, "cugf97");
        setDistHash(13.058534, 80.264039, "cugf98");
        setDistHash(13.058593, 80.263957, "cugf99");
        setDistHash(13.058559, 80.263957, "cugf100");
        setDistHash(13.058534, 80.263957, "cugf101");
        setDistHash(13.058601, 80.263898, "cugf102");
        setDistHash(13.058626, 80.263839, "cugf103");
        setDistHash(13.058631, 80.263817, "cugf104");
        setDistHash(13.058677, 80.263817, "cugf105");
        setDistHash(13.058691, 80.263847, "cugf106");
        setDistHash(13.058704, 80.263891, "cugf107");
        setDistHash(13.058712, 80.263921, "cugf108");
        setDistHash(13.058718, 80.263957, "cugf109");
        setDistHash(13.058719, 80.264002, "cugf110");
        setDistHash(13.05874, 80.264002, "cugf111");
        setDistHash(13.058677, 80.264178, "cugf112");
        setDistHash(13.058692, 80.264142, "cugf113");
        setDistHash(13.058703, 80.264105, "cugf114");
        setDistHash(13.05871, 80.264075, "cugf115");
        setDistHash(13.058716, 80.264038, "cugf116");
        setDistHash(13.05863, 80.263795, "cugf117");
        setDistHash(13.058572, 80.263795, "cugf118");
        setDistHash(13.05863, 80.263765, "cugf119");
        setDistHash(13.05863, 80.263662, "cugf120");
        setDistHash(13.058631, 80.263581, "cugf121");
        setDistHash(13.05863, 80.263551, "cugf122");
        setDistHash(13.05863, 80.263527, "cugf123");
        setDistHash(13.058572, 80.263527, "cugf124");
        setDistHash(13.05863, 80.263497, "cugf125");
        setDistHash(13.05863, 80.263463, "cugf126");
        setDistHash(13.058677, 80.263463, "cugf127");
        setDistHash(13.058677, 80.263512, "cugf128");
        setDistHash(13.058676, 80.263553, "cugf129");
        setDistHash(13.058731, 80.263553, "cugf130");
        setDistHash(13.058758, 80.263553, "cugf131");
        setDistHash(13.058758, 80.263603, "cugf132");
        setDistHash(13.058758, 80.263677, "cugf133");
        setDistHash(13.058758, 80.263728, "cugf134");
        setDistHash(13.058758, 80.263758, "cugf135");
        setDistHash(13.058758, 80.263795, "cugf136");
        setDistHash(13.058677, 80.263795, "cugf137");
        setDistHash(13.058676, 80.26377, "cugf138");
        setDistHash(13.058677, 80.263733, "cugf139");
        setDistHash(13.058676, 80.263696, "cugf140");
        setDistHash(13.058676, 80.263659, "cugf141");
        setDistHash(13.058676, 80.263622, "cugf142");
        setDistHash(13.058676, 80.263581, "cugf143");
        setDistHash(13.058691, 80.263423, "cugf144");
        setDistHash(13.058691, 80.263335, "cugf145");
        setDistHash(13.058676, 80.263305, "cugf146");
        setDistHash(13.058643, 80.263304, "cugf147");
        setDistHash(13.058614, 80.263328, "cugf148");
        setDistHash(13.058612, 80.263396, "cugf149");
        setDistHash(13.058622, 80.263438, "cugf150");
        setDistHash(13.058588, 80.263397, "cugf151");
        setDistHash(13.058563, 80.263397, "cugf152");
        setDistHash(13.058529, 80.263397, "cugf153");
        setDistHash(13.058513, 80.263397, "cugf154");
        setDistHash(13.058492, 80.263397, "cugf155");
        setDistHash(13.058591, 80.263328, "cugf156");
        setDistHash(13.058574, 80.263327, "cugf157");
        setDistHash(13.058551, 80.263327, "cugf158");
        setDistHash(13.058525, 80.263327, "cugf159");
        setDistHash(13.058691, 80.264913, "cugf160");
        setDistHash(13.058758, 80.264912, "cugf161");
        setDistHash(13.058758, 80.264861, "cugf162");
        setDistHash(13.058775, 80.264942, "cugf163");

        setDistHash(Double.parseDouble(srcLat), Double.parseDouble(srcLong), this.srcNumber);
        setDistHash(Double.parseDouble(destLat), Double.parseDouble(destLong), this.destNumber);

        Location srcLocation = new Location("");
        srcLocation.setLatitude(Double.parseDouble(this.srcLat));
        srcLocation.setLongitude(Double.parseDouble(this.srcLong));
        srcBearingLoc = srcLocation;
        nearestToS = getNearesLocation(srcLocation, nodeHash);
        nearestToSource = nearestToS[0];
        sWeight = Double.parseDouble(nearestToS[1]);

        Location destLocation = new Location("");
        destLocation.setLatitude(Double.parseDouble(this.destLat));
        destLocation.setLongitude(Double.parseDouble(this.destLong));
        destBearingLoc = destLocation;
        nearestToD = getNearesLocation(destLocation, nodeHash);
        nearestToDest = nearestToD[0];
        dWeight = Double.parseDouble(nearestToD[1]);
    }

    private void setNodeHash(double lat, double lon, String storeNo)
    {
        lcff1 = new Location(storeNo);
        lcff1.setLatitude(lat);
        lcff1.setLongitude(lon);
        nodeHash.put(lcff1,storeNo);
    }
    private void setDistHash(double lat, double lon, String storeNo)
    {
        lcff1 = new Location(storeNo);
        lcff1.setLatitude(lat);
        lcff1.setLongitude(lon);
        nodeDistTable.put(storeNo, lcff1);
    }
    public String[] getNearesLocation(Location srcLoc, Hashtable<Location, String> srcHash){

        String nearestStore[] = new String[2];
        ArrayList<Double> distValues = new ArrayList<Double>();
        Hashtable<Double, Location> distHash = new Hashtable<Double, Location>();
        Set<Location> locSet = srcHash.keySet();
        for(Location location : locSet){

            double dist = location.distanceTo(srcLoc);
            distValues.add(dist);
            distHash.put(dist,location);
        }

        double smallest = Double.MAX_VALUE;
        for(int i =0;i<distValues.size();i++) {
            if(smallest > distValues.get(i)) {
                smallest = distValues.get(i);
            }
        }

        Location nearLoc = distHash.get(smallest);
        nearestStore[0] = srcHash.get(nearLoc);
        nearestStore[1] = String.valueOf(smallest);

        return nearestStore;
    }

    public ArrayList<String> algorithmImplementation() {

        ArrayList<String> pathVertices = new ArrayList<>();
        pathVertices.clear();

        IntegerNumberSystem NS = IntegerNumberSystem.getInstance();

        MutableDirectedWeightedGraph<String, Integer> graph = MutableDirectedWeightedGraph.create();

        graph.insertVertex("cugf1");
        graph.insertVertex("cugf2");
        graph.insertVertex("cugf3");
        graph.insertVertex("cugf4");
        graph.insertVertex("cugf5");
        graph.insertVertex("cugf6");
        graph.insertVertex("cugf7");
        graph.insertVertex("cugf8");
        graph.insertVertex("cugf9");
        graph.insertVertex("cugf10");
        graph.insertVertex("cugf11");
        graph.insertVertex("cugf12");
        graph.insertVertex("cugf13");
        graph.insertVertex("cugf14");
        graph.insertVertex("cugf15");
        graph.insertVertex("cugf16");
        graph.insertVertex("cugf17");
        graph.insertVertex("cugf18");
        graph.insertVertex("cugf19");
        graph.insertVertex("cugf20");
        graph.insertVertex("cugf21");
        graph.insertVertex("cugf22");
        graph.insertVertex("cugf23");
        graph.insertVertex("cugf24");
        graph.insertVertex("cugf25");
        graph.insertVertex("cugf26");
        graph.insertVertex("cugf27");
        graph.insertVertex("cugf28");
        graph.insertVertex("cugf29");
        graph.insertVertex("cugf30");
        graph.insertVertex("cugf31");
        graph.insertVertex("cugf32");
        graph.insertVertex("cugf33");
        graph.insertVertex("cugf34");
        graph.insertVertex("cugf35");
        graph.insertVertex("cugf36");
        graph.insertVertex("cugf37");
        graph.insertVertex("cugf38");
        graph.insertVertex("cugf39");
        graph.insertVertex("cugf40");
        graph.insertVertex("cugf41");
        graph.insertVertex("cugf42");
        graph.insertVertex("cugf43");
        graph.insertVertex("cugf44");
        graph.insertVertex("cugf45");
        graph.insertVertex("cugf46");
        graph.insertVertex("cugf47");
        graph.insertVertex("cugf48");
        graph.insertVertex("cugf49");
        graph.insertVertex("cugf50");
        graph.insertVertex("cugf51");
        graph.insertVertex("cugf52");
        graph.insertVertex("cugf53");
        graph.insertVertex("cugf54");
        graph.insertVertex("cugf55");
        graph.insertVertex("cugf56");
        graph.insertVertex("cugf57");
        graph.insertVertex("cugf58");
        graph.insertVertex("cugf59");
        graph.insertVertex("cugf60");
        graph.insertVertex("cugf61");
        graph.insertVertex("cugf62");
        graph.insertVertex("cugf63");
        graph.insertVertex("cugf64");
        graph.insertVertex("cugf65");
        graph.insertVertex("cugf66");
        graph.insertVertex("cugf67");
        graph.insertVertex("cugf68");
        graph.insertVertex("cugf69");
        graph.insertVertex("cugf70");
        graph.insertVertex("cugf71");
        graph.insertVertex("cugf72");
        graph.insertVertex("cugf73");
        graph.insertVertex("cugf74");
        graph.insertVertex("cugf75");
        graph.insertVertex("cugf76");
        graph.insertVertex("cugf77");
        graph.insertVertex("cugf78");
        graph.insertVertex("cugf79");
        graph.insertVertex("cugf80");
        graph.insertVertex("cugf81");
        graph.insertVertex("cugf82");
        graph.insertVertex("cugf83");
        graph.insertVertex("cugf84");
        graph.insertVertex("cugf85");
        graph.insertVertex("cugf86");
        graph.insertVertex("cugf87");
        graph.insertVertex("cugf88");
        graph.insertVertex("cugf89");
        graph.insertVertex("cugf90");
        graph.insertVertex("cugf91");
        graph.insertVertex("cugf92");
        graph.insertVertex("cugf93");
        graph.insertVertex("cugf94");
        graph.insertVertex("cugf95");
        graph.insertVertex("cugf96");
        graph.insertVertex("cugf97");
        graph.insertVertex("cugf98");
        graph.insertVertex("cugf99");
        graph.insertVertex("cugf100");
        graph.insertVertex("cugf101");
        graph.insertVertex("cugf102");
        graph.insertVertex("cugf103");
        graph.insertVertex("cugf104");
        graph.insertVertex("cugf105");
        graph.insertVertex("cugf106");
        graph.insertVertex("cugf107");
        graph.insertVertex("cugf108");
        graph.insertVertex("cugf109");
        graph.insertVertex("cugf110");
        graph.insertVertex("cugf111");
        graph.insertVertex("cugf112");
        graph.insertVertex("cugf113");
        graph.insertVertex("cugf114");
        graph.insertVertex("cugf115");
        graph.insertVertex("cugf116");
        graph.insertVertex("cugf117");
        graph.insertVertex("cugf118");
        graph.insertVertex("cugf119");
        graph.insertVertex("cugf120");
        graph.insertVertex("cugf121");
        graph.insertVertex("cugf122");
        graph.insertVertex("cugf123");
        graph.insertVertex("cugf124");
        graph.insertVertex("cugf125");
        graph.insertVertex("cugf126");
        graph.insertVertex("cugf127");
        graph.insertVertex("cugf128");
        graph.insertVertex("cugf129");
        graph.insertVertex("cugf130");
        graph.insertVertex("cugf131");
        graph.insertVertex("cugf132");
        graph.insertVertex("cugf133");
        graph.insertVertex("cugf134");
        graph.insertVertex("cugf135");
        graph.insertVertex("cugf136");
        graph.insertVertex("cugf137");
        graph.insertVertex("cugf138");
        graph.insertVertex("cugf139");
        graph.insertVertex("cugf140");
        graph.insertVertex("cugf141");
        graph.insertVertex("cugf142");
        graph.insertVertex("cugf143");
        graph.insertVertex("cugf144");
        graph.insertVertex("cugf145");
        graph.insertVertex("cugf146");
        graph.insertVertex("cugf147");
        graph.insertVertex("cugf148");
        graph.insertVertex("cugf149");
        graph.insertVertex("cugf150");
        graph.insertVertex("cugf151");
        graph.insertVertex("cugf152");
        graph.insertVertex("cugf153");
        graph.insertVertex("cugf154");
        graph.insertVertex("cugf155");
        graph.insertVertex("cugf156");
        graph.insertVertex("cugf157");
        graph.insertVertex("cugf158");
        graph.insertVertex("cugf159");
        graph.insertVertex("cugf160");
        graph.insertVertex("cugf161");
        graph.insertVertex("cugf162");
        graph.insertVertex("cugf163");


        graph.insertVertex(srcNumber);
        graph.insertVertex(destNumber);

        graph.addEdge(srcNumber, nearestToSource, (int) sWeight);
        graph.addEdge(nearestToDest, destNumber, (int) dWeight);

        graph.addEdge(nearestToSource, srcNumber, (int) sWeight);
        graph.addEdge(destNumber, nearestToDest, (int) dWeight);

        graph.addEdge("cugf1", "cugf2", (int) nodeDistTable.get("cugf1").distanceTo(nodeDistTable.get("cugf2")));
        graph.addEdge("cugf2", "cugf3", (int) nodeDistTable.get("cugf2").distanceTo(nodeDistTable.get("cugf3")));
        graph.addEdge("cugf3", "cugf4", (int) nodeDistTable.get("cugf3").distanceTo(nodeDistTable.get("cugf4")));
        graph.addEdge("cugf4", "cugf5", (int) nodeDistTable.get("cugf4").distanceTo(nodeDistTable.get("cugf5")));
        graph.addEdge("cugf5", "cugf6", (int) nodeDistTable.get("cugf5").distanceTo(nodeDistTable.get("cugf6")));
        graph.addEdge("cugf6", "cugf17", (int) nodeDistTable.get("cugf6").distanceTo(nodeDistTable.get("cugf17")));
        graph.addEdge("cugf17", "cugf18", (int) nodeDistTable.get("cugf17").distanceTo(nodeDistTable.get("cugf18")));
        graph.addEdge("cugf18", "cugf19", (int) nodeDistTable.get("cugf18").distanceTo(nodeDistTable.get("cugf19")));
        graph.addEdge("cugf19", "cugf20", (int) nodeDistTable.get("cugf19").distanceTo(nodeDistTable.get("cugf20")));
        graph.addEdge("cugf20", "cugf21", (int) nodeDistTable.get("cugf20").distanceTo(nodeDistTable.get("cugf21")));
        graph.addEdge("cugf21", "cugf22", (int) nodeDistTable.get("cugf21").distanceTo(nodeDistTable.get("cugf22")));
        graph.addEdge("cugf22", "cugf23", (int) nodeDistTable.get("cugf22").distanceTo(nodeDistTable.get("cugf23")));
        graph.addEdge("cugf23", "cugf24", (int) nodeDistTable.get("cugf23").distanceTo(nodeDistTable.get("cugf24")));
        graph.addEdge("cugf24", "cugf25", (int) nodeDistTable.get("cugf24").distanceTo(nodeDistTable.get("cugf25")));
        graph.addEdge("cugf25", "cugf26", (int) nodeDistTable.get("cugf25").distanceTo(nodeDistTable.get("cugf26")));
        graph.addEdge("cugf26", "cugf27", (int) nodeDistTable.get("cugf26").distanceTo(nodeDistTable.get("cugf27")));
        graph.addEdge("cugf27", "cugf28", (int)nodeDistTable.get("cugf27").distanceTo(nodeDistTable.get("cugf28")));
        graph.addEdge("cugf28", "cugf29", (int)nodeDistTable.get("cugf28").distanceTo(nodeDistTable.get("cugf29")));
        graph.addEdge("cugf30", "cugf31", (int)nodeDistTable.get("cugf30").distanceTo(nodeDistTable.get("cugf31")));
        graph.addEdge("cugf31", "cugf32", (int)nodeDistTable.get("cugf31").distanceTo(nodeDistTable.get("cugf32")));
        graph.addEdge("cugf32", "cugf33", (int) nodeDistTable.get("cugf32").distanceTo(nodeDistTable.get("cugf33")));
        graph.addEdge("cugf33", "cugf34", (int) nodeDistTable.get("cugf33").distanceTo(nodeDistTable.get("cugf34")));
        graph.addEdge("cugf34", "cugf35", (int) nodeDistTable.get("cugf34").distanceTo(nodeDistTable.get("cugf35")));
        graph.addEdge("cugf35", "cugf36", (int) nodeDistTable.get("cugf35").distanceTo(nodeDistTable.get("cugf36")));
        graph.addEdge("cugf36", "cugf37", (int) nodeDistTable.get("cugf36").distanceTo(nodeDistTable.get("cugf37")));
        graph.addEdge("cugf36", "cugf163", (int)nodeDistTable.get("cugf36").distanceTo(nodeDistTable.get("cugf163")));
        graph.addEdge("cugf163", "cugf161", (int)nodeDistTable.get("cugf163").distanceTo(nodeDistTable.get("cugf161")));
        graph.addEdge("cugf161", "cugf162", (int)nodeDistTable.get("cugf161").distanceTo(nodeDistTable.get("cugf162")));
        graph.addEdge("cugf163", "cugf160", (int)nodeDistTable.get("cugf163").distanceTo(nodeDistTable.get("cugf160")));
        graph.addEdge("cugf161", "cugf160", (int)nodeDistTable.get("cugf161").distanceTo(nodeDistTable.get("cugf160")));
        graph.addEdge("cugf162", "cugf160", (int)nodeDistTable.get("cugf162").distanceTo(nodeDistTable.get("cugf160")));
        graph.addEdge("cugf162", "cugf60", (int)nodeDistTable.get("cugf162").distanceTo(nodeDistTable.get("cugf60")));
        graph.addEdge("cugf160", "cugf60", (int)nodeDistTable.get("cugf160").distanceTo(nodeDistTable.get("cugf60")));
        graph.addEdge("cugf60", "cugf58", (int)nodeDistTable.get("cugf60").distanceTo(nodeDistTable.get("cugf58")));
        graph.addEdge("cugf60", "cugf61", (int)nodeDistTable.get("cugf60").distanceTo(nodeDistTable.get("cugf61")));
        graph.addEdge("cugf58", "cugf57", (int)nodeDistTable.get("cugf58").distanceTo(nodeDistTable.get("cugf57")));
        graph.addEdge("cugf57", "cugf56", (int)nodeDistTable.get("cugf57").distanceTo(nodeDistTable.get("cugf56")));
        graph.addEdge("cugf56", "cugf55", (int)nodeDistTable.get("cugf56").distanceTo(nodeDistTable.get("cugf55")));
        graph.addEdge("cugf55", "cugf54", (int)nodeDistTable.get("cugf55").distanceTo(nodeDistTable.get("cugf54")));
        graph.addEdge("cugf54", "cugf53", (int)nodeDistTable.get("cugf54").distanceTo(nodeDistTable.get("cugf53")));
        graph.addEdge("cugf53", "cugf52", (int) nodeDistTable.get("cugf53").distanceTo(nodeDistTable.get("cugf52")));
        graph.addEdge("cugf52", "cugf51", (int) nodeDistTable.get("cugf52").distanceTo(nodeDistTable.get("cugf51")));
        graph.addEdge("cugf51", "cugf50", (int) nodeDistTable.get("cugf51").distanceTo(nodeDistTable.get("cugf50")));
        graph.addEdge("cugf50", "cugf46", (int) nodeDistTable.get("cugf50").distanceTo(nodeDistTable.get("cugf46")));
        graph.addEdge("cugf46", "cugf45", (int) nodeDistTable.get("cugf46").distanceTo(nodeDistTable.get("cugf45")));
        graph.addEdge("cugf45", "cugf44", (int) nodeDistTable.get("cugf45").distanceTo(nodeDistTable.get("cugf44")));
        graph.addEdge("cugf51", "cugf49", (int)nodeDistTable.get("cugf51").distanceTo(nodeDistTable.get("cugf49")));
        graph.addEdge("cugf49", "cugf48", (int)nodeDistTable.get("cugf49").distanceTo(nodeDistTable.get("cugf48")));
        graph.addEdge("cugf143", "cugf129", (int)nodeDistTable.get("cugf143").distanceTo(nodeDistTable.get("cugf129")));
        graph.addEdge("cugf130", "cugf129", (int)nodeDistTable.get("cugf130").distanceTo(nodeDistTable.get("cugf129")));
        graph.addEdge("cugf129", "cugf128", (int)nodeDistTable.get("cugf129").distanceTo(nodeDistTable.get("cugf128")));
        graph.addEdge("cugf128", "cugf127", (int)nodeDistTable.get("cugf128").distanceTo(nodeDistTable.get("cugf127")));
        graph.addEdge("cugf127", "cugf126", (int)nodeDistTable.get("cugf127").distanceTo(nodeDistTable.get("cugf126")));
        graph.addEdge("cugf127", "cugf144", (int)nodeDistTable.get("cugf127").distanceTo(nodeDistTable.get("cugf144")));
        graph.addEdge("cugf144", "cugf145", (int)nodeDistTable.get("cugf144").distanceTo(nodeDistTable.get("cugf145")));
        graph.addEdge("cugf145", "cugf146", (int)nodeDistTable.get("cugf145").distanceTo(nodeDistTable.get("cugf146")));
        graph.addEdge("cugf146", "cugf147", (int)nodeDistTable.get("cugf146").distanceTo(nodeDistTable.get("cugf147")));
        graph.addEdge("cugf147", "cugf148", (int)nodeDistTable.get("cugf147").distanceTo(nodeDistTable.get("cugf148")));
        graph.addEdge("cugf148", "cugf149", (int)nodeDistTable.get("cugf148").distanceTo(nodeDistTable.get("cugf149")));
        graph.addEdge("cugf149", "cugf150", (int)nodeDistTable.get("cugf149").distanceTo(nodeDistTable.get("cugf150")));
        graph.addEdge("cugf150", "cugf126", (int) nodeDistTable.get("cugf150").distanceTo(nodeDistTable.get("cugf126")));
        graph.addEdge("cugf149", "cugf151", (int) nodeDistTable.get("cugf149").distanceTo(nodeDistTable.get("cugf151")));
        graph.addEdge("cugf151", "cugf152", (int) nodeDistTable.get("cugf151").distanceTo(nodeDistTable.get("cugf152")));
        graph.addEdge("cugf152", "cugf153", (int) nodeDistTable.get("cugf152").distanceTo(nodeDistTable.get("cugf153")));
        graph.addEdge("cugf153", "cugf154", (int) nodeDistTable.get("cugf153").distanceTo(nodeDistTable.get("cugf154")));
        graph.addEdge("cugf154", "cugf155", (int)nodeDistTable.get("cugf154").distanceTo(nodeDistTable.get("cugf155")));
        graph.addEdge("cugf148", "cugf156", (int)nodeDistTable.get("cugf148").distanceTo(nodeDistTable.get("cugf156")));
        graph.addEdge("cugf156", "cugf157", (int)nodeDistTable.get("cugf156").distanceTo(nodeDistTable.get("cugf157")));
        graph.addEdge("cugf157", "cugf158", (int)nodeDistTable.get("cugf157").distanceTo(nodeDistTable.get("cugf158")));
        graph.addEdge("cugf158", "cugf159", (int)nodeDistTable.get("cugf158").distanceTo(nodeDistTable.get("cugf159")));
        graph.addEdge("cugf44", "cugf43", (int)nodeDistTable.get("cugf44").distanceTo(nodeDistTable.get("cugf43")));
        graph.addEdge("cugf44", "cugf42", (int)nodeDistTable.get("cugf44").distanceTo(nodeDistTable.get("cugf42")));
        graph.addEdge("cugf43", "cugf42", (int)nodeDistTable.get("cugf43").distanceTo(nodeDistTable.get("cugf42")));
        graph.addEdge("cugf42", "cugf41", (int)nodeDistTable.get("cugf42").distanceTo(nodeDistTable.get("cugf41")));
        graph.addEdge("cugf43", "cugf41", (int)nodeDistTable.get("cugf43").distanceTo(nodeDistTable.get("cugf41")));
        graph.addEdge("cugf42", "cugf40", (int)nodeDistTable.get("cugf42").distanceTo(nodeDistTable.get("cugf40")));
        graph.addEdge("cugf43", "cugf40", (int)nodeDistTable.get("cugf43").distanceTo(nodeDistTable.get("cugf40")));
        graph.addEdge("cugf40", "cugf41", (int)nodeDistTable.get("cugf40").distanceTo(nodeDistTable.get("cugf41")));
        graph.addEdge("cugf40", "cugf39", (int)nodeDistTable.get("cugf40").distanceTo(nodeDistTable.get("cugf39")));
        graph.addEdge("cugf39", "cugf38", (int)nodeDistTable.get("cugf39").distanceTo(nodeDistTable.get("cugf38")));
        graph.addEdge("cugf38", "cugf2", (int) nodeDistTable.get("cugf38").distanceTo(nodeDistTable.get("cugf2")));
        graph.addEdge("cugf6", "cugf7", (int) nodeDistTable.get("cugf6").distanceTo(nodeDistTable.get("cugf7")));
        graph.addEdge("cugf7", "cugf8", (int) nodeDistTable.get("cugf7").distanceTo(nodeDistTable.get("cugf8")));
        graph.addEdge("cugf7", "cugf16", (int) nodeDistTable.get("cugf7").distanceTo(nodeDistTable.get("cugf16")));
        graph.addEdge("cugf7", "cugf9", (int) nodeDistTable.get("cugf7").distanceTo(nodeDistTable.get("cugf9")));
        graph.addEdge("cugf7", "cugf15", (int) nodeDistTable.get("cugf7").distanceTo(nodeDistTable.get("cugf15")));
        graph.addEdge("cugf7", "cugf10", (int)nodeDistTable.get("cugf7").distanceTo(nodeDistTable.get("cugf10")));
        graph.addEdge("cugf7", "cugf14", (int)nodeDistTable.get("cugf7").distanceTo(nodeDistTable.get("cugf14")));
        graph.addEdge("cugf7", "cugf11", (int)nodeDistTable.get("cugf7").distanceTo(nodeDistTable.get("cugf11")));
        graph.addEdge("cugf7", "cugf13", (int)nodeDistTable.get("cugf7").distanceTo(nodeDistTable.get("cugf13")));
        graph.addEdge("cugf7", "cugf12", (int)nodeDistTable.get("cugf7").distanceTo(nodeDistTable.get("cugf12")));
        graph.addEdge("cugf8", "cugf9", (int)nodeDistTable.get("cugf8").distanceTo(nodeDistTable.get("cugf9")));
        graph.addEdge("cugf8", "cugf12", (int)nodeDistTable.get("cugf8").distanceTo(nodeDistTable.get("cugf12")));
        graph.addEdge("cugf8", "cugf13", (int)nodeDistTable.get("cugf8").distanceTo(nodeDistTable.get("cugf13")));
        graph.addEdge("cugf8", "cugf14", (int)nodeDistTable.get("cugf8").distanceTo(nodeDistTable.get("cugf14")));
        graph.addEdge("cugf8", "cugf15", (int)nodeDistTable.get("cugf8").distanceTo(nodeDistTable.get("cugf15")));
        graph.addEdge("cugf9", "cugf10", (int)nodeDistTable.get("cugf9").distanceTo(nodeDistTable.get("cugf10")));
        graph.addEdge("cugf9", "cugf12", (int)nodeDistTable.get("cugf9").distanceTo(nodeDistTable.get("cugf12")));
        graph.addEdge("cugf9", "cugf13", (int)nodeDistTable.get("cugf9").distanceTo(nodeDistTable.get("cugf13")));
        graph.addEdge("cugf9", "cugf14", (int)nodeDistTable.get("cugf9").distanceTo(nodeDistTable.get("cugf14")));
        graph.addEdge("cugf9", "cugf15", (int) nodeDistTable.get("cugf9").distanceTo(nodeDistTable.get("cugf15")));
        graph.addEdge("cugf48", "cugf47", (int) nodeDistTable.get("cugf48").distanceTo(nodeDistTable.get("cugf47")));
        graph.addEdge("cugf47", "cugf46", (int) nodeDistTable.get("cugf47").distanceTo(nodeDistTable.get("cugf46")));
        graph.addEdge("cugf47", "cugf45", (int) nodeDistTable.get("cugf47").distanceTo(nodeDistTable.get("cugf45")));
        graph.addEdge("cugf47", "cugf44", (int) nodeDistTable.get("cugf47").distanceTo(nodeDistTable.get("cugf44")));
        graph.addEdge("cugf61", "cugf62", (int)nodeDistTable.get("cugf61").distanceTo(nodeDistTable.get("cugf62")));
        graph.addEdge("cugf61", "cugf58", (int)nodeDistTable.get("cugf61").distanceTo(nodeDistTable.get("cugf58")));
        graph.addEdge("cugf61", "cugf57", (int)nodeDistTable.get("cugf61").distanceTo(nodeDistTable.get("cugf57")));
        graph.addEdge("cugf61", "cugf56", (int)nodeDistTable.get("cugf61").distanceTo(nodeDistTable.get("cugf56")));
        graph.addEdge("cugf61", "cugf55", (int)nodeDistTable.get("cugf61").distanceTo(nodeDistTable.get("cugf55")));
        graph.addEdge("cugf61", "cugf54", (int)nodeDistTable.get("cugf61").distanceTo(nodeDistTable.get("cugf54")));
        graph.addEdge("cugf62", "cugf58", (int)nodeDistTable.get("cugf62").distanceTo(nodeDistTable.get("cugf58")));
        graph.addEdge("cugf62", "cugf57", (int)nodeDistTable.get("cugf62").distanceTo(nodeDistTable.get("cugf57")));
        graph.addEdge("cugf62", "cugf56", (int)nodeDistTable.get("cugf62").distanceTo(nodeDistTable.get("cugf56")));
        graph.addEdge("cugf62", "cugf87", (int)nodeDistTable.get("cugf62").distanceTo(nodeDistTable.get("cugf87")));
        graph.addEdge("cugf62", "cugf63", (int)nodeDistTable.get("cugf62").distanceTo(nodeDistTable.get("cugf63")));
        graph.addEdge("cugf63", "cugf64", (int)nodeDistTable.get("cugf63").distanceTo(nodeDistTable.get("cugf64")));
        graph.addEdge("cugf64", "cugf65", (int)nodeDistTable.get("cugf64").distanceTo(nodeDistTable.get("cugf65")));
        graph.addEdge("cugf65", "cugf66", (int)nodeDistTable.get("cugf65").distanceTo(nodeDistTable.get("cugf66")));
        graph.addEdge("cugf66", "cugf67", (int)nodeDistTable.get("cugf66").distanceTo(nodeDistTable.get("cugf67")));
        graph.addEdge("cugf67", "cugf68", (int) nodeDistTable.get("cugf67").distanceTo(nodeDistTable.get("cugf68")));
        graph.addEdge("cugf68", "cugf83", (int) nodeDistTable.get("cugf68").distanceTo(nodeDistTable.get("cugf83")));
        graph.addEdge("cugf87", "cugf86", (int) nodeDistTable.get("cugf87").distanceTo(nodeDistTable.get("cugf86")));
        graph.addEdge("cugf86", "cugf85", (int) nodeDistTable.get("cugf86").distanceTo(nodeDistTable.get("cugf85")));
        graph.addEdge("cugf85", "cugf84", (int) nodeDistTable.get("cugf85").distanceTo(nodeDistTable.get("cugf84")));
        graph.addEdge("cugf84", "cugf83", (int) nodeDistTable.get("cugf84").distanceTo(nodeDistTable.get("cugf83")));
        graph.addEdge("cugf84", "cugf88", (int)nodeDistTable.get("cugf84").distanceTo(nodeDistTable.get("cugf88")));
        graph.addEdge("cugf88", "cugf89", (int)nodeDistTable.get("cugf88").distanceTo(nodeDistTable.get("cugf89")));
        graph.addEdge("cugf89", "cugf90", (int)nodeDistTable.get("cugf89").distanceTo(nodeDistTable.get("cugf90")));
        graph.addEdge("cugf90", "cugf91", (int)nodeDistTable.get("cugf90").distanceTo(nodeDistTable.get("cugf91")));
        graph.addEdge("cugf83", "cugf82", (int)nodeDistTable.get("cugf83").distanceTo(nodeDistTable.get("cugf82")));
        graph.addEdge("cugf82", "cugf81", (int)nodeDistTable.get("cugf82").distanceTo(nodeDistTable.get("cugf81")));
        graph.addEdge("cugf81", "cugf80", (int)nodeDistTable.get("cugf81").distanceTo(nodeDistTable.get("cugf80")));
        graph.addEdge("cugf80", "cugf79", (int)nodeDistTable.get("cugf80").distanceTo(nodeDistTable.get("cugf79")));
        graph.addEdge("cugf79", "cugf78", (int)nodeDistTable.get("cugf79").distanceTo(nodeDistTable.get("cugf78")));
        graph.addEdge("cugf78", "cugf91", (int)nodeDistTable.get("cugf78").distanceTo(nodeDistTable.get("cugf91")));
        graph.addEdge("cugf93", "cugf78", (int)nodeDistTable.get("cugf93").distanceTo(nodeDistTable.get("cugf78")));
        graph.addEdge("cugf91", "cugf112", (int)nodeDistTable.get("cugf91").distanceTo(nodeDistTable.get("cugf112")));
        graph.addEdge("cugf93", "cugf112", (int)nodeDistTable.get("cugf93").distanceTo(nodeDistTable.get("cugf112")));
        graph.addEdge("cugf78", "cugf112", (int)nodeDistTable.get("cugf78").distanceTo(nodeDistTable.get("cugf112")));
        graph.addEdge("cugf68", "cugf69", (int) nodeDistTable.get("cugf68").distanceTo(nodeDistTable.get("cugf69")));
        graph.addEdge("cugf69", "cugf70", (int) nodeDistTable.get("cugf69").distanceTo(nodeDistTable.get("cugf70")));
        graph.addEdge("cugf70", "cugf71", (int) nodeDistTable.get("cugf70").distanceTo(nodeDistTable.get("cugf71")));
        graph.addEdge("cugf71", "cugf72", (int) nodeDistTable.get("cugf71").distanceTo(nodeDistTable.get("cugf72")));
        graph.addEdge("cugf72", "cugf73", (int) nodeDistTable.get("cugf72").distanceTo(nodeDistTable.get("cugf73")));
        graph.addEdge("cugf73", "cugf74", (int)nodeDistTable.get("cugf73").distanceTo(nodeDistTable.get("cugf74")));
        graph.addEdge("cugf74", "cugf75", (int)nodeDistTable.get("cugf74").distanceTo(nodeDistTable.get("cugf75")));
        graph.addEdge("cugf75", "cugf76", (int)nodeDistTable.get("cugf75").distanceTo(nodeDistTable.get("cugf76")));
        graph.addEdge("cugf76", "cugf77", (int)nodeDistTable.get("cugf76").distanceTo(nodeDistTable.get("cugf77")));
        graph.addEdge("cugf77", "cugf78", (int)nodeDistTable.get("cugf77").distanceTo(nodeDistTable.get("cugf78")));
        graph.addEdge("cugf112", "cugf113", (int)nodeDistTable.get("cugf112").distanceTo(nodeDistTable.get("cugf113")));
        graph.addEdge("cugf113", "cugf114", (int)nodeDistTable.get("cugf113").distanceTo(nodeDistTable.get("cugf114")));
        graph.addEdge("cugf114", "cugf115", (int)nodeDistTable.get("cugf114").distanceTo(nodeDistTable.get("cugf115")));
        graph.addEdge("cugf115", "cugf116", (int)nodeDistTable.get("cugf115").distanceTo(nodeDistTable.get("cugf116")));
        graph.addEdge("cugf116", "cugf110", (int)nodeDistTable.get("cugf116").distanceTo(nodeDistTable.get("cugf110")));
        graph.addEdge("cugf110", "cugf111", (int)nodeDistTable.get("cugf110").distanceTo(nodeDistTable.get("cugf111")));
        graph.addEdge("cugf110", "cugf109", (int)nodeDistTable.get("cugf110").distanceTo(nodeDistTable.get("cugf109")));
        graph.addEdge("cugf111", "cugf109", (int)nodeDistTable.get("cugf111").distanceTo(nodeDistTable.get("cugf109")));
        graph.addEdge("cugf109", "cugf108", (int)nodeDistTable.get("cugf109").distanceTo(nodeDistTable.get("cugf108")));
        graph.addEdge("cugf108", "cugf107", (int)nodeDistTable.get("cugf108").distanceTo(nodeDistTable.get("cugf107")));
        graph.addEdge("cugf107", "cugf106", (int) nodeDistTable.get("cugf107").distanceTo(nodeDistTable.get("cugf106")));
        graph.addEdge("cugf106", "cugf105", (int) nodeDistTable.get("cugf106").distanceTo(nodeDistTable.get("cugf105")));
        graph.addEdge("cugf93", "cugf94", (int) nodeDistTable.get("cugf93").distanceTo(nodeDistTable.get("cugf94")));
        graph.addEdge("cugf94", "cugf95", (int) nodeDistTable.get("cugf94").distanceTo(nodeDistTable.get("cugf95")));
        graph.addEdge("cugf95", "cugf96", (int) nodeDistTable.get("cugf95").distanceTo(nodeDistTable.get("cugf96")));
        graph.addEdge("cugf96", "cugf97", (int) nodeDistTable.get("cugf96").distanceTo(nodeDistTable.get("cugf97")));
        graph.addEdge("cugf97", "cugf98", (int)nodeDistTable.get("cugf97").distanceTo(nodeDistTable.get("cugf98")));
        graph.addEdge("cugf96", "cugf99", (int)nodeDistTable.get("cugf96").distanceTo(nodeDistTable.get("cugf99")));
        graph.addEdge("cugf99", "cugf100", (int)nodeDistTable.get("cugf99").distanceTo(nodeDistTable.get("cugf100")));
        graph.addEdge("cugf100", "cugf101", (int)nodeDistTable.get("cugf100").distanceTo(nodeDistTable.get("cugf101")));
        graph.addEdge("cugf99", "cugf102", (int)nodeDistTable.get("cugf99").distanceTo(nodeDistTable.get("cugf102")));
        graph.addEdge("cugf102", "cugf103", (int)nodeDistTable.get("cugf102").distanceTo(nodeDistTable.get("cugf103")));
        graph.addEdge("cugf103", "cugf104", (int)nodeDistTable.get("cugf103").distanceTo(nodeDistTable.get("cugf104")));
        graph.addEdge("cugf104", "cugf105", (int)nodeDistTable.get("cugf104").distanceTo(nodeDistTable.get("cugf105")));
        graph.addEdge("cugf117", "cugf105", (int)nodeDistTable.get("cugf117").distanceTo(nodeDistTable.get("cugf105")));
        graph.addEdge("cugf104", "cugf137", (int)nodeDistTable.get("cugf104").distanceTo(nodeDistTable.get("cugf137")));
        graph.addEdge("cugf104", "cugf117", (int)nodeDistTable.get("cugf104").distanceTo(nodeDistTable.get("cugf117")));
        graph.addEdge("cugf117", "cugf118", (int)nodeDistTable.get("cugf117").distanceTo(nodeDistTable.get("cugf118")));
        graph.addEdge("cugf117", "cugf119", (int)nodeDistTable.get("cugf117").distanceTo(nodeDistTable.get("cugf119")));
        graph.addEdge("cugf119", "cugf120", (int)nodeDistTable.get("cugf119").distanceTo(nodeDistTable.get("cugf120")));
        graph.addEdge("cugf120", "cugf121", (int) nodeDistTable.get("cugf120").distanceTo(nodeDistTable.get("cugf121")));
        graph.addEdge("cugf121", "cugf122", (int) nodeDistTable.get("cugf121").distanceTo(nodeDistTable.get("cugf122")));
        graph.addEdge("cugf122", "cugf123", (int) nodeDistTable.get("cugf122").distanceTo(nodeDistTable.get("cugf123")));
        graph.addEdge("cugf123", "cugf124", (int) nodeDistTable.get("cugf123").distanceTo(nodeDistTable.get("cugf124")));
        graph.addEdge("cugf123", "cugf125", (int) nodeDistTable.get("cugf123").distanceTo(nodeDistTable.get("cugf125")));
        graph.addEdge("cugf125", "cugf126", (int)nodeDistTable.get("cugf125").distanceTo(nodeDistTable.get("cugf126")));
        graph.addEdge("cugf105", "cugf137", (int)nodeDistTable.get("cugf105").distanceTo(nodeDistTable.get("cugf137")));
        graph.addEdge("cugf137", "cugf136", (int)nodeDistTable.get("cugf137").distanceTo(nodeDistTable.get("cugf136")));
        graph.addEdge("cugf136", "cugf135", (int)nodeDistTable.get("cugf136").distanceTo(nodeDistTable.get("cugf135")));
        graph.addEdge("cugf135", "cugf134", (int)nodeDistTable.get("cugf135").distanceTo(nodeDistTable.get("cugf134")));
        graph.addEdge("cugf134", "cugf133", (int)nodeDistTable.get("cugf134").distanceTo(nodeDistTable.get("cugf133")));
        graph.addEdge("cugf133", "cugf140", (int)nodeDistTable.get("cugf133").distanceTo(nodeDistTable.get("cugf140")));
        graph.addEdge("cugf133", "cugf132", (int)nodeDistTable.get("cugf133").distanceTo(nodeDistTable.get("cugf132")));
        graph.addEdge("cugf132", "cugf131", (int)nodeDistTable.get("cugf132").distanceTo(nodeDistTable.get("cugf131")));
        graph.addEdge("cugf131", "cugf130", (int)nodeDistTable.get("cugf131").distanceTo(nodeDistTable.get("cugf130")));
        graph.addEdge("cugf137", "cugf138", (int)nodeDistTable.get("cugf137").distanceTo(nodeDistTable.get("cugf138")));
        graph.addEdge("cugf138", "cugf139", (int)nodeDistTable.get("cugf138").distanceTo(nodeDistTable.get("cugf139")));
        graph.addEdge("cugf139", "cugf140", (int)nodeDistTable.get("cugf139").distanceTo(nodeDistTable.get("cugf140")));
        graph.addEdge("cugf140", "cugf141", (int)nodeDistTable.get("cugf140").distanceTo(nodeDistTable.get("cugf141")));
        graph.addEdge("cugf141", "cugf142", (int)nodeDistTable.get("cugf141").distanceTo(nodeDistTable.get("cugf142")));
        graph.addEdge("cugf142", "cugf143", (int)nodeDistTable.get("cugf142").distanceTo(nodeDistTable.get("cugf143")));
        graph.addEdge("cugf9", "cugf16", (int)nodeDistTable.get("cugf9").distanceTo(nodeDistTable.get("cugf16")));
        graph.addEdge("cugf10", "cugf11", (int)nodeDistTable.get("cugf10").distanceTo(nodeDistTable.get("cugf11")));
        graph.addEdge("cugf10", "cugf12", (int)nodeDistTable.get("cugf10").distanceTo(nodeDistTable.get("cugf12")));
        graph.addEdge("cugf10", "cugf14", (int)nodeDistTable.get("cugf10").distanceTo(nodeDistTable.get("cugf14")));
        graph.addEdge("cugf10", "cugf15", (int)nodeDistTable.get("cugf10").distanceTo(nodeDistTable.get("cugf15")));
        graph.addEdge("cugf10", "cugf16", (int)nodeDistTable.get("cugf10").distanceTo(nodeDistTable.get("cugf16")));
        graph.addEdge("cugf11", "cugf12", (int)nodeDistTable.get("cugf11").distanceTo(nodeDistTable.get("cugf12")));
        graph.addEdge("cugf11", "cugf15", (int)nodeDistTable.get("cugf11").distanceTo(nodeDistTable.get("cugf15")));
        graph.addEdge("cugf11", "cugf16", (int)nodeDistTable.get("cugf11").distanceTo(nodeDistTable.get("cugf16")));
        graph.addEdge("cugf12", "cugf13", (int)nodeDistTable.get("cugf12").distanceTo(nodeDistTable.get("cugf13")));
        graph.addEdge("cugf12", "cugf14", (int)nodeDistTable.get("cugf12").distanceTo(nodeDistTable.get("cugf14")));
        graph.addEdge("cugf12", "cugf15", (int)nodeDistTable.get("cugf12").distanceTo(nodeDistTable.get("cugf15")));
        graph.addEdge("cugf12", "cugf16", (int)nodeDistTable.get("cugf12").distanceTo(nodeDistTable.get("cugf16")));
        graph.addEdge("cugf13", "cugf14", (int)nodeDistTable.get("cugf13").distanceTo(nodeDistTable.get("cugf14")));
        graph.addEdge("cugf14", "cugf15", (int)nodeDistTable.get("cugf14").distanceTo(nodeDistTable.get("cugf15")));
        graph.addEdge("cugf15", "cugf16", (int)nodeDistTable.get("cugf15").distanceTo(nodeDistTable.get("cugf16")));
        graph.addEdge("cugf35", "cugf162", (int)nodeDistTable.get("cugf35").distanceTo(nodeDistTable.get("cugf162")));
        graph.addEdge("cugf91", "cugf92", (int)nodeDistTable.get("cugf91").distanceTo(nodeDistTable.get("cugf92")));
        graph.addEdge("cugf91", "cugf93", (int)nodeDistTable.get("cugf91").distanceTo(nodeDistTable.get("cugf93")));
        graph.addEdge( "cugf2","cugf1", (int) nodeDistTable.get("cugf1").distanceTo(nodeDistTable.get("cugf2")));
        graph.addEdge("cugf3","cugf2",  (int) nodeDistTable.get("cugf2").distanceTo(nodeDistTable.get("cugf3")));
        graph.addEdge("cugf4","cugf3",  (int) nodeDistTable.get("cugf3").distanceTo(nodeDistTable.get("cugf4")));
        graph.addEdge( "cugf5","cugf4", (int) nodeDistTable.get("cugf4").distanceTo(nodeDistTable.get("cugf5")));
        graph.addEdge("cugf6", "cugf5", (int) nodeDistTable.get("cugf5").distanceTo(nodeDistTable.get("cugf6")));
        graph.addEdge("cugf17","cugf6",  (int) nodeDistTable.get("cugf6").distanceTo(nodeDistTable.get("cugf17")));
        graph.addEdge("cugf18","cugf17",  (int)nodeDistTable.get("cugf17").distanceTo(nodeDistTable.get("cugf18")));
        graph.addEdge( "cugf19","cugf18", (int)nodeDistTable.get("cugf18").distanceTo(nodeDistTable.get("cugf19")));
        graph.addEdge("cugf20","cugf19",  (int)nodeDistTable.get("cugf19").distanceTo(nodeDistTable.get("cugf20")));
        graph.addEdge("cugf21","cugf20",  (int)nodeDistTable.get("cugf20").distanceTo(nodeDistTable.get("cugf21")));
        graph.addEdge("cugf22","cugf21",  (int)nodeDistTable.get("cugf21").distanceTo(nodeDistTable.get("cugf22")));
        graph.addEdge( "cugf23","cugf22", (int)nodeDistTable.get("cugf22").distanceTo(nodeDistTable.get("cugf23")));
        graph.addEdge( "cugf24","cugf23", (int)nodeDistTable.get("cugf23").distanceTo(nodeDistTable.get("cugf24")));
        graph.addEdge("cugf25","cugf24",  (int)nodeDistTable.get("cugf24").distanceTo(nodeDistTable.get("cugf25")));
        graph.addEdge("cugf26","cugf25",  (int)nodeDistTable.get("cugf25").distanceTo(nodeDistTable.get("cugf26")));
        graph.addEdge( "cugf27","cugf26", (int)nodeDistTable.get("cugf26").distanceTo(nodeDistTable.get("cugf27")));
        graph.addEdge("cugf28", "cugf27", (int)nodeDistTable.get("cugf27").distanceTo(nodeDistTable.get("cugf28")));
        graph.addEdge("cugf29","cugf28",  (int)nodeDistTable.get("cugf28").distanceTo(nodeDistTable.get("cugf29")));
        graph.addEdge("cugf31","cugf30",  (int)nodeDistTable.get("cugf30").distanceTo(nodeDistTable.get("cugf31")));
        graph.addEdge("cugf32","cugf31",  (int)nodeDistTable.get("cugf31").distanceTo(nodeDistTable.get("cugf32")));
        graph.addEdge("cugf33","cugf32",  (int) nodeDistTable.get("cugf32").distanceTo(nodeDistTable.get("cugf33")));
        graph.addEdge("cugf34", "cugf33", (int) nodeDistTable.get("cugf33").distanceTo(nodeDistTable.get("cugf34")));
        graph.addEdge("cugf35","cugf34",  (int) nodeDistTable.get("cugf34").distanceTo(nodeDistTable.get("cugf35")));
        graph.addEdge("cugf36","cugf35",  (int) nodeDistTable.get("cugf35").distanceTo(nodeDistTable.get("cugf36")));
        graph.addEdge("cugf37", "cugf36", (int) nodeDistTable.get("cugf36").distanceTo(nodeDistTable.get("cugf37")));
        graph.addEdge("cugf163","cugf36",  (int)nodeDistTable.get("cugf36").distanceTo(nodeDistTable.get("cugf163")));
        graph.addEdge("cugf161","cugf163",  (int)nodeDistTable.get("cugf163").distanceTo(nodeDistTable.get("cugf161")));
        graph.addEdge("cugf162","cugf161",  (int)nodeDistTable.get("cugf161").distanceTo(nodeDistTable.get("cugf162")));
        graph.addEdge("cugf160","cugf163",  (int)nodeDistTable.get("cugf163").distanceTo(nodeDistTable.get("cugf160")));
        graph.addEdge("cugf160","cugf161",  (int)nodeDistTable.get("cugf161").distanceTo(nodeDistTable.get("cugf160")));
        graph.addEdge("cugf160","cugf162",  (int)nodeDistTable.get("cugf162").distanceTo(nodeDistTable.get("cugf160")));
        graph.addEdge( "cugf60","cugf162", (int)nodeDistTable.get("cugf162").distanceTo(nodeDistTable.get("cugf60")));
        graph.addEdge("cugf60","cugf160",  (int)nodeDistTable.get("cugf160").distanceTo(nodeDistTable.get("cugf60")));
        graph.addEdge("cugf58","cugf60",  (int)nodeDistTable.get("cugf60").distanceTo(nodeDistTable.get("cugf58")));
        graph.addEdge("cugf61", "cugf60", (int)nodeDistTable.get("cugf60").distanceTo(nodeDistTable.get("cugf61")));
        graph.addEdge( "cugf57","cugf58", (int)nodeDistTable.get("cugf58").distanceTo(nodeDistTable.get("cugf57")));
        graph.addEdge("cugf56","cugf57",  (int)nodeDistTable.get("cugf57").distanceTo(nodeDistTable.get("cugf56")));
        graph.addEdge("cugf55","cugf56",  (int)nodeDistTable.get("cugf56").distanceTo(nodeDistTable.get("cugf55")));
        graph.addEdge("cugf54","cugf55",  (int)nodeDistTable.get("cugf55").distanceTo(nodeDistTable.get("cugf54")));
        graph.addEdge("cugf53","cugf54",  (int)nodeDistTable.get("cugf54").distanceTo(nodeDistTable.get("cugf53")));
        graph.addEdge("cugf52","cugf53",  (int) nodeDistTable.get("cugf53").distanceTo(nodeDistTable.get("cugf52")));
        graph.addEdge("cugf51","cugf52",  (int) nodeDistTable.get("cugf52").distanceTo(nodeDistTable.get("cugf51")));
        graph.addEdge( "cugf50","cugf51", (int) nodeDistTable.get("cugf51").distanceTo(nodeDistTable.get("cugf50")));
        graph.addEdge("cugf46","cugf50",  (int) nodeDistTable.get("cugf50").distanceTo(nodeDistTable.get("cugf46")));
        graph.addEdge("cugf45","cugf46",  (int) nodeDistTable.get("cugf46").distanceTo(nodeDistTable.get("cugf45")));
        graph.addEdge( "cugf44","cugf45", (int) nodeDistTable.get("cugf45").distanceTo(nodeDistTable.get("cugf44")));
        graph.addEdge( "cugf49","cugf51", (int)nodeDistTable.get("cugf51").distanceTo(nodeDistTable.get("cugf49")));
        graph.addEdge("cugf48","cugf49",  (int)nodeDistTable.get("cugf49").distanceTo(nodeDistTable.get("cugf48")));
        graph.addEdge("cugf129","cugf143",  (int)nodeDistTable.get("cugf143").distanceTo(nodeDistTable.get("cugf129")));
        graph.addEdge("cugf129","cugf130",  (int)nodeDistTable.get("cugf130").distanceTo(nodeDistTable.get("cugf129")));
        graph.addEdge("cugf128","cugf129",  (int)nodeDistTable.get("cugf129").distanceTo(nodeDistTable.get("cugf128")));
        graph.addEdge("cugf127","cugf128",  (int)nodeDistTable.get("cugf128").distanceTo(nodeDistTable.get("cugf127")));
        graph.addEdge("cugf126","cugf127",  (int)nodeDistTable.get("cugf127").distanceTo(nodeDistTable.get("cugf126")));
        graph.addEdge("cugf144","cugf127",  (int)nodeDistTable.get("cugf127").distanceTo(nodeDistTable.get("cugf144")));
        graph.addEdge("cugf145","cugf144",  (int)nodeDistTable.get("cugf144").distanceTo(nodeDistTable.get("cugf145")));
        graph.addEdge("cugf146","cugf145",  (int)nodeDistTable.get("cugf145").distanceTo(nodeDistTable.get("cugf146")));
        graph.addEdge("cugf147","cugf146",  (int)nodeDistTable.get("cugf146").distanceTo(nodeDistTable.get("cugf147")));
        graph.addEdge("cugf148","cugf147",  (int)nodeDistTable.get("cugf147").distanceTo(nodeDistTable.get("cugf148")));
        graph.addEdge("cugf149","cugf148",  (int)nodeDistTable.get("cugf148").distanceTo(nodeDistTable.get("cugf149")));
        graph.addEdge("cugf150","cugf149",  (int)nodeDistTable.get("cugf149").distanceTo(nodeDistTable.get("cugf150")));
        graph.addEdge("cugf126","cugf150",  (int) nodeDistTable.get("cugf150").distanceTo(nodeDistTable.get("cugf126")));
        graph.addEdge("cugf151","cugf149",  (int) nodeDistTable.get("cugf149").distanceTo(nodeDistTable.get("cugf151")));
        graph.addEdge("cugf152","cugf151",  (int) nodeDistTable.get("cugf151").distanceTo(nodeDistTable.get("cugf152")));
        graph.addEdge("cugf153","cugf152",  (int) nodeDistTable.get("cugf152").distanceTo(nodeDistTable.get("cugf153")));
        graph.addEdge("cugf154","cugf153",  (int) nodeDistTable.get("cugf153").distanceTo(nodeDistTable.get("cugf154")));
        graph.addEdge("cugf155","cugf154",  (int)nodeDistTable.get("cugf154").distanceTo(nodeDistTable.get("cugf155")));
        graph.addEdge("cugf156","cugf148",  (int)nodeDistTable.get("cugf148").distanceTo(nodeDistTable.get("cugf156")));
        graph.addEdge("cugf157","cugf156",  (int)nodeDistTable.get("cugf156").distanceTo(nodeDistTable.get("cugf157")));
        graph.addEdge("cugf158","cugf157",  (int)nodeDistTable.get("cugf157").distanceTo(nodeDistTable.get("cugf158")));
        graph.addEdge( "cugf159","cugf158", (int)nodeDistTable.get("cugf158").distanceTo(nodeDistTable.get("cugf159")));
        graph.addEdge("cugf43","cugf44",  (int)nodeDistTable.get("cugf44").distanceTo(nodeDistTable.get("cugf43")));
        graph.addEdge("cugf42","cugf44",  (int)nodeDistTable.get("cugf44").distanceTo(nodeDistTable.get("cugf42")));
        graph.addEdge("cugf42","cugf43",  (int)nodeDistTable.get("cugf43").distanceTo(nodeDistTable.get("cugf42")));
        graph.addEdge("cugf41","cugf42",  (int)nodeDistTable.get("cugf42").distanceTo(nodeDistTable.get("cugf41")));
        graph.addEdge("cugf41","cugf43",  (int)nodeDistTable.get("cugf43").distanceTo(nodeDistTable.get("cugf41")));
        graph.addEdge("cugf40","cugf42",  (int)nodeDistTable.get("cugf42").distanceTo(nodeDistTable.get("cugf40")));
        graph.addEdge("cugf40","cugf43",  (int)nodeDistTable.get("cugf43").distanceTo(nodeDistTable.get("cugf40")));
        graph.addEdge("cugf41","cugf40",  (int)nodeDistTable.get("cugf40").distanceTo(nodeDistTable.get("cugf41")));
        graph.addEdge("cugf39","cugf40",  (int)nodeDistTable.get("cugf40").distanceTo(nodeDistTable.get("cugf39")));
        graph.addEdge("cugf38","cugf39",  (int)nodeDistTable.get("cugf39").distanceTo(nodeDistTable.get("cugf38")));
        graph.addEdge("cugf2","cugf38",  (int) nodeDistTable.get("cugf38").distanceTo(nodeDistTable.get("cugf2")));
        graph.addEdge("cugf7","cugf6", (int) nodeDistTable.get("cugf6").distanceTo(nodeDistTable.get("cugf7")));
        graph.addEdge("cugf8","cugf7",  (int) nodeDistTable.get("cugf7").distanceTo(nodeDistTable.get("cugf8")));
        graph.addEdge("cugf16","cugf7",  (int) nodeDistTable.get("cugf7").distanceTo(nodeDistTable.get("cugf16")));
        graph.addEdge("cugf9","cugf7",  (int) nodeDistTable.get("cugf7").distanceTo(nodeDistTable.get("cugf9")));
        graph.addEdge("cugf15","cugf7",  (int) nodeDistTable.get("cugf7").distanceTo(nodeDistTable.get("cugf15")));
        graph.addEdge( "cugf10","cugf7", (int)nodeDistTable.get("cugf7").distanceTo(nodeDistTable.get("cugf10")));
        graph.addEdge("cugf14", "cugf7", (int)nodeDistTable.get("cugf7").distanceTo(nodeDistTable.get("cugf14")));
        graph.addEdge("cugf11","cugf7",  (int)nodeDistTable.get("cugf7").distanceTo(nodeDistTable.get("cugf11")));
        graph.addEdge("cugf13","cugf7",  (int)nodeDistTable.get("cugf7").distanceTo(nodeDistTable.get("cugf13")));
        graph.addEdge("cugf12","cugf7",  (int)nodeDistTable.get("cugf7").distanceTo(nodeDistTable.get("cugf12")));
        graph.addEdge( "cugf9","cugf8", (int)nodeDistTable.get("cugf8").distanceTo(nodeDistTable.get("cugf9")));
        graph.addEdge("cugf12","cugf8",  (int)nodeDistTable.get("cugf8").distanceTo(nodeDistTable.get("cugf12")));
        graph.addEdge("cugf13","cugf8",  (int)nodeDistTable.get("cugf8").distanceTo(nodeDistTable.get("cugf13")));
        graph.addEdge("cugf14","cugf8",  (int)nodeDistTable.get("cugf8").distanceTo(nodeDistTable.get("cugf14")));
        graph.addEdge("cugf15","cugf8",  (int)nodeDistTable.get("cugf8").distanceTo(nodeDistTable.get("cugf15")));
        graph.addEdge("cugf10","cugf9",  (int)nodeDistTable.get("cugf9").distanceTo(nodeDistTable.get("cugf10")));
        graph.addEdge("cugf12","cugf9",  (int)nodeDistTable.get("cugf9").distanceTo(nodeDistTable.get("cugf12")));
        graph.addEdge("cugf13","cugf9",  (int)nodeDistTable.get("cugf9").distanceTo(nodeDistTable.get("cugf13")));
        graph.addEdge("cugf14","cugf9",  (int)nodeDistTable.get("cugf9").distanceTo(nodeDistTable.get("cugf14")));
        graph.addEdge( "cugf15","cugf9", (int) nodeDistTable.get("cugf9").distanceTo(nodeDistTable.get("cugf15")));
        graph.addEdge("cugf47","cugf48",  (int) nodeDistTable.get("cugf48").distanceTo(nodeDistTable.get("cugf47")));
        graph.addEdge("cugf46","cugf47",  (int) nodeDistTable.get("cugf47").distanceTo(nodeDistTable.get("cugf46")));
        graph.addEdge( "cugf45","cugf47", (int) nodeDistTable.get("cugf47").distanceTo(nodeDistTable.get("cugf45")));
        graph.addEdge("cugf44","cugf47",  (int) nodeDistTable.get("cugf47").distanceTo(nodeDistTable.get("cugf44")));
        graph.addEdge("cugf62","cugf61",  (int)nodeDistTable.get("cugf61").distanceTo(nodeDistTable.get("cugf62")));
        graph.addEdge("cugf58","cugf61",  (int)nodeDistTable.get("cugf61").distanceTo(nodeDistTable.get("cugf58")));
        graph.addEdge("cugf57","cugf61",  (int)nodeDistTable.get("cugf61").distanceTo(nodeDistTable.get("cugf57")));
        graph.addEdge("cugf56","cugf61",  (int)nodeDistTable.get("cugf61").distanceTo(nodeDistTable.get("cugf56")));
        graph.addEdge("cugf55","cugf61",  (int)nodeDistTable.get("cugf61").distanceTo(nodeDistTable.get("cugf55")));
        graph.addEdge("cugf54","cugf61",  (int)nodeDistTable.get("cugf61").distanceTo(nodeDistTable.get("cugf54")));
        graph.addEdge("cugf58","cugf62",  (int)nodeDistTable.get("cugf62").distanceTo(nodeDistTable.get("cugf58")));
        graph.addEdge("cugf57","cugf62",  (int)nodeDistTable.get("cugf62").distanceTo(nodeDistTable.get("cugf57")));
        graph.addEdge("cugf56","cugf62",  (int)nodeDistTable.get("cugf62").distanceTo(nodeDistTable.get("cugf56")));
        graph.addEdge("cugf87","cugf62",  (int)nodeDistTable.get("cugf62").distanceTo(nodeDistTable.get("cugf87")));
        graph.addEdge("cugf63","cugf62",  (int)nodeDistTable.get("cugf62").distanceTo(nodeDistTable.get("cugf63")));
        graph.addEdge("cugf64","cugf63",  (int)nodeDistTable.get("cugf63").distanceTo(nodeDistTable.get("cugf64")));
        graph.addEdge("cugf65","cugf64",  (int)nodeDistTable.get("cugf64").distanceTo(nodeDistTable.get("cugf65")));
        graph.addEdge( "cugf66","cugf65", (int)nodeDistTable.get("cugf65").distanceTo(nodeDistTable.get("cugf66")));
        graph.addEdge("cugf67","cugf66",  (int)nodeDistTable.get("cugf66").distanceTo(nodeDistTable.get("cugf67")));
        graph.addEdge("cugf68","cugf67",  (int) nodeDistTable.get("cugf67").distanceTo(nodeDistTable.get("cugf68")));
        graph.addEdge( "cugf83","cugf68", (int) nodeDistTable.get("cugf68").distanceTo(nodeDistTable.get("cugf83")));
        graph.addEdge("cugf86","cugf87",  (int) nodeDistTable.get("cugf87").distanceTo(nodeDistTable.get("cugf86")));
        graph.addEdge( "cugf85","cugf86", (int) nodeDistTable.get("cugf86").distanceTo(nodeDistTable.get("cugf85")));
        graph.addEdge("cugf84","cugf85",  (int) nodeDistTable.get("cugf85").distanceTo(nodeDistTable.get("cugf84")));
        graph.addEdge("cugf83","cugf84",  (int) nodeDistTable.get("cugf84").distanceTo(nodeDistTable.get("cugf83")));
        graph.addEdge("cugf88","cugf84",  (int)nodeDistTable.get("cugf84").distanceTo(nodeDistTable.get("cugf88")));
        graph.addEdge( "cugf89","cugf88", (int)nodeDistTable.get("cugf88").distanceTo(nodeDistTable.get("cugf89")));
        graph.addEdge("cugf90","cugf89",  (int)nodeDistTable.get("cugf89").distanceTo(nodeDistTable.get("cugf90")));
        graph.addEdge("cugf91","cugf90",  (int)nodeDistTable.get("cugf90").distanceTo(nodeDistTable.get("cugf91")));
        graph.addEdge("cugf82","cugf83",  (int)nodeDistTable.get("cugf83").distanceTo(nodeDistTable.get("cugf82")));
        graph.addEdge("cugf81","cugf82",  (int)nodeDistTable.get("cugf82").distanceTo(nodeDistTable.get("cugf81")));
        graph.addEdge("cugf80","cugf81",  (int)nodeDistTable.get("cugf81").distanceTo(nodeDistTable.get("cugf80")));
        graph.addEdge("cugf79","cugf80", (int)nodeDistTable.get("cugf80").distanceTo(nodeDistTable.get("cugf79")));
        graph.addEdge("cugf78","cugf79",  (int)nodeDistTable.get("cugf79").distanceTo(nodeDistTable.get("cugf78")));
        graph.addEdge("cugf91","cugf78",  (int)nodeDistTable.get("cugf78").distanceTo(nodeDistTable.get("cugf91")));
        graph.addEdge( "cugf78","cugf93", (int)nodeDistTable.get("cugf93").distanceTo(nodeDistTable.get("cugf78")));
        graph.addEdge( "cugf112","cugf91", (int)nodeDistTable.get("cugf91").distanceTo(nodeDistTable.get("cugf112")));
        graph.addEdge("cugf112","cugf93",  (int)nodeDistTable.get("cugf93").distanceTo(nodeDistTable.get("cugf112")));
        graph.addEdge("cugf112","cugf78",  (int)nodeDistTable.get("cugf78").distanceTo(nodeDistTable.get("cugf112")));
        graph.addEdge("cugf69","cugf68",  (int) nodeDistTable.get("cugf68").distanceTo(nodeDistTable.get("cugf69")));
        graph.addEdge( "cugf70","cugf69", (int) nodeDistTable.get("cugf69").distanceTo(nodeDistTable.get("cugf70")));
        graph.addEdge( "cugf71","cugf70", (int) nodeDistTable.get("cugf70").distanceTo(nodeDistTable.get("cugf71")));
        graph.addEdge("cugf72","cugf71",  (int) nodeDistTable.get("cugf71").distanceTo(nodeDistTable.get("cugf72")));
        graph.addEdge("cugf73","cugf72",  (int) nodeDistTable.get("cugf72").distanceTo(nodeDistTable.get("cugf73")));
        graph.addEdge("cugf74","cugf73",  (int)nodeDistTable.get("cugf73").distanceTo(nodeDistTable.get("cugf74")));
        graph.addEdge("cugf75","cugf74",  (int)nodeDistTable.get("cugf74").distanceTo(nodeDistTable.get("cugf75")));
        graph.addEdge( "cugf76","cugf75", (int)nodeDistTable.get("cugf75").distanceTo(nodeDistTable.get("cugf76")));
        graph.addEdge( "cugf77","cugf76", (int)nodeDistTable.get("cugf76").distanceTo(nodeDistTable.get("cugf77")));
        graph.addEdge( "cugf78","cugf77", (int)nodeDistTable.get("cugf77").distanceTo(nodeDistTable.get("cugf78")));
        graph.addEdge( "cugf113","cugf112", (int)nodeDistTable.get("cugf112").distanceTo(nodeDistTable.get("cugf113")));
        graph.addEdge("cugf114","cugf113",  (int)nodeDistTable.get("cugf113").distanceTo(nodeDistTable.get("cugf114")));
        graph.addEdge("cugf115","cugf114",  (int)nodeDistTable.get("cugf114").distanceTo(nodeDistTable.get("cugf115")));
        graph.addEdge("cugf116","cugf115",  (int)nodeDistTable.get("cugf115").distanceTo(nodeDistTable.get("cugf116")));
        graph.addEdge("cugf110","cugf116",  (int)nodeDistTable.get("cugf116").distanceTo(nodeDistTable.get("cugf110")));
        graph.addEdge( "cugf111","cugf110", (int)nodeDistTable.get("cugf110").distanceTo(nodeDistTable.get("cugf111")));
        graph.addEdge("cugf109","cugf110",  (int)nodeDistTable.get("cugf110").distanceTo(nodeDistTable.get("cugf109")));
        graph.addEdge("cugf109","cugf111",  (int)nodeDistTable.get("cugf111").distanceTo(nodeDistTable.get("cugf109")));
        graph.addEdge("cugf108","cugf109",  (int)nodeDistTable.get("cugf109").distanceTo(nodeDistTable.get("cugf108")));
        graph.addEdge("cugf107","cugf108",  (int)nodeDistTable.get("cugf108").distanceTo(nodeDistTable.get("cugf107")));
        graph.addEdge("cugf106","cugf107", (int) nodeDistTable.get("cugf107").distanceTo(nodeDistTable.get("cugf106")));
        graph.addEdge("cugf105","cugf106",  (int) nodeDistTable.get("cugf106").distanceTo(nodeDistTable.get("cugf105")));
        graph.addEdge("cugf94","cugf93",  (int) nodeDistTable.get("cugf93").distanceTo(nodeDistTable.get("cugf94")));
        graph.addEdge( "cugf95","cugf94", (int) nodeDistTable.get("cugf94").distanceTo(nodeDistTable.get("cugf95")));
        graph.addEdge("cugf96","cugf95",  (int) nodeDistTable.get("cugf95").distanceTo(nodeDistTable.get("cugf96")));
        graph.addEdge("cugf97","cugf96",  (int) nodeDistTable.get("cugf96").distanceTo(nodeDistTable.get("cugf97")));
        graph.addEdge("cugf98","cugf97",  (int)nodeDistTable.get("cugf97").distanceTo(nodeDistTable.get("cugf98")));
        graph.addEdge("cugf99","cugf96",  (int)nodeDistTable.get("cugf96").distanceTo(nodeDistTable.get("cugf99")));
        graph.addEdge("cugf100","cugf99",  (int)nodeDistTable.get("cugf99").distanceTo(nodeDistTable.get("cugf100")));
        graph.addEdge("cugf101","cugf100",  (int)nodeDistTable.get("cugf100").distanceTo(nodeDistTable.get("cugf101")));
        graph.addEdge("cugf102","cugf99",  (int)nodeDistTable.get("cugf99").distanceTo(nodeDistTable.get("cugf102")));
        graph.addEdge("cugf103","cugf102",  (int)nodeDistTable.get("cugf102").distanceTo(nodeDistTable.get("cugf103")));
        graph.addEdge("cugf104","cugf103",  (int)nodeDistTable.get("cugf103").distanceTo(nodeDistTable.get("cugf104")));
        graph.addEdge( "cugf105","cugf104", (int)nodeDistTable.get("cugf104").distanceTo(nodeDistTable.get("cugf105")));
        graph.addEdge("cugf105","cugf117",  (int)nodeDistTable.get("cugf117").distanceTo(nodeDistTable.get("cugf105")));
        graph.addEdge("cugf137","cugf104",  (int)nodeDistTable.get("cugf104").distanceTo(nodeDistTable.get("cugf137")));
        graph.addEdge("cugf117","cugf104",  (int)nodeDistTable.get("cugf104").distanceTo(nodeDistTable.get("cugf117")));
        graph.addEdge("cugf118","cugf117",  (int)nodeDistTable.get("cugf117").distanceTo(nodeDistTable.get("cugf118")));
        graph.addEdge("cugf119","cugf117",  (int)nodeDistTable.get("cugf117").distanceTo(nodeDistTable.get("cugf119")));
        graph.addEdge("cugf120","cugf119",  (int)nodeDistTable.get("cugf119").distanceTo(nodeDistTable.get("cugf120")));
        graph.addEdge("cugf121","cugf120",  (int) nodeDistTable.get("cugf120").distanceTo(nodeDistTable.get("cugf121")));
        graph.addEdge("cugf122", "cugf121", (int) nodeDistTable.get("cugf121").distanceTo(nodeDistTable.get("cugf122")));
        graph.addEdge("cugf123","cugf122",  (int) nodeDistTable.get("cugf122").distanceTo(nodeDistTable.get("cugf123")));
        graph.addEdge( "cugf124","cugf123", (int) nodeDistTable.get("cugf123").distanceTo(nodeDistTable.get("cugf124")));
        graph.addEdge("cugf125","cugf123",  (int) nodeDistTable.get("cugf123").distanceTo(nodeDistTable.get("cugf125")));
        graph.addEdge("cugf126","cugf125",  (int)nodeDistTable.get("cugf125").distanceTo(nodeDistTable.get("cugf126")));
        graph.addEdge("cugf137","cugf105",  (int)nodeDistTable.get("cugf105").distanceTo(nodeDistTable.get("cugf137")));
        graph.addEdge("cugf136","cugf137",  (int)nodeDistTable.get("cugf137").distanceTo(nodeDistTable.get("cugf136")));
        graph.addEdge("cugf135","cugf136",  (int)nodeDistTable.get("cugf136").distanceTo(nodeDistTable.get("cugf135")));
        graph.addEdge("cugf134","cugf135",  (int)nodeDistTable.get("cugf135").distanceTo(nodeDistTable.get("cugf134")));
        graph.addEdge("cugf133","cugf134",  (int)nodeDistTable.get("cugf134").distanceTo(nodeDistTable.get("cugf133")));
        graph.addEdge("cugf140","cugf133",  (int)nodeDistTable.get("cugf133").distanceTo(nodeDistTable.get("cugf140")));
        graph.addEdge( "cugf132","cugf133", (int)nodeDistTable.get("cugf133").distanceTo(nodeDistTable.get("cugf132")));
        graph.addEdge("cugf131","cugf132",  (int)nodeDistTable.get("cugf132").distanceTo(nodeDistTable.get("cugf131")));
        graph.addEdge("cugf130","cugf131",  (int)nodeDistTable.get("cugf131").distanceTo(nodeDistTable.get("cugf130")));
        graph.addEdge("cugf138","cugf137",  (int)nodeDistTable.get("cugf137").distanceTo(nodeDistTable.get("cugf138")));
        graph.addEdge("cugf139","cugf138",  (int)nodeDistTable.get("cugf138").distanceTo(nodeDistTable.get("cugf139")));
        graph.addEdge("cugf140","cugf139",  (int)nodeDistTable.get("cugf139").distanceTo(nodeDistTable.get("cugf140")));
        graph.addEdge("cugf141","cugf140",  (int)nodeDistTable.get("cugf140").distanceTo(nodeDistTable.get("cugf141")));
        graph.addEdge("cugf142","cugf141",  (int)nodeDistTable.get("cugf141").distanceTo(nodeDistTable.get("cugf142")));
        graph.addEdge("cugf143","cugf142",  (int)nodeDistTable.get("cugf142").distanceTo(nodeDistTable.get("cugf143")));
        graph.addEdge("cugf16","cugf9",  (int)nodeDistTable.get("cugf9").distanceTo(nodeDistTable.get("cugf16")));
        graph.addEdge("cugf11","cugf10",  (int)nodeDistTable.get("cugf10").distanceTo(nodeDistTable.get("cugf11")));
        graph.addEdge("cugf12","cugf10",  (int)nodeDistTable.get("cugf10").distanceTo(nodeDistTable.get("cugf12")));
        graph.addEdge("cugf14","cugf10",  (int)nodeDistTable.get("cugf10").distanceTo(nodeDistTable.get("cugf14")));
        graph.addEdge( "cugf15","cugf10", (int)nodeDistTable.get("cugf10").distanceTo(nodeDistTable.get("cugf15")));
        graph.addEdge("cugf16","cugf10",  (int)nodeDistTable.get("cugf10").distanceTo(nodeDistTable.get("cugf16")));
        graph.addEdge("cugf12","cugf11",  (int)nodeDistTable.get("cugf11").distanceTo(nodeDistTable.get("cugf12")));
        graph.addEdge("cugf15","cugf11",  (int)nodeDistTable.get("cugf11").distanceTo(nodeDistTable.get("cugf15")));
        graph.addEdge("cugf16","cugf11",  (int)nodeDistTable.get("cugf11").distanceTo(nodeDistTable.get("cugf16")));
        graph.addEdge("cugf13","cugf12",  (int)nodeDistTable.get("cugf12").distanceTo(nodeDistTable.get("cugf13")));
        graph.addEdge("cugf14","cugf12",  (int)nodeDistTable.get("cugf12").distanceTo(nodeDistTable.get("cugf14")));
        graph.addEdge("cugf15","cugf12",  (int)nodeDistTable.get("cugf12").distanceTo(nodeDistTable.get("cugf15")));
        graph.addEdge("cugf16","cugf12", (int)nodeDistTable.get("cugf12").distanceTo(nodeDistTable.get("cugf16")));
        graph.addEdge("cugf14","cugf13",  (int)nodeDistTable.get("cugf13").distanceTo(nodeDistTable.get("cugf14")));
        graph.addEdge("cugf15","cugf14",  (int)nodeDistTable.get("cugf14").distanceTo(nodeDistTable.get("cugf15")));
        graph.addEdge("cugf16","cugf15",  (int)nodeDistTable.get("cugf15").distanceTo(nodeDistTable.get("cugf16")));
        graph.addEdge( "cugf162","cugf35", (int)nodeDistTable.get("cugf35").distanceTo(nodeDistTable.get("cugf162")));
        graph.addEdge( "cugf92","cugf91", (int)nodeDistTable.get("cugf91").distanceTo(nodeDistTable.get("cugf92")));
        graph.addEdge("cugf93","cugf91",  (int)nodeDistTable.get("cugf91").distanceTo(nodeDistTable.get("cugf93")));


        DijkstraAlgorithm dijkstra = GoodDijkstraAlgorithm.getInstance();
        SingleSourceShortestPathResult<String, Integer, DirectedWeightedEdge<String, Integer>> result = dijkstra.calc(graph, srcNumber, NS);

        Iterable<DirectedWeightedEdge<String, Integer>> pathToD = result.getPath(destNumber);
        System.out.println(pathToD);
        Iterator<DirectedWeightedEdge<String, Integer>> it = pathToD.iterator();

        int count = 0;
        while(it.hasNext()){
            DirectedWeightedEdge<String, Integer> item = it.next();
            if(count == 0){
                pathVertices.add(item.from());
                pathVertices.add(item.to());
            }if(count > 0){
                pathVertices.add(item.to());
            }
            count++;
        }
        System.out.println("shortest path :"+ pathVertices);

        String path = "";
        for(String node : pathVertices){
            path = path + node + "*";
        }

        GlobalState.drawPath = path;

        //Getting junction nodes for taking bearings
        resultantJunctionNodes.clear();
        for(int i=0; i < pathVertices.size(); i++){
            for(String node : junctionNodes){
                if(pathVertices.get(i).equals(node)){
                    resultantJunctionNodes.add(node);
                }
            }
        }

        if(!juctionNodeLocation.isEmpty())
            juctionNodeLocation.clear();

        for(int i = 0; i < resultantJunctionNodes.size(); i++){
            juctionNodeLocation.add(nodeDistTable.get(resultantJunctionNodes.get(i)));
        }

        for (int i=0; i < resultantJunctionNodes.size(); i++){
            if(nearestToSource.equals(resultantJunctionNodes.get(i))){
                resultantJunctionNodes.remove(i);
            }
        }

        float preSrcBearing =  getBearings(srcBearingLoc, nodeDistTable.get(nearestToSource));
        bearingRJNHash.put(preSrcBearing, "NA");
        Location dst = null;
        Location src = null;
        if(resultantJunctionNodes.size() == 0){
            src = nodeDistTable.get(nearestToSource);
            dst = nodeDistTable.get(nearestToDest);
        }else {
            src = nodeDistTable.get(nearestToSource);
            dst = nodeDistTable.get(resultantJunctionNodes.get(0));
        }

        float srcBearing =  getBearings(src, dst);

        if(resultantJunctionNodes.size() ==0)
            bearingRJNHash.put(srcBearing, nearestToDest);
        else
            bearingRJNHash.put(srcBearing, resultantJunctionNodes.get(0));

        bearingsList.add(preSrcBearing);
        bearingsList.add(srcBearing);

        for (int i=0; i < resultantJunctionNodes.size()-1; i++){

            Location firstLoc = nodeDistTable.get(resultantJunctionNodes.get(i));
            Location secondLoc = nodeDistTable.get(resultantJunctionNodes.get(i + 1));
            float resultantBearing =  getBearings(firstLoc, secondLoc);

            bearingRJNHash.put(resultantBearing,resultantJunctionNodes.get(i + 1));

            bearingsList.add(resultantBearing);
        }

        if(resultantJunctionNodes.size() > 2){
            for (int i=0; i < resultantJunctionNodes.size(); i++){
                if(nearestToDest.equals(resultantJunctionNodes.get(i))){
                    resultantJunctionNodes.remove(i);
                }
            }
        }

        float preDestBearing = 0;

        if(resultantJunctionNodes.size() == 0)
            preDestBearing =  getBearings(nodeDistTable.get(nearestToDest),nodeDistTable.get(nearestToDest) );
        else
            preDestBearing =  getBearings(nodeDistTable.get(resultantJunctionNodes.get(resultantJunctionNodes.size()-1)),nodeDistTable.get(nearestToDest) );

        bearingRJNHash.put(preDestBearing, nearestToDest);
        float destBearing =  getBearings(nodeDistTable.get(nearestToDest), destBearingLoc );
        bearingRJNHash.put(destBearing, destNumber);
        bearingsList.add(preDestBearing);
        bearingsList.add(destBearing);

        TextNav textNav = new TextNav(srcBearingLoc, destBearingLoc, pathVertices, nodeDistTable, sortedStoreList, floorName);
        if(!tnForUGF.isEmpty())
            tnForUGF.clear();

        GlobalState.tnPathUGF = "";
        GlobalState.tnSrcStoreUGF = "";
        GlobalState.tnDestStoreUGF = "";

        tnForUGF = textNav.getTextNav();
        for(int i =0; i < tnForUGF.size(); i++){
            GlobalState.tnPathUGF = GlobalState.tnPathUGF + tnForUGF.get(i) + "#";
        }
        GlobalState.tnSrcStoreUGF = srcStoreName;
        GlobalState.tnDestStoreUGF = destStoreName;

        return pathVertices;
    }

    private float getBearings(Location sLoc, Location dLoc){
        float bearing = 0;

        Location firstLoc = sLoc;
        Location secondLoc = dLoc;
        bearing = firstLoc.bearingTo(secondLoc);
        return bearing;
    }
}
