package com.expressavenue.mallapp.algorithm;

import android.location.Location;

import com.expressavenue.mallapp.global.GlobalState;
import com.expressavenue.mallapp.pojos.Store;
import com.expressavenue.mallapp.utils.TextNav;

import org.psjava.algo.graph.shortestpath.DijkstraAlgorithm;
import org.psjava.algo.graph.shortestpath.SingleSourceShortestPathResult;
import org.psjava.ds.graph.DirectedWeightedEdge;
import org.psjava.ds.graph.MutableDirectedWeightedGraph;
import org.psjava.ds.numbersystrem.IntegerNumberSystem;
import org.psjava.goods.GoodDijkstraAlgorithm;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;

public class DijkstraClassForSF {

    private String srcNumber ="";
    private String srcLat ="";
    private String srcLong ="";

    private String destNumber ="";
    private String destLat ="";
    private String destLong ="";

    private String nearestToSource;
    private String nearestToDest;
    private double sWeight;
    private double dWeight;
    private String nearestToS[];
    private String nearestToD[];

    private ArrayList<String> tnForSF;

    private Location srcBearingLoc;
    private Location destBearingLoc;

    private String junctionNodes[]= {"csf1","csf2","csf3","csf5","csf6","csf7","csf37","csf16", "csf19",
                                            "csf23","csf28","csf14","csf36","csf45","csf59","csf65","csf66", "csf44",
                                             "csf49","csf51","csf55","csf56","csf70","csf76","csf75","csf74","csf71","csf69","csf62"};

    private ArrayList<String> resultantJunctionNodes;
    private  Location lcff1;
    private ArrayList<Float> bearingsList;

    private ArrayList<String> nearestStoreName;

    private Hashtable<Location, String> nodeHash;
    private Hashtable<String, Location> nodeDistTable;

    private Hashtable<Float, String> bearingRJNHash;

    private ArrayList<Store> sortedStoreList;

    private String srcStoreName = "";
    private String destStoreName = "";

    private String floorName = "";

    private ArrayList<Location> juctionNodeLocation;

    public DijkstraClassForSF(String srcNumber, String srcLat, String srcLong,
                              String destNumber, String destLat, String destLong, ArrayList<Store> sortedStoreList, String floorName){

        juctionNodeLocation = new ArrayList<Location>();


        for(Store store : sortedStoreList){

            if(store.getStoreNumber().equals(srcNumber)){
                srcStoreName = store.getStoreName();
            }
            if(store.getStoreNumber().equals(destNumber)){
                destStoreName = store.getStoreName();
            }
        }

        if(srcNumber.equals("LT120"))
            srcStoreName = "Main Lift";
        if(srcNumber.equals("LT120LGF") || srcNumber.equals("LT120UGF") || srcNumber.equals("LT120FF") || srcNumber.equals("LT120SF")
                || srcNumber.equals("LT120TF") || srcNumber.equals("LT120B1")|| srcNumber.equals("LT120B2")|| srcNumber.equals("LT120B3") )
            srcStoreName = "Main Lift";
        else if(srcNumber.equals("ESC"))
            srcStoreName = "Escalator";
        else if(srcNumber.equals("ESC"))
            srcStoreName = "Escalator";
        else if(srcNumber.equals("ESC-Left"))
            srcStoreName = "Escalator";
        else if(srcNumber.equals("ESC-Right"))
            srcStoreName = "Escalator";

        if(destNumber.equals("LT120"))
            destStoreName = "Main Lift";
        if(destNumber.equals("LT120LGF") || destNumber.equals("LT120UGF") || destNumber.equals("LT120FF") || destNumber.equals("LT120SF")
                || destNumber.equals("LT120TF") || destNumber.equals("LT120B1")|| destNumber.equals("LT120B2")|| destNumber.equals("LT120B3") )
            destStoreName = "Main Lift";
        else if(destNumber.equals("ESC"))
            destStoreName = "Escalator";
        else if(destNumber.equals("ESC"))
            destStoreName = "Escalator";
        else if(destNumber.equals("ESC-Left"))
            destStoreName = "Escalator";
        else if(destNumber.equals("ESC-Right"))
            destStoreName = "Escalator";

        nodeHash = new Hashtable<Location, String>();
        nodeDistTable = new Hashtable<String, Location>();
        resultantJunctionNodes = new ArrayList<String >();
        bearingsList = new ArrayList<Float>();
        nearestStoreName = new ArrayList<String>();

        bearingRJNHash = new Hashtable<Float, String>();

        if(!bearingRJNHash.isEmpty())
            bearingRJNHash.clear();

        if(!nearestStoreName.isEmpty())
            nearestStoreName.clear();

        this.srcNumber = srcNumber;
        this.srcLat = srcLat;
        this.srcLong = srcLong;

        this.destNumber = destNumber;
        this.destLat = destLat;
        this.destLong = destLong;

        this.sortedStoreList = new ArrayList<Store>();
        this.sortedStoreList.addAll(sortedStoreList);

        this.floorName = floorName;

        tnForSF = new ArrayList<String>();

        setNodeHash(13.058664, 80.263144, "csf1");
        setNodeHash(13.058615,80.263188, "csf2");
        setNodeHash(13.058611,80.263273, "csf3");
        setNodeHash(13.058637,80.263329, "csf4");
        setNodeHash(13.058711,80.263358, "csf5");
        setNodeHash(13.058716,80.263172, "csf6");
        setNodeHash(13.05874,80.263244, "csf7");
        setNodeHash(13.058646,80.263736, "csf8");
        setNodeHash(13.058646,80.263693, "csf9");
        setNodeHash(13.058646,80.263616, "csf10");
        setNodeHash(13.058646,80.263544, "csf11");
        setNodeHash(13.058646,80.263501, "csf12");
        setNodeHash(13.058588,80.263434, "csf13");
        setNodeHash(13.058646,80.263433, "csf14");
        setNodeHash(13.05856,80.263434, "csf15");
        setNodeHash(13.058711,80.263462, "csf16");
        setNodeHash(13.058766,80.263464, "csf17");
        setNodeHash(13.058799,80.263462, "csf18");
        setNodeHash(13.058831,80.263462, "csf19");
        setNodeHash(13.058831,80.263531, "csf20");
        setNodeHash(13.058831,80.263615, "csf21");
        setNodeHash(13.058831,80.263701, "csf22");
        setNodeHash(13.058831,80.263773, "csf23");
        setNodeHash(13.05885,80.263772, "csf24");
        setNodeHash(13.0588,80.263772, "csf25");
        setNodeHash(13.058757,80.263772, "csf26");
        setNodeHash(13.058711,80.263736, "csf27");
        setNodeHash(13.058711,80.263772, "csf28");
        setNodeHash(13.058711,80.263694, "csf29");
        setNodeHash(13.058711,80.263651, "csf30");
        setNodeHash(13.058711,80.263594, "csf31");
        setNodeHash(13.058711,80.26355, "csf32");
        setNodeHash(13.058832,80.263751, "csf33");
        setNodeHash(13.058569,80.263772, "csf34");
        setNodeHash(13.05855, 80.263773, "csf35");
        setNodeHash(13.058646, 80.263773, "csf36");
        setNodeHash(13.058646, 80.263357, "csf37");
        setNodeHash(13.058721,80.263329, "csf38");
        setNodeHash(13.058538,80.263186, "csf39");
        setNodeHash(13.058472,80.263187, "csf40");
        setNodeHash(13.058558,80.263273, "csf41");
        setNodeHash(13.058534,80.263272, "csf42");
        setNodeHash(13.058471,80.263272, "csf43");
        setNodeHash(13.058646,80.263801, "csf44");
        setNodeHash(13.058711,80.2638, "csf45");
        setNodeHash(13.05862,80.263844, "csf46");
        setNodeHash(13.058608,80.26388, "csf47");
        setNodeHash(13.058598,80.263923, "csf48");
        setNodeHash(13.058591,80.263966, "csf49");
        setNodeHash(13.058534,80.263965, "csf50");
        setNodeHash(13.058591,80.264073, "csf51");
        setNodeHash(13.058534,80.264072, "csf52");
        setNodeHash(13.058606,80.264143, "csf53");
        setNodeHash(13.058633,80.264201, "csf54");
        setNodeHash(13.058654,80.264243, "csf55");
        setNodeHash(13.058654,80.264261, "csf56");
        setNodeHash(13.058586,80.264261, "csf57");
        setNodeHash(13.058753,80.263886, "csf58");
        setNodeHash(13.058758,80.263923, "csf59");
        setNodeHash(13.058764,80.263986, "csf60");
        setNodeHash(13.058764,80.264031, "csf61");
        setNodeHash(13.058764,80.264072, "csf62");
        setNodeHash(13.058752,80.264145, "csf63");
        setNodeHash(13.058723,80.264201, "csf64");
        setNodeHash(13.05871,80.264244, "csf65");
        setNodeHash(13.058709,80.264261, "csf66");
        setNodeHash(13.058779,80.264261, "csf67");
        setNodeHash(13.058831,80.264261, "csf68");
        setNodeHash(13.058709,80.264532, "csf69");
        setNodeHash(13.058657,80.264532, "csf70");
        setNodeHash(13.058709,80.264572, "csf71");
        setNodeHash(13.058797, 80.264573, "csf72");
        setNodeHash(13.058845, 80.264572, "csf73");
        setNodeHash(13.058709, 80.264689, "csf74");
        setNodeHash(13.058646, 80.264689, "csf75");
        setNodeHash(13.058646, 80.264605, "csf76");
        setNodeHash(13.058553, 80.264605, "csf77");
        setNodeHash(13.058655, 80.264493, "csf78");
        setNodeHash(13.058655, 80.264444, "csf79");
        setNodeHash(13.058655, 80.264401, "csf80");
        setNodeHash(13.058655, 80.264344, "csf81");
        setNodeHash(13.058655, 80.264287, "csf82");

        setDistHash(13.058664, 80.263144, "csf1");
        setDistHash(13.058615, 80.263188, "csf2");
        setDistHash(13.058611, 80.263273, "csf3");
        setDistHash(13.058637, 80.263329, "csf4");
        setDistHash(13.058711, 80.263358, "csf5");
        setDistHash(13.058716, 80.263172, "csf6");
        setDistHash(13.05874, 80.263244, "csf7");
        setDistHash(13.058646, 80.263736, "csf8");
        setDistHash(13.058646, 80.263693, "csf9");
        setDistHash(13.058646, 80.263616, "csf10");
        setDistHash(13.058646, 80.263544, "csf11");
        setDistHash(13.058646, 80.263501, "csf12");
        setDistHash(13.058588, 80.263434, "csf13");
        setDistHash(13.058646, 80.263433, "csf14");
        setDistHash(13.05856, 80.263434, "csf15");
        setDistHash(13.058711, 80.263462, "csf16");
        setDistHash(13.058766, 80.263464, "csf17");
        setDistHash(13.058799, 80.263462, "csf18");
        setDistHash(13.058831, 80.263462, "csf19");
        setDistHash(13.058831, 80.263531, "csf20");
        setDistHash(13.058831, 80.263615, "csf21");
        setDistHash(13.058831, 80.263701, "csf22");
        setDistHash(13.058831, 80.263773, "csf23");
        setDistHash(13.05885, 80.263772, "csf24");
        setDistHash(13.0588, 80.263772, "csf25");
        setDistHash(13.058757, 80.263772, "csf26");
        setDistHash(13.058711, 80.263736, "csf27");
        setDistHash(13.058711, 80.263772, "csf28");
        setDistHash(13.058711, 80.263694, "csf29");
        setDistHash(13.058711, 80.263651, "csf30");
        setDistHash(13.058711, 80.263594, "csf31");
        setDistHash(13.058711, 80.26355, "csf32");
        setDistHash(13.058832, 80.263751, "csf33");
        setDistHash(13.058569, 80.263772, "csf34");
        setDistHash(13.05855, 80.263773, "csf35");
        setDistHash(13.058646, 80.263773, "csf36");
        setDistHash(13.058646, 80.263357, "csf37");
        setDistHash(13.058721, 80.263329, "csf38");
        setDistHash(13.058538, 80.263186, "csf39");
        setDistHash(13.058472, 80.263187, "csf40");
        setDistHash(13.058558, 80.263273, "csf41");
        setDistHash(13.058534, 80.263272, "csf42");
        setDistHash(13.058471, 80.263272, "csf43");
        setDistHash(13.058646, 80.263801, "csf44");
        setDistHash(13.058711, 80.2638, "csf45");
        setDistHash(13.05862, 80.263844, "csf46");
        setDistHash(13.058608, 80.26388, "csf47");
        setDistHash(13.058598, 80.263923, "csf48");
        setDistHash(13.058591, 80.263966, "csf49");
        setDistHash(13.058534, 80.263965, "csf50");
        setDistHash(13.058591, 80.264073, "csf51");
        setDistHash(13.058534, 80.264072, "csf52");
        setDistHash(13.058606, 80.264143, "csf53");
        setDistHash(13.058633, 80.264201, "csf54");
        setDistHash(13.058654, 80.264243, "csf55");
        setDistHash(13.058654, 80.264261, "csf56");
        setDistHash(13.058586, 80.264261, "csf57");
        setDistHash(13.058753, 80.263886, "csf58");
        setDistHash(13.058758, 80.263923, "csf59");
        setDistHash(13.058764, 80.263986, "csf60");
        setDistHash(13.058764, 80.264031, "csf61");
        setDistHash(13.058764, 80.264072, "csf62");
        setDistHash(13.058752, 80.264145, "csf63");
        setDistHash(13.058723, 80.264201, "csf64");
        setDistHash(13.05871, 80.264244, "csf65");
        setDistHash(13.058709, 80.264261, "csf66");
        setDistHash(13.058779, 80.264261, "csf67");
        setDistHash(13.058831, 80.264261, "csf68");
        setDistHash(13.058709, 80.264532, "csf69");
        setDistHash(13.058657, 80.264532, "csf70");
        setDistHash(13.058709, 80.264572, "csf71");
        setDistHash(13.058797, 80.264573, "csf72");
        setDistHash(13.058845, 80.264572, "csf73");
        setDistHash(13.058709, 80.264689, "csf74");
        setDistHash(13.058646, 80.264689, "csf75");
        setDistHash(13.058646, 80.264605, "csf76");
        setDistHash(13.058553, 80.264605, "csf77");
        setDistHash(13.058655, 80.264493, "csf78");
        setDistHash(13.058655, 80.264444, "csf79");
        setDistHash(13.058655, 80.264401, "csf80");
        setDistHash(13.058655, 80.264344, "csf81");
        setDistHash(13.058655, 80.264287, "csf82");

        setDistHash(Double.parseDouble(srcLat), Double.parseDouble(srcLong), this.srcNumber);
        setDistHash(Double.parseDouble(destLat), Double.parseDouble(destLong), this.destNumber);

        Location srcLocation = new Location("");
        srcLocation.setLatitude(Double.parseDouble(this.srcLat));
        srcLocation.setLongitude(Double.parseDouble(this.srcLong));
        srcBearingLoc = srcLocation;
        nearestToS = getNearesLocation(srcLocation, nodeHash);
        nearestToSource = nearestToS[0];
        sWeight = Double.parseDouble(nearestToS[1]);

        Location destLocation = new Location("");
        destLocation.setLatitude(Double.parseDouble(this.destLat));
        destLocation.setLongitude(Double.parseDouble(this.destLong));
        destBearingLoc = destLocation;
        nearestToD = getNearesLocation(destLocation, nodeHash);
        nearestToDest = nearestToD[0];
        dWeight = Double.parseDouble(nearestToD[1]);
    }

    private void setNodeHash(double lat, double lon, String storeNo)
    {
        lcff1 = new Location(storeNo);
        lcff1.setLatitude(lat);
        lcff1.setLongitude(lon);
        nodeHash.put(lcff1,storeNo);
    }

    private void setDistHash(double lat, double lon, String storeNo)
    {
        lcff1 = new Location(storeNo);
        lcff1.setLatitude(lat);
        lcff1.setLongitude(lon);
        nodeDistTable.put(storeNo, lcff1);
    }

    public String[] getNearesLocation(Location srcLoc, Hashtable<Location, String> srcHash){

        String nearestStore[] = new String[2];
        ArrayList<Double> distValues = new ArrayList<Double>();
        Hashtable<Double, Location> distHash = new Hashtable<Double, Location>();
        Set<Location> locSet = srcHash.keySet();
        for(Location location : locSet){

            double dist = location.distanceTo(srcLoc);
            distValues.add(dist);
            distHash.put(dist,location);
        }

        double smallest = Double.MAX_VALUE;
        for(int i =0;i<distValues.size();i++) {
            if(smallest > distValues.get(i)) {
                smallest = distValues.get(i);
            }
        }

        Location nearLoc = distHash.get(smallest);
        nearestStore[0] = srcHash.get(nearLoc);
        nearestStore[1] = String.valueOf(smallest);

        return nearestStore;
    }

    public ArrayList<String> algorithmImplementation() {

        ArrayList<String> pathVertices = new ArrayList<>();
        pathVertices.clear();

        IntegerNumberSystem NS = IntegerNumberSystem.getInstance();

        MutableDirectedWeightedGraph<String, Integer> graph = MutableDirectedWeightedGraph.create();

        graph.insertVertex("csf1");
        graph.insertVertex("csf2");
        graph.insertVertex("csf3");
        graph.insertVertex("csf4");
        graph.insertVertex("csf5");
        graph.insertVertex("csf6");
        graph.insertVertex("csf7");
        graph.insertVertex("csf8");
        graph.insertVertex("csf9");
        graph.insertVertex("csf10");
        graph.insertVertex("csf11");
        graph.insertVertex("csf12");
        graph.insertVertex("csf13");
        graph.insertVertex("csf14");
        graph.insertVertex("csf15");
        graph.insertVertex("csf16");
        graph.insertVertex("csf17");
        graph.insertVertex("csf18");
        graph.insertVertex("csf19");
        graph.insertVertex("csf20");
        graph.insertVertex("csf21");
        graph.insertVertex("csf22");
        graph.insertVertex("csf23");
        graph.insertVertex("csf24");
        graph.insertVertex("csf25");
        graph.insertVertex("csf26");
        graph.insertVertex("csf27");
        graph.insertVertex("csf28");
        graph.insertVertex("csf29");
        graph.insertVertex("csf30");
        graph.insertVertex("csf31");
        graph.insertVertex("csf32");
        graph.insertVertex("csf33");
        graph.insertVertex("csf34");
        graph.insertVertex("csf35");
        graph.insertVertex("csf36");
        graph.insertVertex("csf37");
        graph.insertVertex("csf38");
        graph.insertVertex("csf39");
        graph.insertVertex("csf40");
        graph.insertVertex("csf41");
        graph.insertVertex("csf42");
        graph.insertVertex("csf43");
        graph.insertVertex("csf44");
        graph.insertVertex("csf45");
        graph.insertVertex("csf46");
        graph.insertVertex("csf47");
        graph.insertVertex("csf48");
        graph.insertVertex("csf49");
        graph.insertVertex("csf50");
        graph.insertVertex("csf51");
        graph.insertVertex("csf52");
        graph.insertVertex("csf53");
        graph.insertVertex("csf54");
        graph.insertVertex("csf55");
        graph.insertVertex("csf56");
        graph.insertVertex("csf57");
        graph.insertVertex("csf58");
        graph.insertVertex("csf59");
        graph.insertVertex("csf60");
        graph.insertVertex("csf61");
        graph.insertVertex("csf62");
        graph.insertVertex("csf63");
        graph.insertVertex("csf64");
        graph.insertVertex("csf65");
        graph.insertVertex("csf66");
        graph.insertVertex("csf67");
        graph.insertVertex("csf68");
        graph.insertVertex("csf69");
        graph.insertVertex("csf70");
        graph.insertVertex("csf71");
        graph.insertVertex("csf72");
        graph.insertVertex("csf73");
        graph.insertVertex("csf74");
        graph.insertVertex("csf75");
        graph.insertVertex("csf76");
        graph.insertVertex("csf77");
        graph.insertVertex("csf78");
        graph.insertVertex("csf79");
        graph.insertVertex("csf80");
        graph.insertVertex("csf81");
        graph.insertVertex("csf82");

        graph.insertVertex(srcNumber);
        graph.insertVertex(destNumber);

        graph.addEdge(srcNumber, nearestToSource, (int) sWeight);
        graph.addEdge(nearestToDest, destNumber, (int) dWeight);

        graph.addEdge(nearestToSource, srcNumber, (int) sWeight);
        graph.addEdge(destNumber, nearestToDest, (int) dWeight);

        graph.addEdge("csf1", "csf2", (int) nodeDistTable.get("csf1").distanceTo(nodeDistTable.get("csf2")));
        graph.addEdge("csf1", "csf6", (int) nodeDistTable.get("csf1").distanceTo(nodeDistTable.get("csf6")));
        graph.addEdge("csf2", "csf39",(int)nodeDistTable.get("csf2").distanceTo(nodeDistTable.get("csf39")) );
        graph.addEdge("csf39", "csf40",(int)nodeDistTable.get("csf39").distanceTo(nodeDistTable.get("csf40")));
        graph.addEdge("csf2", "csf3",(int)nodeDistTable.get("csf2").distanceTo(nodeDistTable.get("csf3")) );
        graph.addEdge("csf3", "csf41",(int)nodeDistTable.get("csf3").distanceTo(nodeDistTable.get("csf41")) );
        graph.addEdge("csf41", "csf42",(int)nodeDistTable.get("csf41").distanceTo(nodeDistTable.get("csf42")) );
        graph.addEdge("csf42", "csf43", (int)nodeDistTable.get("csf42").distanceTo(nodeDistTable.get("csf43")));
        graph.addEdge("csf3", "csf4", (int)nodeDistTable.get("csf3").distanceTo(nodeDistTable.get("csf4")));
        graph.addEdge("csf4", "csf37", (int)nodeDistTable.get("csf4").distanceTo(nodeDistTable.get("csf37")));
        graph.addEdge("csf37", "csf14", (int)nodeDistTable.get("csf37").distanceTo(nodeDistTable.get("csf14")));
        graph.addEdge("csf14", "csf13", (int)nodeDistTable.get("csf14").distanceTo(nodeDistTable.get("csf13")));
        graph.addEdge("csf13", "csf15", (int)nodeDistTable.get("csf13").distanceTo(nodeDistTable.get("csf15")));
        graph.addEdge("csf14", "csf12", (int)nodeDistTable.get("csf14").distanceTo(nodeDistTable.get("csf12")));
        graph.addEdge("csf12", "csf11", (int)nodeDistTable.get("csf12").distanceTo(nodeDistTable.get("csf11")));
        graph.addEdge("csf11", "csf10", (int)nodeDistTable.get("csf11").distanceTo(nodeDistTable.get("csf10")));
        graph.addEdge("csf10", "csf9", (int)nodeDistTable.get("csf10").distanceTo(nodeDistTable.get("csf9")));
        graph.addEdge("csf9", "csf8", (int)nodeDistTable.get("csf9").distanceTo(nodeDistTable.get("csf8")));
        graph.addEdge("csf8", "csf36", (int)nodeDistTable.get("csf8").distanceTo(nodeDistTable.get("csf36")));
        graph.addEdge("csf36", "csf34", (int)nodeDistTable.get("csf36").distanceTo(nodeDistTable.get("csf34")));
        graph.addEdge("csf34", "csf35", (int)nodeDistTable.get("csf34").distanceTo(nodeDistTable.get("csf35")));
        graph.addEdge("csf36", "csf44", (int)nodeDistTable.get("csf36").distanceTo(nodeDistTable.get("csf44")));
        graph.addEdge("csf44", "csf46", (int)nodeDistTable.get("csf44").distanceTo(nodeDistTable.get("csf46")));
        graph.addEdge("csf46", "csf47",(int)nodeDistTable.get("csf46").distanceTo(nodeDistTable.get("csf47")) );
        graph.addEdge("csf47", "csf48",(int)nodeDistTable.get("csf47").distanceTo(nodeDistTable.get("csf48")) );
        graph.addEdge("csf48", "csf49",(int)nodeDistTable.get("csf48").distanceTo(nodeDistTable.get("csf49")) );
        graph.addEdge("csf49", "csf50", (int)nodeDistTable.get("csf49").distanceTo(nodeDistTable.get("csf50")));
        graph.addEdge("csf49", "csf51",(int)nodeDistTable.get("csf49").distanceTo(nodeDistTable.get("csf51")) );
        graph.addEdge("csf51", "csf52", (int)nodeDistTable.get("csf51").distanceTo(nodeDistTable.get("csf52")));
        graph.addEdge("csf51", "csf53", (int)nodeDistTable.get("csf51").distanceTo(nodeDistTable.get("csf53")));
        graph.addEdge("csf53", "csf54", (int)nodeDistTable.get("csf53").distanceTo(nodeDistTable.get("csf54")));
        graph.addEdge("csf54", "csf55",(int)nodeDistTable.get("csf54").distanceTo(nodeDistTable.get("csf55")) );
        graph.addEdge("csf55", "csf56",(int)nodeDistTable.get("csf55").distanceTo(nodeDistTable.get("csf56")) );
        graph.addEdge("csf56", "csf57", (int)nodeDistTable.get("csf56").distanceTo(nodeDistTable.get("csf57")));
        graph.addEdge("csf56", "csf70", (int)nodeDistTable.get("csf56").distanceTo(nodeDistTable.get("csf70")));
        graph.addEdge("csf77", "csf76", (int)nodeDistTable.get("csf77").distanceTo(nodeDistTable.get("csf76")));
        graph.addEdge("csf76", "csf75", (int)nodeDistTable.get("csf76").distanceTo(nodeDistTable.get("csf75")));
        graph.addEdge("csf75", "csf74",(int)nodeDistTable.get("csf75").distanceTo(nodeDistTable.get("csf74")) );
        graph.addEdge("csf74", "csf71", (int)nodeDistTable.get("csf74").distanceTo(nodeDistTable.get("csf71")));
        graph.addEdge("csf71", "csf72", (int)nodeDistTable.get("csf71").distanceTo(nodeDistTable.get("csf72")));
        graph.addEdge("csf72", "csf73",(int)nodeDistTable.get("csf72").distanceTo(nodeDistTable.get("csf73")) );
        graph.addEdge("csf73", "csf68", (int)nodeDistTable.get("csf73").distanceTo(nodeDistTable.get("csf68")));
        graph.addEdge("csf71", "csf69",(int)nodeDistTable.get("csf71").distanceTo(nodeDistTable.get("csf69")));
        graph.addEdge("csf69", "csf70",(int)nodeDistTable.get("csf69").distanceTo(nodeDistTable.get("csf70")) );
        graph.addEdge("csf69", "csf66", (int)nodeDistTable.get("csf69").distanceTo(nodeDistTable.get("csf66")));
        graph.addEdge("csf66", "csf67",(int)nodeDistTable.get("csf66").distanceTo(nodeDistTable.get("csf67")) );
        graph.addEdge("csf67", "csf68",(int)nodeDistTable.get("csf67").distanceTo(nodeDistTable.get("csf68")) );
        graph.addEdge("csf66", "csf65",(int)nodeDistTable.get("csf66").distanceTo(nodeDistTable.get("csf65")) );
        graph.addEdge("csf65", "csf64", (int)nodeDistTable.get("csf65").distanceTo(nodeDistTable.get("csf64")));
        graph.addEdge("csf64", "csf63", (int)nodeDistTable.get("csf64").distanceTo(nodeDistTable.get("csf63")));
        graph.addEdge("csf63", "csf62", (int)nodeDistTable.get("csf63").distanceTo(nodeDistTable.get("csf62")));
        graph.addEdge("csf62", "csf61", (int)nodeDistTable.get("csf62").distanceTo(nodeDistTable.get("csf61")));
        graph.addEdge("csf61", "csf60", (int)nodeDistTable.get("csf61").distanceTo(nodeDistTable.get("csf60")));
        graph.addEdge("csf60", "csf59", (int)nodeDistTable.get("csf60").distanceTo(nodeDistTable.get("csf59")));
        graph.addEdge("csf59", "csf58", (int)nodeDistTable.get("csf59").distanceTo(nodeDistTable.get("csf58")));
        graph.addEdge("csf58", "csf45", (int)nodeDistTable.get("csf58").distanceTo(nodeDistTable.get("csf45")));
        graph.addEdge("csf45", "csf28", (int)nodeDistTable.get("csf45").distanceTo(nodeDistTable.get("csf28")));
        graph.addEdge("csf28", "csf26", (int)nodeDistTable.get("csf28").distanceTo(nodeDistTable.get("csf26")));
        graph.addEdge("csf26", "csf25", (int)nodeDistTable.get("csf26").distanceTo(nodeDistTable.get("csf25")));
        graph.addEdge("csf25", "csf23", (int)nodeDistTable.get("csf25").distanceTo(nodeDistTable.get("csf23")));
        graph.addEdge("csf23", "csf24", (int)nodeDistTable.get("csf23").distanceTo(nodeDistTable.get("csf24")));
        graph.addEdge("csf22", "csf33", (int)nodeDistTable.get("csf22").distanceTo(nodeDistTable.get("csf33")));
        graph.addEdge("csf33", "csf22", (int)nodeDistTable.get("csf33").distanceTo(nodeDistTable.get("csf22")));
        graph.addEdge("csf22", "csf21", (int)nodeDistTable.get("csf22").distanceTo(nodeDistTable.get("csf21")));
        graph.addEdge("csf21", "csf20",(int)nodeDistTable.get("csf21").distanceTo(nodeDistTable.get("csf20")) );
        graph.addEdge("csf20", "csf19",(int)nodeDistTable.get("csf20").distanceTo(nodeDistTable.get("csf19")) );
        graph.addEdge("csf19", "csf18",(int)nodeDistTable.get("csf19").distanceTo(nodeDistTable.get("csf18")) );
        graph.addEdge("csf18", "csf17", (int)nodeDistTable.get("csf18").distanceTo(nodeDistTable.get("csf17")));
        graph.addEdge("csf17", "csf16",(int)nodeDistTable.get("csf17").distanceTo(nodeDistTable.get("csf16")) );
        graph.addEdge("csf28", "csf27", (int)nodeDistTable.get("csf28").distanceTo(nodeDistTable.get("csf27")));
        graph.addEdge("csf27", "csf29", (int)nodeDistTable.get("csf27").distanceTo(nodeDistTable.get("csf29")));
        graph.addEdge("csf29", "csf30", (int)nodeDistTable.get("csf29").distanceTo(nodeDistTable.get("csf30")));
        graph.addEdge("csf30", "csf31",(int)nodeDistTable.get("csf30").distanceTo(nodeDistTable.get("csf31")) );
        graph.addEdge("csf31", "csf32",(int)nodeDistTable.get("csf31").distanceTo(nodeDistTable.get("csf32")) );
        graph.addEdge("csf32", "csf16", (int)nodeDistTable.get("csf32").distanceTo(nodeDistTable.get("csf16")));
        graph.addEdge("csf16", "csf5", (int)nodeDistTable.get("csf16").distanceTo(nodeDistTable.get("csf5")));
        graph.addEdge("csf5", "csf38", (int)nodeDistTable.get("csf5").distanceTo(nodeDistTable.get("csf38")));
        graph.addEdge("csf38", "csf7", (int)nodeDistTable.get("csf38").distanceTo(nodeDistTable.get("csf7")));
        graph.addEdge("csf7", "csf6",(int)nodeDistTable.get("csf7").distanceTo(nodeDistTable.get("csf6")) );
        graph.addEdge("csf37", "csf5", (int)nodeDistTable.get("csf37").distanceTo(nodeDistTable.get("csf5")));
        graph.addEdge("csf36", "csf28", (int)nodeDistTable.get("csf36").distanceTo(nodeDistTable.get("csf28")));
        graph.addEdge("csf44", "csf45",(int)nodeDistTable.get("csf44").distanceTo(nodeDistTable.get("csf45")) );
        graph.addEdge("csf36", "csf45", (int)nodeDistTable.get("csf36").distanceTo(nodeDistTable.get("csf45")));
        graph.addEdge("csf44", "csf28", (int)nodeDistTable.get("csf44").distanceTo(nodeDistTable.get("csf28")));
        graph.addEdge("csf55", "csf65", (int)nodeDistTable.get("csf55").distanceTo(nodeDistTable.get("csf65")));
        graph.addEdge("csf56", "csf66",(int)nodeDistTable.get("csf56").distanceTo(nodeDistTable.get("csf66")) );
        graph.addEdge("csf55", "csf66", (int)nodeDistTable.get("csf55").distanceTo(nodeDistTable.get("csf66")));
        graph.addEdge("csf56", "csf65", (int)nodeDistTable.get("csf56").distanceTo(nodeDistTable.get("csf65")));
        graph.addEdge("csf70", "csf69",(int)nodeDistTable.get("csf70").distanceTo(nodeDistTable.get("csf69")) );

        graph.addEdge("csf70", "csf78",(int)nodeDistTable.get("csf70").distanceTo(nodeDistTable.get("csf78")) );
        graph.addEdge("csf78", "csf79",(int)nodeDistTable.get("csf78").distanceTo(nodeDistTable.get("csf79")) );
        graph.addEdge("csf79", "csf80",(int)nodeDistTable.get("csf79").distanceTo(nodeDistTable.get("csf80")) );
        graph.addEdge("csf80", "csf81",(int)nodeDistTable.get("csf80").distanceTo(nodeDistTable.get("csf81")) );
        graph.addEdge("csf81", "csf82",(int)nodeDistTable.get("csf81").distanceTo(nodeDistTable.get("csf82")) );
        graph.addEdge("csf82", "csf56",(int)nodeDistTable.get("csf82").distanceTo(nodeDistTable.get("csf56")) );


        graph.addEdge("csf2","csf1", (int)nodeDistTable.get("csf2").distanceTo(nodeDistTable.get("csf1")));
        graph.addEdge("csf6","csf1", (int)nodeDistTable.get("csf6").distanceTo(nodeDistTable.get("csf1")));
        graph.addEdge("csf39","csf2",(int)nodeDistTable.get("csf39").distanceTo(nodeDistTable.get("csf2")) );
        graph.addEdge("csf40","csf39",(int)nodeDistTable.get("csf40").distanceTo(nodeDistTable.get("csf39")));
        graph.addEdge("csf3","csf2", (int)nodeDistTable.get("csf3").distanceTo(nodeDistTable.get("csf2")) );
        graph.addEdge("csf41","csf3", (int)nodeDistTable.get("csf41").distanceTo(nodeDistTable.get("csf3")) );
        graph.addEdge("csf42","csf41", (int)nodeDistTable.get("csf42").distanceTo(nodeDistTable.get("csf41")) );
        graph.addEdge("csf43","csf42",  (int)nodeDistTable.get("csf43").distanceTo(nodeDistTable.get("csf42")));
        graph.addEdge("csf4","csf3",  (int)nodeDistTable.get("csf3").distanceTo(nodeDistTable.get("csf4")));
        graph.addEdge("csf37","csf4",  (int)nodeDistTable.get("csf37").distanceTo(nodeDistTable.get("csf4")));
        graph.addEdge("csf14","csf37",  (int)nodeDistTable.get("csf14").distanceTo(nodeDistTable.get("csf37")));
        graph.addEdge("csf13","csf14",  (int)nodeDistTable.get("csf13").distanceTo(nodeDistTable.get("csf14")));
        graph.addEdge("csf15","csf13",  (int)nodeDistTable.get("csf15").distanceTo(nodeDistTable.get("csf13")));
        graph.addEdge("csf12","csf14",  (int)nodeDistTable.get("csf12").distanceTo(nodeDistTable.get("csf14")));
        graph.addEdge("csf11","csf12",  (int)nodeDistTable.get("csf11").distanceTo(nodeDistTable.get("csf12")));
        graph.addEdge("csf10","csf11",  (int)nodeDistTable.get("csf10").distanceTo(nodeDistTable.get("csf11")));
        graph.addEdge("csf9","csf10",  (int)nodeDistTable.get("csf9").distanceTo(nodeDistTable.get("csf10")));
        graph.addEdge("csf8","csf9",  (int)nodeDistTable.get("csf8").distanceTo(nodeDistTable.get("csf9")));
        graph.addEdge("csf36","csf8",  (int)nodeDistTable.get("csf36").distanceTo(nodeDistTable.get("csf8")));
        graph.addEdge("csf34","csf36",  (int)nodeDistTable.get("csf34").distanceTo(nodeDistTable.get("csf36")));
        graph.addEdge("csf35","csf34",  (int)nodeDistTable.get("csf35").distanceTo(nodeDistTable.get("csf34")));
        graph.addEdge("csf44","csf36",  (int)nodeDistTable.get("csf44").distanceTo(nodeDistTable.get("csf36")));
        graph.addEdge("csf46","csf44",  (int)nodeDistTable.get("csf46").distanceTo(nodeDistTable.get("csf44")));
        graph.addEdge("csf47","csf46", (int)nodeDistTable.get("csf47").distanceTo(nodeDistTable.get("csf46")) );
        graph.addEdge("csf48","csf47", (int)nodeDistTable.get("csf48").distanceTo(nodeDistTable.get("csf47")) );
        graph.addEdge("csf49","csf48", (int)nodeDistTable.get("csf49").distanceTo(nodeDistTable.get("csf48")) );
        graph.addEdge("csf50","csf49",  (int)nodeDistTable.get("csf49").distanceTo(nodeDistTable.get("csf50")));
        graph.addEdge("csf51","csf49", (int)nodeDistTable.get("csf51").distanceTo(nodeDistTable.get("csf49")) );
        graph.addEdge("csf52","csf51",  (int)nodeDistTable.get("csf52").distanceTo(nodeDistTable.get("csf51")));
        graph.addEdge("csf53","csf51",  (int)nodeDistTable.get("csf53").distanceTo(nodeDistTable.get("csf51")));
        graph.addEdge("csf54","csf53",  (int)nodeDistTable.get("csf54").distanceTo(nodeDistTable.get("csf53")));
        graph.addEdge("csf55","csf54", (int)nodeDistTable.get("csf55").distanceTo(nodeDistTable.get("csf54")) );
        graph.addEdge("csf56","csf55", (int)nodeDistTable.get("csf56").distanceTo(nodeDistTable.get("csf55")) );
        graph.addEdge("csf57","csf56",  (int)nodeDistTable.get("csf57").distanceTo(nodeDistTable.get("csf56")));
        graph.addEdge("csf70","csf56",  (int)nodeDistTable.get("csf70").distanceTo(nodeDistTable.get("csf56")));
        graph.addEdge("csf76","csf77",  (int)nodeDistTable.get("csf76").distanceTo(nodeDistTable.get("csf77")));
        graph.addEdge("csf75","csf76",  (int)nodeDistTable.get("csf75").distanceTo(nodeDistTable.get("csf76")));
        graph.addEdge("csf74","csf75", (int)nodeDistTable.get("csf74").distanceTo(nodeDistTable.get("csf75")) );
        graph.addEdge("csf71","csf74",  (int)nodeDistTable.get("csf71").distanceTo(nodeDistTable.get("csf74")));
        graph.addEdge("csf72","csf71",  (int)nodeDistTable.get("csf71").distanceTo(nodeDistTable.get("csf72")));
        graph.addEdge("csf73","csf72", (int)nodeDistTable.get("csf73").distanceTo(nodeDistTable.get("csf72")) );
        graph.addEdge("csf68","csf73",  (int)nodeDistTable.get("csf68").distanceTo(nodeDistTable.get("csf73")));
        graph.addEdge("csf69","csf71", (int)nodeDistTable.get("csf69").distanceTo(nodeDistTable.get("csf71")));
        graph.addEdge("csf70","csf69", (int)nodeDistTable.get("csf70").distanceTo(nodeDistTable.get("csf69")) );
        graph.addEdge("csf66","csf69",  (int)nodeDistTable.get("csf66").distanceTo(nodeDistTable.get("csf69")));
        graph.addEdge("csf67","csf66", (int)nodeDistTable.get("csf67").distanceTo(nodeDistTable.get("csf66")) );
        graph.addEdge("csf68","csf67", (int)nodeDistTable.get("csf68").distanceTo(nodeDistTable.get("csf67")) );
        graph.addEdge("csf65","csf66", (int)nodeDistTable.get("csf65").distanceTo(nodeDistTable.get("csf66")) );
        graph.addEdge("csf64","csf65",  (int)nodeDistTable.get("csf64").distanceTo(nodeDistTable.get("csf65")));
        graph.addEdge("csf63","csf64",  (int)nodeDistTable.get("csf63").distanceTo(nodeDistTable.get("csf64")));
        graph.addEdge("csf62","csf63",  (int)nodeDistTable.get("csf62").distanceTo(nodeDistTable.get("csf63")));
        graph.addEdge("csf61","csf62",  (int)nodeDistTable.get("csf61").distanceTo(nodeDistTable.get("csf62")));
        graph.addEdge("csf60","csf61",  (int)nodeDistTable.get("csf60").distanceTo(nodeDistTable.get("csf61")));
        graph.addEdge("csf59","csf60",  (int)nodeDistTable.get("csf59").distanceTo(nodeDistTable.get("csf60")));
        graph.addEdge("csf58","csf59",  (int)nodeDistTable.get("csf58").distanceTo(nodeDistTable.get("csf59")));
        graph.addEdge("csf45","csf58",  (int)nodeDistTable.get("csf45").distanceTo(nodeDistTable.get("csf58")));
        graph.addEdge("csf28","csf45",  (int)nodeDistTable.get("csf28").distanceTo(nodeDistTable.get("csf45")));
        graph.addEdge("csf26","csf28",  (int)nodeDistTable.get("csf26").distanceTo(nodeDistTable.get("csf28")));
        graph.addEdge("csf25","csf26",  (int)nodeDistTable.get("csf25").distanceTo(nodeDistTable.get("csf26")));
        graph.addEdge("csf23","csf25",  (int)nodeDistTable.get("csf23").distanceTo(nodeDistTable.get("csf25")));
        graph.addEdge("csf24","csf23",  (int)nodeDistTable.get("csf24").distanceTo(nodeDistTable.get("csf23")));
        graph.addEdge("csf33","csf22",  (int)nodeDistTable.get("csf33").distanceTo(nodeDistTable.get("csf22")));
        graph.addEdge("csf22","csf33",  (int)nodeDistTable.get("csf22").distanceTo(nodeDistTable.get("csf33")));
        graph.addEdge("csf21","csf22",  (int)nodeDistTable.get("csf21").distanceTo(nodeDistTable.get("csf22")));
        graph.addEdge("csf20","csf21", (int)nodeDistTable.get("csf21").distanceTo(nodeDistTable.get("csf20")) );
        graph.addEdge("csf19","csf20", (int)nodeDistTable.get("csf19").distanceTo(nodeDistTable.get("csf20")) );
        graph.addEdge("csf18","csf19", (int)nodeDistTable.get("csf18").distanceTo(nodeDistTable.get("csf19")) );
        graph.addEdge("csf17","csf18",  (int)nodeDistTable.get("csf17").distanceTo(nodeDistTable.get("csf18")));
        graph.addEdge("csf16","csf17", (int)nodeDistTable.get("csf16").distanceTo(nodeDistTable.get("csf17")) );
        graph.addEdge("csf27","csf28",  (int)nodeDistTable.get("csf27").distanceTo(nodeDistTable.get("csf28")));
        graph.addEdge("csf29","csf27",  (int)nodeDistTable.get("csf29").distanceTo(nodeDistTable.get("csf27")));
        graph.addEdge( "csf30","csf29", (int)nodeDistTable.get("csf30").distanceTo(nodeDistTable.get("csf29")));
        graph.addEdge("csf31","csf30", (int)nodeDistTable.get("csf31").distanceTo(nodeDistTable.get("csf30")) );
        graph.addEdge("csf32","csf31", (int)nodeDistTable.get("csf32").distanceTo(nodeDistTable.get("csf31")) );
        graph.addEdge("csf16","csf32",  (int)nodeDistTable.get("csf16").distanceTo(nodeDistTable.get("csf32")));
        graph.addEdge("csf5", "csf16", (int)nodeDistTable.get("csf5").distanceTo(nodeDistTable.get("csf16")));
        graph.addEdge("csf38","csf5",  (int)nodeDistTable.get("csf38").distanceTo(nodeDistTable.get("csf5")));
        graph.addEdge("csf7","csf38",  (int)nodeDistTable.get("csf7").distanceTo(nodeDistTable.get("csf38")));
        graph.addEdge("csf6","csf7", (int)nodeDistTable.get("csf6").distanceTo(nodeDistTable.get("csf7")) );
        graph.addEdge("csf5","csf37",  (int)nodeDistTable.get("csf5").distanceTo(nodeDistTable.get("csf37")));
        graph.addEdge("csf28","csf36",  (int)nodeDistTable.get("csf28").distanceTo(nodeDistTable.get("csf36")));
        graph.addEdge("csf45","csf44", (int)nodeDistTable.get("csf45").distanceTo(nodeDistTable.get("csf44")) );
        graph.addEdge("csf45","csf36",  (int)nodeDistTable.get("csf45").distanceTo(nodeDistTable.get("csf36")));
        graph.addEdge("csf28","csf44",  (int)nodeDistTable.get("csf28").distanceTo(nodeDistTable.get("csf44")));
        graph.addEdge("csf65","csf55",  (int)nodeDistTable.get("csf65").distanceTo(nodeDistTable.get("csf55")));
        graph.addEdge("csf66","csf56", (int)nodeDistTable.get("csf66").distanceTo(nodeDistTable.get("csf56")) );
        graph.addEdge("csf66","csf55",  (int)nodeDistTable.get("csf66").distanceTo(nodeDistTable.get("csf55")));
        graph.addEdge("csf65","csf56",  (int)nodeDistTable.get("csf65").distanceTo(nodeDistTable.get("csf56")));
        graph.addEdge("csf69","csf70", (int)nodeDistTable.get("csf69").distanceTo(nodeDistTable.get("csf70")) );

        graph.addEdge("csf78", "csf70",(int)nodeDistTable.get("csf70").distanceTo(nodeDistTable.get("csf78")) );
        graph.addEdge("csf79", "csf78",(int)nodeDistTable.get("csf78").distanceTo(nodeDistTable.get("csf79")) );
        graph.addEdge("csf80", "csf79",(int)nodeDistTable.get("csf79").distanceTo(nodeDistTable.get("csf80")) );
        graph.addEdge("csf81", "csf80",(int)nodeDistTable.get("csf80").distanceTo(nodeDistTable.get("csf81")) );
        graph.addEdge("csf82", "csf81",(int)nodeDistTable.get("csf81").distanceTo(nodeDistTable.get("csf82")) );
        graph.addEdge("csf56", "csf82",(int)nodeDistTable.get("csf82").distanceTo(nodeDistTable.get("csf56")) );


        DijkstraAlgorithm dijkstra = GoodDijkstraAlgorithm.getInstance();
        SingleSourceShortestPathResult<String, Integer, DirectedWeightedEdge<String, Integer>> result = dijkstra.calc(graph, srcNumber, NS);

        Iterable<DirectedWeightedEdge<String, Integer>> pathToD = result.getPath(destNumber);
        Iterator<DirectedWeightedEdge<String, Integer>> it = pathToD.iterator();

        int count = 0;
        while(it.hasNext()){
            DirectedWeightedEdge<String, Integer> item = it.next();
            if(count == 0){
                pathVertices.add(item.from());
                pathVertices.add(item.to());
            }if(count > 0){
                pathVertices.add(item.to());
            }
            count++;
        }
        String path = "";
        for(String node : pathVertices){
            path = path + node + "*";
        }

        GlobalState.drawPath = path;

        //Getting junction nodes for taking bearings
        resultantJunctionNodes.clear();
        bearingsList.clear();
        for(int i=0; i < pathVertices.size(); i++){
            for(String node : junctionNodes){
                if(pathVertices.get(i).equals(node)){
                    resultantJunctionNodes.add(node);
                }
            }
        }

        if(!juctionNodeLocation.isEmpty())
            juctionNodeLocation.clear();

        for(int i = 0; i < resultantJunctionNodes.size(); i++){
            juctionNodeLocation.add(nodeDistTable.get(resultantJunctionNodes.get(i)));
        }

        if(resultantJunctionNodes.size() > 2){
            for (int i=0; i < resultantJunctionNodes.size(); i++){
                if(nearestToSource.equals(resultantJunctionNodes.get(i))){
                    resultantJunctionNodes.remove(i);
                }
            }
        }

        float preSrcBearing =  getBearings(srcBearingLoc, nodeDistTable.get(nearestToSource));
        bearingRJNHash.put(preSrcBearing, "NA");

        Location dst = null;
        Location src = null;
        if(resultantJunctionNodes.size() == 0){
            src = nodeDistTable.get(nearestToSource);
            dst = nodeDistTable.get(nearestToDest);
        }else {
            src = nodeDistTable.get(nearestToSource);
            dst = nodeDistTable.get(resultantJunctionNodes.get(0));
        }

        float srcBearing =  getBearings(src, dst);

        if(resultantJunctionNodes.size() ==0)
            bearingRJNHash.put(srcBearing, nearestToDest);
        else
            bearingRJNHash.put(srcBearing, resultantJunctionNodes.get(0));

        bearingsList.add(preSrcBearing);
        bearingsList.add(srcBearing);

        for (int i=0; i < resultantJunctionNodes.size()-1; i++){

            Location firstLoc = nodeDistTable.get(resultantJunctionNodes.get(i));
            Location secondLoc = nodeDistTable.get(resultantJunctionNodes.get(i + 1));
            float resultantBearing =  getBearings(firstLoc, secondLoc);

            bearingRJNHash.put(resultantBearing,resultantJunctionNodes.get(i + 1));

            bearingsList.add(resultantBearing);
        }

        if(resultantJunctionNodes.size() > 2){
            for (int i=0; i < resultantJunctionNodes.size(); i++){
                if(nearestToDest.equals(resultantJunctionNodes.get(i))){
                    resultantJunctionNodes.remove(i);
                }
            }
        }

        float preDestBearing = 0;

        if(resultantJunctionNodes.size() == 0)
            preDestBearing =  getBearings(nodeDistTable.get(nearestToDest),nodeDistTable.get(nearestToDest) );
        else
            preDestBearing =  getBearings(nodeDistTable.get(resultantJunctionNodes.get(resultantJunctionNodes.size()-1)),nodeDistTable.get(nearestToDest) );

        bearingRJNHash.put(preDestBearing, nearestToDest);
        float destBearing =  getBearings(nodeDistTable.get(nearestToDest), destBearingLoc );
        bearingRJNHash.put(destBearing, destNumber);
        bearingsList.add(preDestBearing);
        bearingsList.add(destBearing);

        //TextNavigation textualNavigator =  new TextNavigation(srcBearingLoc, destBearingLoc, juctionNodeLocation, resultantJunctionNodes, sortedStoreList, nodeDistTable, floorName);

        TextNav textNav = new TextNav(srcBearingLoc, destBearingLoc, pathVertices, nodeDistTable, sortedStoreList, floorName);

        if(!tnForSF.isEmpty())
            tnForSF.clear();

        GlobalState.tnPathSF = "";
        GlobalState.tnSrcStoreSF = "";
        GlobalState.tnDestStoreSF = "";

        //tnForSF = textNav.getTextNav();
        tnForSF =  textNav.getTextNav();
        for(int i =0; i < tnForSF.size(); i++){
            GlobalState.tnPathSF = GlobalState.tnPathSF + tnForSF.get(i) + "#";
        }
        GlobalState.tnSrcStoreSF = srcStoreName;
        GlobalState.tnDestStoreSF = destStoreName;

        return pathVertices;
    }

    private float getBearings(Location sLoc, Location dLoc){
        float bearing = 0;

        Location firstLoc = sLoc;
        Location secondLoc = dLoc;
        bearing = firstLoc.bearingTo(secondLoc);
        return bearing;
    }

}
