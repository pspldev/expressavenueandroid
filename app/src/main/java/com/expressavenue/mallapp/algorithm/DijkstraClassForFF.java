package com.expressavenue.mallapp.algorithm;

import android.location.Location;

import com.expressavenue.mallapp.global.GlobalState;
import com.expressavenue.mallapp.pojos.Store;
import com.expressavenue.mallapp.utils.TextNav;

import org.psjava.algo.graph.shortestpath.DijkstraAlgorithm;
import org.psjava.algo.graph.shortestpath.SingleSourceShortestPathResult;
import org.psjava.ds.graph.DirectedWeightedEdge;
import org.psjava.ds.graph.MutableDirectedWeightedGraph;
import org.psjava.ds.numbersystrem.IntegerNumberSystem;
import org.psjava.goods.GoodDijkstraAlgorithm;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;

public class DijkstraClassForFF {

    private String srcNumber ="";
    private String srcLat ="";
    private String srcLong ="";

    private String destNumber ="";
    private String destLat ="";
    private String destLong ="";

    private String nearestToSource;
    private String nearestToDest;
    private double sWeight;
    private double dWeight;
    private String nearestToS[];
    private String nearestToD[];

    private Location srcBearingLoc;
    private Location destBearingLoc;

    private ArrayList<String> tnForFF;

    private String junctionNodes[]= {"cff1","cff7","cff3","cff4","cff5","cff35","cff34","cff8","cff9", "cff12",
            "cff13","cff15","cff14","cff17","cff16","cff18","cff19","cff21", "cff20",
            "cff25","cff24","cff27","cff26","cff30","cff31"};

    private ArrayList<String> resultantJunctionNodes;
    private ArrayList<Float> bearingsList;
    private Hashtable<String, Float> resultantBearingHash;

    private Hashtable<Float, String> bearingRJNHash;

    private ArrayList<Store> sortedStoreList;

    private  Location lcff1;

    private Hashtable<Location, String> nodeHash;
    private Hashtable<String, Location> nodeDistTable;

    private String srcStoreName = "NA";
    private String destStoreName = "NA";
    private String floorName = "";

    private ArrayList<Location> juctionNodeLocation;

    public DijkstraClassForFF(String srcNumber, String srcLat, String srcLong,
                              String destNumber, String destLat, String destLong, ArrayList<Store> sortedStoreList, String floorName){

        juctionNodeLocation = new ArrayList<Location>();


        for(Store store : sortedStoreList){
            if(store.getStoreNumber().equals(srcNumber)){
                srcStoreName = store.getStoreName();
            }
            if(store.getStoreNumber().equals(destNumber)){
                destStoreName = store.getStoreName();
            }
        }

        if(srcNumber.equals("LT120"))
            srcStoreName = "Main Lift";
        if(srcNumber.equals("LT120LGF") || srcNumber.equals("LT120UGF") || srcNumber.equals("LT120FF") || srcNumber.equals("LT120SF") || srcNumber.equals("LT120TF")
                || srcNumber.equals("LT120B1")|| srcNumber.equals("LT120B2")|| srcNumber.equals("LT120B3") )
            srcStoreName = "Main Lift";
        else if(srcNumber.equals("ESC"))
            srcStoreName = "Escalator";
        else if(srcNumber.equals("ESC"))
            srcStoreName = "Escalator";
        else if(srcNumber.equals("ESC-Left"))
            srcStoreName = "Escalator";
        else if(srcNumber.equals("ESC-Right"))
            srcStoreName = "Escalator";

        if(destNumber.equals("LT120"))
            destStoreName = "Main Lift";
        if(destNumber.equals("LT120LGF") || destNumber.equals("LT120UGF") || destNumber.equals("LT120FF") || destNumber.equals("LT120SF")
                || destNumber.equals("LT120TF") || destNumber.equals("LT120B1")|| destNumber.equals("LT120B2")|| destNumber.equals("LT120B3") )
            destStoreName = "Main Lift";
        else if(destNumber.equals("ESC"))
            destStoreName = "Escalator";
        else if(destNumber.equals("ESC"))
            destStoreName = "Escalator";
        else if(destNumber.equals("ESC-Left"))
            destStoreName = "Escalator";
        else if(destNumber.equals("ESC-Right"))
            destStoreName = "Escalator";


        nodeHash = new Hashtable<Location, String>();
        nodeDistTable = new Hashtable<String, Location>();

        resultantJunctionNodes = new ArrayList<String >();
        resultantBearingHash = new Hashtable<String, Float>();
        bearingsList = new ArrayList<Float>();

        bearingRJNHash = new Hashtable<Float, String>();
        if(!bearingRJNHash.isEmpty())
            bearingRJNHash.clear();

        this.srcNumber = srcNumber;
        this.srcLat = srcLat;
        this.srcLong = srcLong;

        this.destNumber = destNumber;
        this.destLat = destLat;
        this.destLong = destLong;

        this.sortedStoreList = sortedStoreList;
        this.floorName = floorName;

        tnForFF = new ArrayList<String>();

        setNodeHash(13.058669,80.263132, "cff1");
        setNodeHash(13.058622,80.263178, "cff2");
        setNodeHash(13.058613,80.263268, "cff3");
        setNodeHash(13.058643,80.263355, "cff4");
        setNodeHash(13.058718,80.263353, "cff5");
        setNodeHash(13.05873,80.263175, "cff6");
        setNodeHash(13.058752,80.263255, "cff7");
        setNodeHash(13.058645,80.263763, "cff8");
        setNodeHash(13.058714,80.263767, "cff9");
        setNodeHash(13.058566,80.263759, "cff10");
        setNodeHash(13.058839,80.263759, "cff11");
        setNodeHash(13.058643,80.263797, "cff12");
        setNodeHash(13.05871,80.263808, "cff13");
        setNodeHash(13.058593,80.263936, "cff14");
        setNodeHash(13.058764,80.26393, "cff15");
        setNodeHash(13.058589,80.264088, "cff16");
        setNodeHash(13.058767,80.26409, "cff17");
        setNodeHash(13.058644,80.26422, "cff18");
        setNodeHash(13.058713,80.264219, "cff19");
        setNodeHash(13.058644,80.264248, "cff20");
        setNodeHash(13.058713,80.264248, "cff21");
        setNodeHash(13.058871,80.264248, "cff22");
        setNodeHash(13.058568,80.264251, "cff23");
        setNodeHash(13.058644,80.264522, "cff24");
        setNodeHash(13.058713,80.264523, "cff25");
        setNodeHash(13.058713,80.264553, "cff26");
        setNodeHash(13.058644,80.264582, "cff27");
        setNodeHash(13.05857,80.264582, "cff28");
        setNodeHash(13.05884,80.264554, "cff29");
        setNodeHash(13.058644,80.264712, "cff30");
        setNodeHash(13.058713,80.264712, "cff31");
        setNodeHash(13.058488, 80.263948, "cff32");
        setNodeHash(13.058489, 80.264063, "cff33");
        setNodeHash(13.058647, 80.263426, "cff34");
        setNodeHash(13.058718, 80.263456, "cff35");
        setNodeHash(13.058788, 80.263456, "cff36");
        setNodeHash(13.058581, 80.263429, "cff37");

        setDistHash(13.058669, 80.263132, "cff1");
        setDistHash(13.058622, 80.263178, "cff2");
        setDistHash(13.058613, 80.263268, "cff3");
        setDistHash(13.058643, 80.263355, "cff4");
        setDistHash(13.058718, 80.263353, "cff5");
        setDistHash(13.05873, 80.263175, "cff6");
        setDistHash(13.058752, 80.263255, "cff7");
        setDistHash(13.058645, 80.263763, "cff8");
        setDistHash(13.058714, 80.263767, "cff9");
        setDistHash(13.058566, 80.263759, "cff10");
        setDistHash(13.058839, 80.263759, "cff11");
        setDistHash(13.058643, 80.263797, "cff12");
        setDistHash(13.05871, 80.263808, "cff13");
        setDistHash(13.058593, 80.263936, "cff14");
        setDistHash(13.058764, 80.26393, "cff15");
        setDistHash(13.058589, 80.264088, "cff16");
        setDistHash(13.058767, 80.26409, "cff17");
        setDistHash(13.058644, 80.26422, "cff18");
        setDistHash(13.058713, 80.264219, "cff19");
        setDistHash(13.058644, 80.264248, "cff20");
        setDistHash(13.058713, 80.264248, "cff21");
        setDistHash(13.058871, 80.264248, "cff22");
        setDistHash(13.058568, 80.264251, "cff23");
        setDistHash(13.058644, 80.264522, "cff24");
        setDistHash(13.058713, 80.264523, "cff25");
        setDistHash(13.058713, 80.264553, "cff26");
        setDistHash(13.058644, 80.264582, "cff27");
        setDistHash(13.05857, 80.264582, "cff28");
        setDistHash(13.05884, 80.264554, "cff29");
        setDistHash(13.058644, 80.264712, "cff30");
        setDistHash(13.058713, 80.264712, "cff31");
        setDistHash(13.058488, 80.263948, "cff32");
        setDistHash(13.058489, 80.264063, "cff33");
        setDistHash(13.058647, 80.263426, "cff34");
        setDistHash(13.058718, 80.263456, "cff35");
        setDistHash(13.058788, 80.263456, "cff36");
        setDistHash(13.058581, 80.263429, "cff37");

        setDistHash(Double.parseDouble(srcLat), Double.parseDouble(srcLong), this.srcNumber);
        setDistHash(Double.parseDouble(destLat), Double.parseDouble(destLong), this.destNumber);

        Location srcLocation = new Location("");
        srcLocation.setLatitude(Double.parseDouble(this.srcLat));
        srcLocation.setLongitude(Double.parseDouble(this.srcLong));
        srcBearingLoc = srcLocation;
        nearestToS = getNearesLocation(srcLocation, nodeHash);
        nearestToSource = nearestToS[0];
        sWeight = Double.parseDouble(nearestToS[1]);

        Location destLocation = new Location("");
        destLocation.setLatitude(Double.parseDouble(this.destLat));
        destLocation.setLongitude(Double.parseDouble(this.destLong));
        destBearingLoc = destLocation;
        nearestToD = getNearesLocation(destLocation, nodeHash);
        nearestToDest = nearestToD[0];
        dWeight = Double.parseDouble(nearestToD[1]);
    }

    private void setNodeHash(double lat, double lon, String storeNo)
    {
        lcff1 = new Location(storeNo);
        lcff1.setLatitude(lat);
        lcff1.setLongitude(lon);
        nodeHash.put(lcff1,storeNo);
    }
    private void setDistHash(double lat, double lon, String storeNo)
    {
        lcff1 = new Location(storeNo);
        lcff1.setLatitude(lat);
        lcff1.setLongitude(lon);
        nodeDistTable.put(storeNo, lcff1);
    }
    public String[] getNearesLocation(Location srcLoc, Hashtable<Location, String> srcHash){

        String nearestStore[] = new String[2];
        ArrayList<Double> distValues = new ArrayList<Double>();
        Hashtable<Double, Location> distHash = new Hashtable<Double, Location>();
        Set<Location> locSet = srcHash.keySet();
        for(Location location : locSet){

            double dist = location.distanceTo(srcLoc);
            distValues.add(dist);
            distHash.put(dist,location);
        }

        double smallest = Double.MAX_VALUE;
        for(int i =0;i<distValues.size();i++) {
            if(smallest > distValues.get(i)) {
                smallest = distValues.get(i);
            }
        }

        Location nearLoc = distHash.get(smallest);
        nearestStore[0] = srcHash.get(nearLoc);
        nearestStore[1] = String.valueOf(smallest);

        return nearestStore;
    }

    public ArrayList<String> algorithmImplementation() {

        ArrayList<String> pathVertices = new ArrayList<>();
        pathVertices.clear();

        IntegerNumberSystem NS = IntegerNumberSystem.getInstance();

        MutableDirectedWeightedGraph<String, Integer> graph = MutableDirectedWeightedGraph.create();

        graph.insertVertex("cff1");
        graph.insertVertex("cff2");
        graph.insertVertex("cff3");
        graph.insertVertex("cff4");
        graph.insertVertex("cff5");
        graph.insertVertex("cff6");
        graph.insertVertex("cff7");
        graph.insertVertex("cff8");
        graph.insertVertex("cff9");
        graph.insertVertex("cff10");
        graph.insertVertex("cff11");
        graph.insertVertex("cff12");
        graph.insertVertex("cff13");
        graph.insertVertex("cff14");
        graph.insertVertex("cff15");
        graph.insertVertex("cff16");
        graph.insertVertex("cff17");
        graph.insertVertex("cff18");
        graph.insertVertex("cff19");
        graph.insertVertex("cff20");
        graph.insertVertex("cff21");
        graph.insertVertex("cff22");
        graph.insertVertex("cff23");
        graph.insertVertex("cff24");
        graph.insertVertex("cff25");
        graph.insertVertex("cff26");
        graph.insertVertex("cff27");
        graph.insertVertex("cff28");
        graph.insertVertex("cff29");
        graph.insertVertex("cff30");
        graph.insertVertex("cff31");
        graph.insertVertex("cff32");
        graph.insertVertex("cff33");
        graph.insertVertex("cff34");
        graph.insertVertex("cff35");
        graph.insertVertex("cff36");
        graph.insertVertex("cff37");

        graph.insertVertex(srcNumber);
        graph.insertVertex(destNumber);

        graph.addEdge(srcNumber, nearestToSource, (int)sWeight);
        graph.addEdge(nearestToDest, destNumber, (int)dWeight);

        graph.addEdge(nearestToSource, srcNumber, (int)sWeight);
        graph.addEdge(destNumber, nearestToDest, (int)dWeight);

        graph.addEdge("cff1", "cff6", (int)nodeDistTable.get("cff1").distanceTo(nodeDistTable.get("cff6")));
        graph.addEdge("cff1", "cff2", (int)nodeDistTable.get("cff1").distanceTo(nodeDistTable.get("cff2")));
        graph.addEdge("cff2", "cff3", (int)nodeDistTable.get("cff2").distanceTo(nodeDistTable.get("cff3")));
        graph.addEdge("cff6", "cff7", (int)nodeDistTable.get("cff6").distanceTo(nodeDistTable.get("cff7")));
        graph.addEdge("cff7", "cff5", (int)nodeDistTable.get("cff7").distanceTo(nodeDistTable.get("cff5")));
        graph.addEdge("cff3", "cff4", (int)nodeDistTable.get("cff3").distanceTo(nodeDistTable.get("cff4")));
        graph.addEdge("cff5", "cff35", (int)nodeDistTable.get("cff5").distanceTo(nodeDistTable.get("cff35")));
        graph.addEdge("cff4", "cff34", (int)nodeDistTable.get("cff4").distanceTo(nodeDistTable.get("cff34")));
        graph.addEdge("cff35", "cff36", (int)nodeDistTable.get("cff35").distanceTo(nodeDistTable.get("cff36")));
        graph.addEdge("cff34", "cff37", (int)nodeDistTable.get("cff34").distanceTo(nodeDistTable.get("cff37")));
        graph.addEdge("cff34", "cff8", (int)nodeDistTable.get("cff34").distanceTo(nodeDistTable.get("cff8")));
        graph.addEdge("cff35", "cff9", (int)nodeDistTable.get("cff35").distanceTo(nodeDistTable.get("cff9")));
        graph.addEdge("cff9", "cff11", (int)nodeDistTable.get("cff9").distanceTo(nodeDistTable.get("cff11")));
        graph.addEdge("cff9", "cff13", (int)nodeDistTable.get("cff9").distanceTo(nodeDistTable.get("cff13")));
        graph.addEdge("cff8", "cff10", (int)nodeDistTable.get("cff8").distanceTo(nodeDistTable.get("cff10")));
        graph.addEdge("cff8", "cff12", (int)nodeDistTable.get("cff8").distanceTo(nodeDistTable.get("cff12")));
        graph.addEdge("cff13", "cff15", (int)nodeDistTable.get("cff13").distanceTo(nodeDistTable.get("cff15")));
        graph.addEdge("cff15", "cff17", (int)nodeDistTable.get("cff15").distanceTo(nodeDistTable.get("cff17")));
        graph.addEdge("cff17", "cff19", (int)nodeDistTable.get("cff17").distanceTo(nodeDistTable.get("cff19")));
        graph.addEdge("cff12", "cff14", (int)nodeDistTable.get("cff12").distanceTo(nodeDistTable.get("cff14")));
        graph.addEdge("cff14", "cff32", (int)nodeDistTable.get("cff14").distanceTo(nodeDistTable.get("cff12")));
        graph.addEdge("cff14", "cff16", (int)nodeDistTable.get("cff14").distanceTo(nodeDistTable.get("cff16")));
        graph.addEdge("cff16", "cff33", (int)nodeDistTable.get("cff16").distanceTo(nodeDistTable.get("cff33")));
        graph.addEdge("cff16", "cff18", (int)nodeDistTable.get("cff16").distanceTo(nodeDistTable.get("cff18")));
        graph.addEdge("cff19", "cff21", (int)nodeDistTable.get("cff19").distanceTo(nodeDistTable.get("cff21")));
        graph.addEdge("cff18", "cff20", (int)nodeDistTable.get("cff18").distanceTo(nodeDistTable.get("cff20")));
        graph.addEdge("cff20", "cff23", (int)nodeDistTable.get("cff20").distanceTo(nodeDistTable.get("cff23")));
        graph.addEdge("cff21", "cff22", (int)nodeDistTable.get("cff21").distanceTo(nodeDistTable.get("cff22")));
        graph.addEdge("cff21", "cff25", (int)nodeDistTable.get("cff21").distanceTo(nodeDistTable.get("cff25")));
        graph.addEdge("cff20", "cff24", (int)nodeDistTable.get("cff20").distanceTo(nodeDistTable.get("cff24")));
        graph.addEdge("cff25", "cff26", (int)nodeDistTable.get("cff25").distanceTo(nodeDistTable.get("cff26")));
        graph.addEdge("cff24", "cff27", (int)nodeDistTable.get("cff24").distanceTo(nodeDistTable.get("cff27")));
        graph.addEdge("cff26", "cff29", (int)nodeDistTable.get("cff26").distanceTo(nodeDistTable.get("cff29")));
        graph.addEdge("cff27", "cff28", (int)nodeDistTable.get("cff27").distanceTo(nodeDistTable.get("cff28")));
        graph.addEdge("cff27", "cff30", (int)nodeDistTable.get("cff27").distanceTo(nodeDistTable.get("cff30")));
        graph.addEdge("cff26", "cff31", (int)nodeDistTable.get("cff26").distanceTo(nodeDistTable.get("cff31")));
        graph.addEdge("cff4", "cff5", (int)nodeDistTable.get("cff4").distanceTo(nodeDistTable.get("cff5")));
        graph.addEdge("cff12", "cff13", (int)nodeDistTable.get("cff12").distanceTo(nodeDistTable.get("cff13")));
        graph.addEdge("cff18", "cff19", (int)nodeDistTable.get("cff18").distanceTo(nodeDistTable.get("cff19")));
        graph.addEdge("cff24", "cff25", (int)nodeDistTable.get("cff24").distanceTo(nodeDistTable.get("cff25")));
        graph.addEdge("cff30", "cff31", (int)nodeDistTable.get("cff30").distanceTo(nodeDistTable.get("cff31")));

        graph.addEdge("cff6", "cff1", (int)nodeDistTable.get("cff1").distanceTo(nodeDistTable.get("cff6")));
        graph.addEdge("cff2", "cff1", (int)nodeDistTable.get("cff1").distanceTo(nodeDistTable.get("cff2")));
        graph.addEdge("cff3", "cff2", (int)nodeDistTable.get("cff2").distanceTo(nodeDistTable.get("cff3")));
        graph.addEdge("cff7", "cff6", (int)nodeDistTable.get("cff6").distanceTo(nodeDistTable.get("cff7")));
        graph.addEdge("cff5", "cff7", (int)nodeDistTable.get("cff7").distanceTo(nodeDistTable.get("cff5")));
        graph.addEdge("cff4", "cff3", (int)nodeDistTable.get("cff3").distanceTo(nodeDistTable.get("cff4")));
        graph.addEdge("cff35", "cff5", (int)nodeDistTable.get("cff5").distanceTo(nodeDistTable.get("cff35")));
        graph.addEdge("cff34", "cff4", (int)nodeDistTable.get("cff4").distanceTo(nodeDistTable.get("cff34")));
        graph.addEdge("cff36", "cff35", (int)nodeDistTable.get("cff35").distanceTo(nodeDistTable.get("cff36")));
        graph.addEdge("cff37", "cff34", (int)nodeDistTable.get("cff34").distanceTo(nodeDistTable.get("cff37")));
        graph.addEdge("cff8", "cff34", (int)nodeDistTable.get("cff34").distanceTo(nodeDistTable.get("cff8")));
        graph.addEdge("cff9", "cff35", (int)nodeDistTable.get("cff35").distanceTo(nodeDistTable.get("cff9")));
        graph.addEdge("cff11", "cff9", (int)nodeDistTable.get("cff9").distanceTo(nodeDistTable.get("cff11")));
        graph.addEdge("cff13", "cff9", (int)nodeDistTable.get("cff9").distanceTo(nodeDistTable.get("cff13")));
        graph.addEdge("cff10", "cff8", (int)nodeDistTable.get("cff8").distanceTo(nodeDistTable.get("cff10")));
        graph.addEdge("cff12", "cff8", (int)nodeDistTable.get("cff8").distanceTo(nodeDistTable.get("cff12")));
        graph.addEdge("cff15", "cff13", (int)nodeDistTable.get("cff13").distanceTo(nodeDistTable.get("cff15")));
        graph.addEdge("cff17", "cff15", (int)nodeDistTable.get("cff15").distanceTo(nodeDistTable.get("cff17")));
        graph.addEdge("cff19", "cff17", (int)nodeDistTable.get("cff17").distanceTo(nodeDistTable.get("cff19")));
        graph.addEdge("cff14", "cff12", (int)nodeDistTable.get("cff12").distanceTo(nodeDistTable.get("cff14")));
        graph.addEdge("cff32", "cff14", (int)nodeDistTable.get("cff14").distanceTo(nodeDistTable.get("cff32")));
        graph.addEdge("cff16", "cff14", (int)nodeDistTable.get("cff14").distanceTo(nodeDistTable.get("cff16")));
        graph.addEdge("cff33", "cff16", (int)nodeDistTable.get("cff16").distanceTo(nodeDistTable.get("cff33")));
        graph.addEdge("cff18", "cff16", (int)nodeDistTable.get("cff16").distanceTo(nodeDistTable.get("cff18")));
        graph.addEdge("cff21", "cff19", (int)nodeDistTable.get("cff19").distanceTo(nodeDistTable.get("cff21")));
        graph.addEdge("cff20", "cff18", (int)nodeDistTable.get("cff18").distanceTo(nodeDistTable.get("cff20")));
        graph.addEdge("cff23", "cff20", (int)nodeDistTable.get("cff20").distanceTo(nodeDistTable.get("cff23")));
        graph.addEdge("cff22", "cff21", (int)nodeDistTable.get("cff21").distanceTo(nodeDistTable.get("cff22")));
        graph.addEdge("cff25", "cff21", (int)nodeDistTable.get("cff21").distanceTo(nodeDistTable.get("cff25")));
        graph.addEdge("cff24", "cff20", (int)nodeDistTable.get("cff20").distanceTo(nodeDistTable.get("cff24")));
        graph.addEdge("cff26", "cff25", (int)nodeDistTable.get("cff25").distanceTo(nodeDistTable.get("cff26")));
        graph.addEdge("cff27", "cff24", (int)nodeDistTable.get("cff24").distanceTo(nodeDistTable.get("cff27")));
        graph.addEdge("cff29", "cff26", (int)nodeDistTable.get("cff26").distanceTo(nodeDistTable.get("cff29")));
        graph.addEdge("cff28", "cff27", (int)nodeDistTable.get("cff27").distanceTo(nodeDistTable.get("cff28")));
        graph.addEdge("cff30", "cff27", (int)nodeDistTable.get("cff27").distanceTo(nodeDistTable.get("cff30")));
        graph.addEdge("cff31", "cff26", (int)nodeDistTable.get("cff26").distanceTo(nodeDistTable.get("cff31")));
        graph.addEdge("cff5", "cff4", (int)nodeDistTable.get("cff4").distanceTo(nodeDistTable.get("cff5")));
        graph.addEdge("cff13", "cff12", (int)nodeDistTable.get("cff12").distanceTo(nodeDistTable.get("cff13")));
        graph.addEdge("cff19", "cff18", (int)nodeDistTable.get("cff18").distanceTo(nodeDistTable.get("cff19")));
        graph.addEdge("cff25", "cff24", (int)nodeDistTable.get("cff24").distanceTo(nodeDistTable.get("cff25")));
        graph.addEdge("cff31", "cff30", (int)nodeDistTable.get("cff30").distanceTo(nodeDistTable.get("cff31")));

        DijkstraAlgorithm dijkstra = GoodDijkstraAlgorithm.getInstance();
        SingleSourceShortestPathResult<String, Integer, DirectedWeightedEdge<String, Integer>> result = dijkstra.calc(graph, srcNumber, NS);

        Iterable<DirectedWeightedEdge<String, Integer>> pathToD = result.getPath(destNumber);
        System.out.println(pathToD);
        Iterator<DirectedWeightedEdge<String, Integer>> it = pathToD.iterator();

        int count = 0;
        while(it.hasNext()){
            DirectedWeightedEdge<String, Integer> item = it.next();
            if(count == 0){
                pathVertices.add(item.from());
                pathVertices.add(item.to());
            }if(count > 0){
                pathVertices.add(item.to());
            }
            count++;
        }
        System.out.println("shortest path :"+ pathVertices);

        String path = "";
        for(String node : pathVertices){
            path = path + node + "*";
        }

        GlobalState.drawPath = path;
        System.out.println("the path :"+ path);

        resultantJunctionNodes.clear();
        for(int i=0; i < pathVertices.size(); i++){
            for(String node : junctionNodes){
                if(pathVertices.get(i).equals(node)){
                    resultantJunctionNodes.add(node);
                }
            }
        }

        if(!juctionNodeLocation.isEmpty())
            juctionNodeLocation.clear();

        for(int i = 0; i < resultantJunctionNodes.size(); i++){
            juctionNodeLocation.add(nodeDistTable.get(resultantJunctionNodes.get(i)));
        }
        if(resultantJunctionNodes.size() ==0){
            for(int i = 0; i < resultantJunctionNodes.size(); i++){
                juctionNodeLocation.add(nodeDistTable.get(nearestToDest));
            }
        }

        for (int i=0; i < resultantJunctionNodes.size(); i++){
            if(nearestToSource.equals(resultantJunctionNodes.get(i))){
                resultantJunctionNodes.remove(i);
            }
        }

        float preSrcBearing =  getBearings(srcBearingLoc, nodeDistTable.get(nearestToSource));
        bearingRJNHash.put(preSrcBearing, "NA");

        Location dst = null;
        Location src = null;
        if(resultantJunctionNodes.size() == 0){
            src = nodeDistTable.get(nearestToSource);
            dst = nodeDistTable.get(nearestToDest);
        }else {
            src = nodeDistTable.get(nearestToSource);
            dst = nodeDistTable.get(resultantJunctionNodes.get(0));
        }

        float srcBearing =  getBearings(src, dst);

        if(resultantJunctionNodes.size() ==0)
            bearingRJNHash.put(srcBearing, nearestToDest);
        else
            bearingRJNHash.put(srcBearing, resultantJunctionNodes.get(0));

        bearingsList.add(preSrcBearing);
        bearingsList.add(srcBearing);

        for (int i=0; i < resultantJunctionNodes.size()-1; i++){

            Location firstLoc = nodeDistTable.get(resultantJunctionNodes.get(i));
            Location secondLoc = nodeDistTable.get(resultantJunctionNodes.get(i + 1));
            float resultantBearing =  getBearings(firstLoc, secondLoc);

            bearingRJNHash.put(resultantBearing,resultantJunctionNodes.get(i + 1));

            bearingsList.add(resultantBearing);
        }

        for (int i=0; i < resultantJunctionNodes.size(); i++){
            if(nearestToDest.equals(resultantJunctionNodes.get(i))){
                resultantJunctionNodes.remove(i);
            }
        }

        float preDestBearing = 0;

        if(resultantJunctionNodes.size() == 0)
            preDestBearing =  getBearings(nodeDistTable.get(nearestToDest),nodeDistTable.get(nearestToDest) );
        else
            preDestBearing =  getBearings(nodeDistTable.get(resultantJunctionNodes.get(resultantJunctionNodes.size()-1)),nodeDistTable.get(nearestToDest) );

        bearingRJNHash.put(preDestBearing, nearestToDest);
        float destBearing =  getBearings(nodeDistTable.get(nearestToDest), destBearingLoc );
        bearingRJNHash.put(destBearing, destNumber);
        bearingsList.add(preDestBearing);
        bearingsList.add(destBearing);


        //TextNavigation textualNavigator =  new TextNavigation(srcBearingLoc, destBearingLoc, juctionNodeLocation, resultantJunctionNodes, sortedStoreList, nodeDistTable, floorName);
        TextNav textNav = new TextNav(srcBearingLoc, destBearingLoc, pathVertices, nodeDistTable, sortedStoreList, floorName);
        if(!tnForFF.isEmpty())
            tnForFF.clear();

        GlobalState.tnPathFF = "";
        GlobalState.tnSrcStoreFF = "";
        GlobalState.tnDestStoreFF = "";

        tnForFF = textNav.getTextNav();

        for(int i =0; i < tnForFF.size(); i++){
            GlobalState.tnPathFF = GlobalState.tnPathFF + tnForFF.get(i) + "#";
        }
        GlobalState.tnSrcStoreFF = srcStoreName;
        GlobalState.tnDestStoreFF = destStoreName;

        return pathVertices ;
    }
    private float getBearings(Location sLoc, Location dLoc){
        float bearing = 0;

        Location firstLoc = sLoc;
        Location secondLoc = dLoc;
        bearing = firstLoc.bearingTo(secondLoc);
        return bearing;
    }
}
