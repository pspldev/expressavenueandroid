package com.expressavenue.mallapp.algorithm;

import android.location.Location;

import com.expressavenue.mallapp.global.GlobalState;
import com.expressavenue.mallapp.pojos.Store;
import com.expressavenue.mallapp.utils.TextNav;

import org.psjava.algo.graph.shortestpath.DijkstraAlgorithm;
import org.psjava.algo.graph.shortestpath.SingleSourceShortestPathResult;
import org.psjava.ds.graph.DirectedWeightedEdge;
import org.psjava.ds.graph.MutableDirectedWeightedGraph;
import org.psjava.ds.numbersystrem.IntegerNumberSystem;
import org.psjava.goods.GoodDijkstraAlgorithm;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by nivedith on 3/17/2016.
 */
public class DijkstraClassForBThree {

    private String srcNumber ="";
    private String srcLat ="";
    private String srcLong ="";

    private String destNumber ="";
    private String destLat ="";
    private String destLong ="";

    private String nearestToSource;
    private String nearestToDest;
    private double sWeight;
    private double dWeight;
    private String nearestToS[];
    private String nearestToD[];

    private Location srcBearingLoc;
    private Location destBearingLoc;

    private ArrayList<String> tnForB3;

    private String junctionNodes[]= {"cbf1_1","cbf1_2","cbf1_3","cbf1_4","cbf1_5","cbf1_6","cbf1_7","cbf1_8","cbf1_9","cbf1_10",
            "cbf1_11","cbf1_12","cbf1_13","cbf1_14","cbf1_15","cbf1_16","cbf1_17","cbf1_18","cbf1_19","cbf1_20","cbf1_21",
            "cbf1_22","cbf1_23","cbf1_24","cbf1_25","cbf1_26","cbf1_27","cbf1_28","cbf1_29","cbf1_30","cbf1_31","cbf1_32"};

    private ArrayList<String> resultantJunctionNodes;
    private ArrayList<Float> bearingsList;
    private Hashtable<String, Float> resultantBearingHash;

    private  Location lcff1;

    private Hashtable<Location, String> nodeHash;
    private Hashtable<String, Location> nodeDistTable;

    private Hashtable<Float, String> bearingRJNHash;

    private ArrayList<Store> sortedStoreList;

    private String srcStoreName = "NA";
    private String destStoreName = "NA";
    private String floorName = "";

    private ArrayList<Location> juctionNodeLocation;

    public DijkstraClassForBThree(String srcNumber, String srcLat, String srcLong,
                                String destNumber, String destLat, String destLong, ArrayList<Store> sortedStoreList, String floorName){

        juctionNodeLocation = new ArrayList<Location>();

        for(Store store : sortedStoreList){

            if(store.getStoreNumber().equals(srcNumber)){
                srcStoreName = store.getStoreName();
            }
            if(store.getStoreNumber().equals(destNumber)){
                destStoreName = store.getStoreName();
            }
        }

        if(srcNumber.equals("LT120"))
            srcStoreName = "Main Lift";
        if(srcNumber.equals("LT120LGF") || srcNumber.equals("LT120UGF") || srcNumber.equals("LT120FF") || srcNumber.equals("LT120SF")
                || srcNumber.equals("LT120TF")|| srcNumber.equals("LT120B1")|| srcNumber.equals("LT120B2")|| srcNumber.equals("LT120B3") )
            srcStoreName = "Main Lift";
        else if(srcNumber.equals("ESC"))
            srcStoreName = "Escalator";
        else if(srcNumber.equals("ESC"))
            srcStoreName = "Escalator";
        else if(srcNumber.equals("ESC-Left"))
            srcStoreName = "Escalator";
        else if(srcNumber.equals("ESC-Right"))
            srcStoreName = "Escalator";

        if(destNumber.equals("LT120"))
            destStoreName = "Main Lift";
        if(destNumber.equals("LT120LGF") || destNumber.equals("LT120UGF") || destNumber.equals("LT120FF") || destNumber.equals("LT120SF")
                || destNumber.equals("LT120TF") || destNumber.equals("LT120B1")|| destNumber.equals("LT120B2")|| destNumber.equals("LT120B3") )
            destStoreName = "Main Lift";
        else if(destNumber.equals("ESC"))
            destStoreName = "Escalator";
        else if(destNumber.equals("ESC"))
            destStoreName = "Escalator";
        else if(destNumber.equals("ESC-Left"))
            destStoreName = "Escalator";
        else if(destNumber.equals("ESC-Right"))
            destStoreName = "Escalator";

        nodeHash = new Hashtable<Location, String>();
        nodeDistTable = new Hashtable<String, Location>();

        resultantJunctionNodes = new ArrayList<String >();
        resultantBearingHash = new Hashtable<String, Float>();
        bearingsList = new ArrayList<Float>();

        bearingRJNHash = new Hashtable<Float, String>();
        if(!bearingRJNHash.isEmpty())
            bearingRJNHash.clear();

        this.srcNumber = srcNumber;
        this.srcLat = srcLat;
        this.srcLong = srcLong;

        this.destNumber = destNumber;
        this.destLat = destLat;
        this.destLong = destLong;

        this.sortedStoreList = sortedStoreList;

        this.floorName = floorName;
        tnForB3 = new ArrayList<String>();


        setNodeHash(13.058687, 80.26336, "cbf1_1");
        setNodeHash(13.058817, 80.263359, "cbf1_2");
        setNodeHash(13.05877, 80.26336, "cbf1_3");
        setNodeHash(13.058703, 80.263361, "cbf1_4");
        setNodeHash(13.058639, 80.26336, "cbf1_5");
        setNodeHash(13.058601, 80.263359, "cbf1_6");
        setNodeHash(13.058576, 80.26336, "cbf1_7");
        setNodeHash(13.058543, 80.26336, "cbf1_8");
        setNodeHash(13.058525, 80.263361, "cbf1_9");
        setNodeHash(13.058509, 80.26336, "cbf1_10");
        setNodeHash(13.058686, 80.263176, "cbf1_11");
        setNodeHash(13.05864, 80.263177, "cbf1_12");
        setNodeHash(13.058603, 80.263176, "cbf1_13");
        setNodeHash(13.058576, 80.263176, "cbf1_14");
        setNodeHash(13.058527, 80.263175, "cbf1_16");
        setNodeHash(13.058509, 80.263176, "cbf1_17");
        setNodeHash(13.058489, 80.263507, "cbf1_18");
        setNodeHash(13.058487, 80.26336, "cbf1_19");
        setNodeHash(13.058488, 80.263655, "cbf1_20");
        setNodeHash(13.058539, 80.263655, "cbf1_21");
        setNodeHash(13.058539, 80.263878, "cbf1_22");
        setNodeHash(13.058572, 80.263877, "cbf1_23");
        setNodeHash(13.058664, 80.263655, "cbf1_24");
        setNodeHash(13.058703, 80.263655, "cbf1_25");
        setNodeHash(13.058753, 80.263655, "cbf1_26");
        setNodeHash(13.058786, 80.263655, "cbf1_27");
        setNodeHash(13.058786, 80.263862, "cbf1_28");
        setNodeHash(13.058786, 80.264115, "cbf1_29");
        setNodeHash(13.05887, 80.264115, "cbf1_30");
        setNodeHash(13.058702, 80.264115, "cbf1_31");
        setNodeHash(13.058577, 80.264115, "cbf1_32");

        setDistHash(13.058687, 80.26336, "cbf1_1");
        setDistHash(13.058817, 80.263359, "cbf1_2");
        setDistHash(13.05877, 80.26336, "cbf1_3");
        setDistHash(13.058703, 80.263361, "cbf1_4");
        setDistHash(13.058639, 80.26336, "cbf1_5");
        setDistHash(13.058601, 80.263359, "cbf1_6");
        setDistHash(13.058576, 80.26336, "cbf1_7");
        setDistHash(13.058543, 80.26336, "cbf1_8");
        setDistHash(13.058525, 80.263361, "cbf1_9");
        setDistHash(13.058509, 80.26336, "cbf1_10");
        setDistHash(13.058686, 80.263176, "cbf1_11");
        setDistHash(13.05864, 80.263177, "cbf1_12");
        setDistHash(13.058603, 80.263176, "cbf1_13");
        setDistHash(13.058576, 80.263176, "cbf1_14");
        setDistHash(13.058527, 80.263175, "cbf1_16");
        setDistHash(13.058509, 80.263176, "cbf1_17");
        setDistHash(13.058489, 80.263507, "cbf1_18");
        setDistHash(13.058487, 80.26336, "cbf1_19");
        setDistHash(13.058488, 80.263655, "cbf1_20");
        setDistHash(13.058539, 80.263655, "cbf1_21");
        setDistHash(13.058539, 80.263878, "cbf1_22");
        setDistHash(13.058572, 80.263877, "cbf1_23");
        setDistHash(13.058664, 80.263655, "cbf1_24");
        setDistHash(13.058703, 80.263655, "cbf1_25");
        setDistHash(13.058753, 80.263655, "cbf1_26");
        setDistHash(13.058786, 80.263655, "cbf1_27");
        setDistHash(13.058786, 80.263862, "cbf1_28");
        setDistHash(13.058786, 80.264115, "cbf1_29");
        setDistHash(13.05887, 80.264115, "cbf1_30");
        setDistHash(13.058702, 80.264115, "cbf1_31");
        setDistHash(13.058577, 80.264115, "cbf1_32");

        setDistHash(Double.parseDouble(srcLat), Double.parseDouble(srcLong), this.srcNumber);
        setDistHash(Double.parseDouble(destLat), Double.parseDouble(destLong), this.destNumber);

        Location srcLocation = new Location("");
        srcLocation.setLatitude(Double.parseDouble(this.srcLat));
        srcLocation.setLongitude(Double.parseDouble(this.srcLong));
        srcBearingLoc = srcLocation;
        nearestToS = getNearesLocation(srcLocation, nodeHash);
        nearestToSource = nearestToS[0];
        sWeight = Double.parseDouble(nearestToS[1]);

        Location destLocation = new Location("");
        destLocation.setLatitude(Double.parseDouble(this.destLat));
        destLocation.setLongitude(Double.parseDouble(this.destLong));
        destBearingLoc = destLocation;
        nearestToD = getNearesLocation(destLocation, nodeHash);
        nearestToDest = nearestToD[0];
        dWeight = Double.parseDouble(nearestToD[1]);
    }

    private void setNodeHash(double lat, double lon, String storeNo)
    {
        lcff1 = new Location(storeNo);
        lcff1.setLatitude(lat);
        lcff1.setLongitude(lon);
        nodeHash.put(lcff1,storeNo);
    }
    private void setDistHash(double lat, double lon, String storeNo)
    {
        lcff1 = new Location(storeNo);
        lcff1.setLatitude(lat);
        lcff1.setLongitude(lon);
        nodeDistTable.put(storeNo, lcff1);
    }
    public String[] getNearesLocation(Location srcLoc, Hashtable<Location, String> srcHash){

        String nearestStore[] = new String[2];
        ArrayList<Double> distValues = new ArrayList<Double>();
        Hashtable<Double, Location> distHash = new Hashtable<Double, Location>();
        Set<Location> locSet = srcHash.keySet();
        for(Location location : locSet){

            double dist = location.distanceTo(srcLoc);
            distValues.add(dist);
            distHash.put(dist,location);
        }

        double smallest = Double.MAX_VALUE;
        for(int i =0;i<distValues.size();i++) {
            if(smallest > distValues.get(i)) {
                smallest = distValues.get(i);
            }
        }

        Location nearLoc = distHash.get(smallest);
        nearestStore[0] = srcHash.get(nearLoc);
        nearestStore[1] = String.valueOf(smallest);

        return nearestStore;
    }

    public ArrayList<String> algorithmImplementation() {

        ArrayList<String> pathVertices = new ArrayList<>();
        pathVertices.clear();

        IntegerNumberSystem NS = IntegerNumberSystem.getInstance();

        MutableDirectedWeightedGraph<String, Integer> graph = MutableDirectedWeightedGraph.create();

        graph.insertVertex("cbf1_1");
        graph.insertVertex("cbf1_2");
        graph.insertVertex("cbf1_3");
        graph.insertVertex("cbf1_4");
        graph.insertVertex("cbf1_5");
        graph.insertVertex("cbf1_6");
        graph.insertVertex("cbf1_7");
        graph.insertVertex("cbf1_8");
        graph.insertVertex("cbf1_9");
        graph.insertVertex("cbf1_10");
        graph.insertVertex("cbf1_11");
        graph.insertVertex("cbf1_12");
        graph.insertVertex("cbf1_13");
        graph.insertVertex("cbf1_14");
        graph.insertVertex("cbf1_15");
        graph.insertVertex("cbf1_16");
        graph.insertVertex("cbf1_17");
        graph.insertVertex("cbf1_18");
        graph.insertVertex("cbf1_19");
        graph.insertVertex("cbf1_20");
        graph.insertVertex("cbf1_21");
        graph.insertVertex("cbf1_22");
        graph.insertVertex("cbf1_23");
        graph.insertVertex("cbf1_24");
        graph.insertVertex("cbf1_25");
        graph.insertVertex("cbf1_26");
        graph.insertVertex("cbf1_27");
        graph.insertVertex("cbf1_28");
        graph.insertVertex("cbf1_29");
        graph.insertVertex("cbf1_30");
        graph.insertVertex("cbf1_31");
        graph.insertVertex("cbf1_32");

        graph.insertVertex(srcNumber);
        graph.insertVertex(destNumber);

        graph.addEdge(srcNumber, nearestToSource, (int) sWeight);
        graph.addEdge(nearestToDest, destNumber, (int) dWeight);

        graph.addEdge(nearestToSource, srcNumber, (int) sWeight);
        graph.addEdge(destNumber, nearestToDest, (int) dWeight);


        //graph.addEdge("cbf1_1", "cbf1_2", (int)nodeDistTable.get("cbf1_1").distanceTo(nodeDistTable.get("cbf1_2")));
        graph.addEdge("cbf1_1", "cbf1_4", (int) nodeDistTable.get("cbf1_1").distanceTo(nodeDistTable.get("cbf1_4")));
        graph.addEdge("cbf1_4", "cbf1_3", (int) nodeDistTable.get("cbf1_4").distanceTo(nodeDistTable.get("cbf1_3")));
        graph.addEdge("cbf1_3", "cbf1_2", (int) nodeDistTable.get("cbf1_3").distanceTo(nodeDistTable.get("cbf1_2")));
        graph.addEdge("cbf1_1", "cbf1_5", (int) nodeDistTable.get("cbf1_1").distanceTo(nodeDistTable.get("cbf1_5")));
        graph.addEdge("cbf1_5", "cbf1_6", (int) nodeDistTable.get("cbf1_5").distanceTo(nodeDistTable.get("cbf1_6")));
        graph.addEdge("cbf1_6", "cbf1_7", (int)nodeDistTable.get("cbf1_6").distanceTo(nodeDistTable.get("cbf1_7")));
        graph.addEdge("cbf1_7", "cbf1_8", (int)nodeDistTable.get("cbf1_7").distanceTo(nodeDistTable.get("cbf1_8")));
        graph.addEdge("cbf1_8", "cbf1_9", (int)nodeDistTable.get("cbf1_8").distanceTo(nodeDistTable.get("cbf1_9")));
        graph.addEdge("cbf1_9", "cbf1_10", (int)nodeDistTable.get("cbf1_9").distanceTo(nodeDistTable.get("cbf1_10")));
        graph.addEdge("cbf1_1", "cbf1_11", (int)nodeDistTable.get("cbf1_1").distanceTo(nodeDistTable.get("cbf1_11")));
        graph.addEdge("cbf1_11", "cbf1_12", (int)nodeDistTable.get("cbf1_11").distanceTo(nodeDistTable.get("cbf1_12")));
        graph.addEdge("cbf1_12", "cbf1_13", (int)nodeDistTable.get("cbf1_12").distanceTo(nodeDistTable.get("cbf1_13")));
        graph.addEdge("cbf1_13", "cbf1_14", (int)nodeDistTable.get("cbf1_13").distanceTo(nodeDistTable.get("cbf1_14")));
        graph.addEdge("cbf1_14", "cbf1_16", (int)nodeDistTable.get("cbf1_14").distanceTo(nodeDistTable.get("cbf1_16")));
        graph.addEdge("cbf1_16", "cbf1_17", (int)nodeDistTable.get("cbf1_16").distanceTo(nodeDistTable.get("cbf1_17")));
        graph.addEdge("cbf1_19", "cbf1_18", (int)nodeDistTable.get("cbf1_19").distanceTo(nodeDistTable.get("cbf1_18")));
        graph.addEdge("cbf1_10", "cbf1_19", (int)nodeDistTable.get("cbf1_10").distanceTo(nodeDistTable.get("cbf1_19")));
        graph.addEdge("cbf1_18", "cbf1_17", (int)nodeDistTable.get("cbf1_18").distanceTo(nodeDistTable.get("cbf1_17")));
        graph.addEdge("cbf1_18", "cbf1_20", (int)nodeDistTable.get("cbf1_18").distanceTo(nodeDistTable.get("cbf1_20")));
        graph.addEdge("cbf1_20", "cbf1_21", (int)nodeDistTable.get("cbf1_20").distanceTo(nodeDistTable.get("cbf1_21")));
        graph.addEdge("cbf1_21", "cbf1_22", (int)nodeDistTable.get("cbf1_21").distanceTo(nodeDistTable.get("cbf1_22")));
        graph.addEdge("cbf1_21", "cbf1_24", (int)nodeDistTable.get("cbf1_21").distanceTo(nodeDistTable.get("cbf1_24")));
        graph.addEdge("cbf1_22", "cbf1_23", (int)nodeDistTable.get("cbf1_22").distanceTo(nodeDistTable.get("cbf1_23")));
        graph.addEdge("cbf1_24", "cbf1_11", (int)nodeDistTable.get("cbf1_24").distanceTo(nodeDistTable.get("cbf1_11")));
        graph.addEdge("cbf1_24", "cbf1_25", (int)nodeDistTable.get("cbf1_24").distanceTo(nodeDistTable.get("cbf1_25")));
        graph.addEdge("cbf1_25", "cbf1_26", (int)nodeDistTable.get("cbf1_25").distanceTo(nodeDistTable.get("cbf1_26")));
        graph.addEdge("cbf1_26", "cbf1_27", (int)nodeDistTable.get("cbf1_26").distanceTo(nodeDistTable.get("cbf1_27")));
        graph.addEdge("cbf1_27", "cbf1_28", (int)nodeDistTable.get("cbf1_27").distanceTo(nodeDistTable.get("cbf1_28")));
        graph.addEdge("cbf1_28", "cbf1_29", (int)nodeDistTable.get("cbf1_28").distanceTo(nodeDistTable.get("cbf1_29")));
        graph.addEdge("cbf1_29", "cbf1_30", (int)nodeDistTable.get("cbf1_29").distanceTo(nodeDistTable.get("cbf1_30")));
        graph.addEdge("cbf1_29", "cbf1_31", (int)nodeDistTable.get("cbf1_29").distanceTo(nodeDistTable.get("cbf1_31")));
        graph.addEdge("cbf1_31", "cbf1_32", (int)nodeDistTable.get("cbf1_31").distanceTo(nodeDistTable.get("cbf1_32")));


        //graph.addEdge("cbf1_2", "cbf1_1", (int)nodeDistTable.get("cbf1_1").distanceTo(nodeDistTable.get("cbf1_2")));
        graph.addEdge("cbf1_4", "cbf1_1", (int)nodeDistTable.get("cbf1_1").distanceTo(nodeDistTable.get("cbf1_4")));
        graph.addEdge("cbf1_3", "cbf1_4", (int)nodeDistTable.get("cbf1_4").distanceTo(nodeDistTable.get("cbf1_3")));
        graph.addEdge("cbf1_2", "cbf1_3", (int)nodeDistTable.get("cbf1_3").distanceTo(nodeDistTable.get("cbf1_2")));
        graph.addEdge("cbf1_5", "cbf1_1", (int)nodeDistTable.get("cbf1_1").distanceTo(nodeDistTable.get("cbf1_5")));
        graph.addEdge("cbf1_6", "cbf1_5", (int)nodeDistTable.get("cbf1_5").distanceTo(nodeDistTable.get("cbf1_6")));
        graph.addEdge("cbf1_7", "cbf1_6", (int)nodeDistTable.get("cbf1_7").distanceTo(nodeDistTable.get("cbf1_6")));
        graph.addEdge("cbf1_8", "cbf1_7", (int)nodeDistTable.get("cbf1_7").distanceTo(nodeDistTable.get("cbf1_8")));
        graph.addEdge("cbf1_9", "cbf1_8", (int)nodeDistTable.get("cbf1_8").distanceTo(nodeDistTable.get("cbf1_9")));
        graph.addEdge("cbf1_10", "cbf1_9", (int)nodeDistTable.get("cbf1_9").distanceTo(nodeDistTable.get("cbf1_10")));
        graph.addEdge("cbf1_11", "cbf1_1", (int)nodeDistTable.get("cbf1_1").distanceTo(nodeDistTable.get("cbf1_11")));
        graph.addEdge("cbf1_12", "cbf1_11", (int)nodeDistTable.get("cbf1_11").distanceTo(nodeDistTable.get("cbf1_12")));
        graph.addEdge("cbf1_13", "cbf1_12", (int)nodeDistTable.get("cbf1_12").distanceTo(nodeDistTable.get("cbf1_13")));
        graph.addEdge("cbf1_14", "cbf1_13", (int)nodeDistTable.get("cbf1_13").distanceTo(nodeDistTable.get("cbf1_14")));
        graph.addEdge("cbf1_16", "cbf1_14", (int)nodeDistTable.get("cbf1_14").distanceTo(nodeDistTable.get("cbf1_16")));
        graph.addEdge("cbf1_17", "cbf1_16", (int)nodeDistTable.get("cbf1_16").distanceTo(nodeDistTable.get("cbf1_17")));
        graph.addEdge("cbf1_18", "cbf1_19", (int)nodeDistTable.get("cbf1_19").distanceTo(nodeDistTable.get("cbf1_18")));
        graph.addEdge("cbf1_11", "cbf1_24", (int)nodeDistTable.get("cbf1_11").distanceTo(nodeDistTable.get("cbf1_24")));
        graph.addEdge("cbf1_19", "cbf1_10", (int)nodeDistTable.get("cbf1_10").distanceTo(nodeDistTable.get("cbf1_19")));
        graph.addEdge("cbf1_17", "cbf1_18", (int)nodeDistTable.get("cbf1_18").distanceTo(nodeDistTable.get("cbf1_17")));
        graph.addEdge("cbf1_20", "cbf1_18", (int)nodeDistTable.get("cbf1_18").distanceTo(nodeDistTable.get("cbf1_20")));
        graph.addEdge("cbf1_21", "cbf1_20", (int)nodeDistTable.get("cbf1_20").distanceTo(nodeDistTable.get("cbf1_21")));
        graph.addEdge("cbf1_22", "cbf1_21", (int)nodeDistTable.get("cbf1_21").distanceTo(nodeDistTable.get("cbf1_22")));
        graph.addEdge("cbf1_24", "cbf1_21", (int)nodeDistTable.get("cbf1_21").distanceTo(nodeDistTable.get("cbf1_24")));
        graph.addEdge("cbf1_23", "cbf1_22", (int)nodeDistTable.get("cbf1_22").distanceTo(nodeDistTable.get("cbf1_23")));
        graph.addEdge("cbf1_25", "cbf1_24", (int)nodeDistTable.get("cbf1_24").distanceTo(nodeDistTable.get("cbf1_25")));
        graph.addEdge("cbf1_26", "cbf1_25", (int)nodeDistTable.get("cbf1_25").distanceTo(nodeDistTable.get("cbf1_26")));
        graph.addEdge("cbf1_27", "cbf1_26", (int)nodeDistTable.get("cbf1_26").distanceTo(nodeDistTable.get("cbf1_27")));
        graph.addEdge("cbf1_28", "cbf1_27", (int)nodeDistTable.get("cbf1_27").distanceTo(nodeDistTable.get("cbf1_28")));
        graph.addEdge("cbf1_29", "cbf1_28", (int)nodeDistTable.get("cbf1_28").distanceTo(nodeDistTable.get("cbf1_29")));
        graph.addEdge("cbf1_30", "cbf1_29", (int)nodeDistTable.get("cbf1_29").distanceTo(nodeDistTable.get("cbf1_30")));
        graph.addEdge("cbf1_31", "cbf1_29", (int)nodeDistTable.get("cbf1_29").distanceTo(nodeDistTable.get("cbf1_31")));
        graph.addEdge("cbf1_32", "cbf1_31", (int)nodeDistTable.get("cbf1_31").distanceTo(nodeDistTable.get("cbf1_32")));


        DijkstraAlgorithm dijkstra = GoodDijkstraAlgorithm.getInstance();
        SingleSourceShortestPathResult<String, Integer, DirectedWeightedEdge<String, Integer>> result = dijkstra.calc(graph, srcNumber, NS);

        Iterable<DirectedWeightedEdge<String, Integer>> pathToD = result.getPath(destNumber);
        System.out.println(pathToD);
        Iterator<DirectedWeightedEdge<String, Integer>> it = pathToD.iterator();

        int count = 0;
        while(it.hasNext()){
            DirectedWeightedEdge<String, Integer> item = it.next();
            if(count == 0){
                pathVertices.add(item.from());
                pathVertices.add(item.to());
            }if(count > 0){
                pathVertices.add(item.to());
            }
            count++;
        }
        System.out.println("shortest path :"+ pathVertices);

        String path = "";
        for(String node : pathVertices){
            path = path + node + "*";
        }

        GlobalState.drawPath = path;
        //Getting junction nodes for taking bearings
        resultantJunctionNodes.clear();
        for(int i=0; i < pathVertices.size(); i++){
            for(String node : junctionNodes){
                if(pathVertices.get(i).equals(node)){
                    resultantJunctionNodes.add(node);
                }
            }
        }

        if(!juctionNodeLocation.isEmpty())
            juctionNodeLocation.clear();

        for(int i = 0; i < resultantJunctionNodes.size(); i++){
            juctionNodeLocation.add(nodeDistTable.get(resultantJunctionNodes.get(i)));
        }

        for (int i=0; i < resultantJunctionNodes.size(); i++){
            if(nearestToSource.equals(resultantJunctionNodes.get(i))){
                resultantJunctionNodes.remove(i);
            }
        }

        float preSrcBearing =  getBearings(srcBearingLoc, nodeDistTable.get(nearestToSource));
        bearingRJNHash.put(preSrcBearing, "NA");

        Location dst = null;
        Location src = null;
        if(resultantJunctionNodes.size() == 0){
            src = nodeDistTable.get(nearestToSource);
            dst = nodeDistTable.get(nearestToDest);
        }else {
            src = nodeDistTable.get(nearestToSource);
            dst = nodeDistTable.get(resultantJunctionNodes.get(0));
        }

        float srcBearing =  getBearings(src, dst);

        if(resultantJunctionNodes.size() ==0)
            bearingRJNHash.put(srcBearing, nearestToDest);
        else
            bearingRJNHash.put(srcBearing, resultantJunctionNodes.get(0));

        bearingsList.add(preSrcBearing);
        bearingsList.add(srcBearing);

        for (int i=0; i < resultantJunctionNodes.size()-1; i++){

            Location firstLoc = nodeDistTable.get(resultantJunctionNodes.get(i));
            Location secondLoc = nodeDistTable.get(resultantJunctionNodes.get(i + 1));
            float resultantBearing =  getBearings(firstLoc, secondLoc);

            bearingRJNHash.put(resultantBearing,resultantJunctionNodes.get(i + 1));

            bearingsList.add(resultantBearing);
        }

        for (int i=0; i < resultantJunctionNodes.size(); i++){
            if(nearestToDest.equals(resultantJunctionNodes.get(i))){
                resultantJunctionNodes.remove(i);
            }
        }

        float preDestBearing = 0;

        if(resultantJunctionNodes.size() == 0)
            preDestBearing =  getBearings(nodeDistTable.get(nearestToDest),nodeDistTable.get(nearestToDest) );
        else
            preDestBearing =  getBearings(nodeDistTable.get(resultantJunctionNodes.get(resultantJunctionNodes.size()-1)),nodeDistTable.get(nearestToDest) );

        bearingRJNHash.put(preDestBearing, nearestToDest);
        float destBearing =  getBearings(nodeDistTable.get(nearestToDest), destBearingLoc );
        bearingRJNHash.put(destBearing, destNumber);
        bearingsList.add(preDestBearing);
        bearingsList.add(destBearing);

        TextNav textNav = new TextNav(srcBearingLoc, destBearingLoc, pathVertices, nodeDistTable, sortedStoreList, floorName);
        if(!tnForB3.isEmpty())
            tnForB3.clear();

        GlobalState.tnPathB3 = "";
        GlobalState.tnSrcStoreB3 = "";
        GlobalState.tnDestStoreB3 = "";

        tnForB3 = textNav.getTextNav();
        for(int i =0; i < tnForB3.size(); i++){
            GlobalState.tnPathB3 = GlobalState.tnPathB3 + tnForB3.get(i) + "#";
        }
        GlobalState.tnSrcStoreB3 = srcStoreName;
        GlobalState.tnDestStoreB3 = destStoreName;

        return pathVertices;
    }
    private float getBearings(Location sLoc, Location dLoc){
        float bearing = 0;

        Location firstLoc = sLoc;
        Location secondLoc = dLoc;
        bearing = firstLoc.bearingTo(secondLoc);
        return bearing;
    }
}
