package com.expressavenue.mallapp.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.expressavenue.mallapp.R;
import com.expressavenue.mallapp.services.UpdateStoresService;
import com.expressavenue.mallapp.utils.NetworkUtils;
import com.expressavenue.mallapp.utils.Utils;

/**
 * Created by anisha on 2/3/17.
 */

public class NetworkChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, final Intent intent) {
        String status = NetworkUtils.getConnectivityStatusString(context);
        if(!Utils.isAppIsInBackground(context)){
            Utils.showToast(context, status);

            String wifi = context.getResources().getString(R.string.app_name) + " "+
                    context.getResources().getString(R.string.wifi_enabled);
            String mobile = context.getResources().getString(R.string.app_name) + " "+
                    context.getResources().getString(R.string.mobile_data_enabled);

            if(status.equals(wifi) || status.equals(mobile)){
                context.startService(new Intent(context, UpdateStoresService.class));
            }
        }
    }
}
