package com.expressavenue.mallapp.receivers;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.parse.ParsePushBroadcastReceiver;
import com.expressavenue.mallapp.R;
import com.expressavenue.mallapp.activity.EventDetailsActivity;
import com.expressavenue.mallapp.activity.OfferByCategoryActivity;
import com.expressavenue.mallapp.activity.OfferDetailsActivity; ;
import com.expressavenue.mallapp.global.GlobalState;
import com.expressavenue.mallapp.services.OfflineDataSyncService;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by nivedith on 1/18/2016.
 */
public class CustomPushReceiver extends ParsePushBroadcastReceiver{


    private String contentString = "";
    private JSONObject notificationJsonObject = new JSONObject();
    private String[] notificationContentArray ;
    private String notificationTitle = "";
    private String notificationBody = "";
    private String alert = "";
    private String storeName = "";
    private String storeCategory = "";
    private String storeFloor = "";
    private String storeLatitude = "";
    private String storeLongitude = "";
    private String storeLogo = "";
    private String storeDesc = "";
    private String storeNumber = "";

    private String offerId = "";
    private String offerType = "";
    private String offerName = "";
    private String storeId = "";
    private String offerLogo = "";
    private String offerDesc = "";
    private String offerStartDate = "";
    private String offerEndDate = "";
    private String offerStartTime = "";
    private String offerEndTime = "";

    private boolean storeStatus = false;
    private boolean isStoreDeleted = true;

    final private int NOTIFICATION_ID = 1;
    private int pendingNotificationsCount = 0;

    private SharedPreferences eapref = GlobalState.eaPref;;
    private SharedPreferences.Editor editor = GlobalState.editor;

    private Context context;

    @Override
    protected void onPushReceive(Context context, Intent intent) {
       // super.onPushReceive(context, intent);
        this.context = context;

        Intent startServiceIntent = new Intent(context, OfflineDataSyncService.class);
        context.startService(startServiceIntent);

        Log.i("INSIDE", "onPushReceive");
        pendingNotificationsCount = eapref.getInt("NOTIFICATION_COUNT", 0) + 1;
        editor.putInt("NOTIFICATION_COUNT", pendingNotificationsCount);
        editor.commit();

        try {
            contentString = new String(intent.getExtras().getString("com.parse.Data"));
            String notificationText = contentString;
            try {

                notificationJsonObject = new JSONObject(notificationText);
                Log.i("JSON Notification", notificationText);
                alert = notificationJsonObject.getString("alert");
                offerType = notificationJsonObject.getString("offer_type");


            } catch (Throwable t) {
                Log.e("Exception", "Could not parse malformed JSON: \"" + notificationText + "\"");
            }

        } catch (Exception e) {
            Log.i("Exception: " , e.getMessage());
        }

    if(offerType.equals("Offer")){

        try {
            offerName = notificationJsonObject.getString("offer_name");
            storeId = notificationJsonObject.getString("storeId");
            offerDesc = notificationJsonObject.getString("offerDesc");
            offerStartDate = notificationJsonObject.getString("start_date_string");
            offerStartTime = notificationJsonObject.getString("start_time");
            offerEndDate = notificationJsonObject.getString("end_date_string");
            offerEndTime = notificationJsonObject.getString("end_time");

            storeName = notificationJsonObject.getString("sName");
            storeCategory = notificationJsonObject.getString("sCategory");
            storeFloor = notificationJsonObject.getString("sFloor");
            storeLatitude = notificationJsonObject.getString("sLatitude");
            storeLongitude = notificationJsonObject.getString("sLongitude");
            storeDesc = "NA";
            storeNumber = notificationJsonObject.getString("sNumber");
            isStoreDeleted = notificationJsonObject.getBoolean("deleted");
            storeLogo = notificationJsonObject.getString("sLogoName");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        notificationContentArray = alert.split("!!");
        notificationTitle = notificationContentArray[0];
        notificationBody = notificationContentArray[1];
        Intent resultIntent = new Intent(context, OfferDetailsActivity.class);
        resultIntent.putExtra("FROM", "PUSH");
        resultIntent.putExtra("offerType",offerType);
        resultIntent.putExtra("offerName",offerName);
        resultIntent.putExtra("storeId",storeId);
        resultIntent.putExtra("offerDesc",offerDesc);
        resultIntent.putExtra("offerStartDate",offerStartDate);
        resultIntent.putExtra("offerStartTime",offerStartTime);
        resultIntent.putExtra("offerEndDate",offerEndDate);
        resultIntent.putExtra("offerEndTime",offerEndTime);
        resultIntent.putExtra("storeName",storeName);
        resultIntent.putExtra("storeNumber",storeNumber);
        resultIntent.putExtra("storeCategory",storeCategory);
        resultIntent.putExtra("storeFloor",storeFloor);
        resultIntent.putExtra("storeLatitude",storeLatitude);
        resultIntent.putExtra("storeLongitude",storeLongitude);
        resultIntent.putExtra("storeDesc",storeDesc);
        resultIntent.putExtra("storeLogo", storeLogo);
        resultIntent.putExtra("storeStatus",storeStatus);
        resultIntent.putExtra("isStoreDeleted",isStoreDeleted);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(OfferDetailsActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification.InboxStyle inboxStyle = new Notification.InboxStyle();
        NotificationManager nManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
        Notification.Builder builder = new Notification.Builder(context);
        builder.setContentIntent(resultPendingIntent);
        builder.setAutoCancel(true);
        if(pendingNotificationsCount == 1){
            builder.setContentTitle(notificationTitle);
            builder.setContentText(notificationBody);
        }else {
            builder.setContentTitle("You have " + pendingNotificationsCount + " notifications.");
            builder.setContentText(alert);
        }
        builder.setSmallIcon(R.mipmap.ic_launchericon);
        builder.setStyle(inboxStyle);
        builder.setVibrate(new long[]{500,1000});
        Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder.setSound(uri);
        Notification notification = new Notification.BigTextStyle(builder)
                .bigText(alert).build();
        nManager.notify(NOTIFICATION_ID, notification);
    }else if(offerType.equals("Event")){

        try {
            offerName = notificationJsonObject.getString("offer_name");
            offerDesc = notificationJsonObject.getString("offerDesc");
            offerStartDate = notificationJsonObject.getString("start_date_string");
            offerStartTime = notificationJsonObject.getString("start_time");
            offerEndDate = notificationJsonObject.getString("end_date_string");
            offerEndTime = notificationJsonObject.getString("end_time");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        notificationContentArray = alert.split("!!");
        notificationTitle = notificationContentArray[0];
        notificationBody = notificationContentArray[1];
        Intent resultIntent = new Intent(context, EventDetailsActivity.class);
        resultIntent.putExtra("FROM", "PUSH");
        resultIntent.putExtra("offerName",offerName);
        resultIntent.putExtra("offerDesc",offerDesc);
        resultIntent.putExtra("offerStartDate",offerStartDate);
        resultIntent.putExtra("offerStartTime",offerStartTime);
        resultIntent.putExtra("offerEndDate",offerEndDate);
        resultIntent.putExtra("offerEndTime",offerEndTime);
        resultIntent.putExtra("storeId",storeId);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(OfferByCategoryActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification.InboxStyle inboxStyle = new Notification.InboxStyle();
        NotificationManager nManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
        Notification.Builder builder = new Notification.Builder(context);
        builder.setContentIntent(resultPendingIntent);
        builder.setAutoCancel(true);
        if(pendingNotificationsCount == 1){
            builder.setContentTitle(notificationTitle);
            builder.setContentText(notificationBody);
        }else {
            builder.setContentTitle("You have " + pendingNotificationsCount + " notifications.");
            builder.setContentText(alert);
        }
        builder.setSmallIcon(R.mipmap.ic_launchericon);
        builder.setStyle(inboxStyle);
        builder.setVibrate(new long[]{500,1000});
        Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder.setSound(uri);
        Notification notification = new Notification.BigTextStyle(builder)
                .bigText(alert).build();
        nManager.notify(NOTIFICATION_ID, notification);
    }


    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
    }

  /*  @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        this.context = context;

        eapref = GlobalState.eaPref;
        editor = GlobalState.editor;

        Log.i("INSIDE", "onReceive");
        pendingNotificationsCount = eapref.getInt("NOTIFICATION_COUNT", 0) + 1;
        editor.putInt("NOTIFICATION_COUNT", pendingNotificationsCount);
        editor.commit();

        try {
            contentString = new String(intent.getExtras().getString("com.parse.Data"));
            String notificationText = contentString;
            try {

                notificationJsonObject = new JSONObject(notificationText);
                alert = notificationJsonObject.getString("alert");
                notificationContentArray = alert.split("!!");
                notificationTitle = notificationContentArray[0];
                notificationBody = notificationContentArray[1];

            } catch (Throwable t) {
                Log.e("Exception", "Could not parse malformed JSON: \"" + notificationText + "\"");
            }

        } catch (Exception e) {
            Log.i("Exception: " , e.getMessage());
        }

        Intent resultIntent = new Intent(context, OfferByCategoryActivity.class);
        resultIntent.putExtra("FROM", "PUSH");
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(OfferByCategoryActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification.InboxStyle inboxStyle = new Notification.InboxStyle();
        NotificationManager nManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
        Notification.Builder builder = new Notification.Builder(context);
        builder.setContentIntent(resultPendingIntent);
        builder.setAutoCancel(true);
        if(pendingNotificationsCount == 1){
            builder.setContentTitle(notificationTitle);
            builder.setContentText(notificationBody);
        }else {
            builder.setContentTitle("You have " + pendingNotificationsCount + " notifications.");
            builder.setContentText(alert);
        }
        builder.setSmallIcon(R.mipmap.ic_launchericon);
        builder.setStyle(inboxStyle);
        builder.setVibrate(new long[]{500,1000});
        Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder.setSound(uri);
        nManager.notify(NOTIFICATION_ID, builder.build());
    }*/

    @Override
    protected void onPushOpen(Context context, Intent intent) {
        super.onPushOpen(context, intent);
        Log.i("INSIDE", "onPushOpen");
        editor.putInt("NOTIFICATION_COUNT", 0);
        editor.commit();

    }

    @Override
    protected void onPushDismiss(Context context, Intent intent) {
        super.onPushDismiss(context, intent);
        Log.i("INSIDE", "onPushDismiss");
        editor.putInt("NOTIFICATION_COUNT", 0);
        editor.commit();
    }
}
