package com.expressavenue.mallapp.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.expressavenue.mallapp.R;
import com.expressavenue.mallapp.adapters.EntertainmentListAdapter;
import com.expressavenue.mallapp.pojos.Store;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by nivedith on 12/28/2015.
 */
public class EntertainmentsFragment extends Fragment {

    private ArrayList entertainmentArrayList;
    private ArrayList sortedErtainmentList;

    private EntertainmentListAdapter entertainmentListAdapter;
    private RecyclerView recyclerView;

    private ProgressBar progressBar;

    public EntertainmentsFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_stores, container, false);

        progressBar = (ProgressBar) view.findViewById(R.id.progressbar_stores);

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerview_store);
        recyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getActivity())
                .marginResId(R.dimen.pad_10dp, R.dimen.pad_10dp).build());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);


        loadAtmsInAlphabeticOrder();

        return view;
    }

    private int loadAtmsInAlphabeticOrder()
    {
        entertainmentArrayList = new ArrayList<Store>();
        sortedErtainmentList = new ArrayList<Store>(entertainmentArrayList);
        entertainmentArrayList.clear();

        ParseQuery<ParseObject> query = ParseQuery.getQuery(getResources().getString(R.string.store_class_name));
        query.fromLocalDatastore();
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> storesList, ParseException e) {
                if (e == null) {
                    progressBar.setVisibility(View.GONE);
                    for (ParseObject store : storesList) {

                        String storeName = "";
                        String storeCategory = "";
                        String storeFloor = "";
                        String storeLatitude = "";
                        String storeLongitude = "";
                        String storeLogo = "";
                        String storeDesc = "";
                        String storeNumber = "";

                        boolean storeStatus = false;
                        boolean isStoreDeleted = true;

                        if(store.has("sCategory")) {
                            if (store.get("sCategory").equals("ENTERTAINMENT")) {

                                if(store.has("deleted"))
                                    isStoreDeleted = store.getBoolean("deleted");
                                else
                                    isStoreDeleted = true;

                                if(store.has("sStatus"))
                                    storeStatus = store.getBoolean("sStatus");
                                else
                                    storeStatus = false;

                                if(store.has("sName"))
                                    storeName = store.getString("sName");
                                else
                                    storeName = "NA";

                                if(store.has("sCategory"))
                                    storeCategory = store.getString("sCategory");
                                else
                                    storeCategory = "NA";

                                if(store.has("sFloor"))
                                    storeFloor = store.getString("sFloor");
                                else
                                    storeFloor = "NA";

                                if(store.has("sLatitude"))
                                    storeLatitude = store.getString("sLatitude");
                                else
                                    storeLatitude = "NA";

                                if(store.has("sLongitude"))
                                    storeLongitude = store.getString("sLongitude");
                                else
                                    storeLongitude = "NA";

                                if(store.has("sLogoName")){
                                    storeLogo = store.getString("sLogoName");
                                }
                                else
                                    storeLogo = "NA";

                                if(store.has("sDesc"))
                                    storeDesc = store.getString("sDesc");
                                else
                                    storeDesc = "NA";

                                if(store.has("sNumber"))
                                    storeNumber = store.getString("sNumber");
                                else
                                    storeNumber = "NA";

                                if(storeStatus == true && isStoreDeleted==false){
                                    entertainmentArrayList.add(new Store(storeName, storeCategory, storeFloor, storeLatitude,
                                            storeLongitude, storeLogo, storeDesc, storeNumber, storeStatus, isStoreDeleted));
                                }

                            }
                        }

                    }
                    Collections.sort(entertainmentArrayList, new CustomComparator());

                    sortedErtainmentList.clear();
                    sortedErtainmentList.addAll(entertainmentArrayList);

                    entertainmentListAdapter = new EntertainmentListAdapter(sortedErtainmentList, getActivity());
                    recyclerView.setAdapter(entertainmentListAdapter);

                } else {

                }
            }

        });

        return sortedErtainmentList.size();
    }

    public class CustomComparator implements Comparator<Store> {
        @Override
        public int compare(Store o1, Store o2) {
            return o1.getStoreName().compareTo(o2.getStoreName());
        }
    }

}
