package com.expressavenue.mallapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.expressavenue.mallapp.R;
import com.expressavenue.mallapp.adapters.RestaurantListAdapter;
import com.expressavenue.mallapp.global.GlobalState;
import com.expressavenue.mallapp.pojos.Store;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by nivedith on 1/14/2016.
 */
public class RestaurantsFragment extends Fragment {

    private ArrayList restaurantArrayList;
    private ArrayList sortedRestaurantList;

    private RestaurantListAdapter restaurantListAdapter;
    private RecyclerView recyclerView;

    private ProgressBar progressBar;

    public RestaurantsFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_stores, container, false);

        progressBar = (ProgressBar) view.findViewById(R.id.progressbar_stores);

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerview_store);
        recyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getActivity())
                .marginResId(R.dimen.pad_10dp, R.dimen.pad_10dp).build());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);


        loadRestaurantsInAlphabeticOrder();

        return view;
    }

    private int loadRestaurantsInAlphabeticOrder()
    {
        restaurantArrayList = new ArrayList<Store>();
        sortedRestaurantList = new ArrayList<Store>(restaurantArrayList);
        restaurantArrayList.clear();

        ParseQuery<ParseObject> query = ParseQuery.getQuery(getResources().getString(R.string.store_class_name));
        query.fromLocalDatastore();
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> storesList, ParseException e) {
                if (e == null) {
                    progressBar.setVisibility(View.GONE);
                    for (ParseObject store : storesList) {

                        String storeName = "";
                        String storeCategory = "";
                        String storeFloor = "";
                        String storeLatitude = "";
                        String storeLongitude = "";
                        String storeLogo = "";
                        String storeDesc = "";
                        String storeNumber = "";

                        boolean storeStatus = false;
                        boolean isStoreDeleted = true;

                        if(store.has("sCategory")) {
                            if (store.get("sCategory").equals("RESTAURANTS-CAFE")) {

                                if(store.has("deleted"))
                                    isStoreDeleted = store.getBoolean("deleted");
                                else
                                    isStoreDeleted = true;

                                if(store.has("sStatus"))
                                    storeStatus = store.getBoolean("sStatus");
                                else
                                    storeStatus = false;

                                if(store.has("sName"))
                                    storeName = store.getString("sName");
                                else
                                    storeName = "NA";

                                if(store.has("sCategory"))
                                    storeCategory = store.getString("sCategory");
                                else
                                    storeCategory = "NA";

                                if(store.has("sFloor"))
                                    storeFloor = store.getString("sFloor");
                                else
                                    storeFloor = "NA";

                                if(store.has("sLatitude"))
                                    storeLatitude = store.getString("sLatitude");
                                else
                                    storeLatitude = "NA";

                                if(store.has("sLongitude"))
                                    storeLongitude = store.getString("sLongitude");
                                else
                                    storeLongitude = "NA";

                                if(store.has("sLogoName")){
                                    storeLogo = store.getString("sLogoName");
                                }
                                else
                                    storeLogo = "NA";

                                if(store.has("sDesc"))
                                    storeDesc = store.getString("sDesc");
                                else
                                    storeDesc = "NA";

                                if(store.has("sNumber"))
                                    storeNumber = store.getString("sNumber");
                                else
                                    storeNumber = "NA";

                                if(storeStatus == true && isStoreDeleted==false){
                                    restaurantArrayList.add(new Store(storeName, storeCategory, storeFloor, storeLatitude,
                                            storeLongitude, storeLogo, storeDesc, storeNumber, storeStatus, isStoreDeleted));
                                }

                            }
                        }

                    }
                    Collections.sort(restaurantArrayList, new CustomComparator());

                    sortedRestaurantList.clear();
                    sortedRestaurantList.addAll(restaurantArrayList);

                    restaurantListAdapter = new RestaurantListAdapter(sortedRestaurantList, GlobalState.appContext);
                    recyclerView.setAdapter(restaurantListAdapter);

                } else {

                }
            }

        });

        return sortedRestaurantList.size();
    }

    public class CustomComparator implements Comparator<Store> {
        @Override
        public int compare(Store o1, Store o2) {
            return o1.getStoreName().compareTo(o2.getStoreName());
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(this.isVisible()){
            GlobalState.currentFragment = "RESTAURANT";
        }
    }
}

