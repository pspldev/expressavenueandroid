package com.expressavenue.mallapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.expressavenue.mallapp.R;

/**
 * Created by dhanil on 03-01-2016.
 */
public class AboutUsFragment extends Fragment {

    private TextView txtAboutUs;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_aboutus, container, false);
        txtAboutUs = (TextView) view.findViewById(R.id.txtaboutus);
        txtAboutUs.setText(Html.fromHtml("<p>Express Avenue – The Destination Mall South India's second largest mall, is designed keeping in mind the 'shopper-tainment' concept and this green building developed on 11 acres of land has many architecture splendor and has been designed by ace designer - Mohit Gujral.\n<br><br>" +
                "\n" +
                "It has 4 floors of retail space including corporate offices and a hotel. The 9,00,000 sq. ft. of retail space houses 210 premium national and international brands. The mall has plenty of drive-in space and ample car park that extends to 5,00,000 sq.ft. There are three basement parking lots that can easily accommodate 2000 vehicles. Built at a cost of 750 crore, the mall houses a classy food court and a multiplex. The 50,000 sq.ft. food court called EA Garden, has 25 counters with several food and beverage options. That's not all, on the entertainment front - Satyam Cinemas' is redefining the cinema experience with an eight-screen multiplex a total of 1,600 seats in a sprawling 40,000 sq.ft.\n<br><br>" +
                "\n" +
                "Designed to global standards, the mall has six atriums with a height not less than 75 feet each. The atrium areas allow plenty of natural light into the common area. It's covered with tensile fabric which is an environmentally sensitive medium and provides a combined daytime translucency and night-time luminosity that gives a magical feeling of being outdoors. The centre of the atrium is covered with glass. Out of the total 11 acres, only 3.57 acres will be taken up by buildings. The rest has been provided for car parking, drive-ways and landscaping. In all, the mall has 26 lifts, 34 escalators and four travelators. Realizing the need for open spaces amid concrete structures, the building has ensured that mall-hoppers will enjoy the luxury of a huge open area in the heart of the city. It's designed in such a way that the mall is flanked by 10-storey buildings. One building will be the corporate block and the other will be the hotel.</p>"));
        return view;
    }
}
