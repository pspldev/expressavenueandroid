package com.expressavenue.mallapp.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.expressavenue.mallapp.R;
import com.expressavenue.mallapp.adapters.CategoryListAdapter;
import com.expressavenue.mallapp.global.GlobalState;
import com.expressavenue.mallapp.pojos.Category;
import com.expressavenue.mallapp.pojos.Store;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by dhanil on 20-12-2015.
 */
public class CategoryFragment extends Fragment {

    private ArrayList<Category> sortedCategoriesList;
    private ArrayList<Category> categoryArrayList;

    private CategoryListAdapter categoryListAdapter;
    private RecyclerView recyclerView;

    private ProgressBar progressBar;
    private TextView txtNoCat;

    ArrayList<Store> storeArrayList;
    ArrayList<Store> sortedStoreList;

    public CategoryFragment() {
        getStores();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_categories, container, false);

        progressBar = (ProgressBar) view.findViewById(R.id.progressbar_category);
        txtNoCat = (TextView) view.findViewById(R.id.txt_nocat);
        txtNoCat.setVisibility(View.GONE);

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerview_category);
        recyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getActivity())
                .marginResId(R.dimen.pad_10dp, R.dimen.pad_10dp).build());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        loadCategoriesInAlphabeticOrder();

        return view;
    }

    private void loadCategoriesInAlphabeticOrder()
    {
        categoryArrayList = new ArrayList<Category>();
        sortedCategoriesList = new ArrayList<Category>(categoryArrayList);
        categoryArrayList.clear();

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Categories");
        query.fromPin();
        query.fromLocalDatastore();
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> categoriesList, ParseException e) {
                if (e == null) {
                    progressBar.setVisibility(View.GONE);
                    for (ParseObject category : categoriesList) {

                        String categoryId;
                        String categoryName;
                        String categoyDesc;

                        boolean isCategoryDeleted = false;

                        if (!category.getString("categoryName").equals("LIFT")) {

                            if (category.has("deleted"))
                                isCategoryDeleted = category.getBoolean("deleted");
                            else
                                isCategoryDeleted = true;

                            if (category.has("categoryId"))
                                categoryId = category.getString("categoryId");
                            else
                                categoryId = "NA";

                            if (category.has("categoryName"))
                                categoryName = category.getString("categoryName");
                            else
                                categoryName = "NA";

                            if (category.has("categoyDesc"))
                                categoyDesc = category.getString("categoyDesc");
                            else
                                categoyDesc = "NA";

                            if (!isCategoryDeleted) {
                                int count = 0;
                                for (Store store : sortedStoreList) {
                                    String catName = store.getStoreCategory();
                                    if (categoryName.equals(catName)) {
                                        count = 1;
                                    }
                                }
                                if (count == 1) {
                                    categoryArrayList.add(new Category(categoryId, categoryName, categoyDesc, isCategoryDeleted));
                                }
                            }
                        }

                    }
                    Collections.sort(categoryArrayList, new CustomComparator());

                    sortedCategoriesList.clear();
                    sortedCategoriesList.addAll(categoryArrayList);
                    if (sortedCategoriesList.size() == 0) {
                        txtNoCat.setVisibility(View.VISIBLE);
                    } else {
                        categoryListAdapter = new CategoryListAdapter(sortedCategoriesList, getActivity());
                        recyclerView.setAdapter(categoryListAdapter);
                    }

                } else {

                }
            }
        });
    }

    public class CustomComparator implements Comparator<Category> {
        @Override
        public int compare(Category o1, Category o2) {
            return o1.getCategoryName().compareTo(o2.getCategoryName());
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (this.isVisible()) {
            GlobalState.currentFragment = "CATEGORY";
        }
    }

    private void getStores(){

        storeArrayList = new ArrayList<Store>();
        storeArrayList.clear();
        sortedStoreList = new ArrayList<Store>(storeArrayList);

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Store");
        query.fromLocalDatastore();
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> storesListFromLocal, ParseException e) {
                if (e == null) {
                    for (ParseObject store : storesListFromLocal) {

                        String storeName = "";
                        String storeCategory = "";
                        String storeFloor = "";
                        String storeLatitude = "";
                        String storeLongitude = "";
                        String storeLogo = "";
                        String storeDesc = "";
                        String storeNumber = "";

                        boolean storeStatus = false;
                        boolean isStoreDeleted = true;

                        if (store.has("sFloor")) {
                            if (!store.get("sCategory").equals("GENERAL") && !store.get("sCategory").equals("RESTROOM") &&
                                    !store.get("sCategory").equals("ATM") && !store.get("sCategory").equals("MEDICAL") &&
                                    !store.get("sCategory").equals("ENTERTAINMENT") && !store.get("sCategory").equals("LIFT")) {

                                if (store.has("deleted"))
                                    isStoreDeleted = store.getBoolean("deleted");
                                else
                                    isStoreDeleted = true;

                                if (store.has("sStatus"))
                                    storeStatus = store.getBoolean("sStatus");
                                else
                                    storeStatus = false;

                                if (store.has("sName"))
                                    storeName = store.getString("sName");
                                else
                                    storeName = "NA";

                                if (store.has("sCategory"))
                                    storeCategory = store.getString("sCategory");
                                else
                                    storeCategory = "NA";

                                if (store.has("sFloor"))
                                    storeFloor = store.getString("sFloor");
                                else
                                    storeFloor = "NA";

                                if (store.has("sLatitude"))
                                    storeLatitude = store.getString("sLatitude");
                                else
                                    storeLatitude = "NA";

                                if (store.has("sLongitude"))
                                    storeLongitude = store.getString("sLongitude");
                                else
                                    storeLongitude = "NA";

                                if (store.has("sLogoName")) {
                                    storeLogo = store.getString("sLogoName");
                                } else
                                    storeLogo = "NA";

                                if (store.has("sDesc"))
                                    storeDesc = store.getString("sDesc");
                                else
                                    storeDesc = "NA";

                                if (store.has("sNumber"))
                                    storeNumber = store.getString("sNumber");
                                else
                                    storeNumber = "NA";

                                if (storeStatus == true && isStoreDeleted == false) {
                                    storeArrayList.add(new Store(storeName, storeCategory, storeFloor, storeLatitude,
                                            storeLongitude, storeLogo, storeDesc, storeNumber, storeStatus, isStoreDeleted));
                                }
                            }
                        }
                    }
                    Collections.sort(storeArrayList, new CustomComparatorForStores());
                    sortedStoreList.clear();
                    sortedStoreList.addAll(storeArrayList);

                } else {

                }
            }
        });
    }

    public class CustomComparatorForStores implements Comparator<Store> {
        @Override
        public int compare(Store o1, Store o2) {
            return o1.getStoreName().compareTo(o2.getStoreName());
        }
    }

}
