package com.expressavenue.mallapp.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.expressavenue.mallapp.R;
import com.expressavenue.mallapp.adapters.OfferCategoryListAdapter;
import com.expressavenue.mallapp.global.GlobalState;
import com.expressavenue.mallapp.pojos.Category;
import com.expressavenue.mallapp.pojos.Store;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by nivedith on 12/29/2015.
 */
public class OffersByCategoryFragment extends Fragment {

    private Category category = null;

    private SearchView searchView;

    private ArrayList<Store> storeArrayList;
    private HashMap<String, Integer> categoryCountHash;

    private ArrayList<Category> sortedCategoriesList;
    private ArrayList<Category> categoryArrayList;
    private ListView categoryList;

    private static String categoryQuery = "";
    private ArrayList<Category> sortedCategoryListTemp;

    private String fromWhere = "";
    private ProgressBar progressBar;
    private TextView txtNoEvent;
    private RecyclerView recyclerView;

    private OfferCategoryListAdapter storeListAdapter;

    private int offerCount = 0;

    public OffersByCategoryFragment() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_offers, container, false);

        progressBar = (ProgressBar) view.findViewById(R.id.progressbar_offers);
        txtNoEvent = (TextView) view.findViewById(R.id.txt_nooffer);
        txtNoEvent.setVisibility(View.GONE);

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerview_offers);
        recyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getActivity())
                .marginResId(R.dimen.pad_10dp, R.dimen.pad_10dp).build());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        sortedCategoriesList = new ArrayList<Category>();
        categoryCountHash = new HashMap<String, Integer>();

        loadOffersInAlphabeticOrder();

        return view;
    }

    private int loadOffersInAlphabeticOrder()
    {
        storeArrayList = new ArrayList<Store>();
        storeArrayList.clear();

        ParseQuery<ParseObject> storeQuery = ParseQuery.getQuery(getResources().getString(R.string.store_class_name));
        storeQuery.fromLocalDatastore();
        storeQuery.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> storesListFromLocal, ParseException e) {
                if (e == null) {
                    progressBar.setVisibility(View.GONE);
                    for (ParseObject store : storesListFromLocal) {

                        String storeObjectId = "";
                        String storeCategory = "";
                        String storeName = "";

                        //Restricting RESTROOMs from store listing
                        if (store.has("sCategory")) {

                            if (!store.get("sCategory").equals("GENERAL") && !store.get("sCategory").equals("RESTROOM")) {

                                storeObjectId = store.getObjectId();

                                if (store.has("sCategory"))
                                    storeCategory = store.getString("sCategory");
                                else
                                    storeCategory = "NA";

                                if (store.has("sName"))
                                    storeName = store.getString("sName");
                                else
                                    storeName = "NA";

                                storeArrayList.add(new Store(storeObjectId, storeCategory, storeName));
                            }
                        }
                    }
                } else {

                }
            }

        });

        ParseQuery<ParseObject> offerQuery = ParseQuery.getQuery("Offer");
        offerQuery.fromPin();
        offerQuery.fromLocalDatastore();
        offerQuery.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> offersList, ParseException e) {
                if (e == null) {
                    for (ParseObject offer : offersList) {

                        String storeId = "";
                        boolean isOfferDeleted = true;
                        Date offerEndDate = null;
                        Date offerStartDate = null;

                        if (offer.has("offerType")) {

                            if (offer.get("offerType").equals("Offer")) {

                                if (offer.has("deleted"))
                                    isOfferDeleted = offer.getBoolean("deleted");
                                else
                                    isOfferDeleted = true;

                                if (offer.has("storeId"))
                                    storeId = offer.getString("storeId");
                                else
                                    storeId = "NA";

                                if (offer.has("offerEndDate"))
                                    offerEndDate = offer.getDate("offerEndDate");
                                else
                                    offerEndDate = null;

                                if (offer.has("offerStartDate"))
                                    offerStartDate = offer.getDate("offerStartDate");
                                else
                                    offerStartDate = null;

                                if (!isOfferDeleted) {

                                    Date today = Calendar.getInstance().getTime();

                                    if ((offerEndDate.compareTo(today)) >= 0) {

                                        for (Store stores : storeArrayList) {

                                            if (storeId.equals(stores.getStoreObjectId())) {

                                                String categoryName = stores.getStoreCategory();

                                                if (categoryCountHash.containsKey(categoryName)) {
                                                    int count = categoryCountHash.get(categoryName);
                                                    categoryCountHash.remove(categoryName);
                                                    count = count + 1;
                                                    categoryCountHash.put(categoryName, count);
                                                } else {
                                                    categoryCountHash.put(categoryName, 1);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    categoryArrayList = new ArrayList<Category>();
                    categoryArrayList.clear();

                    ParseQuery<ParseObject> query = ParseQuery.getQuery("Categories");
                    query.fromPin();
                    query.fromLocalDatastore();
                    query.findInBackground(new FindCallback<ParseObject>() {
                        public void done(List<ParseObject> categoriesList, ParseException e) {
                            if (e == null) {
                                for (ParseObject category : categoriesList) {

                                    String categoryId;
                                    String categoryName;
                                    String categoyDesc;

                                    boolean isCategoryDeleted = false;

                                    if (category.has("deleted"))
                                        isCategoryDeleted = category.getBoolean("deleted");
                                    else
                                        isCategoryDeleted = true;

                                    if (category.has("categoryId"))
                                        categoryId = category.getString("categoryId");
                                    else
                                        categoryId = "NA";

                                    if (category.has("categoryName"))
                                        categoryName = category.getString("categoryName");
                                    else
                                        categoryName = "NA";

                                    if (category.has("categoyDesc"))
                                        categoyDesc = category.getString("categoyDesc");
                                    else
                                        categoyDesc = "NA";

                                    if (!isCategoryDeleted) {
                                        if (categoryCountHash.containsKey(categoryName)){
                                            categoryArrayList.add(new Category(categoryId, categoryName, categoyDesc, isCategoryDeleted, categoryCountHash.get(categoryName)));
                                        }
                                    }

                                }
                                Collections.sort(categoryArrayList, new CustomComparator());

                                sortedCategoriesList.clear();
                                sortedCategoriesList.addAll(categoryArrayList);
                                if(sortedCategoriesList.size() == 0){
                                    txtNoEvent.setVisibility(View.VISIBLE);
                                }else {
                                    storeListAdapter = new OfferCategoryListAdapter(sortedCategoriesList, getActivity());
                                    recyclerView.setAdapter(storeListAdapter);
                                }

                                /*storeListAdapter = new OfferCategoryListAdapter(sortedCategoriesList, getActivity());
                                recyclerView.setAdapter(storeListAdapter);*/
                            } else {

                            }
                        }
                    });

                } else {
                }
            }
        });
        return sortedCategoriesList.size();
    }

    public class CustomComparator implements Comparator<Category> {
        @Override
        public int compare(Category o1, Category o2) {
            return o1.getCategoryName().compareTo(o2.getCategoryName());
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (this.isVisible()) {
            GlobalState.currentFragment = "OFFER";
        }
    }

}
