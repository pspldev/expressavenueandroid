package com.expressavenue.mallapp.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.expressavenue.mallapp.R;
import com.expressavenue.mallapp.activity.OfferDetailsActivity;
import com.expressavenue.mallapp.adapters.EventListAdapter;
import com.expressavenue.mallapp.global.GlobalState;
import com.expressavenue.mallapp.pojos.Offer;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Created by nivedith on 1/15/2016.
 */
public class EventsFragment extends Fragment {

    private ArrayList<Offer> sortedEventsList;
    private ArrayList<Offer> eventsArrayList;
    private Offer offer = null;
    private OfferDetailsActivity offerDetailsActivity = null;

    private ProgressBar progressBar;
    private TextView txtNoEvent;
    private EventListAdapter eventListAdapter;
    private RecyclerView recyclerView;

    public EventsFragment() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_events, container, false);

        progressBar = (ProgressBar) view.findViewById(R.id.progressbar_events);
        txtNoEvent = (TextView) view.findViewById(R.id.txt_noevent);
        txtNoEvent.setVisibility(View.GONE);
        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerview_events);
        recyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getActivity())
                .marginResId(R.dimen.pad_10dp, R.dimen.pad_10dp).build());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        loadEventsInAlphabeticOrder();

        return view;
    }

    private void loadEventsInAlphabeticOrder()
    {
        eventsArrayList = new ArrayList<Offer>();
        eventsArrayList.clear();
        sortedEventsList = new ArrayList<Offer>(eventsArrayList);

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Offer");
        query.fromPin();
        query.fromLocalDatastore();
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> eventsList, ParseException e) {
                if (e == null) {
                    progressBar.setVisibility(View.GONE);
                    for (ParseObject event : eventsList) {

                        String offerId = "";
                        String offerType = "";
                        String offerName = "";
                        String storeId = "";
                        String offerLogo = "";
                        String offerDesc = "";
                        Date offerStartDate = null;
                        Date offerEndDate = null;
                        String offerStartTime = "";
                        String offerEndTime = "";

                        boolean isOfferDeleted = true;

                        if (event.has("offerType")) {
                            if (event.get("offerType").equals("Event")) {

                                if (event.has("deleted"))
                                    isOfferDeleted = event.getBoolean("deleted");
                                else
                                    isOfferDeleted = true;

                                if (event.has("offerId"))
                                    offerId = event.getString("offerId");
                                else
                                    offerId = "NA";

                                if (event.has("offerType"))
                                    offerType = event.getString("offerType");
                                else
                                    offerType = "NA";

                                if (event.has("offerName"))
                                    offerName = event.getString("offerName");
                                else
                                    offerName = "NA";

                                if (event.has("storeId"))
                                    storeId = event.getString("storeId");
                                else
                                    storeId = "NA";

                                if (event.has("offerLogo"))
                                    offerLogo = event.getString("offerLogo");
                                else
                                    offerLogo = "NA";

                                if (event.has("offerDesc")) {
                                    offerDesc = event.getString("offerDesc");
                                } else
                                    offerDesc = "NA";

                                if (event.has("offerStartDate"))
                                    offerStartDate = event.getDate("offerStartDate");
                                else
                                    offerStartDate = null;

                                if (event.has("offerEndDate"))
                                    offerEndDate = event.getDate("offerEndDate");
                                else
                                    offerEndDate = null;

                                if (event.has("offerStartTime"))
                                    offerStartTime = event.getString("offerStartTime");
                                else
                                    offerStartTime = null;

                                if (event.has("offerEndTime"))
                                    offerEndTime = event.getString("offerEndTime");
                                else
                                    offerEndTime = null;

                                if (!isOfferDeleted) {
                                    Date today = Calendar.getInstance().getTime();
                                    if ((offerEndDate.compareTo(today)) >= 0) {
                                        eventsArrayList.add(new Offer(offerId, offerType, offerName, storeId,
                                                offerLogo, offerDesc, offerStartDate, offerEndDate, isOfferDeleted, offerStartTime, offerEndTime));
                                    }
                                }
                            }
                        }

                    }

                    Collections.sort(eventsArrayList, new CustomComparator());

                    sortedEventsList.clear();
                    sortedEventsList.addAll(eventsArrayList);
                    if(sortedEventsList.size() == 0){
                        txtNoEvent.setVisibility(View.VISIBLE);
                    }else {
                        eventListAdapter = new EventListAdapter(sortedEventsList, getActivity());
                        recyclerView.setAdapter(eventListAdapter);
                    }
                } else {
                }
            }
        });
    }

    public class CustomComparator implements Comparator<Offer> {
        @Override
        public int compare(Offer o1, Offer o2) {
            Date today = Calendar.getInstance().getTime();
            return o1.getOfferEndDate().compareTo(today);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(this.isVisible()){
            GlobalState.currentFragment = "EVENT";
        }
    }
}
