package com.expressavenue.mallapp.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.expressavenue.mallapp.R;
import com.expressavenue.mallapp.activity.OfferDetailsActivity;
import com.expressavenue.mallapp.adapters.OfferListAdapter;
import com.expressavenue.mallapp.global.GlobalState;
import com.expressavenue.mallapp.pojos.Offer;
import com.expressavenue.mallapp.pojos.Store;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Created by dhanil on 20-12-2015.
 */
public class OffersFragment extends Fragment {

    private ArrayList<Offer> sortedOfferList;
    private ArrayList<Offer> offerArrayList;
    private Offer offer = null;
    private Store store = null;
    private OfferDetailsActivity offerDetailsActivity = null;

    private ProgressBar progressBar;
    private TextView txtNoEvent;
    private ArrayList<Store> storeArrayList;
    private String categoryNameFromList;

    private OfferListAdapter offerListAdapter;
    private RecyclerView recyclerView;

    public OffersFragment() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_offers, container, false);

        progressBar = (ProgressBar) view.findViewById(R.id.progressbar_offers);
        txtNoEvent = (TextView) view.findViewById(R.id.txt_nooffer);
        txtNoEvent.setVisibility(View.GONE);

        categoryNameFromList = GlobalState.eaPref.getString("categoryGlobal", "noCategory");
        GlobalState.editor.putString("categoryGlobal", "noCategory");
        GlobalState.editor.commit();

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerview_offers);
        recyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getActivity())
                .marginResId(R.dimen.pad_10dp, R.dimen.pad_10dp).build());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        loadOffersInAlphabeticOrder();

        return view;
    }

    private void loadOffersInAlphabeticOrder()
    {
        offerArrayList = new ArrayList<Offer>();
        offerArrayList.clear();
        sortedOfferList = new ArrayList<Offer>(offerArrayList);

        storeArrayList = new ArrayList<Store>();
        storeArrayList.clear();

        ParseQuery<ParseObject> storeQuery = ParseQuery.getQuery(getResources().getString(R.string.store_class_name));
        storeQuery.fromLocalDatastore();
        storeQuery.findInBackground(new FindCallback<ParseObject>() {

            public void done(List<ParseObject> storesListFromLocal, ParseException e) {
                if (e == null) {
                    progressBar.setVisibility(View.GONE);
                    for (ParseObject store : storesListFromLocal) {

                        String storeName = "";
                        String storeCategory = "";
                        String storeFloor = "";
                        String storeLatitude = "";
                        String storeLongitude = "";
                        String storeLogo = "";
                        String storeDesc = "";
                        String storeNumber = "";
                        String storeObjectId = "";

                        boolean storeStatus = false;
                        boolean isStoreDeleted = true;

                        if (store.has("sFloor")) {

                            if (!store.get("sCategory").equals("GENERAL") && !store.get("sCategory").equals("RESTROOM") &&
                                    !store.get("sCategory").equals("ATM") ) {

                                storeObjectId = store.getObjectId();

                                if (store.has("deleted"))
                                    isStoreDeleted = store.getBoolean("deleted");
                                else
                                    isStoreDeleted = true;

                                if (store.has("sStatus"))
                                    storeStatus = store.getBoolean("sStatus");
                                else
                                    storeStatus = false;

                                if (store.has("sName"))
                                    storeName = store.getString("sName");
                                else
                                    storeName = "NA";

                                if (store.has("sCategory"))
                                    storeCategory = store.getString("sCategory");
                                else
                                    storeCategory = "NA";

                                if (store.has("sFloor"))
                                    storeFloor = store.getString("sFloor");
                                else
                                    storeFloor = "NA";

                                if (store.has("sLatitude"))
                                    storeLatitude = store.getString("sLatitude");
                                else
                                    storeLatitude = "NA";

                                if (store.has("sLongitude"))
                                    storeLongitude = store.getString("sLongitude");
                                else
                                    storeLongitude = "NA";

                                if (store.has("sLogoName")) {
                                    storeLogo = store.getString("sLogoName");
                                } else
                                    storeLogo = "NA";

                                if (store.has("sDesc"))
                                    storeDesc = store.getString("sDesc");
                                else
                                    storeDesc = "NA";

                                if (store.has("sNumber"))
                                    storeNumber = store.getString("sNumber");
                                else
                                    storeNumber = "NA";

                                if (storeStatus == true && isStoreDeleted == false) {
                                    storeArrayList.add(new Store(storeObjectId, storeName, storeCategory, storeFloor, storeLatitude,
                                            storeLongitude, storeLogo, storeDesc, storeNumber, storeStatus, isStoreDeleted));
                                }
                            }

                        }
                    }
                } else {

                }
            }

        });

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Offer");
        query.fromPin();
        query.fromLocalDatastore();
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> offersList, ParseException e) {
                if (e == null) {
                    for (ParseObject offer : offersList) {

                        String offerId = "";
                        String offerType = "";
                        String offerName = "";
                        String storeId = "";
                        String offerLogo = "";
                        String offerDesc = "";
                        Date offerStartDate = null;
                        Date offerEndDate = null;
                        String offerStartTime = "";
                        String offerEndTime = "";

                        boolean isOfferDeleted = true;

                        if (offer.has("offerType")) {
                            if (offer.get("offerType").equals("Offer")) {

                                if (offer.has("deleted"))
                                    isOfferDeleted = offer.getBoolean("deleted");
                                else
                                    isOfferDeleted = true;

                                if (offer.has("offerId"))
                                    offerId = offer.getString("offerId");
                                else
                                    offerId = "NA";

                                if (offer.has("offerType"))
                                    offerType = offer.getString("offerType");
                                else
                                    offerType = "NA";

                                if (offer.has("offerName"))
                                    offerName = offer.getString("offerName");
                                else
                                    offerName = "NA";

                                if (offer.has("storeId"))
                                    storeId = offer.getString("storeId");
                                else
                                    storeId = "NA";

                                if (offer.has("offerLogo"))
                                    offerLogo = offer.getString("offerLogo");
                                else
                                    offerLogo = "NA";

                                if (offer.has("offerDesc")) {
                                    offerDesc = offer.getString("offerDesc");
                                } else
                                    offerDesc = "NA";

                                if (offer.has("offerStartDate"))
                                    offerStartDate = offer.getDate("offerStartDate");
                                else
                                    offerStartDate = null;

                                if (offer.has("offerEndDate"))
                                    offerEndDate = offer.getDate("offerEndDate");
                                else
                                    offerEndDate = null;

                                if (offer.has("offerStartTime"))
                                    offerStartTime = offer.getString("offerStartTime");
                                else
                                    offerStartTime = null;

                                if (offer.has("offerEndTime"))
                                    offerEndTime = offer.getString("offerEndTime");
                                else
                                    offerEndTime = null;

                                if (!isOfferDeleted) {

                                    Date today = Calendar.getInstance().getTime();
                                    if ((offerEndDate.compareTo(today)) >= 0) {

                                        for (Store stores : storeArrayList) {

                                            if (storeId.equals(stores.getStoreObjectId())) {
                                                String categoryName = stores.getStoreCategory();
                                                if (categoryNameFromList.equals(categoryName)) {
                                                    offerArrayList.add(new Offer(offerId, offerType, offerName, storeId,
                                                            offerLogo, offerDesc, offerStartDate, offerEndDate, isOfferDeleted, offerStartTime, offerEndTime));
                                                } else if (categoryNameFromList.equals("noCategory")) {
                                                    offerArrayList.add(new Offer(offerId, offerType, offerName, storeId,
                                                            offerLogo, offerDesc, offerStartDate, offerEndDate, isOfferDeleted, offerStartTime, offerEndTime));
                                                }

                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }

                    Collections.sort(offerArrayList, new CustomComparator());

                    sortedOfferList.clear();
                    sortedOfferList.addAll(offerArrayList);
                    if(sortedOfferList.size() == 0){
                        txtNoEvent.setVisibility(View.VISIBLE);
                    }else {
                        offerListAdapter = new OfferListAdapter(sortedOfferList, getActivity(), storeArrayList);
                        recyclerView.setAdapter(offerListAdapter);
                    }

                    /*offerListAdapter = new OfferListAdapter(sortedOfferList, getActivity(), storeArrayList);
                    recyclerView.setAdapter(offerListAdapter);*/


                } else {
                }
            }
        });
    }

    public class CustomComparator implements Comparator<Offer> {
        @Override
        public int compare(Offer o1, Offer o2) {
            Date today = Calendar.getInstance().getTime();
            return o1.getOfferEndDate().compareTo(today);
        }
    }

}