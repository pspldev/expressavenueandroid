package com.expressavenue.mallapp.pojos;

/**
 * Created by nivedithkn on 15-10-2015.
 */
public class Category {

    private String categoryId;
    private String categoryName;
    private String categoryDesc;

    private boolean isCategoryDeleted;
    private int offerCount;

    public Category (String categoryId, String categoryName, String categoryDesc, boolean isCategoryDeleted ){

        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.categoryDesc = categoryDesc;
        this.isCategoryDeleted = isCategoryDeleted;

    }

    public Category (String categoryId, String categoryName, String categoryDesc, boolean isCategoryDeleted, int offerCount ){

        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.categoryDesc = categoryDesc;
        this.isCategoryDeleted = isCategoryDeleted;
        this.offerCount = offerCount;

    }

    public int getOfferCount() {
        return offerCount;
    }

    public void setOfferCount(int offerCount) {
        this.offerCount = offerCount;
    }

    public boolean isCategoryDeleted() {
        return isCategoryDeleted;
    }

    public void setCategoryStatus(boolean isCategoryDeleted) {
        this.isCategoryDeleted = isCategoryDeleted;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryDesc() {
        return categoryDesc;
    }

    public void setCategoryDesc(String categoryDesc) {
        this.categoryDesc = categoryDesc;
    }
}

