package com.expressavenue.mallapp.pojos;

/**
 * Created by anisha on 1/3/17.
 */

public class StoreDto {
    //private variables
    int  id;
    String name;
    String url;
    String timeStamp;

    // Empty constructor
    public StoreDto(){

    }
    // constructor
    public StoreDto(int id, String name, String url,String timeStamp){
        this.id = id;
        this.name = name;
        this.url = url;
        this.timeStamp = timeStamp;
    }

    // getting ID
    public int getID(){
        return this.id;
    }

    // setting id
    public void setID(int id){
        this.id = id;
    }

    // getting name
    public String getName(){
        return this.name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
