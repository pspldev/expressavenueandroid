package com.expressavenue.mallapp.pojos;


import java.util.Date;

public class Offer {

    private String offerId;
    private String offerType;
    private String offerName;
    private String storeId;
    private String offerLogo;
    private String offerDesc;
    private Date offerStartDate;
    private Date offerEndDate;
    private String offerStartTime;
    private String offerEndTime;

    private boolean isOfferDeleted =  true;

    public Offer(String offerId, String offerType, String offerName,
                 String storeId, String offerLogo, String offerDesc,
                 Date offerStartDate, Date offerEndDate, boolean isOfferDeleted,
                 String offerStartTime, String offerEndTime){
        this.offerId = offerId;
        this.offerType = offerType;
        this.offerName = offerName;
        this.storeId = storeId;
        this.offerLogo = offerLogo;
        this.offerDesc = offerDesc;
        this.offerStartDate = offerStartDate;
        this.offerEndDate = offerEndDate;
        this.isOfferDeleted = isOfferDeleted;
        this.offerStartTime = offerStartTime;
        this.offerEndTime = offerEndTime;
    }

    public String getOfferStartTime() {
        return offerStartTime;
    }

    public void setOfferStartTime(String offerStartTime) {
        this.offerStartTime = offerStartTime;
    }

    public String getOfferEndTime() {
        return offerEndTime;
    }

    public void setOfferEndTime(String offerEndTime) {
        this.offerEndTime = offerEndTime;
    }

    public boolean isOfferDeleted() {
        return isOfferDeleted;
    }

    public void isOfferDeleted(boolean isOfferDeleted) {
        this.isOfferDeleted = isOfferDeleted;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public String getOfferType() {
        return offerType;
    }

    public void setOfferType(String offerType) {
        this.offerType = offerType;
    }

    public String getOfferName() {
        return offerName;
    }

    public void setOfferName(String offerName) {
        this.offerName = offerName;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getOfferLogo() {
        return offerLogo;
    }

    public void setOfferLogo(String offerLogo) {
        this.offerLogo = offerLogo;
    }

    public String getOfferDesc() {
        return offerDesc;
    }

    public void setOfferDesc(String offerDesc) {
        this.offerDesc = offerDesc;
    }

    public Date getOfferStartDate() {
        return offerStartDate;
    }

    public void setOfferStartDate(Date offerStartDate) {
        this.offerStartDate = offerStartDate;
    }

    public Date getOfferEndDate() {
        return offerEndDate;
    }

    public void setOfferEndDate(Date offerEndDate) {
        this.offerEndDate = offerEndDate;
    }
}
