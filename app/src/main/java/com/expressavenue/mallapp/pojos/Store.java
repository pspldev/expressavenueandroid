package com.expressavenue.mallapp.pojos;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONObject;

public class Store implements Parcelable
{
	private String storeName;
	private String storeCategory;
	private String storeFloor;
	private String storeLatitude;
	private String storeLongitude;
	private String storeLogo;
	private String storeDesc;
	private String storeNumber;
	private String id;
	private String imgUrl;
	private String timeStamp;
	private boolean storeStatus;

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	private boolean isStoreDeleted;

	private String storeObjectId;



	public Store(String storeName, String storeCategory, String storeFloor, String storeLatitude,
				 String storeLongitude, String storeLogo, String storeDesc, String storeNumber, boolean storeStatus, boolean isStoreDeleted)
	{
		this.storeName = storeName;
		this.storeCategory = storeCategory;
		this.storeFloor = storeFloor;
		this.storeLatitude = storeLatitude;
		this.storeLongitude = storeLongitude;
		this.storeLogo = storeLogo;
		this.storeDesc = storeDesc;
		this.storeNumber = storeNumber;

		this.storeStatus = storeStatus;
		this.isStoreDeleted = isStoreDeleted;
	}

	public Store(String storeObjectId, String storeCategory, String storeName)
	{
		this.storeObjectId = storeObjectId;
		this.storeCategory = storeCategory;
		this.storeName = storeName;
	}

	public Store(String storeObjectId, String storeName, String storeCategory, String storeFloor, String storeLatitude,
				 String storeLongitude, String storeLogo, String storeDesc, String storeNumber, boolean storeStatus, boolean isStoreDeleted)
	{
		this.storeObjectId = storeObjectId;
		this.storeName = storeName;
		this.storeCategory = storeCategory;
		this.storeFloor = storeFloor;
		this.storeLatitude = storeLatitude;
		this.storeLongitude = storeLongitude;
		this.storeLogo = storeLogo;
		this.storeDesc = storeDesc;
		this.storeNumber = storeNumber;

		this.storeStatus = storeStatus;
		this.isStoreDeleted = isStoreDeleted;
	}

	public Store(Parcel parcel){
		storeObjectId = parcel.readString();
		storeName = parcel.readString();
		storeCategory = parcel.readString();
		storeFloor = parcel.readString();
		storeLatitude = parcel.readString();
		storeLongitude = parcel.readString();
		storeLogo = parcel.readString();
		storeDesc = parcel.readString();
		storeNumber = parcel.readString();

		storeStatus = parcel.readByte() != 0;
		isStoreDeleted = parcel.readByte() != 0;
	}


	public String getStoreObjectId() {
		return storeObjectId;
	}

	public void setStoreObjectId(String storeObjectId) {
		this.storeObjectId = storeObjectId;
	}

	public boolean isStoreDeleted() {
		return isStoreDeleted;
	}

	public void setIsStoreDeleted(boolean isStoreDeleted) {
		this.isStoreDeleted = isStoreDeleted;
	}

	public boolean isStoreStatus() {
		return storeStatus;
	}

	public void setStoreStatus(boolean storeStatus) {
		this.storeStatus = storeStatus;
	}

	public String getStoreCategory() {
		return storeCategory;
	}

	public void setStoreCategory(String storeCategory) {
		this.storeCategory = storeCategory;
	}

	public String getStoreDesc() {
		return storeDesc;
	}

	public void setStoreDesc(String storeDesc) {
		this.storeDesc = storeDesc;
	}

	public String getStoreFloor() {
		return storeFloor;
	}

	public void setStoreFloor(String storeFloor) {
		this.storeFloor = storeFloor;
	}

	public String getStoreLatitude() {
		return storeLatitude;
	}

	public void setStoreLatitude(String storeLatitude) {
		this.storeLatitude = storeLatitude;
	}

	public String getStoreLogo() {
		return storeLogo;
	}

	public void setStoreLogo(String storeLogo) {
		this.storeLogo = storeLogo;
	}

	public String getStoreLongitude() {
		return storeLongitude;
	}

	public void setStoreLongitude(String storeLongitude) {
		this.storeLongitude = storeLongitude;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getStoreNumber() {
		return storeNumber;
	}

	public void setStoreNumber(String storeNumber) {
		this.storeNumber = storeNumber;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeString(storeObjectId);
		parcel.writeString(storeName);
		parcel.writeString(storeCategory);
		parcel.writeString(storeFloor);
		parcel.writeString(storeLatitude);
		parcel.writeString(storeLongitude);
		parcel.writeString(storeLogo);
		parcel.writeString(storeDesc);
		parcel.writeString(storeNumber);

		parcel.writeByte((byte) (storeStatus ? 1 : 0));
		parcel.writeByte((byte) (isStoreDeleted ? 1 : 0));
	}

	public static final Parcelable.Creator<Store> CREATOR = new Creator<Store>() {
		@Override
		public Store createFromParcel(Parcel parcel) {
			return new Store(parcel);
		}

		@Override
		public Store[] newArray(int i) {
			return new Store[i];
		}
	};
}
