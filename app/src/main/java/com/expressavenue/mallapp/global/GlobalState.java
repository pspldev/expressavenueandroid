package com.expressavenue.mallapp.global;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.StringDef;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import com.expressavenue.mallapp.R;
import com.expressavenue.mallapp.interfaces.RestCallBack;
import com.expressavenue.mallapp.rest.RestServiceFactory;
import com.expressavenue.mallapp.rest.request.InstallationRequestDTO;
import com.google.firebase.iid.FirebaseInstanceId;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.SaveCallback;

import java.util.List;

public class GlobalState extends MultiDexApplication {

    public static String PARSE_APP_ID = "Q4f08Hq61i6x13AfCTnGgJ49AdDjwT05";
    public static String PARSE_CLIENT_KEY = "MAf08Hq61i6x13AfCTnGgJ49AdDjwT05";
    public static String PARSE_SERVER_URL = "http://35.154.97.166:1337/parse";
    public static String BASE_URL = "http://35.154.97.166:1337/parse/";
    public static final String INSTALLATION = "installations";

    public static SharedPreferences eaPref;
    public static SharedPreferences.Editor editor;

    public static String drawPath = "";
    public static String _drawPath = "";
    public static String source = "";
    public static String _source = "";
    public static String destination = "";
    public static String _destination = "";

    public static String liftDest = "";
    public static String liftSource = "";

    public static String mapTitle = "";
    public static String sourceStore = "";
    public static String destStore = "";

    public static String _mapTitle = "";
    public static String _sourceStore = "";
    public static String _destStore = "";

    public static String soloMap = "";
    public static String firstMap = "";
    public static String secondMap = "";

    public static ParseObject userObject;
    public static String deviceImei = "";

    public static String tnPathFF = "";
    public static String tnPathSF = "";
    public static String tnPathTF = "";
    public static String tnPathLGF = "";
    public static String tnPathUGF = "";
    public static String tnPathB1 = "";
    public static String tnPathB2 = "";
    public static String tnPathB3 = "";

    public static String tnSrcStoreFF = "";
    public static String tnDestStoreFF = "";

    public static String tnSrcStoreSF = "";
    public static String tnDestStoreSF = "";

    public static String tnSrcStoreTF = "";
    public static String tnDestStoreTF = "";

    public static String tnSrcStoreLGF = "";
    public static String tnDestStoreLGF = "";

    public static String tnSrcStoreUGF = "";
    public static String tnDestStoreUGF = "";

    public static String tnSrcStoreB1 = "";
    public static String tnDestStoreB1 = "";

    public static String tnSrcStoreB2 = "";
    public static String tnDestStoreB2 = "";

    public static String tnSrcStoreB3 = "";
    public static String tnDestStoreB3 = "";

    public static String currentFragment = "STORE";

    private static int pendingNotificationsCount = 0;

    public static boolean offerValidation = false;

    public static Context appContext;

    @Override
    public void onCreate() {
        super.onCreate();

        Parse.enableLocalDatastore(getApplicationContext());
        Parse.initialize(new Parse.Configuration.Builder(getApplicationContext())
                .applicationId(PARSE_APP_ID)
                .clientKey(PARSE_CLIENT_KEY)
                .server(PARSE_SERVER_URL)
                .enableLocalDataStore()
                .build()
        );

        String token = FirebaseInstanceId.getInstance().getToken();
        if(token != null && !TextUtils.isEmpty(token)){
            InstallationRequestDTO installationRequestDTO = new InstallationRequestDTO("android", token);
            doInstallation(installationRequestDTO);
        }

        eaPref = this.getSharedPreferences("eapref", Context.MODE_PRIVATE);
        editor = eaPref.edit();

        /*TelephonyManager mngr = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        deviceImei = mngr.getDeviceId();
        userObject = new ParseObject("AppUsers");
        userObject.put("deviceImei", deviceImei);*/

        appContext = getApplicationContext();
    }

    public static int getPendingNotificationsCount() {
        return pendingNotificationsCount;
    }

    public static void setPendingNotificationsCount(int pendingNotifications) {
        pendingNotificationsCount = pendingNotifications;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }


    private void doInstallation(InstallationRequestDTO installationRequestDTO){
        RestServiceFactory.getInstance().doInstallation(this, installationRequestDTO);
    }
}
