package com.expressavenue.mallapp.rest.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by dhanil on 16-03-2017.
 */

public class InstallationRequestDTO {

    @SerializedName("pushType")
    @Expose
    private String pushType;

    @SerializedName("localeIdentifier")
    @Expose
    private String localeIdentifier;

    @SerializedName("appVersion")
    @Expose
    private String appVersion;

    @SerializedName("deviceType")
    @Expose
    private String deviceType;

    @SerializedName("appIdentifier")
    @Expose
    private String appIdentifier;

    @SerializedName("installationId")
    @Expose
    private String installationId;

    @SerializedName("parseVersion")
    @Expose
    private String parseVersion;

    @SerializedName("appName")
    @Expose
    private String appName;

    @SerializedName("GCMSenderId")
    @Expose
    private String gcmSenderId;

    @SerializedName("timeZone")
    @Expose
    private String timeZone;

    @SerializedName("deviceToken")
    @Expose
    private String deviceToken;

    public InstallationRequestDTO(String pushType, String localeIdentifier,
                                  String appVersion, String deviceType,
                                  String appIdentifier, String installationId,
                                  String parseVersion, String appName, String gcmSenderId,
                                  String timeZone, String deviceToken) {
        this.pushType = pushType;
        this.localeIdentifier = localeIdentifier;
        this.appVersion = appVersion;
        this.deviceType = deviceType;
        this.appIdentifier = appIdentifier;
        this.installationId = installationId;
        this.parseVersion = parseVersion;
        this.appName = appName;
        this.gcmSenderId = gcmSenderId;
        this.timeZone = timeZone;
        this.deviceToken = deviceToken;
    }

    public InstallationRequestDTO(String deviceType, String deviceToken) {
        this.deviceType = deviceType;
        this.deviceToken = deviceToken;
    }
}
