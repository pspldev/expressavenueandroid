package com.expressavenue.mallapp.rest;

import android.content.Context;

import com.expressavenue.mallapp.global.GlobalState;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by dhanil on 20-12-2016.
 */

public class BaseRestService {
    private Retrofit retrofit;

    final OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .readTimeout(30, TimeUnit.SECONDS)
            .connectTimeout(30, TimeUnit.SECONDS)
            .build();

    public Retrofit getRetrofit(Context context){
        retrofit = new Retrofit.Builder()
                .baseUrl(GlobalState.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
        return retrofit;
    }
}
