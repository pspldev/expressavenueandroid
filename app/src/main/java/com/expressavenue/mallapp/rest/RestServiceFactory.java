package com.expressavenue.mallapp.rest;

import android.content.Context;
import android.util.Log;

import com.expressavenue.mallapp.global.GlobalState;
import com.expressavenue.mallapp.interfaces.RestCallBack;
import com.expressavenue.mallapp.rest.request.InstallationRequestDTO;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by dhanil on 08-02-2017.
 */

public class RestServiceFactory extends BaseRestService {

    private static RestServiceFactory instance = new RestServiceFactory();

    public RestServiceFactory() {
    }

    public static RestServiceFactory getInstance() {
        return instance;
    }


    public interface RetrofitService {

        /**
         * API Call to post installations
         * @param installationRequestDTO
         * @return
         */
        @Headers({
                "Accept: application/json",
                "Content-Type: application/json",
                "X-Parse-Application-Id: Q4f08Hq61i6x13AfCTnGgJ49AdDjwT05",
                "X-Parse-REST-API-Key: oiwHdBP8Z3zH3rzgKQf5oTiUm5P3812h",
                "X-Parse-Master-Key: MAf08Hq61i6x13AfCTnGgJ49AdDjwT05"
        })
        @POST(GlobalState.INSTALLATION)
        Call<ResponseBody> doInstallation(@Body InstallationRequestDTO installationRequestDTO);
    }

    /**
     * Method to perform login
     * @param context Application Context
     * @param installationRequestDTO InstallationRequest Model
     */
    public void doInstallation(final Context context, InstallationRequestDTO installationRequestDTO){
        final RetrofitService retrofitService = getRetrofit(context).create(RetrofitService.class);
        final Call<ResponseBody> call = retrofitService.doInstallation(installationRequestDTO);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    Log.i("*******", " " + response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.i("*******", " " + t.getMessage());
            }
        });
    }
}
