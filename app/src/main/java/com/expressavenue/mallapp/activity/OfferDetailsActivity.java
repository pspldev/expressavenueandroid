package com.expressavenue.mallapp.activity;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.expressavenue.mallapp.R;
import com.expressavenue.mallapp.database.SimpleDatabaseHelper;
import com.expressavenue.mallapp.global.GlobalState;
import com.expressavenue.mallapp.pojos.Store;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by nivedith on 1/5/2016.
 */
public class OfferDetailsActivity extends AppCompatActivity{

    private TextView txtOfferName;
    private TextView txtOfferStart;
    private TextView txtOfferEnd;
    private TextView txtOfferDesc;
    private ImageView offerStoreLogo;
    private CardView cardGoStore;

    private String offerName = "";
    private String offerStartDate = "";
    private String offerEndDate = "";
    private String offerStartTime = "";
    private String offerEndTime = "";
    private String offerDesc = "";
    private String offerStoreId = "";
    private String offerType = "";

    private String storeName = "";
    private String storeCategory = "";
    private String storeFloor = "";
    private String storeLatitude = "";
    private String storeLongitude = "";
    private String storeLogo = "";
    private String storeDesc = "";
    private String storeNumber = "";
    private String storeId = "";
    private String from = "";

    private ArrayList<Store> storeArrayList = null;
    private Toolbar toolbar;

    private SimpleDatabaseHelper simpleDatabaseHelper;
    private Cursor cursor;

    private SharedPreferences eapref = GlobalState.eaPref;
    private SharedPreferences.Editor editor = GlobalState.editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer_details);

        toolbar = (Toolbar) findViewById(R.id.toolbar_locate_store);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        simpleDatabaseHelper = new SimpleDatabaseHelper(GlobalState.appContext);
        cursor = simpleDatabaseHelper.getAllStores();

        txtOfferName = (TextView)findViewById(R.id.offerName);
        txtOfferStart = (TextView)findViewById(R.id.startDate);
        txtOfferEnd = (TextView) findViewById(R.id.endDate);
        txtOfferDesc = (TextView) findViewById(R.id.offerDesc);
        cardGoStore = (CardView) findViewById(R.id.card_offer_details);
        offerStoreLogo = (ImageView) findViewById(R.id.offerStoreLogo);

        editor.putInt("NOTIFICATION_COUNT", 0);
        editor.commit();

        from = getIntent().getStringExtra("FROM");
        if(from.equals("OFFERLIST")){
            offerName = getIntent().getStringExtra("offerName");
            offerStartDate = getIntent().getStringExtra("offerStartDate");
            offerEndDate = getIntent().getStringExtra("offerEndDate");
            offerStartTime = getIntent().getStringExtra("offerStartTime");
            offerEndTime = getIntent().getStringExtra("offerEndTime");
            offerDesc = getIntent().getStringExtra("offerDesc");
            offerStoreId = getIntent().getStringExtra("offerStoreId");
            storeArrayList = getIntent().getParcelableArrayListExtra("offerStoreArrayList");

            Date sDate = new Date(offerStartDate);
            Date dDate = new Date(offerEndDate);

            String startingDate = "";
            String endingDate = "";

            if(sDate != null){
                String[] startDate = sDate.toString().split("\\s+");
                startingDate = "Starting on: "+startDate[0] +" "+ startDate[1] +" "+ startDate[2] +" "+ startDate[5];
            }
            if(dDate != null){
                String[] endDate = dDate.toString().split("\\s+");
                endingDate = "Ending on: "+endDate[0] +" "+ endDate[1] +" "+ endDate[2] +" "+ endDate[5];
            }


            txtOfferStart.setText(startingDate + " @ " + offerStartTime);
            txtOfferEnd.setText(endingDate + " @ " + offerEndTime);
            txtOfferDesc.setText(offerDesc);

            storeId = offerStoreId;

            for(Store store : storeArrayList){
                if(store.getStoreObjectId().equals(storeId)){
                    storeName = store.getStoreName();
                    storeCategory = store.getStoreCategory();
                    storeFloor = store.getStoreFloor();
                    storeLatitude = store.getStoreLatitude();
                    storeLongitude = store.getStoreLongitude();
                    storeLogo = store.getStoreLogo();
                    storeDesc = store.getStoreDesc();
                    storeNumber = store.getStoreNumber();
                    break;
                }
            }
        }else if(from.equals("PUSH")){
            offerName = getIntent().getStringExtra("offerName");
            offerStoreId = getIntent().getStringExtra("storeId");
            offerDesc = getIntent().getStringExtra("offerDesc");
            offerStartDate = getIntent().getStringExtra("offerStartDate");
            offerStartTime = getIntent().getStringExtra("offerStartTime");
            offerEndDate = getIntent().getStringExtra("offerEndDate");
            offerEndTime = getIntent().getStringExtra("offerEndTime");

            storeName = getIntent().getStringExtra("storeName");
            Log.i("StoreFloor is ", storeName+storeNumber+storeCategory+storeFloor+storeLatitude+storeLongitude+storeDesc+storeLogo);
            storeNumber = getIntent().getStringExtra("storeNumber");
            storeCategory = getIntent().getStringExtra("storeCategory");
            storeFloor = getIntent().getStringExtra("storeFloor");
            storeLatitude = getIntent().getStringExtra("storeLatitude");
            storeLongitude = getIntent().getStringExtra("storeLongitude");
            storeDesc = getIntent().getStringExtra("storeDesc");
            storeLogo = getIntent().getStringExtra("storeLogo");

            String startingDate = "";
            String endingDate = "";
            Log.i("offer start date", offerStartDate);
            Log.i("offer end date", offerEndDate);

            if(!offerStartDate.isEmpty()){
                startingDate = "Starting on: " + offerStartDate;
            }
            if(!offerEndDate.isEmpty()){
                endingDate = "Ending on: " + offerEndDate;
            }

            Log.i("offer start date2", startingDate);
            Log.i("offer end date2", endingDate);

            txtOfferStart.setText(startingDate + " @ " + offerStartTime);
            txtOfferEnd.setText(endingDate + " @ " + offerEndTime);
            txtOfferDesc.setText(offerDesc);

            storeId = offerStoreId;

        }

        Bitmap storeLogoBitmap = null;
        if(cursor != null){
            if (cursor.moveToFirst()) {
                do {
                    String storeId = cursor.getString(1);
                    if(storeId.equals(storeName)){
                        storeLogoBitmap = loadImageFromStorage(storeId);
                    }
                } while (cursor.moveToNext());
            }
        }
        if(storeLogoBitmap != null){
            offerStoreLogo.setImageBitmap(storeLogoBitmap);
        } else {
            Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.ea_logo_whitebg);
            offerStoreLogo.setImageBitmap(icon);
        }

        txtOfferName.setText(offerName + " @ " + storeName);
        cardGoStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(OfferDetailsActivity.this, LocateStoreActivity.class)
                        .putExtra("storeName", storeName)
                        .putExtra("storeNumber", storeNumber)
                        .putExtra("storeCategory", storeCategory)
                        .putExtra("storeFloor", storeFloor)
                        .putExtra("storeLatitude", storeLatitude)
                        .putExtra("storeLongitude", storeLongitude)
                        .putExtra("storeDesc", storeDesc)
                        .putExtra("storeLogo", storeLogo));
                finish();
            }
        });

        new CHECKINTERNETCONNECTION().execute();
    }

    private Bitmap loadImageFromStorage(String storeId)
    {
        try {
            ContextWrapper cw = new ContextWrapper(this);
            File directory = cw.getDir("ealogos", Context.MODE_PRIVATE);
            File f = new File(directory.getAbsolutePath(), storeId + ".png");
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            return b;
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    private class CHECKINTERNETCONNECTION extends AsyncTask<String, Integer, String> {

        String status = "null";
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected String doInBackground(String... f_url) {
            try {
                if (isNetworkAvailable()) {
                    try {
                        HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
                        urlc.setRequestProperty("User-Agent", "Test");
                        urlc.setRequestProperty("Connection", "close");
                        urlc.setConnectTimeout(1500);
                        urlc.connect();
                        if((urlc.getResponseCode() == 200)){
                            status = "connected";
                        }else{
                            status = "notConnected";
                        }
                    } catch (IOException e) {
                    }
                } else {
                }
            } catch (Exception e) {
            }
            return null;
        }
        @Override
        protected void onPostExecute(String url) {
            if(status.equals("connected")){
                ParseQuery<ParseObject> offerQuery = ParseQuery.getQuery("Offer");
                offerQuery.setLimit(1000);
                offerQuery.findInBackground(new FindCallback<ParseObject>() {
                    public void done(List<ParseObject> offerList, ParseException e) {
                        if (e == null) {
                            ParseObject.pinAllInBackground(offerList);
                        }
                    }
                });
            }
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }
}
