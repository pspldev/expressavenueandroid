package com.expressavenue.mallapp.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.expressavenue.mallapp.R;
import com.expressavenue.mallapp.fragments.EntertainmentsFragment;
import com.expressavenue.mallapp.pojos.Store;
import com.expressavenue.mallapp.utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by nivedith on 12/28/2015.
 */
public class EntertainmentListActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private ArrayList<Store> sortedStoreList;
    private ArrayList<Store> storeArrayList;
    private ArrayList<String> storeWithoutFloor;
    private ArrayList<String> mStores;

    private String storeName = "";
    private String floorName = "";

    private Dialog toolbarSearchDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_tabs);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbarSearchDialog = new Dialog(EntertainmentListActivity.this, R.style.MaterialSearch);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.colorPrimary));

        getStores();
        storeWithoutFloor = new ArrayList<String>();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_search:
                    loadToolBarSearchStores();
                break;
            case android.R.id.home:
                super.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        if(toolbarSearchDialog.isShowing())
            toolbarSearchDialog.dismiss();
        else
            super.onBackPressed();
    }


    public void loadToolBarSearchStores() {

        ArrayList<String> storeStored;
        final View view = EntertainmentListActivity.this.getLayoutInflater().inflate(R.layout.activity_store_tabs, null);
        setViewForLollipop(false, view);
        LinearLayout parentToolbarSearch = (LinearLayout) view.findViewById(R.id.parent_toolbar_search);
        LinearLayout imgToolBack = (LinearLayout) view.findViewById(R.id.layout_img_tool_back);
        final EditText edtToolSearch = (EditText) view.findViewById(R.id.edt_tool_search);
        LinearLayout imgToolClose = (LinearLayout) view.findViewById(R.id.layout_img_tool_close);
        final ListView listSearch = (ListView) view.findViewById(R.id.list_search);
        final TextView txtEmpty = (TextView) view.findViewById(R.id.txt_empty);
        parentToolbarSearch.setVisibility(View.VISIBLE);

        Utils.setListViewHeightBasedOnChildren(listSearch);

        edtToolSearch.setHint("Search Store");

        toolbarSearchDialog.setContentView(view);
        toolbarSearchDialog.setCancelable(false);
        toolbarSearchDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        toolbarSearchDialog.getWindow().setGravity(Gravity.BOTTOM);
        toolbarSearchDialog.show();

        toolbarSearchDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        storeStored = new ArrayList<String>();
        final com.expressavenue.mallapp.adapters.SearchAdapter searchAdapter = new com.expressavenue.mallapp.adapters.SearchAdapter(getApplicationContext(), storeStored, false);

        listSearch.setVisibility(View.VISIBLE);
        listSearch.setAdapter(searchAdapter);

        listSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                RelativeLayout rlView = (RelativeLayout)view;
                if(rlView != null){
                    TextView txtView = (TextView)rlView.getChildAt(0);
                    String str = txtView.getText().toString();
                    String tag = (String) txtView.getTag();

                    Store store = sortedStoreList.get(Integer.parseInt(tag));

                    startActivity(new Intent(getApplicationContext(), LocateStoreActivity.class)
                            .putExtra("storeName", store.getStoreName())
                            .putExtra("storeNumber", store.getStoreNumber())
                            .putExtra("storeCategory", store.getStoreCategory())
                            .putExtra("storeFloor", store.getStoreFloor())
                            .putExtra("storeLatitude", store.getStoreLatitude())
                            .putExtra("storeLongitude", store.getStoreLongitude())
                            .putExtra("storeDesc", store.getStoreDesc())
                            .putExtra("storeLogo", store.getStoreLogo()));
                    finish();
                }
            }
        });

        if(!storeWithoutFloor.isEmpty()){
            storeWithoutFloor.clear();
        }

        for(int i = 0; i < sortedStoreList.size(); i++){
            Store store = sortedStoreList.get(i);
            storeName = store.getStoreName();
            floorName = store.getStoreFloor();
            storeWithoutFloor.add(storeName+"*"+floorName+"*"+i);
        }

        edtToolSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                String[] storesStored = storeWithoutFloor.toArray(new String[storeWithoutFloor.size()]);
                mStores = new ArrayList<String>(Arrays.asList(storesStored));
                listSearch.setVisibility(View.VISIBLE);
                searchAdapter.updateList(mStores, true);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ArrayList<String> filterList = new ArrayList<String>();
                boolean isNodata = false;
                if (s.length() > 0) {
                    for(int j = 0; j < mStores.size(); j++){
                        if (mStores.get(j).toLowerCase().startsWith(s.toString().trim().toLowerCase())) {

                            filterList.add(mStores.get(j));

                            listSearch.setVisibility(View.VISIBLE);
                            searchAdapter.updateList(filterList, true);
                            isNodata = true;
                        }

                    }
                    if (!isNodata) {
                        listSearch.setVisibility(View.GONE);
                        txtEmpty.setVisibility(View.VISIBLE);
                        txtEmpty.setText("No data found");
                    }
                } else {
                    listSearch.setVisibility(View.GONE);
                    txtEmpty.setVisibility(View.GONE);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        imgToolBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toolbarSearchDialog.dismiss();
                setViewForLollipop(true, view);
            }
        });

        imgToolClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtToolSearch.setText("");
                toolbarSearchDialog.dismiss();
                setViewForLollipop(true, view);
            }
        });

    }

    private void getStores(){

        storeArrayList = new ArrayList<Store>();
        storeArrayList.clear();
        sortedStoreList = new ArrayList<Store>(storeArrayList);

        ParseQuery<ParseObject> query = ParseQuery.getQuery(getResources().getString(R.string.store_class_name));
        query.fromLocalDatastore();
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> storesListFromLocal, ParseException e) {
                if (e == null) {
                    for (ParseObject store : storesListFromLocal) {

                        String storeName = "";
                        String storeCategory = "";
                        String storeFloor = "";
                        String storeLatitude = "";
                        String storeLongitude = "";
                        String storeLogo = "";
                        String storeDesc = "";
                        String storeNumber = "";

                        boolean storeStatus = false;
                        boolean isStoreDeleted = true;

                        if (store.has("sFloor")) {
                            if (store.get("sCategory").equals("ENTERTAINMENT")) {

                                if(store.has("deleted"))
                                    isStoreDeleted = store.getBoolean("deleted");
                                else
                                    isStoreDeleted = true;

                                if (store.has("sStatus"))
                                    storeStatus = store.getBoolean("sStatus");
                                else
                                    storeStatus = false;

                                if (store.has("sName"))
                                    storeName = store.getString("sName");
                                else
                                    storeName = "NA";

                                if (store.has("sCategory"))
                                    storeCategory = store.getString("sCategory");
                                else
                                    storeCategory = "NA";

                                if (store.has("sFloor"))
                                    storeFloor = store.getString("sFloor");
                                else
                                    storeFloor = "NA";

                                if (store.has("sLatitude"))
                                    storeLatitude = store.getString("sLatitude");
                                else
                                    storeLatitude = "NA";

                                if (store.has("sLongitude"))
                                    storeLongitude = store.getString("sLongitude");
                                else
                                    storeLongitude = "NA";

                                if (store.has("sLogoName")) {
                                    storeLogo = store.getString("sLogoName");
                                } else
                                    storeLogo = "NA";

                                if (store.has("sDesc"))
                                    storeDesc = store.getString("sDesc");
                                else
                                    storeDesc = "NA";

                                if (store.has("sNumber"))
                                    storeNumber = store.getString("sNumber");
                                else
                                    storeNumber = "NA";

                                if(storeStatus == true && isStoreDeleted==false){
                                    storeArrayList.add(new Store(storeName, storeCategory, storeFloor, storeLatitude,
                                            storeLongitude, storeLogo, storeDesc, storeNumber, storeStatus, isStoreDeleted));
                                }
                            }
                        }
                    }
                    Collections.sort(storeArrayList, new CustomComparatorForStores());
                    sortedStoreList.clear();
                    sortedStoreList.addAll(storeArrayList);

                } else {

                }
            }
        });
    }
    public class CustomComparatorForStores implements Comparator<Store> {
        @Override
        public int compare(Store o1, Store o2) {
            return o1.getStoreName().compareTo(o2.getStoreName());
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new EntertainmentsFragment(), "Entertainments");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
    private void setViewForLollipop(boolean flag, View view){

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.LOLLIPOP){
            if(flag){
                view.findViewById(R.id.app_bar).setVisibility(View.VISIBLE);
                view.findViewById(R.id.viewpager).setVisibility(View.VISIBLE);
                view.findViewById(R.id.toolbar).setVisibility(View.VISIBLE);
            }  else {
                view.findViewById(R.id.app_bar).setVisibility(View.GONE);
                view.findViewById(R.id.viewpager).setVisibility(View.GONE);
                view.findViewById(R.id.toolbar).setVisibility(View.GONE);
            }
        }
    }
}

