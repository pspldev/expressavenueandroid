package com.expressavenue.mallapp.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.expressavenue.mallapp.R;
import com.expressavenue.mallapp.global.GlobalState;

/**
 * Created by nivedith on 1/7/2016.
 */
public class SignupActivity extends AppCompatActivity {

    private static final String TAG = "SignupActivity";

    private Toolbar toolbar;

    private Button btn_signup;
    private EditText edit_nametext;
    private EditText edit_emailtext;
    private EditText edit_phonetext;

    private String name;
    private String phone;
    private String email;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        toolbar = (Toolbar) findViewById(R.id.toolbar_signup);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Create Profile");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        btn_signup = (Button)findViewById(R.id.btn_signup);
        edit_nametext = (EditText)findViewById(R.id.input_name);
        edit_emailtext = (EditText)findViewById(R.id.input_email);
        edit_phonetext = (EditText)findViewById(R.id.input_phone);

        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signup();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        return super.onOptionsItemSelected(item);
    }


    public void signup() {

        if (!validate()) {
            onSignupFailed();
            return;
        }

        btn_signup.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(SignupActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Creating Account...");
        progressDialog.show();

        name = edit_nametext.getText().toString();
        email = edit_emailtext.getText().toString();
        phone = edit_phonetext.getText().toString();

        // TODO: Implement your own signup logic here.

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        onSignupSuccess();
                        progressDialog.dismiss();
                    }
                }, 3000);
    }


    public void onSignupSuccess() {
        btn_signup.setEnabled(true);
        setResult(RESULT_OK, null);

        GlobalState.userObject.put("userName", name);
        GlobalState.userObject.put("phoneNumber", phone);
        GlobalState.userObject.put("email", email);
        GlobalState.userObject.put("userType", "ANDROID_USER");
        GlobalState.userObject.saveEventually();

        GlobalState.editor.putString("profileStatus", "Registered");
        GlobalState.editor.putString("userName", name);
        GlobalState.editor.putString("phoneNumber", phone);
        GlobalState.editor.putString("email", email);
        GlobalState.editor.commit();

        AlertDialog.Builder builder = new AlertDialog.Builder(SignupActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setMessage("Profile created successfully.");
        builder.setCancelable(false);
        builder.setPositiveButton("View Profile", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                startActivity(new Intent(getApplicationContext(), ViewProfileActivity.class));
                SignupActivity.this.finish();
            }
        });
        builder.show();

    }

    public void onSignupFailed() {

        Toast.makeText(getBaseContext(), "Profile creation failed", Toast.LENGTH_LONG).show();

        btn_signup.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String name = edit_nametext.getText().toString();
        String email = edit_emailtext.getText().toString();
        String phone = edit_phonetext.getText().toString();

        if (name.isEmpty() || name.length() < 3) {
            edit_nametext.setError("at least 3 characters");
            valid = false;
        } else {
            edit_nametext.setError(null);
        }

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            edit_emailtext.setError("enter a valid email address");
            valid = false;
        } else {
            edit_emailtext.setError(null);
        }

        if (phone.isEmpty() || phone.length() < 10) {
            edit_phonetext.setError("minimum 10 numbers");
            valid = false;
        } else {
            edit_phonetext.setError(null);
        }

        return valid;
    }
    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class));
        SignupActivity.this.finish();
    }
}
