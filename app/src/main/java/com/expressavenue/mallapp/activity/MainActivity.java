package com.expressavenue.mallapp.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PointF;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.text.TextUtilsCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;
import com.expressavenue.mallapp.database.SimpleDatabaseHelper;
import com.expressavenue.mallapp.utils.LongThread;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.expressavenue.mallapp.R;
import com.expressavenue.mallapp.custom.ProcessQRTextWMI;
import com.expressavenue.mallapp.fragments.AboutUsFragment;
import com.expressavenue.mallapp.fragments.HomeFragment;
import com.expressavenue.mallapp.fragments.SettingsFragment;
import com.expressavenue.mallapp.global.GlobalState;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import android.content.res.Configuration;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener, View.OnClickListener, QRCodeReaderView.OnQRCodeReadListener,
        Handler.Callback {

    private SimpleDatabaseHelper simpleDatabaseHelper;

    private static final long DRAWER_CLOSE_DELAY_MS = 250;
    private static final String NAV_ITEM_ID = "navItemId";

    private final Handler mDrawerActionHandler = new Handler();
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private int mNavItemId;

    private Context context;
    public ProgressDialog progressDialogStores;

    private Dialog qrDialog;
    private QRCodeReaderView myDecoderView;
    private String sourcePoint;
    private Animation animBounce;

    private final String OPTION_WHEREAMI = "WHERE AM I";
    private final String OPTION_PARKING = "PARKING";
    private String OPTION = "";
    private Dialog parkingOptionDialog;

    private final String P_LAT = "KEY_PARKING_LATITUDE";
    private final String P_LON = "KEY_PARKING_LONGITUDE";
    private final String P_STORE_NO = "KEY_PARKING_STORE_NUMBER";
    private final String P_STORE_NAME = "KEY_PARKING_STORE_NAME";
    private final String P_STORE_FLOOR = "KEY_PARKING_STORE_FLOOR";
    private final String P_STATUS = "KEY_PARKING_TRACKER_STATUS";
    private final String INVALID = "INVALID";
    private final String P_VALID = "PARKING";

    private final String SAVE_RESPONSE = "Your vehicle's parking location saved successfully, please come back later to navigate!";
    private final String NO_LOCATION = "No location has been saved yet!";

    private SharedPreferences eapref;
    private SharedPreferences.Editor editor;
    private HashMap<String, String> hashIdUpdated;

    public static ArrayList<CharSequence> searchStoreArrayList;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    private int storeCount = 0;
    private int totalStores = 0;
    private int countedStores = 0;

    ProgressDialog progressBar;
    private int progressBarStatus = 0;
    private Handler progressBarHandler = new Handler();
    private long fileSize = 0;


    private Handler handler;
    TextView tvStatus;
    int curCount = 0;
    ProgressBar progressBarHorizontal;
    float totalCount = 100F;
    private ThreadPoolExecutor executor;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBarHorizontal = (ProgressBar) findViewById(R.id.progressBarStatus);

        hashIdUpdated = new HashMap<>();
        simpleDatabaseHelper = new SimpleDatabaseHelper(GlobalState.appContext);

        Cursor cursor = simpleDatabaseHelper.getAllStores();
        if(cursor != null){
            if (cursor.moveToFirst()) {
                do {
                    hashIdUpdated.put(cursor.getString(0), cursor.getString(3));
                } while (cursor.moveToNext());
            }
        }

        context = this.getApplicationContext();
        animBounce = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);

        eapref = GlobalState.eaPref;
        editor = GlobalState.editor;

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);

        if (null == savedInstanceState) {
            mNavItemId = R.id.nav_home;
        } else {
            mNavItemId = savedInstanceState.getInt(NAV_ITEM_ID);
        }
        mNavItemId = R.id.nav_home;

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        navigationView.getMenu().findItem(mNavItemId).setChecked(true);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        navigate(mNavItemId);

        String downloadStatus = GlobalState.eaPref.getString("storeDownload", "notExists");
        if (!downloadStatus.equals("exists"))
            new CHECKINTERNETCONNECTION().execute();

        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        simpleDatabaseHelper = new SimpleDatabaseHelper(this);

        int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();
        executor = new ThreadPoolExecutor(
                NUMBER_OF_CORES * 2,
                NUMBER_OF_CORES * 2,
                60L,
                TimeUnit.SECONDS,
                new LinkedBlockingQueue<Runnable>()
        );
    }

    private void navigate(final int id) {
        Fragment fragment = null;
        if (id == R.id.nav_home) {
            fragment = new HomeFragment();
        } else if (id == R.id.nav_settings) {
            fragment = new SettingsFragment();
        } else if (id == R.id.nav_aboutus) {
            fragment = new AboutUsFragment();
        }
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onNavigationItemSelected(final MenuItem menuItem) {
        menuItem.setChecked(true);
        mNavItemId = menuItem.getItemId();

        mDrawerLayout.closeDrawer(GravityCompat.START);
        mDrawerActionHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                navigate(menuItem.getItemId());
            }
        }, DRAWER_CLOSE_DELAY_MS);
        return true;
    }

    @Override
    public void onConfigurationChanged(final Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.support.v7.appcompat.R.id.home) {
            return mDrawerToggle.onOptionsItemSelected(item);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            alertClosing();
            //super.onBackPressed();
        }
    }

    private void alertClosing() {
        AlertDialog.Builder closingAlertDialog = new AlertDialog.Builder(MainActivity.this, R.style.AppCompatAlertDialogStyle);
        closingAlertDialog.setMessage("Do you want to exit?");
        closingAlertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        closingAlertDialog.setNegativeButton("No", null);
        closingAlertDialog.show();
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(NAV_ITEM_ID, mNavItemId);
    }

    @Override
    public void onClick(View v) {

        int id = v.getId();

        switch (id) {
            case R.id.card_view_stores:
                startActivity(new Intent(getApplicationContext(), StoreListActivity.class)
                        .putCharSequenceArrayListExtra("searchStoreArrayList", searchStoreArrayList));
                break;
            case R.id.card_view_offers:
                startActivity(new Intent(getApplicationContext(), OfferByCategoryActivity.class));
                break;
            case R.id.card_view_foodcourt:
                startActivity(new Intent(getApplicationContext(), FoodCourtListActivity.class));
                break;
            case R.id.card_view_entertainment:
                startActivity(new Intent(getApplicationContext(), EntertainmentListActivity.class));
                break;
            case R.id.card_view_whereami:
                viewQrDialog(OPTION_WHEREAMI);
                break;
            case R.id.card_view_parking_tracker:
                viewParkingTrackerOptionDialog();
                break;
            case R.id.card_view_atm:
                startActivity(new Intent(getApplicationContext(), AtmListActivity.class));
                break;
            case R.id.card_view_restroom:
                startActivity(new Intent(getApplicationContext(), RestroomsListActivity.class));
                break;
        }
    }

    private void viewParkingTrackerOptionDialog() {

        parkingOptionDialog = new Dialog(this);
        parkingOptionDialog.getWindow().getAttributes().windowAnimations = R.style.ScreenshotDialogAnimation;
        parkingOptionDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        parkingOptionDialog.setContentView(R.layout.parking_optionview);
        parkingOptionDialog.setCancelable(true);

        Window window = parkingOptionDialog.getWindow();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.CENTER;
        window.setAttributes(lp);

        Drawable d = new ColorDrawable(getResources().getColor(R.color.lightBackground));
        window.setBackgroundDrawable(d);
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        parkingOptionDialog.show();

        parkingOptionDialog.findViewById(R.id.qrview_parking).startAnimation(animBounce);
        parkingOptionDialog.findViewById(R.id.trackview_parking).startAnimation(animBounce);

        Button btn_close = (Button) parkingOptionDialog.findViewById(R.id.btnoptioncancel_parking);
        FrameLayout frame_qr_parking = (FrameLayout) parkingOptionDialog.findViewById(R.id.qrframe_parking);
        FrameLayout frame_track_parking = (FrameLayout) parkingOptionDialog.findViewById(R.id.trackframe_parking);

        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parkingOptionDialog.dismiss();
            }
        });
        frame_qr_parking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parkingOptionDialog.dismiss();
                viewQrDialog(OPTION_PARKING);
            }
        });
        frame_track_parking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                processTrackRequest();
            }
        });
    }

    private void viewQrDialog(String option) {

        OPTION = option;

        qrDialog = new Dialog(this);
        qrDialog.getWindow().getAttributes().windowAnimations = R.style.ScreenshotDialogAnimation;
        qrDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        qrDialog.setContentView(R.layout.qrview);
        qrDialog.setCancelable(true);

        Window window = qrDialog.getWindow();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.CENTER;
        window.setAttributes(lp);

        Drawable d = new ColorDrawable(getResources().getColor(R.color.lightBackground));
        window.setBackgroundDrawable(d);
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        qrDialog.show();

        myDecoderView = (QRCodeReaderView) qrDialog.findViewById(R.id.qrdecoderview);
        myDecoderView.setOnQRCodeReadListener(this);
        myDecoderView.getCameraManager().startPreview();

        Button btn_close = (Button) qrDialog.findViewById(R.id.btnqrcancel);
        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qrDialog.dismiss();
            }
        });

        qrDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                myDecoderView.getCameraManager().stopPreview();
            }
        });
    }

    private ProgressDialog viewProgressDialog(String message) {
        final ProgressDialog progressDialog = new ProgressDialog(context, R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(message);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Main Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

    private class CHECKINTERNETCONNECTION extends AsyncTask<String, String, String> {

        ProgressDialog progressDialog;
        String status = "null";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(MainActivity.this, R.style.AppTheme_Dark_Dialog);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Checking for Internet Connection...");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();

            progressBar = new ProgressDialog(MainActivity.this);
            progressBar.setCancelable(false);
            progressBar.setMessage("Updating stores, please wait...");
            progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressBar.setProgress(0);
            progressBar.setMax(100);

        }

        @Override
        protected String doInBackground(String... f_url) {
            try {
                if (isNetworkAvailable()) {
                    try {
                        HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
                        urlc.setRequestProperty("User-Agent", "Test");
                        urlc.setRequestProperty("Connection", "close");
                        urlc.setConnectTimeout(1500);
                        urlc.connect();
                        if ((urlc.getResponseCode() == 200)) {
                            status = "connected";
                        } else {
                            status = "notConnected";
                        }
                    } catch (IOException e) {
                    }
                } else {
                }

            } catch (Exception e) {

            }
            return null;
        }

        protected void onProgressUpdate(String... progress) {
            super.onProgressUpdate(progress);
        }

        @Override
        protected void onPostExecute(String url) {

            if (status.equals("connected")) {
                progressDialog.dismiss();

                progressBar.show();

                new Thread(new Runnable() {
                    public void run() {
                        while (progressBarStatus < 100) {
                            progressBarStatus = doOperation();
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            progressBarHandler.post(new Runnable() {
                                public void run() {
                                    progressBar.setProgress(progressBarStatus);
                                }
                            });
                        }
                        if (progressBarStatus >= 100) {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this,
                                            R.style.AppCompatAlertDialogStyle);
                                    builder.setMessage("Updated Successfully...!");
                                    builder.setCancelable(false);
                                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            progressBar.dismiss();
                                        }
                                    });
                                    builder.show();
                                }
                            });
                        }
                    }
                }).start();

                ParseQuery<ParseObject> userQuery = ParseQuery.getQuery("AppUsers");
                userQuery.setLimit(1000);
                userQuery.findInBackground(new FindCallback<ParseObject>() {
                    public void done(List<ParseObject> userList, ParseException e) {
                        if (e == null) {
                            for (ParseObject user : userList) {
                                String userName = "";
                                String userPhone = "";
                                String userEmail = "";

                                if (user.has("deviceImei")) {
                                    if (user.getString("deviceImei").equals(GlobalState.deviceImei)) {
                                        userName = user.getString("userName");
                                        userPhone = user.getString("phoneNumber");
                                        userEmail = user.getString("email");

                                        GlobalState.editor.putString("profileStatus", "Registered");
                                        GlobalState.editor.putString("userName", userName);
                                        GlobalState.editor.putString("phoneNumber", userPhone);
                                        GlobalState.editor.putString("email", userEmail);
                                        GlobalState.editor.commit();
                                    } else {
                                        GlobalState.editor.putString("profileStatus", "NA");
                                        GlobalState.editor.commit();
                                    }
                                }
                            }
                            ParseObject.pinAllInBackground(userList);
                        } else {
                            showAlertForNoInternet();
                        }
                    }
                });

                ParseQuery<ParseObject> storeQuery = ParseQuery.getQuery(getResources().getString(R.string.store_class_name));
                storeQuery.setLimit(1000);
                storeQuery.findInBackground(new FindCallback<ParseObject>() {
                    public void done(List<ParseObject> storeList, ParseException e) {
                        if (e == null) {
                            ParseObject.pinAllInBackground(storeList);
                            storeCount = storeList.size();
                            totalStores = storeList.size();
                            totalCount = storeList.size();
                            int count = 0;
                            searchStoreArrayList = new ArrayList<CharSequence>();
                            for (ParseObject store : storeList) {
                                String storeName = "";
                                String floorName = "";
                                if (store.has("sName")) {
                                    storeName = store.getString("sName");
                                    floorName = store.getString("sFloor");

                                    searchStoreArrayList.add(storeName + "*" + floorName );

                                    String storeId = "";
                                    String storeLogoUrl = "";
                                    String updatedAt = "";

                                    storeId = store.getString("sId");
                                    storeLogoUrl = store.getString("sLogo");
                                    updatedAt = store.getString("sUpdated_at");

                                    count = count + 1;
                                    storeToLocalDb(count, storeId, storeName, storeLogoUrl, updatedAt);
                                }
                            }
                        } else {
                            showAlertForNoInternet();
                        }
                    }
                });

                ParseQuery<ParseObject> offerQuery = ParseQuery.getQuery("Offer");
                offerQuery.setLimit(1000);
                offerQuery.findInBackground(new FindCallback<ParseObject>() {
                    public void done(List<ParseObject> offerList, ParseException e) {
                        if (e == null) {
                            ParseObject.pinAllInBackground(offerList);
                            if(offerList.size() > 0)
                                Log.i("****** ", "Offerlist is Not Empty ");
                            else
                                Log.i("****** ", "Offerlist is Empty ");
                        } else {
                            showAlertForNoInternet();
                        }
                    }
                });

                ParseQuery<ParseObject> categoryQuery = ParseQuery.getQuery("Categories");
                categoryQuery.findInBackground(new FindCallback<ParseObject>() {
                    public void done(List<ParseObject> categoryList, ParseException e) {
                        if (e == null) {

                            ParseObject.pinAllInBackground(categoryList);

                            long date = System.currentTimeMillis();
                            SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy h:mm a");
                            String dateString = sdf.format(date);

                            GlobalState.editor.putString("lastUpdatedOn", dateString);
                            GlobalState.editor.commit();

                            GlobalState.editor.putString("storeDownload", "exists");
                            GlobalState.editor.commit();

                        } else {

                            showAlertForNoInternet();
                        }
                    }
                });

            } else if (status.equals("notConnected")) {

                progressDialog.dismiss();

                AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
                builder.setTitle("Update failed");
                builder.setMessage("Problem connecting to the internet, Please try again...");
                builder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        new CHECKINTERNETCONNECTION().execute();
                    }
                });
                builder.show();

            } else {
                progressDialog.dismiss();

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this, R.style.AppCompatAlertDialogStyle);
                builder.setTitle("Update failed");
                builder.setMessage("Problem connecting to the internet, Please try again...");
                builder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        new CHECKINTERNETCONNECTION().execute();
                    }
                });
                builder.show();
            }
        }
    }

    private void storeToLocalDb(int count, String storeId, String storeName, String storeLogoUrl, String updatedAt){
        countedStores = countedStores + 1;

        if(!TextUtils.isEmpty(storeLogoUrl) && storeLogoUrl != null){
            executor.execute(new LongThread(this, count, storeId, storeName, storeLogoUrl,
                    updatedAt, "add", simpleDatabaseHelper, new Handler(this)));
            simpleDatabaseHelper.addAStore(storeId, storeName, storeLogoUrl, updatedAt);
        }
        else{
            simpleDatabaseHelper.addAStore(storeId, storeName, "", updatedAt);
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    public void showAlertForNoInternet() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Update failed");
        builder.setMessage("Problem connecting to the internet, Please try again...");
        builder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                new CHECKINTERNETCONNECTION().execute();
            }
        });
        builder.show();
    }

    @Override
    public void onQRCodeRead(String text, PointF[] points) {

        if (OPTION.equals(OPTION_WHEREAMI)) {
            OPTION = "";
            doForWhereAmI(text);
        } else if (OPTION.equals(OPTION_PARKING)) {
            OPTION = "";
            doForParkingTracker(text);
        }
    }

    @Override
    public void cameraNotFound() {

    }

    @Override
    public void QRCodeNotFoundOnCamImage() {

    }

    private void processTrackRequest() {

        String parkingStatus = eapref.getString(P_STATUS, INVALID);
        if (parkingStatus.equals(P_VALID)) {

            parkingOptionDialog.dismiss();

            String p_name = eapref.getString(P_STORE_NAME, "NA");
            String p_floor = eapref.getString(P_STORE_FLOOR, "NA");
            String p_store_no = eapref.getString(P_STORE_NO, "NA");
            String p_lat = eapref.getString(P_LAT, "NA");
            String p_lon = eapref.getString(P_LON, "NA");

            context.startActivity(new Intent(context, VehiclesLocationActivity.class)
                    .putExtra("storeName", p_name)
                    .putExtra("storeNumber", p_store_no)
                    .putExtra("storeCategory", "PARKING")
                    .putExtra("storeFloor", p_floor)
                    .putExtra("storeLatitude", p_lat)
                    .putExtra("storeLongitude", p_lon)
                    .putExtra("storeDesc", "PARKING")
                    .putExtra("storeLogo", "NA").addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));

        } else {
            viewAlertForParking(NO_LOCATION);
        }
    }

    private void doForWhereAmI(String text) {

        if (!text.equals("null") && text.trim().startsWith("EAQR")) {
            Vibrator v = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(500);
            myDecoderView.getCameraManager().stopPreview();

            String message = "Processing the data, please wait...";
            final ProgressDialog progressDialog = viewProgressDialog(message);
            progressDialog.show();

            ProcessQRTextWMI processQRText = new ProcessQRTextWMI(text);
            final String result = processQRText.processText();

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (!result.equals(INVALID)) {

                        String[] srcStoreDetails = result.split("\\*");
                        String srcStoreLatitude = srcStoreDetails[0];
                        String srcStoreLongitude = srcStoreDetails[1];
                        String srcStoreName = srcStoreDetails[2];
                        String srcStoreFloor = srcStoreDetails[3];
                        String srcStoreNumber = srcStoreDetails[4];

                        String storeCategory = "";

                        if (srcStoreFloor.equals("B1") || srcStoreFloor.equals("B2") || srcStoreFloor.equals("B3"))
                            storeCategory = "PARKING";
                        else
                            storeCategory = "NA";


                        sourcePoint = srcStoreLatitude + "*" + srcStoreLongitude + "*" + srcStoreName;
                        GlobalState.source = sourcePoint;

                        startActivity(new Intent(context, WhereAmIActivity.class)
                                .putExtra("storeName", srcStoreName)
                                .putExtra("storeNumber", srcStoreNumber)
                                .putExtra("storeCategory", storeCategory)
                                .putExtra("storeFloor", srcStoreFloor)
                                .putExtra("storeLatitude", srcStoreLatitude)
                                .putExtra("storeLongitude", srcStoreLongitude)
                                .putExtra("storeDesc", "NA")
                                .putExtra("storeLogo", "NA"));

                        progressDialog.dismiss();
                        qrDialog.dismiss();

                    } else {
                        progressDialog.dismiss();
                        progressDialog.dismiss();
                        qrDialog.dismiss();
                    }
                }
            }, 3000);

        } else {
            myDecoderView.getCameraManager().stopPreview();
            qrDialog.dismiss();
            Snackbar.make(this.getWindow().getDecorView().findViewById(android.R.id.content),
                    "Invalid QR Code!", Snackbar.LENGTH_LONG).show();
        }
    }

    private void doForParkingTracker(String text) {

        if (!text.equals("null") && text.trim().startsWith("EAQR")) {
            Vibrator v = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(500);
            myDecoderView.getCameraManager().stopPreview();

            String message = "Processing the data, please wait...";
            final ProgressDialog progressDialog = viewProgressDialog(message);
            progressDialog.show();

            ProcessQRTextWMI processQRText = new ProcessQRTextWMI(text);
            final String result = processQRText.processText();

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (!result.equals(INVALID)) {

                        String[] parkingDetails = result.split("\\*");
                        String parkingLatitude = parkingDetails[0];
                        String parkingLongitude = parkingDetails[1];
                        String parkingName = parkingDetails[2];
                        String parkingFloor = parkingDetails[3];
                        String parkingNumber = parkingDetails[4];

                        editor.putString(P_STATUS, P_VALID);
                        editor.putString(P_LAT, parkingLatitude);
                        editor.putString(P_LON, parkingLongitude);
                        editor.putString(P_STORE_NAME, parkingName);
                        editor.putString(P_STORE_FLOOR, parkingFloor);
                        editor.putString(P_STORE_NO, parkingNumber);
                        editor.commit();

                        progressDialog.dismiss();
                        qrDialog.dismiss();
                        viewAlertForParking(SAVE_RESPONSE);

                    } else {
                        progressDialog.dismiss();
                        qrDialog.dismiss();
                    }
                }
            }, 3000);

        } else {
            myDecoderView.getCameraManager().stopPreview();
            qrDialog.dismiss();
            Snackbar.make(this.getWindow().getDecorView().findViewById(android.R.id.content),
                    "Invalid QR Code!", Snackbar.LENGTH_LONG).show();
        }
    }

    private void viewAlertForParking(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setMessage(message);
        builder.setPositiveButton("OK", null);
        builder.show();
    }


    // DownloadImage AsyncTask
    private class DownloadImage extends AsyncTask<String, Void, Bitmap> {

        String id = "";
        String name = "";
        String url = "";
        String updatedAt = "";
        String todo = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected Bitmap doInBackground(String... URL) {

            id = URL[0];
            name = URL[1];
            url = URL[2];
            updatedAt = URL[3];
            todo = URL[4];

            Bitmap bitmap = null;
            try {
                InputStream input = new java.net.URL(url).openStream();
                bitmap = BitmapFactory.decodeStream(input);

                ContextWrapper cw = new ContextWrapper(getApplicationContext());
                File directory = cw.getDir("ealogos", Context.MODE_PRIVATE);
                if (!directory.exists()) {
                    directory.mkdir();
                }
                File mypath = new File(directory, name + ".png");
                Log.i("*** ", "doInBackground: " + mypath.getAbsolutePath());
                FileOutputStream fos = null;
                try {
                    fos = new FileOutputStream(mypath);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                    fos.close();
                } catch (Exception e) {
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if(result != null){
                Log.i("*** ", "doInBackground: Name " + name);
                if(todo.equals("add"))
                    simpleDatabaseHelper.addAStore(id, name, url, updatedAt);
                else if (todo.equals("update"))
                    simpleDatabaseHelper.update(id, name, url, updatedAt);
            }
        }
    }


    public static byte[] getBitmapAsByteArray(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, outputStream);
        return outputStream.toByteArray();
    }

    public int doOperation() {
        while (getCountedStores() <= totalStores) {
            if(getCountedStores() > 0)
                return (getCountedStores() / totalStores) * 100;
        }
        return 100;
    }

    private int getCountedStores(){
        return countedStores;
    }

    @Override
    public boolean handleMessage(Message msg) {
        curCount++;
        float per = (curCount / totalCount) * 100;
        progressBarHorizontal.setProgress((int) per);
        if (per >= 100){
            /*AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this, R.style.AppCompatAlertDialogStyle);
            builder.setMessage("Updated Successfully...!");
            builder.setCancelable(false);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    progressBar.dismiss();
                }
            });
            builder.show();*/
        }
        return true;
    }
}