package com.expressavenue.mallapp.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.expressavenue.mallapp.R;
import com.expressavenue.mallapp.global.GlobalState;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.List;

/**
 * Created by nivedith on 1/15/2016.
 */
public class EventDetailsActivity extends AppCompatActivity {

    private TextView txtEventName;
    private TextView txtEventStart;
    private TextView txtEventEnd;
    private TextView txtEventDesc;
    private ImageView eventStoreLogo;
    private CardView cardClose;

    private String eventName = "";
    private String eventStartDate = "";
    private String eventEndDate = "";
    private String eventStartTime = "";
    private String eventEndTime = "";
    private String eventDesc = "";
    private String eventStoreId = "";
    private String from = "";

    private Toolbar toolbar;

    private SharedPreferences eapref = GlobalState.eaPref;
    private SharedPreferences.Editor editor = GlobalState.editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_details);

        toolbar = (Toolbar) findViewById(R.id.toolbar_locate_store);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        txtEventName = (TextView)findViewById(R.id.eventName);
        txtEventStart = (TextView)findViewById(R.id.startDate);
        txtEventEnd = (TextView) findViewById(R.id.endDate);
        txtEventDesc = (TextView) findViewById(R.id.eventDesc);
        cardClose = (CardView) findViewById(R.id.card_event_details);
        eventStoreLogo = (ImageView) findViewById(R.id.eventLogo);

        editor.putInt("NOTIFICATION_COUNT", 0);
        editor.commit();

        from = getIntent().getStringExtra("FROM");
        if(from.equals("EVENTLIST")){
            eventName = getIntent().getStringExtra("offerName");
            eventStartDate = getIntent().getStringExtra("offerStartDate");
            eventEndDate = getIntent().getStringExtra("offerEndDate");
            eventStartTime = getIntent().getStringExtra("offerStartTime");
            eventEndTime = getIntent().getStringExtra("offerEndTime");
            eventDesc = getIntent().getStringExtra("offerDesc");

            Date sDate = new Date(eventStartDate);
            Date dDate = new Date(eventEndDate);

            String startingDate = "";
            String endingDate = "";

            if(sDate != null){
                String[] startDate = sDate.toString().split("\\s+");
                startingDate = "Starting on: "+startDate[0] +" "+ startDate[1] +" "+ startDate[2] +" "+ startDate[5];
            }
            if(dDate != null){
                String[] endDate = dDate.toString().split("\\s+");
                endingDate = "Ending on: "+endDate[0] +" "+ endDate[1] +" "+ endDate[2] +" "+ endDate[5];
            }
            txtEventStart.setText(startingDate + " @ " + eventStartTime);
            txtEventEnd.setText(endingDate + " @ " + eventEndTime);
            txtEventDesc.setText(eventDesc);
        }else if(from.equals("PUSH")){
            eventName = getIntent().getStringExtra("offerName");
            eventStartDate = getIntent().getStringExtra("offerStartDate");
            eventEndDate = getIntent().getStringExtra("offerEndDate");
            eventStartTime = getIntent().getStringExtra("offerStartTime");
            eventEndTime = getIntent().getStringExtra("offerEndTime");
            eventDesc = getIntent().getStringExtra("offerDesc");
            eventStoreId = getIntent().getStringExtra("offerStoreId");

            String startingDate = "";
            String endingDate = "";

            if(!eventStartDate.isEmpty()){
                startingDate = "Starting on: " + eventStartDate;
            }
            if(!eventEndDate.isEmpty()){
                endingDate = "Ending on: " + eventEndDate;
            }
            txtEventStart.setText(startingDate + " @ " + eventStartTime);
            txtEventEnd.setText(endingDate + " @ " + eventEndTime);
            txtEventDesc.setText(eventDesc);
        }



        AssetManager assetManager = getApplicationContext().getAssets();
        try {
            InputStream ims = assetManager.open("EAStoreLogos/ealogoforstores.png");
            Drawable d = Drawable.createFromStream(ims, null);
            eventStoreLogo.setImageDrawable(d);
        }
        catch(IOException ex) {
            eventStoreLogo.setImageResource(R.drawable.ealogoforstores);
        }
        txtEventName.setText(eventName);
        cardClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        new CHECKINTERNETCONNECTION().execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case android.R.id.home:
                super.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class CHECKINTERNETCONNECTION extends AsyncTask<String, Integer, String> {

        String status = "null";
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected String doInBackground(String... f_url) {
            try {
                if (isNetworkAvailable()) {
                    try {
                        HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
                        urlc.setRequestProperty("User-Agent", "Test");
                        urlc.setRequestProperty("Connection", "close");
                        urlc.setConnectTimeout(1500);
                        urlc.connect();
                        if((urlc.getResponseCode() == 200)){
                            status = "connected";
                        }else{
                            status = "notConnected";
                        }
                    } catch (IOException e) {
                    }
                } else {
                }
            } catch (Exception e) {
            }
            return null;
        }
        @Override
        protected void onPostExecute(String url) {
            if(status.equals("connected")){
                ParseQuery<ParseObject> offerQuery = ParseQuery.getQuery("Offer");
                offerQuery.setLimit(1000);
                offerQuery.findInBackground(new FindCallback<ParseObject>() {
                    public void done(List<ParseObject> offerList, ParseException e) {
                        if (e == null) {
                            ParseObject.pinAllInBackground(offerList);
                        }
                    }
                });
            }
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }
}
