package com.expressavenue.mallapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.expressavenue.mallapp.R;
import com.expressavenue.mallapp.global.GlobalState;

/**
 * Created by nivedith on 1/7/2016.
 */
public class ViewProfileActivity extends AppCompatActivity{

    private Toolbar toolbar;

    private TextView txtname;
    private TextView txtEmail;
    private TextView txtPhone;
    private Button btnCloseProfile;

    private String profileName = "";
    private String profileEmail = "";
    private String profilePhone = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewprofile);

        toolbar = (Toolbar) findViewById(R.id.toolbar_view_profile);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("View Profile");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        profileName = GlobalState.eaPref.getString("userName", "NA");
        profileEmail = GlobalState.eaPref.getString("email", "NA");
        profilePhone = GlobalState.eaPref.getString("phoneNumber", "NA");

        txtname = (TextView)findViewById(R.id.txtName);
        txtEmail = (TextView)findViewById(R.id.txtMail);
        txtPhone = (TextView)findViewById(R.id.txtPhone);
        btnCloseProfile = (Button)findViewById(R.id.btncloseprofile);

        txtname.setText(profileName);
        txtEmail.setText(profileEmail);
        txtPhone.setText(profilePhone);

        btnCloseProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                ViewProfileActivity.this.finish();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class));
        ViewProfileActivity.this.finish();
    }
}
