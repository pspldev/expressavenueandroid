package com.expressavenue.mallapp.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.expressavenue.mallapp.R;
import com.expressavenue.mallapp.fragments.EventsFragment;
import com.expressavenue.mallapp.fragments.OffersByCategoryFragment;
import com.expressavenue.mallapp.global.GlobalState;
import com.expressavenue.mallapp.pojos.Category;
import com.expressavenue.mallapp.pojos.Offer;
import com.expressavenue.mallapp.pojos.Store;
import com.expressavenue.mallapp.utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by nivedith on 12/29/2015.
 */
public class OfferByCategoryActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private ArrayList<String> offerCategorynameList;
    private ArrayList<Category> sortedOfferCategoriesList;
    private HashMap<String, Integer> categoryCountHash;
    private ArrayList<Store> storeArrayList;
    private ArrayList<Category> categoryArrayList;
    private ArrayList<String> mOfferCategories;

    private String offerCategoryName = "";
    private Dialog toolbarSearchDialog;

    private ArrayList<String> eventNamelist;
    private ArrayList<Offer> sortedEventSearchList;
    private ArrayList<Offer> sortedEventsList;
    private ArrayList<Offer> eventsArrayList;
    private ArrayList<String> mEvents;

    private boolean isEventsListEmpty = false;

    private String fromPush = "";
    private ViewPagerAdapter adapter;

    public ProgressDialog progressDialogStores;
    public Snackbar snackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_tabs);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbarSearchDialog = new Dialog(OfferByCategoryActivity.this, R.style.MaterialSearch);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);

        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(OfferByCategoryActivity.this, R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Updating offers and events...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        try{
            String FROM = this.getIntent().getStringExtra("FROM");
            if(FROM.equals("PUSH")){
                ParseQuery<ParseObject> offerQuery = ParseQuery.getQuery("Offer");
                offerQuery.setLimit(500);
                offerQuery.findInBackground(new FindCallback<ParseObject>() {
                    public void done(List<ParseObject> offerList, ParseException e) {
                        if (e == null) {
                            ParseObject.pinAllInBackground(offerList);

                            setupViewPager(viewPager);
                            viewPager.setCurrentItem(0);
                            GlobalState.currentFragment = "OFFER";
                            tabLayout.setupWithViewPager(viewPager);
                            progressDialog.dismiss();
                        }
                    }
                });
            }
        }catch (NullPointerException ex){
            setupViewPager(viewPager);
            viewPager.setCurrentItem(0);
            GlobalState.currentFragment = "OFFER";
            tabLayout.setupWithViewPager(viewPager);
            progressDialog.dismiss();
        }

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int pos = tab.getPosition();
                switch (pos) {
                    case 0:
                        viewPager.setCurrentItem(0);
                        GlobalState.currentFragment = "OFFER";
                        break;
                    case 1:
                        viewPager.setCurrentItem(1);
                        GlobalState.currentFragment = "EVENT";
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        getOfferCategories();
        getEvents();
        offerCategorynameList = new ArrayList<String>();

        categoryCountHash = new HashMap<String, Integer>();
        sortedOfferCategoriesList = new ArrayList<Category>();

        eventNamelist = new ArrayList<String>();
    }

    private void getEvents(){
        eventsArrayList = new ArrayList<Offer>();
        eventsArrayList.clear();
        sortedEventsList = new ArrayList<Offer>(eventsArrayList);

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Offer");
        query.fromPin();
        query.fromLocalDatastore();
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> eventsList, ParseException e) {
                if (e == null) {
                    for (ParseObject event : eventsList) {

                        String offerId = "";
                        String offerType = "";
                        String offerName = "";
                        String storeId = "";
                        String offerLogo = "";
                        String offerDesc = "";
                        Date offerStartDate = null;
                        Date offerEndDate = null;
                        String offerStartTime = "";
                        String offerEndTime = "";

                        boolean isOfferDeleted = true;

                        if (event.has("offerType")) {
                            if (event.get("offerType").equals("Event")) {

                                if (event.has("deleted"))
                                    isOfferDeleted = event.getBoolean("deleted");
                                else
                                    isOfferDeleted = true;

                                if (event.has("offerId"))
                                    offerId = event.getString("offerId");
                                else
                                    offerId = "NA";

                                if (event.has("offerType"))
                                    offerType = event.getString("offerType");
                                else
                                    offerType = "NA";

                                if (event.has("offerName"))
                                    offerName = event.getString("offerName");
                                else
                                    offerName = "NA";

                                if (event.has("storeId"))
                                    storeId = event.getString("storeId");
                                else
                                    storeId = "NA";

                                if (event.has("offerLogo"))
                                    offerLogo = event.getString("offerLogo");
                                else
                                    offerLogo = "NA";

                                if (event.has("offerDesc")) {
                                    offerDesc = event.getString("offerDesc");
                                } else
                                    offerDesc = "NA";

                                if (event.has("offerStartDate"))
                                    offerStartDate = event.getDate("offerStartDate");
                                else
                                    offerStartDate = null;

                                if (event.has("offerEndDate"))
                                    offerEndDate = event.getDate("offerEndDate");
                                else
                                    offerEndDate = null;

                                if (event.has("offerStartTime"))
                                    offerStartTime = event.getString("offerStartTime");
                                else
                                    offerStartTime = null;

                                if (event.has("offerEndTime"))
                                    offerEndTime = event.getString("offerEndTime");
                                else
                                    offerEndTime = null;

                                if (!isOfferDeleted) {
                                    Date today = Calendar.getInstance().getTime();
                                    if ((offerEndDate.compareTo(today)) >= 0) {
                                        eventsArrayList.add(new Offer(offerId, offerType, offerName, storeId,
                                                offerLogo, offerDesc, offerStartDate, offerEndDate, isOfferDeleted, offerStartTime, offerEndTime));
                                    }
                                }
                            }
                        }

                    }

                    Collections.sort(eventsArrayList, new CustomComparatorForEvents());

                    sortedEventsList.clear();
                    sortedEventsList.addAll(eventsArrayList);
                    if(sortedEventsList.size() == 0){
                        isEventsListEmpty = true;
                    }
                } else {
                }
            }
        });
    }

    private void getOfferCategories(){
        storeArrayList = new ArrayList<Store>();
        storeArrayList.clear();

        ParseQuery<ParseObject> storeQuery = ParseQuery.getQuery(getResources().getString(R.string.store_class_name));
        storeQuery.fromLocalDatastore();
        storeQuery.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> storesListFromLocal, ParseException e) {
                if (e == null) {
                    for (ParseObject store : storesListFromLocal) {

                        String storeObjectId = "";
                        String storeCategory = "";
                        String storeName = "";

                        //Restricting RESTROOMs from store listing
                        if (store.has("sCategory")) {

                            if (!store.get("sCategory").equals("GENERAL") && !store.get("sCategory").equals("RESTROOM")) {

                                storeObjectId = store.getObjectId();

                                if (store.has("sCategory"))
                                    storeCategory = store.getString("sCategory");
                                else
                                    storeCategory = "NA";

                                if (store.has("sName"))
                                    storeName = store.getString("sName");
                                else
                                    storeName = "NA";

                                storeArrayList.add(new Store(storeObjectId, storeCategory, storeName));
                            }
                        }
                    }
                } else {

                }
            }

        });

        ParseQuery<ParseObject> offerQuery = ParseQuery.getQuery("Offer");
        offerQuery.fromPin();
        offerQuery.fromLocalDatastore();
        offerQuery.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> offersList, ParseException e) {
                if (e == null) {
                    for (ParseObject offer : offersList) {

                        String storeId = "";
                        boolean isOfferDeleted = true;
                        Date offerEndDate = null;

                        if (offer.has("offerType")) {

                            if (offer.get("offerType").equals("Offer")) {

                                if (offer.has("deleted"))
                                    isOfferDeleted = offer.getBoolean("deleted");
                                else
                                    isOfferDeleted = true;

                                if (offer.has("storeId"))
                                    storeId = offer.getString("storeId");
                                else
                                    storeId = "NA";

                                if (offer.has("offerEndDate"))
                                    offerEndDate = offer.getDate("offerEndDate");
                                else
                                    offerEndDate = null;

                                if (!isOfferDeleted) {

                                    Date today = Calendar.getInstance().getTime();

                                    if ((offerEndDate.compareTo(today)) >= 0) {

                                        for (Store stores : storeArrayList) {

                                            if (storeId.equals(stores.getStoreObjectId())) {

                                                String categoryName = stores.getStoreCategory();

                                                if (categoryCountHash.containsKey(categoryName)) {
                                                    int count = categoryCountHash.get(categoryName);
                                                    categoryCountHash.remove(categoryName);
                                                    count = count + 1;
                                                    categoryCountHash.put(categoryName, count);
                                                } else {
                                                    categoryCountHash.put(categoryName, 1);
                                                }
                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }
                    categoryArrayList = new ArrayList<Category>();
                    categoryArrayList.clear();

                    ParseQuery<ParseObject> query = ParseQuery.getQuery("Categories");
                    query.fromPin();
                    query.fromLocalDatastore();
                    query.findInBackground(new FindCallback<ParseObject>() {
                        public void done(List<ParseObject> categoriesList, ParseException e) {
                            if (e == null) {
                                for (ParseObject category : categoriesList) {

                                    String categoryId;
                                    String categoryName;
                                    String categoyDesc;

                                    boolean isCategoryDeleted = false;

                                    if (category.has("deleted"))
                                        isCategoryDeleted = category.getBoolean("deleted");
                                    else
                                        isCategoryDeleted = true;

                                    if (category.has("categoryId"))
                                        categoryId = category.getString("categoryId");
                                    else
                                        categoryId = "NA";

                                    if (category.has("categoryName"))
                                        categoryName = category.getString("categoryName");
                                    else
                                        categoryName = "NA";

                                    if (category.has("categoyDesc"))
                                        categoyDesc = category.getString("categoyDesc");
                                    else
                                        categoyDesc = "NA";

                                    if (!isCategoryDeleted) {
                                        if (categoryCountHash.containsKey(categoryName))
                                            categoryArrayList.add(new Category(categoryId, categoryName, categoyDesc, isCategoryDeleted, categoryCountHash.get(categoryName)));
                                    }

                                }
                                Collections.sort(categoryArrayList, new CustomComparatorForCategories());

                                sortedOfferCategoriesList.clear();
                                sortedOfferCategoriesList.addAll(categoryArrayList);
                            } else {

                            }
                        }
                    });

                } else {
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_search:
                if(GlobalState.currentFragment.equals("OFFER")){
                    loadToolBarSearchCategoriesForOffer();
                }else if(GlobalState.currentFragment.equals("EVENT")) {
                    loadToolBarSearchEvents();
                }
                break;
            case android.R.id.home:
               super.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        if(toolbarSearchDialog.isShowing()) {
                toolbarSearchDialog.dismiss();
        }else{
            //startActivity(new Intent(OfferByCategoryActivity.this, MainActivity.class));
            //finish();
            super.onBackPressed();
        }

    }

    public void loadToolBarSearchEvents(){

        ArrayList<String> eventStored;
        String eventName = "";

        final View searchView = OfferByCategoryActivity.this.getLayoutInflater().inflate(R.layout.activity_store_tabs, null);
        setViewForLollipop(false, searchView);
        LinearLayout parentToolbarSearch = (LinearLayout) searchView.findViewById(R.id.parent_toolbar_search);
        LinearLayout imgToolBack = (LinearLayout) searchView.findViewById(R.id.layout_img_tool_back);
        final EditText edtToolSearch = (EditText) searchView.findViewById(R.id.edt_tool_search);
        LinearLayout imgToolClose = (LinearLayout) searchView.findViewById(R.id.layout_img_tool_close);
        final ListView listSearch = (ListView) searchView.findViewById(R.id.list_search);
        final TextView txtEmpty = (TextView) searchView.findViewById(R.id.txt_empty);
        parentToolbarSearch.setVisibility(View.VISIBLE);

        Utils.setListViewHeightBasedOnChildren(listSearch);

        edtToolSearch.setHint("Search Event");

        toolbarSearchDialog.setContentView(searchView);
        toolbarSearchDialog.setCancelable(false);
        toolbarSearchDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        toolbarSearchDialog.getWindow().setGravity(Gravity.BOTTOM);
        toolbarSearchDialog.setCanceledOnTouchOutside(true);
        toolbarSearchDialog.show();

        toolbarSearchDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        eventStored = new ArrayList<String>();
        final com.expressavenue.mallapp.adapters.EventSearchAdapter eventSearchAdapter = new com.expressavenue.mallapp.adapters.EventSearchAdapter(getApplicationContext(), eventStored, false);

        listSearch.setVisibility(View.VISIBLE);
        listSearch.setAdapter(eventSearchAdapter);

        if(!eventNamelist.isEmpty()){
            eventNamelist.clear();
        }

        for(int i = 0; i < sortedEventsList.size(); i++){
            Offer event = sortedEventsList.get(i);
            eventName = event.getOfferName();
            eventNamelist.add(eventName + "*" + i);
        }

        listSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                RelativeLayout rlView = (RelativeLayout) view;
                if (rlView != null) {
                    TextView txtView = (TextView) rlView.getChildAt(0);
                    String str = txtView.getText().toString();
                    String tag = (String) txtView.getTag();

                    Offer event = sortedEventsList.get(Integer.parseInt(tag));
                    startActivity(new Intent(getApplicationContext(), EventDetailsActivity.class)
                            .putExtra("offerName", event.getOfferName())
                            .putExtra("offerStartDate", event.getOfferStartDate().toString())
                            .putExtra("offerEndDate", event.getOfferEndDate().toString())
                            .putExtra("offerStartTime", event.getOfferStartTime())
                            .putExtra("offerEndTime", event.getOfferEndTime())
                            .putExtra("offerDesc", event.getOfferDesc())
                            .putExtra("offerStoreId", event.getStoreId()));
                    finish();
                }
            }
        });


        edtToolSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                String[] eventsStored = eventNamelist.toArray(new String[eventNamelist.size()]);
                mEvents = new ArrayList<String>(Arrays.asList(eventsStored));
                listSearch.setVisibility(View.VISIBLE);
                eventSearchAdapter.updateList(mEvents, true);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ArrayList<String> filterList = new ArrayList<String>();
                boolean isNodata = false;
                if (s.length() > 0) {
                    for(int j = 0; j < mEvents.size(); j++){
                        if (mEvents.get(j).toLowerCase().startsWith(s.toString().trim().toLowerCase())) {

                            filterList.add(mEvents.get(j));

                            listSearch.setVisibility(View.VISIBLE);
                            eventSearchAdapter.updateList(filterList, true);
                            isNodata = true;
                        }

                    }
                    if (!isNodata) {
                        listSearch.setVisibility(View.GONE);
                        txtEmpty.setVisibility(View.VISIBLE);
                        txtEmpty.setText("No data found");
                    }
                } else {
                    listSearch.setVisibility(View.GONE);
                    txtEmpty.setVisibility(View.GONE);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        imgToolBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toolbarSearchDialog.dismiss();
                setViewForLollipop(true, searchView);
            }
        });

        imgToolClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                edtToolSearch.setText("");
                toolbarSearchDialog.dismiss();
                setViewForLollipop(true, searchView);
            }
        });
    }

    public void loadToolBarSearchCategoriesForOffer(){

        ArrayList<String> storeStored;
        final View searchView = OfferByCategoryActivity.this.getLayoutInflater().inflate(R.layout.activity_store_tabs, null);
        setViewForLollipop(false, searchView);
        LinearLayout parentToolbarSearch = (LinearLayout) searchView.findViewById(R.id.parent_toolbar_search);
        LinearLayout imgToolBack = (LinearLayout) searchView.findViewById(R.id.layout_img_tool_back);
        final EditText edtToolSearch = (EditText) searchView.findViewById(R.id.edt_tool_search);
        LinearLayout imgToolClose = (LinearLayout) searchView.findViewById(R.id.layout_img_tool_close);
        final ListView listSearch = (ListView) searchView.findViewById(R.id.list_search);
        final TextView txtEmpty = (TextView) searchView.findViewById(R.id.txt_empty);
        parentToolbarSearch.setVisibility(View.VISIBLE);

        Utils.setListViewHeightBasedOnChildren(listSearch);

        edtToolSearch.setHint("Search Offer Category");

        toolbarSearchDialog.setContentView(searchView);
        toolbarSearchDialog.setCancelable(false);
        toolbarSearchDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        toolbarSearchDialog.getWindow().setGravity(Gravity.BOTTOM);
        toolbarSearchDialog.setCanceledOnTouchOutside(true);
        toolbarSearchDialog.show();

        toolbarSearchDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        storeStored = new ArrayList<String>();
        final com.expressavenue.mallapp.adapters.CategorySearchAdapter categorySearchAdapter = new com.expressavenue.mallapp.adapters.CategorySearchAdapter(getApplicationContext(), storeStored, false);

        listSearch.setVisibility(View.VISIBLE);
        listSearch.setAdapter(categorySearchAdapter);

        listSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                RelativeLayout rlView = (RelativeLayout) view;
                if (rlView != null) {
                    TextView txtView = (TextView) rlView.getChildAt(0);
                    String str = txtView.getText().toString();
                    String tag = (String) txtView.getTag();

                    Category category = sortedOfferCategoriesList.get(Integer.parseInt(tag));
                    GlobalState.editor.putString("categoryGlobal", category.getCategoryName());
                    GlobalState.editor.commit();
                    startActivity(new Intent(getApplicationContext(), OfferListActivity.class)
                            .putExtra("category", "category"));
                    finish();
                }
            }
        });

        if(!offerCategorynameList.isEmpty()){
            offerCategorynameList.clear();
        }

        for(int i = 0; i < sortedOfferCategoriesList.size(); i++){
            Category category = sortedOfferCategoriesList.get(i);
            offerCategoryName = category.getCategoryName();
            offerCategorynameList.add(offerCategoryName + "*" + i);
        }

        edtToolSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                String[] categoriesStored = offerCategorynameList.toArray(new String[offerCategorynameList.size()]);
                mOfferCategories = new ArrayList<String>(Arrays.asList(categoriesStored));
                listSearch.setVisibility(View.VISIBLE);
                categorySearchAdapter.updateList(mOfferCategories, true);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ArrayList<String> filterList = new ArrayList<String>();
                boolean isNodata = false;
                if (s.length() > 0) {
                    for(int j = 0; j < mOfferCategories.size(); j++){
                        if (mOfferCategories.get(j).toLowerCase().startsWith(s.toString().trim().toLowerCase())) {

                            filterList.add(mOfferCategories.get(j));

                            listSearch.setVisibility(View.VISIBLE);
                            categorySearchAdapter.updateList(filterList, true);
                            isNodata = true;
                        }

                    }
                    if (!isNodata) {
                        listSearch.setVisibility(View.GONE);
                        txtEmpty.setVisibility(View.VISIBLE);
                        txtEmpty.setText("No data found");
                    }
                } else {
                    listSearch.setVisibility(View.GONE);
                    txtEmpty.setVisibility(View.GONE);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        imgToolBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toolbarSearchDialog.dismiss();
                setViewForLollipop(true, searchView);
            }
        });

        imgToolClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                edtToolSearch.setText("");
                toolbarSearchDialog.dismiss();
                setViewForLollipop(true, searchView);
            }
        });
    }

    public class CustomComparatorForCategories implements Comparator<Category> {
        @Override
        public int compare(Category o1, Category o2) {
            return o1.getCategoryName().compareTo(o2.getCategoryName());
        }
    }

    public class CustomComparatorForEvents implements Comparator<Offer> {
        @Override
        public int compare(Offer o1, Offer o2) {
            Date today = Calendar.getInstance().getTime();
            return o1.getOfferEndDate().compareTo(today);
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new OffersByCategoryFragment(), "Categories");
        adapter.addFragment(new EventsFragment(), "Events");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void setViewForLollipop(boolean flag, View view){

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.LOLLIPOP){
            if(flag){
                view.findViewById(R.id.app_bar).setVisibility(View.VISIBLE);
                view.findViewById(R.id.viewpager).setVisibility(View.VISIBLE);
                view.findViewById(R.id.toolbar).setVisibility(View.VISIBLE);
            }  else {
                view.findViewById(R.id.app_bar).setVisibility(View.GONE);
                view.findViewById(R.id.viewpager).setVisibility(View.GONE);
                view.findViewById(R.id.toolbar).setVisibility(View.GONE);
            }
        }
    }
}

