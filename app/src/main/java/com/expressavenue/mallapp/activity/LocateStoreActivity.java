package com.expressavenue.mallapp.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PointF;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;
import com.expressavenue.mallapp.utils.Utils;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.expressavenue.mallapp.R;
import com.expressavenue.mallapp.adapters.TextNavAdapter;
import com.expressavenue.mallapp.adapters.WebViewSlidePagerAdapter;
import com.expressavenue.mallapp.algorithm.DijkstraClassForBOne;
import com.expressavenue.mallapp.algorithm.DijkstraClassForBThree;
import com.expressavenue.mallapp.algorithm.DijkstraClassForBTwo;
import com.expressavenue.mallapp.algorithm.DijkstraClassForFF;
import com.expressavenue.mallapp.algorithm.DijkstraClassForLGF;
import com.expressavenue.mallapp.algorithm.DijkstraClassForSF;
import com.expressavenue.mallapp.algorithm.DijkstraClassForTF;
import com.expressavenue.mallapp.algorithm.DijkstraClassForUGF;
import com.expressavenue.mallapp.custom.CustomViewPager;
import com.expressavenue.mallapp.custom.ProcessQRText;
import com.expressavenue.mallapp.global.GlobalState;
import com.expressavenue.mallapp.pojos.Store;
import com.expressavenue.mallapp.utils.TooltipWindow;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

/**
 * Created by dhanil on 25-12-2015.
 */
public class LocateStoreActivity extends AppCompatActivity implements QRCodeReaderView.OnQRCodeReadListener {

    private Toolbar toolbar;

    private TextView maptitleSolo;
    private TextView destStoreSolo;
    private TextView srcStoreSolo;
    private ImageView imageFloorSoloFrom;
    private ImageView imageFloorSoloTo;
    private ImageView imageArrowTo;
    private RelativeLayout layoutTitleFloor;

    private String storeName;
    private String storeNumber;
    private String storeFloor;
    private String storeCategory;
    private String storeDesc;
    private String storeLatitude;
    private String storeLongitude;
    private String storeLogo;

    private String srcStoreName;
    private String srcStoreNumber;
    private String srcStoreFloor;
    private String srcStoreCategory;
    private String srcStoreDesc;
    private String srcStoreLatitude;
    private String srcStoreLongitude;
    private String srcStoreLogo;

    private String fullFloorNameDest;
    private String currentFloor;

    private TextView txt_StoreName;
    private TextView txt_StoreCategory;
    private TextView txt_StoreFloor;
    private TextView txt_nodatatoviewmap;

    private String url;
    private WebView webview_solo_map;
    private CustomViewPager mPager;
    private PagerAdapter mPagerAdapter;
    private String pathVerticesToHtml;
    private String destinationPoint;
    private String sourcePoint;
    public ArrayList<String> arrayOfVertices;
    private Hashtable<String, Location> nodeHash;

    private Hashtable<String, Location> liftHash;
    private Hashtable<String, Location> leftLiftHash;

    private CardView cardView_previousmap;
    private CardView cardView_nextmap;
    private LinearLayout card_switchPage_layout;
    private TextView txt_previousmap;
    private TextView txt_nextmap;

    private Animation animBounce;
    private QRCodeReaderView myDecoderView;
    private Dialog qrDialog;

    private ArrayList<Store> storeArrayList;
    private ArrayList<Store> sortedStoreList;
    private ArrayList<Store> storeArrayListWithoutFilter;
    private ArrayList<Store> sortedStoreListWithoutFilter;
    private ArrayList<Store> spinnerSelectedStore;

    private Spinner floorSpinner;
    private Spinner storeSpinner;
    private ArrayAdapter<String> storeAdapter;
    private String floorName;

    private String nearestToS[];
    private Hashtable<Location, String> threeDest;
    private Hashtable<Location, String> twoDest;

    private FloatingActionButton fab_takeMeThere;
    private FloatingActionButton fab_textNav;

    private CardView card_selectedStore;
    private ImageView img_logo_manual;
    private TextView txt_storename_manual;
    private TextView txt_storecategory_manual;
    private TextView txt_storefloor_manual;
    private Button btn_takeme_manual;
    private Store storeManualSelected;

    private ArrayList<String> textNavArray;
    private TextNavAdapter textNavAdapter;
    String srcName = "";
    String destName = "";

    TooltipWindow tipWindow;
    private static final int POINTER_SIZE = 15;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locate_store);

        toolbar = (Toolbar) findViewById(R.id.toolbar_locate_store);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txt_StoreName = (TextView)findViewById(R.id.store_title_name);
        txt_StoreName = (TextView)findViewById(R.id.store_title_name);
        txt_StoreCategory = (TextView)findViewById(R.id.store_title_category);
        txt_StoreFloor = (TextView)findViewById(R.id.store_title_floor);
        webview_solo_map = (WebView)findViewById(R.id.webViewSoloMap);
        mPager = (CustomViewPager)findViewById(R.id.pager);
        txt_nodatatoviewmap = (TextView)findViewById(R.id.txt_nodatatoviewmap);

        maptitleSolo = (TextView)findViewById(R.id.txtFloorTitle_solo);
        destStoreSolo = (TextView)findViewById(R.id.txtDestFloor_solo);
        srcStoreSolo = (TextView)findViewById(R.id.txtSourceFloor_solo);
        imageFloorSoloFrom = (ImageView)findViewById(R.id.image_floor_solo_from);
        imageFloorSoloTo= (ImageView)findViewById(R.id.image_floor_solo_to);
        imageArrowTo= (ImageView)findViewById(R.id.image_arrow_solo);
        layoutTitleFloor = (RelativeLayout)findViewById(R.id.layoutTitleFloor);
        layoutTitleFloor.setVisibility(View.GONE);

        fab_takeMeThere = (FloatingActionButton)findViewById(R.id.fab_takeme_there);
        fab_textNav = (FloatingActionButton)findViewById(R.id.fab_text_nav);
        fab_textNav.setVisibility(View.GONE);
        textNavArray = new ArrayList<String>();

        cardView_previousmap = (CardView)findViewById(R.id.card_store_details_previousmap);
        cardView_nextmap = (CardView)findViewById(R.id.card_store_details_nextmap);
        card_switchPage_layout = (LinearLayout)findViewById(R.id.card_store_details_switchmap_layout);
        txt_previousmap = (TextView) findViewById(R.id.previousmap_text);
        txt_nextmap = (TextView) findViewById(R.id.nextmap_txt);

        cardView_previousmap.setEnabled(false);
        txt_previousmap.setTextColor(getResources().getColor(R.color.iron));

        animBounce = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
        spinnerSelectedStore = new ArrayList<Store>();

        getStores();
        getStoreWithoutFilter();

        nodeHash = new Hashtable<String, Location>();
        liftHash = new Hashtable<String, Location>();
        leftLiftHash = new Hashtable<String, Location>();
        threeDest = new Hashtable<Location, String>();
        twoDest = new Hashtable<Location, String>();

        Location loc_escalatorLeft = new Location("ESC-Left");
        loc_escalatorLeft.setLatitude(13.058682);
        loc_escalatorLeft.setLongitude(80.263317);
        threeDest.put(loc_escalatorLeft, "ESC-Left");
        nodeHash.put("ESC-Left", loc_escalatorLeft);

        Location loc_escalatorRight = new Location("ESC-Right");
        loc_escalatorRight.setLatitude(13.058678);
        loc_escalatorRight.setLongitude(80.26455);
        threeDest.put(loc_escalatorRight, "ESC-Right");
        twoDest.put(loc_escalatorRight, "ESC-Right");
        nodeHash.put("ESC-Right", loc_escalatorLeft);

        Location loc_mainLift = new Location("LT120");
        loc_mainLift.setLatitude(13.058802);
        loc_mainLift.setLongitude(80.264586);
        threeDest.put(loc_mainLift, "LT120");
        twoDest.put(loc_mainLift, "LT120");
        nodeHash.put("LT120", loc_mainLift);

        Location liftFFR = new Location("LT120FF");
        liftFFR.setLatitude(13.058814);
        liftFFR.setLongitude(80.264585);
        liftHash.put("LT120FF", liftFFR);

        Location liftSFR = new Location("LT120SF");
        liftSFR.setLatitude(13.058806);
        liftSFR.setLongitude(80.264585);
        liftHash.put("LT120SF", liftSFR);

        Location liftTFR = new Location("LT120TF");
        liftTFR.setLatitude(13.058765);
        liftTFR.setLongitude(80.264576);
        liftHash.put("LT120TF", liftTFR);

        Location liftLGFR = new Location("LT120LGF");
        liftLGFR.setLatitude(13.058776);
        liftLGFR.setLongitude(80.264636);
        liftHash.put("LT120LGF", liftLGFR);

        Location liftUGFR = new Location("LT120UGF");
        liftUGFR.setLatitude(13.058786);
        liftUGFR.setLongitude(80.264691);
        liftHash.put("LT120UGF", liftUGFR);

       /* Location liftB1L = new Location("LT120B1");
        liftB1L.setLatitude(13.058668);
        liftB1L.setLongitude(80.263343);
        liftHash.put("LT120B1", liftB1L);*/

        Location liftB1L = new Location("LT120B1");
        liftB1L.setLatitude(13.058764);
        liftB1L.setLongitude(80.26464);
        liftHash.put("LT120B1", liftB1L);

        /*Location liftB2L = new Location("LT120B2");
        liftB2L.setLatitude(13.058668);
        liftB2L.setLongitude(80.263343);
        liftHash.put("LT120B2", liftB2L);*/
        Location liftB2L = new Location("LT120B2");
        liftB2L.setLatitude(13.058764);
        liftB2L.setLongitude(80.26464);
        liftHash.put("LT120B2", liftB2L);

        /*Location liftB3L = new Location("LT120B3");
        liftB3L.setLatitude(13.058668);
        liftB3L.setLongitude(80.263343);
        liftHash.put("LT120B3", liftB3L);*/
        Location liftB3L = new Location("LT120B3");
        liftB3L.setLatitude(13.058764);
        liftB3L.setLongitude(80.26464);
        liftHash.put("LT120B3", liftB3L);

        storeName = getIntent().getStringExtra("storeName");
        storeNumber = getIntent().getStringExtra("storeNumber");
        storeFloor = getIntent().getStringExtra("storeFloor");
        storeCategory = getIntent().getStringExtra("storeCategory");
        storeDesc = getIntent().getStringExtra("storeDesc");
        storeLatitude = getIntent().getStringExtra("storeLatitude");
        storeLongitude = getIntent().getStringExtra("storeLongitude");
        storeLogo = getIntent().getStringExtra("storeLogo");

        switch (storeFloor){
            case "LGF" :
                fullFloorNameDest = "Lower Ground Floor";
                currentFloor = "LowerGroundFloorMap.html";
                break;
            case "UGF" :
                fullFloorNameDest = "Upper Ground Floor";
                currentFloor = "UpperGroundFloorMap.html";
                break;
            case "FF"  :
                fullFloorNameDest = "First Floor";
                currentFloor = "FirstFloorMap.html";
                break;
            case "SF"  :
                fullFloorNameDest = "Second Floor";
                currentFloor = "SecondFloorMap.html";
                break;
            case "TF"  :
                fullFloorNameDest = "Third Floor";
                currentFloor = "ThirdFloorMap.html";
                break;
            case "B1"  :
                fullFloorNameDest = "Basement One";
                currentFloor = "LowerGroundFloorMap.html";
                break;
            case "B2"  :
                fullFloorNameDest = "Basement Two";
                currentFloor = "BasementTwoFloorMap.html";
                break;
            case "B3"  :
                fullFloorNameDest = "Basement Three";
                currentFloor = "BasementThreeFloorMap.html";
                break;
        }

        txt_StoreName.setText(storeName);
        txt_StoreCategory.setText(storeCategory);
        txt_StoreFloor.setText(fullFloorNameDest);

        final MyJavaScriptInterface myJavaScriptInterface = new MyJavaScriptInterface(this);
        webview_solo_map.addJavascriptInterface(myJavaScriptInterface, "webAppInterfaceObject");
        webview_solo_map.getSettings().setJavaScriptEnabled(true);

        pathVerticesToHtml = storeLatitude+"*"+storeLongitude+"*"+storeName;
        destinationPoint = pathVerticesToHtml;
        GlobalState.destination = destinationPoint;
        url = "file:///android_asset/samples/"+currentFloor+"?param=" + pathVerticesToHtml + "&navigate=false"+"&floor=null";

        if(storeLatitude.equals("NA") || storeLongitude.equals("NA")){
            txt_nodatatoviewmap.setVisibility(View.VISIBLE);
            fab_takeMeThere.setVisibility(View.GONE);
            webview_solo_map.setVisibility(View.GONE);
            mPager.setVisibility(View.GONE);
        }else{
            txt_nodatatoviewmap.setVisibility(View.GONE);
            fab_takeMeThere.setVisibility(View.VISIBLE);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    tipWindow = new TooltipWindow(LocateStoreActivity.this);
                    tipWindow.showToolTip(fab_takeMeThere, getString(R.string.tooltip_takeme));
                }
            }, 500);
            mPager.setVisibility(View.GONE);
            card_switchPage_layout.setVisibility(View.GONE);
            webview_solo_map.loadUrl(url);
        }

        fab_takeMeThere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewOptionsDialog();
            }
        });

        fab_textNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Toast.makeText(getApplicationContext(), "Feature is temporarily disabled!", Toast.LENGTH_SHORT).show();
                if (!GlobalState.soloMap.equals("NA")) {
                    viewTextNavDialog(GlobalState.soloMap);
                } else if (mPager.getCurrentItem() == 0) {
                    viewTextNavDialog(GlobalState.firstMap);
                } else if (mPager.getCurrentItem() == 1) {
                    viewTextNavDialog(GlobalState.secondMap);
                }

            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }

        setNodeHashForFF();
        setNodeHashForSF();
        setNodeHashForTF();
        setNodeHashForUGF();
        setNodeHashForLGF();
        setNodeHashForB1();
        setNodeHashForB2();
        setNodeHashForB3();

        cardView_nextmap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPager.setCurrentItem(1);
                cardView_previousmap.setEnabled(true);
                txt_previousmap.setTextColor(getResources().getColor(R.color.white));
                cardView_nextmap.setEnabled(false);
                txt_nextmap.setTextColor(getResources().getColor(R.color.iron));
            }
        });

        cardView_previousmap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPager.setCurrentItem(0);
                cardView_previousmap.setEnabled(false);
                txt_previousmap.setTextColor(getResources().getColor(R.color.iron));
                cardView_nextmap.setEnabled(true);
                txt_nextmap.setTextColor(getResources().getColor(R.color.white));
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                super.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void viewTextNavDialog(String map) {

        final Dialog textNavDialog = new Dialog(LocateStoreActivity.this);
        textNavDialog.getWindow().getAttributes().windowAnimations = R.style.ScreenshotDialogAnimation;
        textNavDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        textNavDialog.setContentView(R.layout.textnavview);
        textNavDialog.setCancelable(true);

        Window window = textNavDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        Drawable d = new ColorDrawable(getResources().getColor(R.color.white));
        window.setBackgroundDrawable(d);
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        Button btTextNavClose = (Button)textNavDialog.findViewById(R.id.btntextnavclose);
        btTextNavClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textNavDialog.dismiss();
            }
        });

        ListView textNavList = (ListView)textNavDialog.findViewById(R.id.listviewtextnav);

        String textNavString = "";
        switch (map){
            case "FF":
                textNavString = GlobalState.tnPathFF;
                srcName = GlobalState.tnSrcStoreFF;
                destName = GlobalState.tnDestStoreFF;
                break;
            case "SF":
                textNavString = GlobalState.tnPathSF;
                srcName = GlobalState.tnSrcStoreSF;
                destName = GlobalState.tnDestStoreSF;
                break;
            case "TF":
                textNavString = GlobalState.tnPathTF;
                srcName = GlobalState.tnSrcStoreTF;
                destName = GlobalState.tnDestStoreTF;
                break;
            case "LGF":
                textNavString = GlobalState.tnPathLGF;
                srcName = GlobalState.tnSrcStoreLGF;
                destName = GlobalState.tnDestStoreLGF;
                break;
            case "UGF":
                textNavString = GlobalState.tnPathUGF;
                srcName = GlobalState.tnSrcStoreUGF;
                destName = GlobalState.tnDestStoreUGF;
                break;
            case "B1":
                textNavString = GlobalState.tnPathB1;
                srcName = GlobalState.tnSrcStoreB1;
                destName = GlobalState.tnDestStoreB1;
                break;
            case "B2":
                textNavString = GlobalState.tnPathB2;
                srcName = GlobalState.tnSrcStoreB2;
                destName = GlobalState.tnDestStoreB2;
                break;
            case "B3":
                textNavString = GlobalState.tnPathB3;
                srcName = GlobalState.tnSrcStoreB3;
                destName = GlobalState.tnDestStoreB3;
                break;
        }

        if(!textNavArray.isEmpty())
            textNavArray.clear();

        textNavArray.add("S*"+srcName);
        String textNavItems[] = textNavString.split("\\#");
        for(int i = 0; i < textNavItems.length; i++){
            textNavArray.add(textNavItems[i]);
        }
        textNavArray.add("D*" + destName);

        textNavAdapter = new TextNavAdapter(textNavArray, getApplicationContext(), srcName, destName);
        textNavList.setAdapter(textNavAdapter);
        textNavAdapter.notifyDataSetChanged();

        textNavDialog.show();
    }

    public class MyJavaScriptInterface {
        Context mContext;

        MyJavaScriptInterface(Context c) {
            mContext = c;
        }
        @JavascriptInterface
        public String getDrawPath() {
            return GlobalState.drawPath + "#" + GlobalState.source + "#" + GlobalState.destination;

        }
        @JavascriptInterface
        public String getDrawPaths() {
            return GlobalState._drawPath + "#" + GlobalState._source + "#" + GlobalState._destination;

        }
    }

    @Override
    public void onQRCodeRead(String text, PointF[] points) {

        if(!text.equals("null") && text.trim().startsWith("EAQR") && !text.trim().endsWith("*0")){
            Vibrator v = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(500);
            myDecoderView.getCameraManager().stopPreview();

            String message = "Processing the data, please wait...";
            final ProgressDialog progressDialog = viewProgressDialog(message);
            progressDialog.show();

            ProcessQRText processQRText =  new ProcessQRText(text);
            final String result = processQRText.processText();

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(!result.equals("INVALID")){

                        String[] srcStoreDetails = result.split("\\*");
                        srcStoreLatitude = srcStoreDetails[0];
                        srcStoreLongitude = srcStoreDetails[1];
                        srcStoreName = srcStoreDetails[2];
                        srcStoreFloor = srcStoreDetails[3];
                        srcStoreNumber = srcStoreDetails[4];

                        progressDialog.dismiss();
                        qrDialog.dismiss();

                        sourcePoint = srcStoreLatitude + "*"+srcStoreLongitude+"*"+srcStoreName;
                        GlobalState.source = sourcePoint;

                        if(storeFloor.equals(srcStoreFloor) || (srcStoreFloor.equals("LGF") && storeFloor.equals("B1") ) || (srcStoreFloor.equals("B1") && storeFloor.equals("LGF") )){
                            drawRouteForSolo(sourcePoint, destinationPoint);
                        }else{
                            if(srcStoreLatitude.equals("NA") || srcStoreLongitude.equals("NA")){
                                Toast.makeText(getApplicationContext(), "Not enough data to route!", Toast.LENGTH_SHORT).show();
                            }else {
                                Location srcLocation = new Location(srcStoreName);
                                srcLocation.setLatitude(Double.parseDouble(srcStoreLatitude));
                                srcLocation.setLongitude(Double.parseDouble(srcStoreLongitude));
                                drawRouteForDual(sourcePoint, destinationPoint, srcStoreFloor, storeFloor, srcLocation);
                            }
                        }

                    }else{
                        progressDialog.dismiss();
                        qrDialog.dismiss();
                    }
                }
            }, 3000);

        }else {
            myDecoderView.getCameraManager().stopPreview();
            qrDialog.dismiss();
            Snackbar.make(this.getWindow().getDecorView().findViewById(android.R.id.content),
                    "Invalid QR Code!", Snackbar.LENGTH_LONG).show();
        }
    }
    @Override
    public void cameraNotFound() {}
    @Override
    public void QRCodeNotFoundOnCamImage() {}

    private void drawRouteForSolo(String sourcePoint, String destinationPoint){

        layoutTitleFloor.setVisibility(View.VISIBLE);

        webview_solo_map.setVisibility(View.VISIBLE);
        mPager.setVisibility(View.GONE);
        card_switchPage_layout.setVisibility(View.GONE);

        fab_textNav.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                tipWindow = new TooltipWindow(LocateStoreActivity.this);
                tipWindow.showToolTip(fab_textNav, getString(R.string.tooltip_textual));
            }
        }, 500);

        if(srcStoreFloor.equals(storeFloor) || srcStoreFloor.equals("B1") || storeFloor.equals("B1")){
            if(!srcStoreNumber.equals("NA") && !srcStoreLatitude.equals("NA") && !srcStoreLongitude.equals("NA") &&
                    !storeNumber.equals("NA") && !storeLatitude.equals("NA") && !storeLongitude.equals("NA")){
                if(srcStoreNumber.equals(storeNumber)){
                    fab_textNav.setVisibility(View.GONE);
                    AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
                    builder.setMessage("You have selected the same store, please select another store nearby you.");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            viewOptionsDialog();
                        }
                    });
                    builder.setNegativeButton("CANCEL", null);
                    builder.show();
                }else{
                    switch(storeFloor){
                        case "FF":
                            DijkstraClassForFF dijkstraObjectForFF = new DijkstraClassForFF(srcStoreNumber, srcStoreLatitude, srcStoreLongitude,
                                    storeNumber, storeLatitude, storeLongitude, sortedStoreListWithoutFilter, storeFloor);
                            arrayOfVertices = dijkstraObjectForFF.algorithmImplementation();
                            break;

                        case "SF":
                            DijkstraClassForSF dijkstraObjectForSF = new DijkstraClassForSF(srcStoreNumber, srcStoreLatitude, srcStoreLongitude,
                                    storeNumber, storeLatitude, storeLongitude, sortedStoreListWithoutFilter, storeFloor);
                            arrayOfVertices = dijkstraObjectForSF.algorithmImplementation();
                            break;
                        case "TF":
                            DijkstraClassForTF dijkstraObjectForTF = new DijkstraClassForTF(srcStoreNumber, srcStoreLatitude, srcStoreLongitude,
                                    storeNumber, storeLatitude, storeLongitude, sortedStoreListWithoutFilter, storeFloor);
                            arrayOfVertices = dijkstraObjectForTF.algorithmImplementation();
                            break;
                        case "UGF":
                            DijkstraClassForUGF dijkstraObjectForUGF = new DijkstraClassForUGF(srcStoreNumber, srcStoreLatitude, srcStoreLongitude,
                                    storeNumber, storeLatitude, storeLongitude, sortedStoreListWithoutFilter, storeFloor);
                            arrayOfVertices = dijkstraObjectForUGF.algorithmImplementation();
                            break;
                        case "LGF":
                            DijkstraClassForLGF dijkstraObjectForLGF = new DijkstraClassForLGF(srcStoreNumber, srcStoreLatitude, srcStoreLongitude,
                                    storeNumber, storeLatitude, storeLongitude, sortedStoreListWithoutFilter, storeFloor);
                            arrayOfVertices = dijkstraObjectForLGF.algorithmImplementation();
                            break;
                        case "B1":
                            DijkstraClassForLGF dijkstraObjectForBOne = new DijkstraClassForLGF(srcStoreNumber, srcStoreLatitude, srcStoreLongitude,
                                    storeNumber, storeLatitude, storeLongitude, sortedStoreListWithoutFilter, storeFloor);
                            arrayOfVertices = dijkstraObjectForBOne.algorithmImplementation();
                            break;
                        case "B2":
                            DijkstraClassForBTwo dijkstraObjectForBTwo = new DijkstraClassForBTwo(srcStoreNumber, srcStoreLatitude, srcStoreLongitude,
                                    storeNumber, storeLatitude, storeLongitude, sortedStoreListWithoutFilter, storeFloor);
                            arrayOfVertices = dijkstraObjectForBTwo.algorithmImplementation();
                            break;
                        case "B3":
                            DijkstraClassForBThree dijkstraObjectForBThree = new DijkstraClassForBThree(srcStoreNumber, srcStoreLatitude, srcStoreLongitude,
                                    storeNumber, storeLatitude, storeLongitude, sortedStoreListWithoutFilter, storeFloor);
                            arrayOfVertices = dijkstraObjectForBThree.algorithmImplementation();
                            break;
                    }

                    String dPath = "";
                    for(int i=0; i< arrayOfVertices.size(); i++){
                        if(i == 0){
                            dPath = dPath+srcStoreLatitude+"*"+srcStoreLongitude+"$";
                        }else if(i == (arrayOfVertices.size()-1)){
                            dPath = dPath+storeLatitude+"*"+storeLongitude+"$";
                        }else{
                            Location loc = nodeHash.get(arrayOfVertices.get(i));
                            dPath = dPath + String.valueOf(loc.getLatitude()) + "*"+ String.valueOf(loc.getLongitude()) + "$";
                        }
                    }

                    GlobalState.drawPath = dPath;

                    if(arrayOfVertices.size() > 0){

                        GlobalState.soloMap = storeFloor;
                        GlobalState.firstMap = "NA";
                        GlobalState.secondMap = "NA";

                        maptitleSolo.setText(fullFloorNameDest);
                        destStoreSolo.setText(storeName);
                        srcStoreSolo.setText(srcStoreName);
                        imageFloorSoloFrom.setImageResource(R.mipmap.whereami_green);
                        imageFloorSoloTo.setImageResource(R.mipmap.stores);
                        imageArrowTo.setImageResource(R.mipmap.rightarrow);

                        webview_solo_map.loadUrl("file:///android_asset/samples/" + currentFloor + "?param=" + sourcePoint + "*" + destinationPoint + "&navigate=true" + "&floor=null");
                    }

                }
            }else{
                fab_textNav.setVisibility(View.GONE);
                AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
                builder.setMessage("Not enough data to navigate to "+storeName);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        webview_solo_map.loadUrl(url);
                    }
                });
                builder.show();
            }
        }
    }

    private void drawRouteForDual(String sourcPoint,  String destinationPoint,
                                  String srcStoreFloor, String storeFloor, Location srcLocation){

        webview_solo_map.setVisibility(View.GONE);
        mPager.setVisibility(View.VISIBLE);
        card_switchPage_layout.setVisibility(View.VISIBLE);

        cardView_previousmap.setEnabled(false);
        txt_previousmap.setTextColor(getResources().getColor(R.color.iron));
        cardView_nextmap.setEnabled(true);
        txt_nextmap.setTextColor(getResources().getColor(R.color.white));

        layoutTitleFloor.setVisibility(View.GONE);

        String ee_storeNumber = "";
        String _ee_storeNumber = "";

        String currentFloorSrc = "";
        String fullFloorName = "";

        switch (srcStoreFloor){
            case "LGF" :
                fullFloorName = "Lower Ground Floor";
                currentFloorSrc = "LowerGroundFloorMap.html";
                break;
            case "UGF" :
                fullFloorName = "Upper Ground Floor";
                currentFloorSrc = "UpperGroundFloorMap.html";
                break;
            case "FF"  :
                fullFloorName = "First Floor";
                currentFloorSrc = "FirstFloorMap.html";
                break;
            case "SF"  :
                fullFloorName = "Second Floor";
                currentFloorSrc = "SecondFloorMap.html";
                break;
            case "TF"  :
                fullFloorName = "Third Floor";
                currentFloorSrc = "ThirdFloorMap.html";
                break;
            case "B1"  :
                fullFloorName = "Basement One";
                currentFloorSrc = "LowerGroundFloorMap.html";
                break;
            case "B2"  :
                fullFloorName = "Basement Two";
                currentFloorSrc = "BasementTwoFloorMap.html";
                break;
            case "B3"  :
                fullFloorName = "Basement Three";
                currentFloorSrc = "BasementThreeFloorMap.html";
                break;
        }


        mPager.setVisibility(View.VISIBLE);
        card_switchPage_layout.setVisibility(View.VISIBLE);
        webview_solo_map.setVisibility(View.GONE);
        fab_textNav.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                tipWindow = new TooltipWindow(LocateStoreActivity.this);
                tipWindow.showToolTip(fab_textNav, getString(R.string.tooltip_textual));
            }
        }, 500);

        if(srcStoreFloor.equals("FF") || (srcStoreFloor.equals("SF"))){
            if(storeFloor.equals("FF") || (storeFloor.equals("SF"))){
                nearestToS = getNearesLocation(srcLocation, threeDest);
                ee_storeNumber = nearestToS[0];
            }else if(storeFloor.equals("TF")){
                nearestToS = getNearesLocation(srcLocation, twoDest);
                ee_storeNumber = nearestToS[0];
            }else{
                ee_storeNumber = "LT120";
            }
        }else if(srcStoreFloor.equals("TF")){
            if(storeFloor.equals("FF") || (storeFloor.equals("SF"))){
                nearestToS = getNearesLocation(srcLocation, twoDest);
                ee_storeNumber = nearestToS[0];
            }else{
                ee_storeNumber = "LT120";
            }
        }else if(srcStoreFloor.equals("LGF") || srcStoreFloor.equals("UGF")
                || srcStoreFloor.equals("B1") || srcStoreFloor.equals("B2")
                || srcStoreFloor.equals("B3")){
            ee_storeNumber = "LT120";
        }

        Location liftFFL = new Location("LT120FF");
        liftFFL.setLatitude(13.058672);
        liftFFL.setLongitude(80.263101);
        leftLiftHash.put("LT120FF", liftFFL);

        Location liftSFL = new Location("LT120SF");
        liftSFL.setLatitude(13.058664);
        liftSFL.setLongitude(80.2631);
        leftLiftHash.put("LT120SF", liftSFL);

        Location liftTFL = new Location("LT120TF");
        liftTFL.setLatitude(13.058654);
        liftTFL.setLongitude(80.26314);
        leftLiftHash.put("LT120TF", liftTFL);

        Location liftLGFL = new Location("LT120LGF");
        liftLGFL.setLatitude(13.058668);
        liftLGFL.setLongitude(80.263316);
        leftLiftHash.put("LT120LGF", liftLGFL);

        Location liftUGFL = new Location("LT120UGF");
        liftUGFL.setLatitude(13.058646);
        liftUGFL.setLongitude(80.263265);
        leftLiftHash.put("LT120UGF", liftUGFL);

        /*Location liftB1L = new Location("LT120B1");
        liftB1L.setLatitude(13.058668);
        liftB1L.setLongitude(80.263343);
        leftLiftHash.put("LT120B1", liftB1L);*/

        Location liftB1L = new Location("LT120B1");
        liftB1L.setLatitude(13.058764);
        liftB1L.setLongitude(80.26464);
        liftHash.put("LT120B1", liftB1L);

        /*Location liftB2L = new Location("LT120B2");
        liftB2L.setLatitude(13.058668);
        liftB2L.setLongitude(80.263343);
        leftLiftHash.put("LT120B2", liftB2L);*/

        Location liftB2L = new Location("LT120B2");
        liftB2L.setLatitude(13.058764);
        liftB2L.setLongitude(80.26464);
        leftLiftHash.put("LT120B2", liftB2L);

        /*Location liftB3L = new Location("LT120B3");
        liftB3L.setLatitude(13.058668);
        liftB3L.setLongitude(80.263343);
        leftLiftHash.put("LT120B3", liftB3L);*/

        Location liftB3L = new Location("LT120B3");
        liftB3L.setLatitude(13.058764);
        liftB3L.setLongitude(80.26464);
        liftHash.put("LT120B3", liftB3L);

        _ee_storeNumber = ee_storeNumber;
        Location entryExitLoc = null;
        if(ee_storeNumber.equals("LT120")){
             ee_storeNumber = ee_storeNumber + srcStoreFloor;
             entryExitLoc = liftHash.get(ee_storeNumber);
        }else{
             entryExitLoc = nodeHash.get(ee_storeNumber);
        }
        String ee_lat =  String.valueOf(entryExitLoc.getLatitude());
        String ee_lon =  String.valueOf(entryExitLoc.getLongitude());

        if(!srcStoreFloor.equals(storeFloor)){
            if(!srcStoreNumber.equals("NA") && !srcStoreLatitude.equals("NA") && !srcStoreLongitude.equals("NA") &&
                    !storeNumber.equals("NA") && !storeLatitude.equals("NA") && !storeLongitude.equals("NA")){

                card_switchPage_layout.setVisibility(View.VISIBLE);

                switch (srcStoreFloor){
                    case "FF":
                        DijkstraClassForFF dijkstraObjectForFF = new DijkstraClassForFF(srcStoreNumber, srcStoreLatitude, srcStoreLongitude,
                                ee_storeNumber, ee_lat, ee_lon, sortedStoreListWithoutFilter, srcStoreFloor);
                        arrayOfVertices = dijkstraObjectForFF.algorithmImplementation();
                        break;

                    case "SF":
                        DijkstraClassForSF dijkstraObjectForSF = new DijkstraClassForSF(srcStoreNumber, srcStoreLatitude, srcStoreLongitude,
                                ee_storeNumber, ee_lat, ee_lon, sortedStoreListWithoutFilter, srcStoreFloor);
                        arrayOfVertices = dijkstraObjectForSF.algorithmImplementation();
                        break;
                    case "TF":
                        DijkstraClassForTF dijkstraObjectForTF = new DijkstraClassForTF(srcStoreNumber, srcStoreLatitude, srcStoreLongitude,
                                ee_storeNumber, ee_lat, ee_lon, sortedStoreListWithoutFilter, srcStoreFloor);
                        arrayOfVertices = dijkstraObjectForTF.algorithmImplementation();
                        break;
                    case "UGF":
                        DijkstraClassForUGF dijkstraObjectForUGF = new DijkstraClassForUGF(srcStoreNumber, srcStoreLatitude, srcStoreLongitude,
                                ee_storeNumber, ee_lat, ee_lon, sortedStoreListWithoutFilter, srcStoreFloor);
                        arrayOfVertices = dijkstraObjectForUGF.algorithmImplementation();
                        break;
                    case "LGF":
                        DijkstraClassForLGF dijkstraObjectForLGF = new DijkstraClassForLGF(srcStoreNumber, srcStoreLatitude, srcStoreLongitude,
                                ee_storeNumber, ee_lat, ee_lon, sortedStoreListWithoutFilter, srcStoreFloor);
                        arrayOfVertices = dijkstraObjectForLGF.algorithmImplementation();
                        break;
                    case "B1":
                        DijkstraClassForBOne dijkstraObjectForBOne = new DijkstraClassForBOne(srcStoreNumber, srcStoreLatitude, srcStoreLongitude,
                                ee_storeNumber, ee_lat, ee_lon, sortedStoreListWithoutFilter, srcStoreFloor);
                        arrayOfVertices = dijkstraObjectForBOne.algorithmImplementation();
                        break;
                    case "B2":
                        DijkstraClassForBTwo dijkstraObjectForBTwo = new DijkstraClassForBTwo(srcStoreNumber, srcStoreLatitude, srcStoreLongitude,
                                ee_storeNumber, ee_lat, ee_lon, sortedStoreListWithoutFilter, srcStoreFloor);
                        arrayOfVertices = dijkstraObjectForBTwo.algorithmImplementation();
                        break;
                    case "B3":
                        DijkstraClassForBThree dijkstraObjectForBThree = new DijkstraClassForBThree(srcStoreNumber, srcStoreLatitude, srcStoreLongitude,
                                ee_storeNumber, ee_lat, ee_lon, sortedStoreListWithoutFilter, srcStoreFloor);
                        arrayOfVertices = dijkstraObjectForBThree.algorithmImplementation();
                        break;
                }

                String dPath = "";
                for(int i=0; i< arrayOfVertices.size(); i++){
                    if(i == 0){
                        dPath = dPath+srcStoreLatitude+"*"+srcStoreLongitude+"$";
                    }else if(i == (arrayOfVertices.size()-1)){
                        dPath = dPath+ee_lat+"*"+ee_lon+"$";
                    }else{
                        Location loc = nodeHash.get(arrayOfVertices.get(i));
                        dPath = dPath + String.valueOf(loc.getLatitude()) + "*"+ String.valueOf(loc.getLongitude()) + "$";
                    }
                }

                sourcPoint = srcStoreLatitude+"*"+srcStoreLongitude+"*"+srcStoreName;
                GlobalState._source = sourcPoint;
                GlobalState._drawPath = dPath;
                String ee_destinationPoint = ee_lat+"*"+ee_lon+"*"+ee_storeNumber;
                GlobalState._destination = destinationPoint;
                GlobalState.mapTitle = fullFloorName;
                GlobalState.sourceStore = srcStoreName;
                if(ee_storeNumber.equals("LT120FF") || ee_storeNumber.equals("LT120SF") || ee_storeNumber.equals("LT120TF")
                        || ee_storeNumber.equals("LT120LGF") || ee_storeNumber.equals("LT120UGF")|| ee_storeNumber.equals("LT120B1")
                        || ee_storeNumber.equals("LT120B2")|| ee_storeNumber.equals("LT120B3")){
                    GlobalState.destStore = "Lift";
                }else if(ee_storeNumber.equals("ESC-Left") || ee_storeNumber.equals("ESC-Right")){
                    GlobalState.destStore = "Escalator";
                }

                if(arrayOfVertices.size() > 0){
                    GlobalState.soloMap = "NA";
                    GlobalState.firstMap = srcStoreFloor;
                    GlobalState.secondMap = storeFloor;

                    GlobalState.liftSource = "file:///android_asset/samples/" + currentFloorSrc + "?param=" + sourcPoint+"*"+ee_destinationPoint + "&navigate=true"+ "&floor=src";
                }

                if(_ee_storeNumber.equals("LT120")){
                    _ee_storeNumber = _ee_storeNumber + storeFloor;
                    entryExitLoc = liftHash.get(_ee_storeNumber);
                }else{
                    entryExitLoc = nodeHash.get(_ee_storeNumber);
                }
                ee_lat =  String.valueOf(entryExitLoc.getLatitude());
                ee_lon =  String.valueOf(entryExitLoc.getLongitude());

                switch(storeFloor){

                    case "FF":
                        DijkstraClassForFF dijkstraObjectForFF = new DijkstraClassForFF(ee_storeNumber, ee_lat, ee_lon,
                                storeNumber, storeLatitude, storeLongitude, sortedStoreListWithoutFilter,storeFloor);
                        arrayOfVertices = dijkstraObjectForFF.algorithmImplementation();
                        break;

                    case "SF":
                        DijkstraClassForSF dijkstraObjectForSF = new DijkstraClassForSF(ee_storeNumber, ee_lat, ee_lon,
                                storeNumber, storeLatitude, storeLongitude, sortedStoreListWithoutFilter, storeFloor);
                        arrayOfVertices = dijkstraObjectForSF.algorithmImplementation();
                        break;
                    case "TF":
                        DijkstraClassForTF dijkstraObjectForTF = new DijkstraClassForTF(ee_storeNumber, ee_lat, ee_lon,
                                storeNumber, storeLatitude, storeLongitude, sortedStoreListWithoutFilter, storeFloor);
                        arrayOfVertices = dijkstraObjectForTF.algorithmImplementation();
                        break;
                    case "UGF":
                        DijkstraClassForUGF dijkstraObjectForUGF = new DijkstraClassForUGF(ee_storeNumber, ee_lat, ee_lon,
                                storeNumber, storeLatitude, storeLongitude, sortedStoreListWithoutFilter, storeFloor);
                        arrayOfVertices = dijkstraObjectForUGF.algorithmImplementation();
                        break;
                    case "LGF":
                        DijkstraClassForLGF dijkstraObjectForLGF = new DijkstraClassForLGF(ee_storeNumber, ee_lat, ee_lon,
                                storeNumber, storeLatitude, storeLongitude, sortedStoreListWithoutFilter, storeFloor);
                        arrayOfVertices = dijkstraObjectForLGF.algorithmImplementation();
                        break;
                    case "B1":
                        DijkstraClassForBOne dijkstraObjectForBOne = new DijkstraClassForBOne(srcStoreNumber, srcStoreLatitude, srcStoreLongitude,
                                storeNumber, storeLatitude, storeLongitude, sortedStoreListWithoutFilter, storeFloor);
                        arrayOfVertices = dijkstraObjectForBOne.algorithmImplementation();
                        break;
                    case "B2":
                        DijkstraClassForBTwo dijkstraObjectForBTwo = new DijkstraClassForBTwo(srcStoreNumber, srcStoreLatitude, srcStoreLongitude,
                                storeNumber, storeLatitude, storeLongitude, sortedStoreListWithoutFilter, storeFloor);
                        arrayOfVertices = dijkstraObjectForBTwo.algorithmImplementation();
                        break;
                    case "B3":
                        DijkstraClassForBThree dijkstraObjectForBThree = new DijkstraClassForBThree(srcStoreNumber, srcStoreLatitude, srcStoreLongitude,
                                storeNumber, storeLatitude, storeLongitude, sortedStoreListWithoutFilter, storeFloor);
                        arrayOfVertices = dijkstraObjectForBThree.algorithmImplementation();
                        break;
                }

                dPath = "";
                for(int i=0; i< arrayOfVertices.size(); i++){
                    if(i == 0){
                        dPath = dPath+ee_lat+"*"+ee_lon+"$";
                    }else if(i == (arrayOfVertices.size()-1)){
                        dPath = dPath+storeLatitude+"*"+storeLongitude+"$";
                    }else{
                        Location loc = nodeHash.get(arrayOfVertices.get(i));
                        dPath = dPath + String.valueOf(loc.getLatitude()) + "*"+ String.valueOf(loc.getLongitude()) + "$";
                    }
                }

                String ee_sourcePoint = ee_lat+"*"+ee_lon+"*"+ee_storeNumber;
                GlobalState.source = ee_sourcePoint;
                GlobalState.drawPath = dPath;
                GlobalState._mapTitle = fullFloorNameDest;
                if(ee_storeNumber.equals("LT120FF") || ee_storeNumber.equals("LT120SF") || ee_storeNumber.equals("LT120TF")
                        || ee_storeNumber.equals("LT120LGF") || ee_storeNumber.equals("LT120UGF")|| ee_storeNumber.equals("LT120B1")
                        || ee_storeNumber.equals("LT120B2")|| ee_storeNumber.equals("LT120B3")){
                    GlobalState._sourceStore = "Lift";
                }else if(ee_storeNumber.equals("ESC-Left") || ee_storeNumber.equals("ESC-Right")){
                    GlobalState._sourceStore = "Escalator";
                }
                GlobalState._destStore = storeName;

                if(arrayOfVertices.size() > 0){

                    GlobalState.liftDest = "file:///android_asset/samples/" + currentFloor + "?param=" + ee_sourcePoint+"*"+destinationPoint + "&navigate=true"+"&floor=dst";
                }

            }else{
                fab_textNav.setVisibility(View.GONE);
                card_switchPage_layout.setVisibility(View.GONE);
                AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
                builder.setMessage("Not enough data to navigate to "+storeName);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mPager.setVisibility(View.GONE);
                        webview_solo_map.setVisibility(View.VISIBLE);
                        webview_solo_map.loadUrl(url);
                    }
                });
                builder.show();
            }

        }

        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        mPagerAdapter = new WebViewSlidePagerAdapter(fm);
        mPager.setAdapter(mPagerAdapter);

    }

    private ProgressDialog viewProgressDialog(String message){
        final ProgressDialog progressDialog = new ProgressDialog(this, R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(message);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    private void viewOptionsDialog(){

        final Dialog optionsDialog = new Dialog(this);
        optionsDialog.getWindow().getAttributes().windowAnimations = R.style.ScreenshotDialogAnimation;
        optionsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        optionsDialog.setContentView(R.layout.optionsview);
        optionsDialog.setCancelable(true);

        Window window = optionsDialog.getWindow();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.CENTER ;
        window.setAttributes(lp);

        Drawable d = new ColorDrawable(getResources().getColor(R.color.lightBackground));
        window.setBackgroundDrawable(d);
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        optionsDialog.show();

        optionsDialog.findViewById(R.id.qrview).startAnimation(animBounce);
        optionsDialog.findViewById(R.id.manualview).startAnimation(animBounce);

        Button btn_close = (Button)optionsDialog.findViewById(R.id.btnoptioncancel);
        FrameLayout frame_qr = (FrameLayout)optionsDialog.findViewById(R.id.qrframe);
        FrameLayout frame_manual = (FrameLayout)optionsDialog.findViewById(R.id.manualframe);

        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                optionsDialog.dismiss();
            }
        });

        frame_qr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewQrDialog();
                optionsDialog.dismiss();
            }
        });

        frame_manual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                storesInputDialog();
                optionsDialog.dismiss();

            }
        });
    }

    private void storesInputDialog(){

        final Dialog storesInputDialog = new Dialog(this);
        storesInputDialog.getWindow().getAttributes().windowAnimations = R.style.ScreenshotDialogAnimation;
        storesInputDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        storesInputDialog.setContentView(R.layout.manualview);
        storesInputDialog.setCancelable(true);

        Window window = storesInputDialog.getWindow();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.CENTER ;
        window.setAttributes(lp);

        Drawable d = new ColorDrawable(getResources().getColor(R.color.lightBackground));
        window.setBackgroundDrawable(d);
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        storesInputDialog.show();

        Button btn_cancel = (Button) storesInputDialog.findViewById(R.id.btnmaualcancel);

        floorSpinner = (Spinner) storesInputDialog.findViewById(R.id.floorspinner);
        storeSpinner = (Spinner) storesInputDialog.findViewById(R.id.storespinner);
        storeSpinner.setEnabled(false);

        card_selectedStore = (CardView)storesInputDialog.findViewById(R.id.card_selectedStore);
        img_logo_manual = (ImageView)storesInputDialog.findViewById(R.id.store_logo_manual);
        txt_storename_manual = (TextView)storesInputDialog.findViewById(R.id.store_name_manual);
        txt_storecategory_manual = (TextView)storesInputDialog.findViewById(R.id.store_category_manual);
        txt_storefloor_manual = (TextView)storesInputDialog.findViewById(R.id.store_floor_manual);
        btn_takeme_manual = (Button)storesInputDialog.findViewById(R.id.btnmaualtakeme);

        storeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView)v.findViewById(android.R.id.text1)).setText("");
                    ((TextView)v.findViewById(android.R.id.text1)).setHint(getItem(getCount()));
                }
                return v;
            }
            @Override
            public int getCount() {
                return super.getCount()-1;
            }
        };

        final ArrayAdapter<String> floorAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);

                if (position == getCount()) {
                    ((TextView)v.findViewById(android.R.id.text1)).setText("");
                    ((TextView)v.findViewById(android.R.id.text1)).setHint(getItem(getCount()));
                }
                return v;
            }
            @Override
            public int getCount() {
                return super.getCount()-1;
            }
        };

        floorAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        floorAdapter.add("Lower Ground Floor");
        floorAdapter.add("Upper Ground Floor");
        floorAdapter.add("First Floor");
        floorAdapter.add("Second Floor");
        floorAdapter.add("Third Floor");
        floorAdapter.add("--Select floor--");
        floorSpinner.setAdapter(floorAdapter);
        floorSpinner.setSelection(floorAdapter.getCount());

        floorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                String floor = ((TextView) view).getText().toString().trim();
                if (!floor.equals("")) {
                    card_selectedStore.setVisibility(View.INVISIBLE);
                    btn_takeme_manual.setVisibility(View.INVISIBLE);
                    getStoresofSelectedFloor(floor);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        storeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String storeName = ((TextView) view).getText().toString().trim();
                if(!storeName.equals("--Select store--")){

                    storeManualSelected = spinnerSelectedStore.get(position - 1);

                    card_selectedStore.setVisibility(View.VISIBLE);
                    btn_takeme_manual.setVisibility(View.VISIBLE);

                    Bitmap storeLogoBitmap = null;
                    storeLogoBitmap = loadImageFromStorage(storeManualSelected.getStoreName());
                    if(storeLogoBitmap != null){
                        img_logo_manual.setImageBitmap(storeLogoBitmap);
                    } else {
                        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.ea_logo_whitebg);
                        img_logo_manual.setImageBitmap(icon);
                    }

                    txt_storename_manual.setText(storeManualSelected.getStoreName());
                    txt_storecategory_manual.setText(storeManualSelected.getStoreCategory());

                    String floorName = storeManualSelected.getStoreFloor();
                    switch (floorName){
                        case "LGF" : floorName = "Lower Ground Floor";break;
                        case "UGF" : floorName = "Upper Ground Floor";break;
                        case "FF"  : floorName = "First Floor";break;
                        case "SF"  : floorName = "Second Floor";break;
                        case "TF"  : floorName = "Third Floor";break;
                    }
                    txt_storefloor_manual.setText(floorName);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                storesInputDialog.dismiss();
            }
        });

        btn_takeme_manual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                srcStoreLatitude = storeManualSelected.getStoreLatitude();
                srcStoreLongitude = storeManualSelected.getStoreLongitude();
                srcStoreName = storeManualSelected.getStoreName();
                srcStoreFloor = storeManualSelected.getStoreFloor();
                srcStoreNumber = storeManualSelected.getStoreNumber();

                sourcePoint = srcStoreLatitude + "*"+srcStoreLongitude+"*"+srcStoreName;
                GlobalState.source = sourcePoint;

                if(storeFloor.equals(srcStoreFloor)  || (srcStoreFloor.equals("LGF") && storeFloor.equals("B1") ) || (srcStoreFloor.equals("B1") && storeFloor.equals("LGF") ) ){
                    drawRouteForSolo(sourcePoint, destinationPoint);
                }else{
                    if(srcStoreLatitude.equals("NA") || srcStoreLongitude.equals("NA")){
                        Toast.makeText(getApplicationContext(), "Not enough data to route!", Toast.LENGTH_SHORT).show();
                    }else {
                        Location srcLocation = new Location(srcStoreName);
                        srcLocation.setLatitude(Double.parseDouble(srcStoreLatitude));
                        srcLocation.setLongitude(Double.parseDouble(srcStoreLongitude));
                        drawRouteForDual(sourcePoint, destinationPoint, srcStoreFloor, storeFloor, srcLocation);
                    }
                }
                storesInputDialog.dismiss();
            }
        });

    }

    public void getStoresofSelectedFloor(String floor){

        if(!spinnerSelectedStore.isEmpty())
            spinnerSelectedStore.clear();

        switch (floor){
            case "Lower Ground Floor" : floorName = "LGF";break;
            case "Upper Ground Floor" : floorName = "UGF";break;
            case "First Floor"  : floorName = "FF";break;
            case "Second Floor"  : floorName = "SF";break;
            case "Third Floor"  : floorName = "TF";break;
        }

        storeAdapter.clear();
        storeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        storeAdapter.add("--Select store--");
        for(Store store : sortedStoreList){
            if (store.getStoreFloor().equals(floorName)) {
                storeAdapter.add(store.getStoreName());
                spinnerSelectedStore.add(store);
            }
        }
        storeSpinner.setAdapter(storeAdapter);
        storeSpinner.setSelection(0);
        storeSpinner.setEnabled(true);
    }
    private void viewQrDialog(){

        qrDialog = new Dialog(this);
        qrDialog.getWindow().getAttributes().windowAnimations = R.style.ScreenshotDialogAnimation;
        qrDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        qrDialog.setContentView(R.layout.qrview);
        qrDialog.setCancelable(true);

        Window window = qrDialog.getWindow();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        lp.gravity = Gravity.CENTER ;
        window.setAttributes(lp);

        Drawable d = new ColorDrawable(getResources().getColor(R.color.lightBackground));
        window.setBackgroundDrawable(d);
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        qrDialog.show();

        myDecoderView = (QRCodeReaderView) qrDialog.findViewById(R.id.qrdecoderview);
        myDecoderView.setOnQRCodeReadListener(this);
        myDecoderView.getCameraManager().startPreview();

        Button btn_close = (Button)qrDialog.findViewById(R.id.btnqrcancel);
        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qrDialog.dismiss();
            }
        });

        qrDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                myDecoderView.getCameraManager().stopPreview();
            }
        });
    }

    private void getStores(){

        storeArrayList = new ArrayList<Store>();
        storeArrayList.clear();
        sortedStoreList = new ArrayList<Store>(storeArrayList);

        ParseQuery<ParseObject> query = ParseQuery.getQuery(getResources().getString(R.string.store_class_name));
        query.fromLocalDatastore();
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> storesListFromLocal, ParseException e) {
                if (e == null) {
                    for (ParseObject store : storesListFromLocal) {

                        String storeName = "";
                        String storeCategory = "";
                        String storeFloor = "";
                        String storeLatitude = "";
                        String storeLongitude = "";
                        String storeLogo = "";
                        String storeDesc = "";
                        String storeNumber = "";

                        boolean storeStatus = false;
                        boolean isStoreDeleted = true;

                        if (store.has("sFloor")) {
                            if(!store.getString("sCategory").equals("LIFT") && !store.getString("sCategory").equals("GENERAL")){

                                if(store.has("deleted"))
                                    isStoreDeleted = store.getBoolean("deleted");
                                else
                                    isStoreDeleted = true;

                                if (store.has("sStatus"))
                                    storeStatus = store.getBoolean("sStatus");
                                else
                                    storeStatus = false;

                                if (store.has("sName"))
                                    storeName = store.getString("sName");
                                else
                                    storeName = "NA";

                                if (store.has("sCategory"))
                                    storeCategory = store.getString("sCategory");
                                else
                                    storeCategory = "NA";

                                if (store.has("sFloor"))
                                    storeFloor = store.getString("sFloor");
                                else
                                    storeFloor = "NA";

                                if (store.has("sLatitude"))
                                    storeLatitude = store.getString("sLatitude");
                                else
                                    storeLatitude = "NA";

                                if (store.has("sLongitude"))
                                    storeLongitude = store.getString("sLongitude");
                                else
                                    storeLongitude = "NA";

                                if (store.has("sLogoName")) {
                                    storeLogo = store.getString("sLogoName");
                                } else
                                    storeLogo = "NA";

                                if (store.has("sDesc"))
                                    storeDesc = store.getString("sDesc");
                                else
                                    storeDesc = "NA";

                                if (store.has("sNumber"))
                                    storeNumber = store.getString("sNumber");
                                else
                                    storeNumber = "NA";

                                if(storeStatus == true && isStoreDeleted==false){
                                    storeArrayList.add(new Store(storeName, storeCategory, storeFloor, storeLatitude,
                                            storeLongitude, storeLogo, storeDesc, storeNumber, storeStatus, isStoreDeleted));
                                }
                            }

                        }
                    }
                    Collections.sort(storeArrayList, new CustomComparator());
                    sortedStoreList.clear();
                    sortedStoreList.addAll(storeArrayList);

                } else {

                }
            }
        });
    }
    public class CustomComparator implements Comparator<Store> {
        @Override
        public int compare(Store o1, Store o2) {
            return o1.getStoreName().compareTo(o2.getStoreName());
        }
    }

    private void getStoreWithoutFilter(){
        storeArrayListWithoutFilter = new ArrayList<Store>();
        storeArrayListWithoutFilter.clear();
        sortedStoreListWithoutFilter = new ArrayList<Store>(storeArrayListWithoutFilter);

        ParseQuery<ParseObject> query = ParseQuery.getQuery(getResources().getString(R.string.store_class_name));
        query.fromLocalDatastore();
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> storesListFromLocal, ParseException e) {
                if (e == null) {
                    for (ParseObject store : storesListFromLocal) {

                        String storeName = "";
                        String storeCategory = "";
                        String storeFloor = "";
                        String storeLatitude = "";
                        String storeLongitude = "";
                        String storeLogo = "";
                        String storeDesc = "";
                        String storeNumber = "";

                        boolean storeStatus = false;
                        boolean isStoreDeleted = true;

                        if (store.has("sFloor")) {

                                if(store.has("deleted"))
                                    isStoreDeleted = store.getBoolean("deleted");
                                else
                                    isStoreDeleted = true;

                                if (store.has("sStatus"))
                                    storeStatus = store.getBoolean("sStatus");
                                else
                                    storeStatus = false;

                                if (store.has("sName"))
                                    storeName = store.getString("sName");
                                else
                                    storeName = "NA";

                                if (store.has("sCategory"))
                                    storeCategory = store.getString("sCategory");
                                else
                                    storeCategory = "NA";

                                if (store.has("sFloor"))
                                    storeFloor = store.getString("sFloor");
                                else
                                    storeFloor = "NA";

                                if (store.has("sLatitude"))
                                    storeLatitude = store.getString("sLatitude");
                                else
                                    storeLatitude = "NA";

                                if (store.has("sLongitude"))
                                    storeLongitude = store.getString("sLongitude");
                                else
                                    storeLongitude = "NA";

                                if (store.has("sLogoName")) {
                                    storeLogo = store.getString("sLogoName");
                                } else
                                    storeLogo = "NA";

                                if (store.has("sDesc"))
                                    storeDesc = store.getString("sDesc");
                                else
                                    storeDesc = "NA";

                                if (store.has("sNumber"))
                                    storeNumber = store.getString("sNumber");
                                else
                                    storeNumber = "NA";

                                if(storeStatus == true && isStoreDeleted==false){
                                    storeArrayListWithoutFilter.add(new Store(storeName, storeCategory, storeFloor, storeLatitude,
                                            storeLongitude, storeLogo, storeDesc, storeNumber, storeStatus, isStoreDeleted));
                                }

                        }
                    }
                    Collections.sort(storeArrayListWithoutFilter, new CustomComparator());
                    sortedStoreListWithoutFilter.clear();
                    sortedStoreListWithoutFilter.addAll(storeArrayListWithoutFilter);

                } else {

                }
            }
        });
    }

    public String[] getNearesLocation(Location srcLoc, Hashtable<Location, String> srcHash){

        String nearestStore[] = new String[2];
        ArrayList<Double> distValues = new ArrayList<Double>();
        Hashtable<Double, Location> distHash = new Hashtable<Double, Location>();
        Set<Location> locSet = srcHash.keySet();
        for(Location location : locSet){

            double dist = location.distanceTo(srcLoc);
            distValues.add(dist);
            distHash.put(dist,location);
        }

        double smallest = Double.MAX_VALUE;
        for(int i =0;i<distValues.size();i++) {
            if(smallest > distValues.get(i)) {
                smallest = distValues.get(i);
            }
        }

        Location nearLoc = distHash.get(smallest);
        nearestStore[0] = srcHash.get(nearLoc);
        nearestStore[1] = String.valueOf(smallest);

        return nearestStore;
    }

    public void setNodeHashForFF(){
        setNodeHash(13.058669,80.263132, "cff1");
        setNodeHash(13.058622,80.263178, "cff2");
        setNodeHash(13.058613,80.263268, "cff3");
        setNodeHash(13.058643,80.263355, "cff4");
        setNodeHash(13.058718,80.263353, "cff5");
        setNodeHash(13.05873,80.263175, "cff6");
        setNodeHash(13.058752,80.263255, "cff7");
        setNodeHash(13.058645,80.263763, "cff8");
        setNodeHash(13.058714,80.263767, "cff9");
        setNodeHash(13.058566,80.263759, "cff10");
        setNodeHash(13.058839,80.263759, "cff11");
        setNodeHash(13.058643,80.263797, "cff12");
        setNodeHash(13.05871,80.263808, "cff13");
        setNodeHash(13.058593,80.263936, "cff14");
        setNodeHash(13.058764,80.26393, "cff15");
        setNodeHash(13.058589,80.264088, "cff16");
        setNodeHash(13.058767,80.26409, "cff17");
        setNodeHash(13.058644,80.26422, "cff18");
        setNodeHash(13.058713,80.264219, "cff19");
        setNodeHash(13.058644,80.264248, "cff20");
        setNodeHash(13.058713,80.264248, "cff21");
        setNodeHash(13.058871,80.264248, "cff22");
        setNodeHash(13.058568,80.264251, "cff23");
        setNodeHash(13.058644,80.264522, "cff24");
        setNodeHash(13.058713,80.264523, "cff25");
        setNodeHash(13.058713,80.264553, "cff26");
        setNodeHash(13.058644,80.264582, "cff27");
        setNodeHash(13.05857,80.264582, "cff28");
        setNodeHash(13.05884,80.264554, "cff29");
        setNodeHash(13.058644,80.264712, "cff30");
        setNodeHash(13.058713,80.264712, "cff31");
        setNodeHash(13.058488,80.263948, "cff32");
        setNodeHash(13.058489,80.264063, "cff33");
        setNodeHash(13.058647,80.263426, "cff34");
        setNodeHash(13.058718, 80.263456, "cff35");
        setNodeHash(13.058788, 80.263456, "cff36");
        setNodeHash(13.058581, 80.263429, "cff37");
    }
    public void setNodeHashForSF(){

        setNodeHash(13.058664,80.263144, "csf1");
        setNodeHash(13.058615,80.263188, "csf2");
        setNodeHash(13.058611,80.263273, "csf3");
        setNodeHash(13.058637,80.263329, "csf4");
        setNodeHash(13.058711,80.263358, "csf5");
        setNodeHash(13.058716,80.263172, "csf6");
        setNodeHash(13.05874,80.263244, "csf7");
        setNodeHash(13.058646,80.263736, "csf8");
        setNodeHash(13.058646,80.263693, "csf9");
        setNodeHash(13.058646,80.263616, "csf10");
        setNodeHash(13.058646,80.263544, "csf11");
        setNodeHash(13.058646,80.263501, "csf12");
        setNodeHash(13.058588,80.263434, "csf13");
        setNodeHash(13.058646,80.263433, "csf14");
        setNodeHash(13.05856,80.263434, "csf15");
        setNodeHash(13.058711,80.263462, "csf16");
        setNodeHash(13.058766,80.263464, "csf17");
        setNodeHash(13.058799,80.263462, "csf18");
        setNodeHash(13.058831,80.263462, "csf19");
        setNodeHash(13.058831,80.263531, "csf20");
        setNodeHash(13.058831,80.263615, "csf21");
        setNodeHash(13.058831,80.263701, "csf22");
        setNodeHash(13.058831,80.263773, "csf23");
        setNodeHash(13.05885,80.263772, "csf24");
        setNodeHash(13.0588,80.263772, "csf25");
        setNodeHash(13.058757,80.263772, "csf26");
        setNodeHash(13.058711,80.263736, "csf27");
        setNodeHash(13.058711,80.263772, "csf28");
        setNodeHash(13.058711,80.263694, "csf29");
        setNodeHash(13.058711,80.263651, "csf30");
        setNodeHash(13.058711,80.263594, "csf31");
        setNodeHash(13.058711,80.26355, "csf32");
        setNodeHash(13.058832,80.263751, "csf33");
        setNodeHash(13.058569,80.263772, "csf34");
        setNodeHash(13.05855, 80.263773, "csf35");
        setNodeHash(13.058646, 80.263773, "csf36");
        setNodeHash(13.058646, 80.263357, "csf37");
        setNodeHash(13.058721,80.263329, "csf38");
        setNodeHash(13.058538,80.263186, "csf39");
        setNodeHash(13.058472,80.263187, "csf40");
        setNodeHash(13.058558,80.263273, "csf41");
        setNodeHash(13.058534,80.263272, "csf42");
        setNodeHash(13.058471,80.263272, "csf43");
        setNodeHash(13.058646,80.263801, "csf44");
        setNodeHash(13.058711,80.2638, "csf45");
        setNodeHash(13.05862,80.263844, "csf46");
        setNodeHash(13.058608,80.26388, "csf47");
        setNodeHash(13.058598,80.263923, "csf48");
        setNodeHash(13.058591,80.263966, "csf49");
        setNodeHash(13.058534,80.263965, "csf50");
        setNodeHash(13.058591,80.264073, "csf51");
        setNodeHash(13.058534,80.264072, "csf52");
        setNodeHash(13.058606,80.264143, "csf53");
        setNodeHash(13.058633,80.264201, "csf54");
        setNodeHash(13.058654,80.264243, "csf55");
        setNodeHash(13.058654,80.264261, "csf56");
        setNodeHash(13.058586,80.264261, "csf57");
        setNodeHash(13.058753,80.263886, "csf58");
        setNodeHash(13.058758,80.263923, "csf59");
        setNodeHash(13.058764,80.263986, "csf60");
        setNodeHash(13.058764,80.264031, "csf61");
        setNodeHash(13.058764,80.264072, "csf62");
        setNodeHash(13.058752,80.264145, "csf63");
        setNodeHash(13.058723,80.264201, "csf64");
        setNodeHash(13.05871,80.264244, "csf65");
        setNodeHash(13.058709,80.264261, "csf66");
        setNodeHash(13.058779,80.264261, "csf67");
        setNodeHash(13.058831,80.264261, "csf68");
        setNodeHash(13.058709,80.264532, "csf69");
        setNodeHash(13.058657,80.264532, "csf70");
        setNodeHash(13.058709,80.264572, "csf71");
        setNodeHash(13.058797, 80.264573, "csf72");
        setNodeHash(13.058845, 80.264572, "csf73");
        setNodeHash(13.058709, 80.264689, "csf74");
        setNodeHash(13.058646, 80.264689, "csf75");
        setNodeHash(13.058646, 80.264605, "csf76");
        setNodeHash(13.058553, 80.264605, "csf77");
        setNodeHash(13.058655, 80.264493, "csf78");
        setNodeHash(13.058655, 80.264444, "csf79");
        setNodeHash(13.058655, 80.264401, "csf80");
        setNodeHash(13.058655, 80.264344, "csf81");
        setNodeHash(13.058655, 80.264287, "csf82");
    }

    public void setNodeHashForTF(){

        setNodeHash(13.058668,80.263083, "ctf1");
        setNodeHash(13.058667,80.263125, "ctf2");
        setNodeHash(13.058667,80.263167, "ctf3");
        setNodeHash(13.05862,80.263209, "ctf4");
        setNodeHash(13.058529,80.26321, "ctf5");
        setNodeHash(13.058457,80.263209, "ctf6");
        setNodeHash(13.058612,80.263294, "ctf7");
        setNodeHash(13.058533,80.263294, "ctf8");
        setNodeHash(13.058457,80.263294, "ctf9");
        setNodeHash(13.058637,80.263358, "ctf10");
        setNodeHash(13.05869,80.263358, "ctf11");
        setNodeHash(13.058699,80.263196, "ctf12");
        setNodeHash(13.058719,80.263266, "ctf13");
        setNodeHash(13.058743,80.263267, "ctf14");
        setNodeHash(13.058743,80.263167, "ctf15");
        setNodeHash(13.058807,80.263168, "ctf16");
        setNodeHash(13.058842,80.263168, "ctf17");
        setNodeHash(13.058842,80.263125, "ctf18");
        setNodeHash(13.058743,80.263379, "ctf19");
        setNodeHash(13.058766,80.263379, "ctf20");
        setNodeHash(13.058637,80.263379, "ctf21");
        setNodeHash(13.05869,80.263449, "ctf22");
        setNodeHash(13.05869,80.263379, "ctf23");
        setNodeHash(13.05869,80.263619, "ctf24");
        setNodeHash(13.058637,80.263453, "ctf25");
        setNodeHash(13.05857,80.263454, "ctf26");
        setNodeHash(13.058637,80.263496, "ctf27");
        setNodeHash(13.058637,80.263552, "ctf28");
        setNodeHash(13.058637,80.263623, "ctf29");
        setNodeHash(13.058637,80.263704, "ctf30");
        setNodeHash(13.058638,80.263746, "ctf31");
        setNodeHash(13.058638,80.263781, "ctf32");
        setNodeHash(13.05869,80.263781, "ctf33");
        setNodeHash(13.058637,80.263816, "ctf34");
        setNodeHash(13.05862, 80.263842, "ctf35");
        setNodeHash(13.058584, 80.263842, "ctf36");
        setNodeHash(13.058584, 80.263788, "ctf37");
        setNodeHash(13.0586,80.263901, "ctf38");
        setNodeHash(13.058592,80.263979, "ctf39");
        setNodeHash(13.058592,80.264042, "ctf40");
        setNodeHash(13.058592,80.264105, "ctf41");
        setNodeHash(13.058517,80.264105, "ctf42");
        setNodeHash(13.058517,80.263979, "ctf43");
        setNodeHash(13.058517,80.264056, "ctf44");
        setNodeHash(13.058592,80.264134, "ctf45");
        setNodeHash(13.05858,80.264134, "ctf46");
        setNodeHash(13.058564,80.264134, "ctf47");
        setNodeHash(13.058549,80.264134, "ctf48");
        setNodeHash(13.058529,80.264134, "ctf49");
        setNodeHash(13.058505,80.264134, "ctf50");
        setNodeHash(13.0586,80.264176, "ctf51");
        setNodeHash(13.0586,80.264226, "ctf52");
        setNodeHash(13.058569,80.264254, "ctf53");
        setNodeHash(13.058637,80.264253, "ctf54");
        setNodeHash(13.058692,80.264254, "ctf55");
        setNodeHash(13.058683,80.264225, "ctf56");
        setNodeHash(13.058633,80.264225, "ctf58");
        setNodeHash(13.058644,80.264519, "ctf59");
        setNodeHash(13.058644,80.26455, "ctf60");
        setNodeHash(13.058644,80.264592, "ctf61");
        setNodeHash(13.058644,80.264631, "ctf62");
        setNodeHash(13.058644,80.26466, "ctf63");
        setNodeHash(13.058633,80.264722, "ctf64");
        setNodeHash(13.058527,80.264722, "ctf65");
        setNodeHash(13.058527,80.264673, "ctf66");
        setNodeHash(13.058528,80.264635, "ctf67");
        setNodeHash(13.058567,80.264635, "ctf69");
        setNodeHash(13.058567,80.264586, "ctf70");
        setNodeHash(13.058527,80.264586, "ctf71");
        setNodeHash(13.058528, 80.264558, "ctf72");
        setNodeHash(13.058528, 80.264527, "ctf73");
        setNodeHash(13.058551, 80.264527, "ctf74");
        setNodeHash(13.058599, 80.264527, "ctf75");
        setNodeHash(13.058527, 80.264499, "ctf76");
        setNodeHash(13.058528, 80.264443, "ctf77");
        setNodeHash(13.058496,80.264442, "ctf78");
        setNodeHash(13.058547,80.264442, "ctf79");
        setNodeHash(13.058563,80.264442, "ctf80");
        setNodeHash(13.058598,80.264442, "ctf81");
        setNodeHash(13.058614,80.264442, "ctf82");
        setNodeHash(13.058626,80.264442, "ctf83");
        setNodeHash(13.058533,80.264442, "ctf84");
        setNodeHash(13.058533,80.26442, "ctf85");
        setNodeHash(13.058531,80.26437, "ctf86");
        setNodeHash(13.058529,80.264328, "ctf87");
        setNodeHash(13.058529,80.264258, "ctf88");
        setNodeHash(13.058529,80.264222, "ctf89");
        setNodeHash(13.058693,80.264519, "ctf90");
        setNodeHash(13.058693,80.264547, "ctf91");
        setNodeHash(13.058748, 80.264547, "ctf92");
        setNodeHash(13.058637, 80.264352, "ctf93");
        setNodeHash(13.058582, 80.264225, "ctf94");
        setNodeHash(13.058582, 80.26425, "ctf95");
        setNodeHash(13.058637, 80.264442, "ctf96");

    }
    public void setNodeHashForUGF(){

        setNodeHash( 13.058868,80.262972,"cugf1");
        setNodeHash(13.058868,80.263009, "cugf2");
        setNodeHash( 13.058867,80.263113,"cugf3");
        setNodeHash(13.058868,80.263201, "cugf4");
        setNodeHash( 13.058868,80.263282,"cugf5");
        setNodeHash( 13.058868,80.263363,"cugf6");
        setNodeHash( 13.058813,80.263363,"cugf7");
        setNodeHash( 13.058813,80.263289,"cugf8");
        setNodeHash(13.058788,80.263289, "cugf9");
        setNodeHash( 13.058754,80.263289,"cugf10");
        setNodeHash( 13.058733,80.263289,"cugf11");
        setNodeHash(13.058733,80.263363, "cugf12");
        setNodeHash( 13.058733,80.263459,"cugf13");
        setNodeHash(13.05875,80.263459, "cugf14");
        setNodeHash( 13.058775,80.263459,"cugf15");
        setNodeHash(13.058813,80.263459, "cugf16");
        setNodeHash(13.058868,80.263444, "cugf17");
        setNodeHash( 13.058868,80.263518,"cugf18");
        setNodeHash( 13.058867,80.263607,"cugf19");
        setNodeHash(13.058867,80.263687, "cugf20");
        setNodeHash(13.058868,80.263762, "cugf21");
        setNodeHash(13.058867,80.263835, "cugf22");
        setNodeHash(13.058867,80.263917, "cugf23");
        setNodeHash(13.058868,80.26399, "cugf24");
        setNodeHash( 13.058868,80.264064,"cugf25");
        setNodeHash(13.058867,80.264138, "cugf26");
        setNodeHash( 13.058868,80.264212,"cugf27");
        setNodeHash( 13.058867,80.264293,"cugf28");
        setNodeHash(13.058868,80.264381, "cugf29");
        setNodeHash(13.058868,80.264455, "cugf30");
        setNodeHash(13.058868,80.264544, "cugf31");
        setNodeHash( 13.058868,80.264618,"cugf32");
        setNodeHash(13.058867,80.264691, "cugf33");
        setNodeHash( 13.058868,80.264765,"cugf34");
        setNodeHash( 13.058867,80.264854,"cugf35");
        setNodeHash(13.058867,80.264964, "cugf36");
        setNodeHash( 13.058868,80.265031,"cugf37");
        setNodeHash(13.058868,80.265031, "cugf38");
        setNodeHash(13.0588,80.263016, "cugf39");
        setNodeHash(13.058775,80.263016, "cugf40");
        setNodeHash( 13.058775,80.263061,"cugf41");
        setNodeHash( 13.058637,80.263061,"cugf42");
        setNodeHash(13.058637,80.263038, "cugf43");
        setNodeHash( 13.058406,80.263039,"cugf44");
        setNodeHash(13.058397,80.263067, "cugf45");
        setNodeHash( 13.058397,80.263171,"cugf46");
        setNodeHash( 13.058433,80.263171,"cugf47");
        setNodeHash(  13.058433,80.263238,"cugf48");
        setNodeHash(13.058433,80.263311, "cugf49");
        setNodeHash( 13.058397,80.263237,"cugf50");
        setNodeHash( 13.058397,80.263312,"cugf51");
        setNodeHash(13.058406,80.2645, "cugf52");
        setNodeHash(13.058431,80.264691,"cugf53");
        setNodeHash( 13.058448,80.264735,"cugf54");
        setNodeHash( 13.058482,80.26478,"cugf55");
        setNodeHash( 13.058515,80.264824,"cugf56");
        setNodeHash(13.058549,80.264854, "cugf57");
        setNodeHash( 13.058582,80.264869,"cugf58");
        setNodeHash( 13.058633, 80.264876,"cugf60");
        setNodeHash( 13.058632, 80.264758,"cugf61");
        setNodeHash(13.058633, 80.264632, "cugf62");
        setNodeHash(13.058675, 80.264632,"cugf63");
        setNodeHash( 13.058675, 80.264566, "cugf64");
        setNodeHash( 13.058675, 80.264522,"cugf65");
        setNodeHash(13.058674, 80.264492, "cugf66");
        setNodeHash(13.058675, 80.264462,"cugf67");
        setNodeHash(13.058674, 80.264441, "cugf68");
        setNodeHash( 13.058733, 80.26444,"cugf69");
        setNodeHash(13.05875, 80.264441, "cugf70");
        setNodeHash(13.058761, 80.26444,"cugf71");
        setNodeHash(13.05876, 80.264411, "cugf72");
        setNodeHash(13.058761, 80.264374, "cugf73");
        setNodeHash(13.058761, 80.264315,"cugf74");
        setNodeHash( 13.058761, 80.264256,"cugf75");
        setNodeHash( 13.058761, 80.264223,"cugf76");
        setNodeHash( 13.05876, 80.264194,"cugf77");
        setNodeHash(13.058677, 80.26419, "cugf78");
        setNodeHash(13.058677, 80.264223, "cugf79");
        setNodeHash(13.058677, 80.26426, "cugf80");
        setNodeHash(13.058677, 80.264319, "cugf81");
        setNodeHash(13.058677, 80.264371, "cugf82");
        setNodeHash(13.058677, 80.26441, "cugf83");
        setNodeHash(13.05863, 80.26441, "cugf84");
        setNodeHash( 13.05863, 80.264444,"cugf85");
        setNodeHash(13.05863, 80.264481,"cugf86");
        setNodeHash( 13.058632, 80.264588,"cugf87");
        setNodeHash(13.05863, 80.264371,"cugf88");
        setNodeHash( 13.05863, 80.264297,"cugf89");
        setNodeHash(13.058631, 80.264223,"cugf90");
        setNodeHash( 13.05863, 80.264194,"cugf91");
        setNodeHash(13.058567, 80.264193, "cugf92");
        setNodeHash( 13.05863, 80.264179,"cugf93");
        setNodeHash(13.058626, 80.264156,"cugf94");
        setNodeHash( 13.058601, 80.264098,"cugf95");
        setNodeHash( 13.058593, 80.264038,"cugf96");
        setNodeHash(13.058559, 80.264039, "cugf97");
        setNodeHash(13.058534, 80.264039, "cugf98");
        setNodeHash( 13.058593, 80.263957,"cugf99");
        setNodeHash( 13.058559, 80.263957,"cugf100");
        setNodeHash( 13.058534, 80.263957,"cugf101");
        setNodeHash(13.058601, 80.263898, "cugf102");
        setNodeHash(13.058626, 80.263839, "cugf103");
        setNodeHash(13.058631, 80.263817, "cugf104");
        setNodeHash( 13.058677, 80.263817,"cugf105");
        setNodeHash(13.058691, 80.263847, "cugf106");
        setNodeHash(13.058704, 80.263891, "cugf107");
        setNodeHash(  13.058712, 80.263921,"cugf108");
        setNodeHash( 13.058718, 80.263957, "cugf109");
        setNodeHash( 13.058719, 80.264002,"cugf110");
        setNodeHash(13.05874, 80.264002, "cugf111");
        setNodeHash( 13.058677, 80.264178,"cugf112");
        setNodeHash(13.058692, 80.264142, "cugf113");
        setNodeHash(13.058703, 80.264105, "cugf114");
        setNodeHash(13.05871, 80.264075, "cugf115");
        setNodeHash(13.058716, 80.264038, "cugf116");
        setNodeHash( 13.05863, 80.263795,"cugf117");
        setNodeHash( 13.058572, 80.263795,"cugf118");
        setNodeHash( 13.05863, 80.263765, "cugf119");
        setNodeHash( 13.05863, 80.263662,"cugf120");
        setNodeHash( 13.058631, 80.263581,"cugf121");
        setNodeHash(  13.05863, 80.263551,"cugf122");
        setNodeHash( 13.05863, 80.263527,"cugf123");
        setNodeHash(13.058572, 80.263527, "cugf124");
        setNodeHash( 13.05863, 80.263497, "cugf125");
        setNodeHash(13.05863, 80.263463, "cugf126");
        setNodeHash(  13.058677, 80.263463,"cugf127");
        setNodeHash( 13.058677, 80.263512,"cugf128");
        setNodeHash( 13.058676, 80.263553,"cugf129");
        setNodeHash( 13.058731, 80.263553,"cugf130");
        setNodeHash( 13.058758, 80.263553,"cugf131");
        setNodeHash( 13.058758, 80.263603,"cugf132");
        setNodeHash( 13.058758, 80.263677,"cugf133");
        setNodeHash( 13.058758, 80.263728,"cugf134");
        setNodeHash( 13.058758, 80.263758,"cugf135");
        setNodeHash( 13.058758, 80.263795,"cugf136");
        setNodeHash( 13.058677, 80.263795,"cugf137");
        setNodeHash( 13.058676, 80.26377,"cugf138");
        setNodeHash( 13.058677, 80.263733,"cugf139");
        setNodeHash( 13.058676, 80.263696,"cugf140");
        setNodeHash( 13.058676, 80.263659,"cugf141");
        setNodeHash(  13.058676, 80.263622,"cugf142");
        setNodeHash( 13.058676, 80.263581,"cugf143");
        setNodeHash(13.058691, 80.263423, "cugf144");
        setNodeHash(13.058691, 80.263335, "cugf145");
        setNodeHash(13.058676, 80.263305, "cugf146");
        setNodeHash( 13.058643, 80.263304,"cugf147");
        setNodeHash(13.058614, 80.263328, "cugf148");
        setNodeHash(  13.058612, 80.263396,"cugf149");
        setNodeHash(13.058622, 80.263438, "cugf150");
        setNodeHash(  13.058588, 80.263397,"cugf151");
        setNodeHash( 13.058563, 80.263397,"cugf152");
        setNodeHash(13.058529, 80.263397, "cugf153");
        setNodeHash(13.058513, 80.263397, "cugf154");
        setNodeHash(  13.058492, 80.263397,"cugf155");
        setNodeHash( 13.058591, 80.263328,"cugf156");
        setNodeHash(13.058574, 80.263327, "cugf157");
        setNodeHash(13.058551, 80.263327, "cugf158");
        setNodeHash( 13.058525, 80.263327,"cugf159");
        setNodeHash( 13.058691, 80.264913,"cugf160");
        setNodeHash( 13.058758, 80.264912,"cugf161");
        setNodeHash(13.058758, 80.264861, "cugf162");
        setNodeHash( 13.058775, 80.264942,"cugf163");

    }
    public void setNodeHashForLGF(){
        setNodeHash(13.058677,80.264134, "clgf1");
        setNodeHash(13.058653,  80.264134,"clgf2");
        setNodeHash(13.058706,  80.264285, "clgf3");
        setNodeHash(13.058706,  80.264337, "clgf4");
        setNodeHash( 13.05879,  80.264337,"clgf6");
        setNodeHash( 13.058795,  80.264382,"clgf7");
        setNodeHash(13.058794,  80.26447, "clgf8");
        setNodeHash( 13.058794,  80.264559,"clgf9");
        setNodeHash(13.058794,  80.264613, "clgf10");
        setNodeHash(13.058765,  80.264613, "clgf11");
        setNodeHash(13.058706,  80.264613, "clgf12");
        setNodeHash(13.058706,  80.264643, "clgf13");
        setNodeHash(13.058706,  80.264582,"clgf14");
        setNodeHash(13.058706,  80.264537, "clgf15");
        setNodeHash( 13.058706,  80.264492,"clgf16");
        setNodeHash(13.058706,  80.264448, "clgf17");
        setNodeHash(13.058706,  80.264411, "clgf18");
        setNodeHash( 13.058706,  80.264367, "clgf19");
        setNodeHash( 13.058653,  80.264367,"clgf20");
        setNodeHash( 13.058653,  80.264337,"clgf21");
        setNodeHash(13.058601,  80.264337,"clgf22");
        setNodeHash( 13.058653,  80.264396,"clgf23");
        setNodeHash(  13.058651,  80.264455,"clgf24");
        setNodeHash( 13.058652,  80.2645,"clgf25");
        setNodeHash(13.058651,  80.264537, "clgf26");
        setNodeHash(13.058651,  80.264589,"clgf27");
        setNodeHash(13.058651,  80.264634, "clgf28");
        setNodeHash(13.058651,  80.264664, "clgf29");
        setNodeHash(13.058651,  80.264701, "clgf30");
        setNodeHash( 13.058651,  80.264734,"clgf31");
        setNodeHash(13.058633,  80.264734, "clgf32");
        setNodeHash(  13.058614,  80.264734,"clgf33");
        setNodeHash( 13.058564,  80.264734,"clgf34");
        setNodeHash( 13.058547,  80.264735,"clgf35");
        setNodeHash( 13.058547,  80.264616,"clgf36");
        setNodeHash( 13.058547,  80.264579, "clgf37");
        setNodeHash(13.058589,  80.264579, "clgf38");
        setNodeHash(13.058651,  80.264765, "clgf40");
        setNodeHash( 13.058651,  80.264794,"clgf41");
        setNodeHash( 13.058651,  80.264823,"clgf42");
        setNodeHash( 13.058651,  80.264871,"clgf43");
        setNodeHash( 13.058651,  80.264908, "clgf44");
        setNodeHash( 13.058653,  80.264936,"clgf45");
        setNodeHash(13.0587,  80.264824, "clgf46");
        setNodeHash(13.0587,  80.264893, "clgf47");
        setNodeHash( 13.0587,  80.264959,"clgf48");
    }
    public void setNodeHashForB1(){
        setNodeHash(13.058687, 80.26336, "cbf1_1");
        setNodeHash(13.058817, 80.263359, "cbf1_2");
        setNodeHash(13.05877, 80.26336, "cbf1_3");
        setNodeHash(13.058703, 80.263361, "cbf1_4");
        setNodeHash(13.058639, 80.26336, "cbf1_5");
        setNodeHash(13.058601, 80.263359, "cbf1_6");
        setNodeHash(13.058576, 80.26336, "cbf1_7");
        setNodeHash(13.058543, 80.26336, "cbf1_8");
        setNodeHash(13.058525, 80.263361, "cbf1_9");
        setNodeHash(13.058509, 80.26336, "cbf1_10");
        setNodeHash(13.058686, 80.263176, "cbf1_11");
        setNodeHash(13.05864, 80.263177, "cbf1_12");
        setNodeHash(13.058603, 80.263176, "cbf1_13");
        setNodeHash(13.058576, 80.263176, "cbf1_14");
        setNodeHash(13.058527, 80.263175, "cbf1_16");
        setNodeHash(13.058509, 80.263176, "cbf1_17");
        setNodeHash(13.058489, 80.263507, "cbf1_18");
        setNodeHash(13.058487, 80.26336, "cbf1_19");
        setNodeHash(13.058488, 80.263655, "cbf1_20");
        setNodeHash(13.058539, 80.263655, "cbf1_21");
        setNodeHash(13.058539, 80.263878, "cbf1_22");
        setNodeHash(13.058572, 80.263877, "cbf1_23");
        setNodeHash(13.058664, 80.263655, "cbf1_24");
        setNodeHash(13.058703, 80.263655, "cbf1_25");
        setNodeHash(13.058753, 80.263655, "cbf1_26");
        setNodeHash(13.058786, 80.263655, "cbf1_27");
        setNodeHash(13.058786, 80.263862, "cbf1_28");
        setNodeHash(13.058786, 80.264115, "cbf1_29");
        setNodeHash(13.05887, 80.264115, "cbf1_30");
        setNodeHash(13.058702, 80.264115, "cbf1_31");
        setNodeHash(13.058577, 80.264115, "cbf1_32");

    }
    public void setNodeHashForB2(){
        setNodeHash(13.058687, 80.26336, "cbf1_1");
        setNodeHash(13.058817, 80.263359, "cbf1_2");
        setNodeHash(13.05877, 80.26336, "cbf1_3");
        setNodeHash(13.058703, 80.263361, "cbf1_4");
        setNodeHash(13.058639, 80.26336, "cbf1_5");
        setNodeHash(13.058601, 80.263359, "cbf1_6");
        setNodeHash(13.058576, 80.26336, "cbf1_7");
        setNodeHash(13.058543, 80.26336, "cbf1_8");
        setNodeHash(13.058525, 80.263361, "cbf1_9");
        setNodeHash(13.058509, 80.26336, "cbf1_10");
        setNodeHash(13.058686, 80.263176, "cbf1_11");
        setNodeHash(13.05864, 80.263177, "cbf1_12");
        setNodeHash(13.058603, 80.263176, "cbf1_13");
        setNodeHash(13.058576, 80.263176, "cbf1_14");
        setNodeHash(13.058527, 80.263175, "cbf1_16");
        setNodeHash(13.058509, 80.263176, "cbf1_17");
        setNodeHash(13.058489, 80.263507, "cbf1_18");
        setNodeHash(13.058487, 80.26336, "cbf1_19");
        setNodeHash(13.058488, 80.263655, "cbf1_20");
        setNodeHash(13.058539, 80.263655, "cbf1_21");
        setNodeHash(13.058539, 80.263878, "cbf1_22");
        setNodeHash(13.058572, 80.263877, "cbf1_23");
        setNodeHash(13.058664, 80.263655, "cbf1_24");
        setNodeHash(13.058703, 80.263655, "cbf1_25");
        setNodeHash(13.058753, 80.263655, "cbf1_26");
        setNodeHash(13.058786, 80.263655, "cbf1_27");
        setNodeHash(13.058786, 80.263862, "cbf1_28");
        setNodeHash(13.058786, 80.264115, "cbf1_29");
        setNodeHash(13.05887, 80.264115, "cbf1_30");
        setNodeHash(13.058702, 80.264115, "cbf1_31");
        setNodeHash(13.058577, 80.264115, "cbf1_32");

    }
    public void setNodeHashForB3(){
        setNodeHash(13.058687, 80.26336, "cbf1_1");
        setNodeHash(13.058817, 80.263359, "cbf1_2");
        setNodeHash(13.05877, 80.26336, "cbf1_3");
        setNodeHash(13.058703, 80.263361, "cbf1_4");
        setNodeHash(13.058639, 80.26336, "cbf1_5");
        setNodeHash(13.058601, 80.263359, "cbf1_6");
        setNodeHash(13.058576, 80.26336, "cbf1_7");
        setNodeHash(13.058543, 80.26336, "cbf1_8");
        setNodeHash(13.058525, 80.263361, "cbf1_9");
        setNodeHash(13.058509, 80.26336, "cbf1_10");
        setNodeHash(13.058686, 80.263176, "cbf1_11");
        setNodeHash(13.05864, 80.263177, "cbf1_12");
        setNodeHash(13.058603, 80.263176, "cbf1_13");
        setNodeHash(13.058576, 80.263176, "cbf1_14");
        setNodeHash(13.058527, 80.263175, "cbf1_16");
        setNodeHash(13.058509, 80.263176, "cbf1_17");
        setNodeHash(13.058489, 80.263507, "cbf1_18");
        setNodeHash(13.058487, 80.26336, "cbf1_19");
        setNodeHash(13.058488, 80.263655, "cbf1_20");
        setNodeHash(13.058539, 80.263655, "cbf1_21");
        setNodeHash(13.058539, 80.263878, "cbf1_22");
        setNodeHash(13.058572, 80.263877, "cbf1_23");
        setNodeHash(13.058664, 80.263655, "cbf1_24");
        setNodeHash(13.058703, 80.263655, "cbf1_25");
        setNodeHash(13.058753, 80.263655, "cbf1_26");
        setNodeHash(13.058786, 80.263655, "cbf1_27");
        setNodeHash(13.058786, 80.263862, "cbf1_28");
        setNodeHash(13.058786, 80.264115, "cbf1_29");
        setNodeHash(13.05887, 80.264115, "cbf1_30");
        setNodeHash(13.058702, 80.264115, "cbf1_31");
        setNodeHash(13.058577, 80.264115, "cbf1_32");

    }
    private void setNodeHash(double lat, double lon, String storeNo)
    {
        Location loc = new Location(storeNo);
        loc.setLatitude(lat);
        loc.setLongitude(lon);
        nodeHash.put(storeNo, loc);
    }

    private Bitmap loadImageFromStorage(String storeId)
    {
        try {
            ContextWrapper cw = new ContextWrapper(this);
            File directory = cw.getDir("ealogos", Context.MODE_PRIVATE);
            File f = new File(directory.getAbsolutePath(), storeId + ".png");
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            return b;
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        return null;
    }

}
