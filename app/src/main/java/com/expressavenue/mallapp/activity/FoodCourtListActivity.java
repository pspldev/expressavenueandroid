package com.expressavenue.mallapp.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.expressavenue.mallapp.R;
import com.expressavenue.mallapp.fragments.FoodCourtsFragment;
import com.expressavenue.mallapp.fragments.RestaurantsFragment;
import com.expressavenue.mallapp.global.GlobalState;
import com.expressavenue.mallapp.pojos.Store;
import com.expressavenue.mallapp.utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by nivedith on 1/5/2016.
 */
public class FoodCourtListActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private ArrayList<Store> sortedFoodCourtList;
    private ArrayList<Store> foodCourtArrayList;
    private ArrayList<String> foodCourtWithoutFloor;
    private ArrayList<String> mFoodCourt;

    private String foodCourtName = "";
    private String foodCourtFloorName = "";

    private ArrayList<Store> sortedRestaurantList;
    private ArrayList<Store> restaurantArrayList;
    private ArrayList<String> restaurantWithoutFloor;
    private ArrayList<String> mRestaurants;

    private String restaurantName = "";
    private String restaurantFloorName = "";

    private Dialog toolbarSearchDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_tabs);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbarSearchDialog = new Dialog(FoodCourtListActivity.this, R.style.MaterialSearch);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        viewPager.setCurrentItem(0);
        GlobalState.currentFragment = "FOODCOURT";

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int pos = tab.getPosition();
                switch (pos) {
                    case 0:
                        viewPager.setCurrentItem(0);
                        GlobalState.currentFragment = "FOODCOURT";
                        break;
                    case 1:
                        viewPager.setCurrentItem(1);
                        GlobalState.currentFragment = "RESTAURANT";
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        getFoodCourts();
        getRestaurants();
        foodCourtWithoutFloor= new ArrayList<String>();
        restaurantWithoutFloor= new ArrayList<String>();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_search:
                if(GlobalState.currentFragment.equals("FOODCOURT")){
                    loadToolBarSearchFoodCourts();
                }else if(GlobalState.currentFragment.equals("RESTAURANT")) {
                    loadToolBarSearchRestaurants();
                }
                break;
            case android.R.id.home:
                super.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {

        if(toolbarSearchDialog.isShowing())
            toolbarSearchDialog.dismiss();
        else
            super.onBackPressed();
    }

    public void loadToolBarSearchFoodCourts() {

        ArrayList<String> storeStored;
        final View view = FoodCourtListActivity.this.getLayoutInflater().inflate(R.layout.activity_store_tabs, null);
        setViewForLollipop(false, view);
        LinearLayout parentToolbarSearch = (LinearLayout) view.findViewById(R.id.parent_toolbar_search);
        LinearLayout imgToolBack = (LinearLayout) view.findViewById(R.id.layout_img_tool_back);
        final EditText edtToolSearch = (EditText) view.findViewById(R.id.edt_tool_search);
        LinearLayout imgToolClose = (LinearLayout) view.findViewById(R.id.layout_img_tool_close);
        final ListView listSearch = (ListView) view.findViewById(R.id.list_search);
        final TextView txtEmpty = (TextView) view.findViewById(R.id.txt_empty);
        parentToolbarSearch.setVisibility(View.VISIBLE);

        Utils.setListViewHeightBasedOnChildren(listSearch);

        edtToolSearch.setHint("Search Foodcourt");

        toolbarSearchDialog.setContentView(view);
        toolbarSearchDialog.setCancelable(false);
        toolbarSearchDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        toolbarSearchDialog.getWindow().setGravity(Gravity.BOTTOM);
        toolbarSearchDialog.setCanceledOnTouchOutside(true);
        toolbarSearchDialog.show();

        toolbarSearchDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        storeStored = new ArrayList<String>();
        final com.expressavenue.mallapp.adapters.SearchAdapter searchAdapter = new com.expressavenue.mallapp.adapters.SearchAdapter(getApplicationContext(), storeStored, false);

        listSearch.setVisibility(View.VISIBLE);
        listSearch.setAdapter(searchAdapter);

        listSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                RelativeLayout rlView = (RelativeLayout)view;
                if(rlView != null){
                    TextView txtView = (TextView)rlView.getChildAt(0);
                    String str = txtView.getText().toString();
                    String tag = (String) txtView.getTag();

                    Store store = sortedFoodCourtList.get(Integer.parseInt(tag));

                    startActivity(new Intent(getApplicationContext(), LocateStoreActivity.class)
                            .putExtra("storeName", store.getStoreName())
                            .putExtra("storeNumber", store.getStoreNumber())
                            .putExtra("storeCategory", store.getStoreCategory())
                            .putExtra("storeFloor", store.getStoreFloor())
                            .putExtra("storeLatitude", store.getStoreLatitude())
                            .putExtra("storeLongitude", store.getStoreLongitude())
                            .putExtra("storeDesc", store.getStoreDesc())
                            .putExtra("storeLogo", store.getStoreLogo()));
                    finish();
                }
            }
        });

        if(!foodCourtWithoutFloor.isEmpty()){
            foodCourtWithoutFloor.clear();
        }
        for(int i = 0; i < sortedFoodCourtList.size(); i++){
            Store store = sortedFoodCourtList.get(i);
            foodCourtName = store.getStoreName();
            foodCourtFloorName = store.getStoreFloor();
            foodCourtWithoutFloor.add(foodCourtName+"*"+foodCourtFloorName+"*"+i);
        }

        edtToolSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                String[] storesStored = foodCourtWithoutFloor.toArray(new String[foodCourtWithoutFloor.size()]);
                mFoodCourt = new ArrayList<String>(Arrays.asList(storesStored));
                listSearch.setVisibility(View.VISIBLE);
                searchAdapter.updateList(mFoodCourt, true);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ArrayList<String> filterList = new ArrayList<String>();
                boolean isNodata = false;
                if (s.length() > 0) {
                    for(int j = 0; j < mFoodCourt.size(); j++){
                        if (mFoodCourt.get(j).toLowerCase().startsWith(s.toString().trim().toLowerCase())) {

                            filterList.add(mFoodCourt.get(j));

                            listSearch.setVisibility(View.VISIBLE);
                            searchAdapter.updateList(filterList, true);
                            isNodata = true;
                        }

                    }
                    if (!isNodata) {
                        listSearch.setVisibility(View.GONE);
                        txtEmpty.setVisibility(View.VISIBLE);
                        txtEmpty.setText("No data found");
                    }
                } else {
                    listSearch.setVisibility(View.GONE);
                    txtEmpty.setVisibility(View.GONE);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        imgToolBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toolbarSearchDialog.dismiss();
                setViewForLollipop(true, view);
            }
        });

        imgToolClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                edtToolSearch.setText("");
                toolbarSearchDialog.dismiss();
                setViewForLollipop(true, view);
            }
        });

    }

    public void loadToolBarSearchRestaurants() {

        ArrayList<String> storeStored;
        final View view = FoodCourtListActivity.this.getLayoutInflater().inflate(R.layout.activity_store_tabs, null);
        setViewForLollipop(false, view);
        LinearLayout parentToolbarSearch = (LinearLayout) view.findViewById(R.id.parent_toolbar_search);
        LinearLayout imgToolBack = (LinearLayout) view.findViewById(R.id.layout_img_tool_back);
        final EditText edtToolSearch = (EditText) view.findViewById(R.id.edt_tool_search);
        LinearLayout imgToolClose = (LinearLayout) view.findViewById(R.id.layout_img_tool_close);
        final ListView listSearch = (ListView) view.findViewById(R.id.list_search);
        final TextView txtEmpty = (TextView) view.findViewById(R.id.txt_empty);
        parentToolbarSearch.setVisibility(View.VISIBLE);

        Utils.setListViewHeightBasedOnChildren(listSearch);

        edtToolSearch.setHint("Search Restaurant");

        toolbarSearchDialog.setContentView(view);
        toolbarSearchDialog.setCancelable(false);
        toolbarSearchDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        toolbarSearchDialog.getWindow().setGravity(Gravity.BOTTOM);
        toolbarSearchDialog.setCanceledOnTouchOutside(true);
        toolbarSearchDialog.show();

        toolbarSearchDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        storeStored = new ArrayList<String>();
        final com.expressavenue.mallapp.adapters.SearchAdapter searchAdapter = new com.expressavenue.mallapp.adapters.SearchAdapter(getApplicationContext(), storeStored, false);

        listSearch.setVisibility(View.VISIBLE);
        listSearch.setAdapter(searchAdapter);

        listSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                RelativeLayout rlView = (RelativeLayout)view;
                if(rlView != null){
                    TextView txtView = (TextView)rlView.getChildAt(0);
                    String str = txtView.getText().toString();
                    String tag = (String) txtView.getTag();

                    Store store = sortedRestaurantList.get(Integer.parseInt(tag));

                    startActivity(new Intent(getApplicationContext(), LocateStoreActivity.class)
                            .putExtra("storeName", store.getStoreName())
                            .putExtra("storeNumber", store.getStoreNumber())
                            .putExtra("storeCategory", store.getStoreCategory())
                            .putExtra("storeFloor", store.getStoreFloor())
                            .putExtra("storeLatitude", store.getStoreLatitude())
                            .putExtra("storeLongitude", store.getStoreLongitude())
                            .putExtra("storeDesc", store.getStoreDesc())
                            .putExtra("storeLogo", store.getStoreLogo()));
                    finish();
                }
            }
        });

        if(!restaurantWithoutFloor.isEmpty()){
            restaurantWithoutFloor.clear();
        }
        for(int i = 0; i < sortedRestaurantList.size(); i++){
            Store store = sortedRestaurantList.get(i);
            restaurantName = store.getStoreName();
            restaurantFloorName = store.getStoreFloor();
            restaurantWithoutFloor.add(restaurantName+"*"+restaurantFloorName+"*"+i);
        }

        edtToolSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                String[] storesStored = restaurantWithoutFloor.toArray(new String[restaurantWithoutFloor.size()]);
                mRestaurants = new ArrayList<String>(Arrays.asList(storesStored));
                listSearch.setVisibility(View.VISIBLE);
                searchAdapter.updateList(mRestaurants, true);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ArrayList<String> filterList = new ArrayList<String>();
                boolean isNodata = false;
                if (s.length() > 0) {
                    for(int j = 0; j < mRestaurants.size(); j++){
                        if (mRestaurants.get(j).toLowerCase().startsWith(s.toString().trim().toLowerCase())) {

                            filterList.add(mRestaurants.get(j));

                            listSearch.setVisibility(View.VISIBLE);
                            searchAdapter.updateList(filterList, true);
                            isNodata = true;
                        }

                    }
                    if (!isNodata) {
                        listSearch.setVisibility(View.GONE);
                        txtEmpty.setVisibility(View.VISIBLE);
                        txtEmpty.setText("No data found");
                    }
                } else {
                    listSearch.setVisibility(View.GONE);
                    txtEmpty.setVisibility(View.GONE);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        imgToolBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toolbarSearchDialog.dismiss();
                setViewForLollipop(true, view);
            }
        });

        imgToolClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                edtToolSearch.setText("");
                toolbarSearchDialog.dismiss();
                setViewForLollipop(true, view);
            }
        });

    }

    private void getFoodCourts(){

        foodCourtArrayList = new ArrayList<Store>();
        foodCourtArrayList.clear();
        sortedFoodCourtList = new ArrayList<Store>(foodCourtArrayList);

        ParseQuery<ParseObject> query = ParseQuery.getQuery(getResources().getString(R.string.store_class_name));
        query.fromLocalDatastore();
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> storesListFromLocal, ParseException e) {
                if (e == null) {
                    for (ParseObject foodCourt : storesListFromLocal) {

                        String storeName = "";
                        String storeCategory = "";
                        String storeFloor = "";
                        String storeLatitude = "";
                        String storeLongitude = "";
                        String storeLogo = "";
                        String storeDesc = "";
                        String storeNumber = "";

                        boolean storeStatus = false;
                        boolean isStoreDeleted = true;

                        if (foodCourt.has("sFloor")) {
                            if (foodCourt.get("sCategory").equals("FOOD-COURT")) {

                                if (foodCourt.has("deleted"))
                                    isStoreDeleted = foodCourt.getBoolean("deleted");
                                else
                                    isStoreDeleted = true;

                                if (foodCourt.has("sStatus"))
                                    storeStatus = foodCourt.getBoolean("sStatus");
                                else
                                    storeStatus = false;

                                if (foodCourt.has("sName"))
                                    storeName = foodCourt.getString("sName");
                                else
                                    storeName = "NA";

                                if (foodCourt.has("sCategory"))
                                    storeCategory = foodCourt.getString("sCategory");
                                else
                                    storeCategory = "NA";

                                if (foodCourt.has("sFloor"))
                                    storeFloor = foodCourt.getString("sFloor");
                                else
                                    storeFloor = "NA";

                                if (foodCourt.has("sLatitude"))
                                    storeLatitude = foodCourt.getString("sLatitude");
                                else
                                    storeLatitude = "NA";

                                if (foodCourt.has("sLongitude"))
                                    storeLongitude = foodCourt.getString("sLongitude");
                                else
                                    storeLongitude = "NA";

                                if (foodCourt.has("sLogoName")) {
                                    storeLogo = foodCourt.getString("sLogoName");
                                } else
                                    storeLogo = "NA";

                                if (foodCourt.has("sDesc"))
                                    storeDesc = foodCourt.getString("sDesc");
                                else
                                    storeDesc = "NA";

                                if (foodCourt.has("sNumber"))
                                    storeNumber = foodCourt.getString("sNumber");
                                else
                                    storeNumber = "NA";

                                if (storeStatus == true && isStoreDeleted == false) {
                                    foodCourtArrayList.add(new Store(storeName, storeCategory, storeFloor, storeLatitude,
                                            storeLongitude, storeLogo, storeDesc, storeNumber, storeStatus, isStoreDeleted));
                                }
                            }
                        }
                    }
                    Collections.sort(foodCourtArrayList, new CustomComparatorForStores());
                    sortedFoodCourtList.clear();
                    sortedFoodCourtList.addAll(foodCourtArrayList);

                } else {

                }
            }
        });
    }


    private void getRestaurants(){

        restaurantArrayList = new ArrayList<Store>();
        restaurantArrayList.clear();
        sortedRestaurantList = new ArrayList<Store>(restaurantArrayList);

        ParseQuery<ParseObject> query = ParseQuery.getQuery(getResources().getString(R.string.store_class_name));
        query.fromLocalDatastore();
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> storesListFromLocal, ParseException e) {
                if (e == null) {
                    for (ParseObject restaurant : storesListFromLocal) {

                        String storeName = "";
                        String storeCategory = "";
                        String storeFloor = "";
                        String storeLatitude = "";
                        String storeLongitude = "";
                        String storeLogo = "";
                        String storeDesc = "";
                        String storeNumber = "";

                        boolean storeStatus = false;
                        boolean isStoreDeleted = true;

                        if (restaurant.has("sFloor")) {
                            if (restaurant.get("sCategory").equals("RESTAURANTS-CAFE")) {

                                if (restaurant.has("deleted"))
                                    isStoreDeleted = restaurant.getBoolean("deleted");
                                else
                                    isStoreDeleted = true;

                                if (restaurant.has("sStatus"))
                                    storeStatus = restaurant.getBoolean("sStatus");
                                else
                                    storeStatus = false;

                                if (restaurant.has("sName"))
                                    storeName = restaurant.getString("sName");
                                else
                                    storeName = "NA";

                                if (restaurant.has("sCategory"))
                                    storeCategory = restaurant.getString("sCategory");
                                else
                                    storeCategory = "NA";

                                if (restaurant.has("sFloor"))
                                    storeFloor = restaurant.getString("sFloor");
                                else
                                    storeFloor = "NA";

                                if (restaurant.has("sLatitude"))
                                    storeLatitude = restaurant.getString("sLatitude");
                                else
                                    storeLatitude = "NA";

                                if (restaurant.has("sLongitude"))
                                    storeLongitude = restaurant.getString("sLongitude");
                                else
                                    storeLongitude = "NA";

                                if (restaurant.has("sLogoName")) {
                                    storeLogo = restaurant.getString("sLogoName");
                                } else
                                    storeLogo = "NA";

                                if (restaurant.has("sDesc"))
                                    storeDesc = restaurant.getString("sDesc");
                                else
                                    storeDesc = "NA";

                                if (restaurant.has("sNumber"))
                                    storeNumber = restaurant.getString("sNumber");
                                else
                                    storeNumber = "NA";

                                if (storeStatus == true && isStoreDeleted == false) {
                                    restaurantArrayList.add(new Store(storeName, storeCategory, storeFloor, storeLatitude,
                                            storeLongitude, storeLogo, storeDesc, storeNumber, storeStatus, isStoreDeleted));
                                }
                            }
                        }
                    }
                    Collections.sort(restaurantArrayList, new CustomComparatorForStores());
                    sortedRestaurantList.clear();
                    sortedRestaurantList.addAll(restaurantArrayList);

                } else {

                }
            }
        });
    }

    public class CustomComparatorForStores implements Comparator<Store> {
        @Override
        public int compare(Store o1, Store o2) {
            return o1.getStoreName().compareTo(o2.getStoreName());
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new FoodCourtsFragment(), "Food Courts");
        adapter.addFragment(new RestaurantsFragment(), "Restaurants");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void setViewForLollipop(boolean flag, View view){

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.LOLLIPOP){
            if(flag){
                view.findViewById(R.id.app_bar).setVisibility(View.VISIBLE);
                view.findViewById(R.id.viewpager).setVisibility(View.VISIBLE);
                view.findViewById(R.id.toolbar).setVisibility(View.VISIBLE);
            }  else {
                view.findViewById(R.id.app_bar).setVisibility(View.GONE);
                view.findViewById(R.id.viewpager).setVisibility(View.GONE);
                view.findViewById(R.id.toolbar).setVisibility(View.GONE);
            }
        }

    }
}
