package com.expressavenue.mallapp.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Window;

import com.expressavenue.mallapp.R;
import com.expressavenue.mallapp.global.GlobalState;
import com.expressavenue.mallapp.utils.Utils;
import com.parse.ParseObject;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_PHONE_STATE;

/**
 * Created by nivedith on 1/5/2016.
 */
public class SplashScreenActivity extends Activity
{
    private boolean isRunning;
    private static int SPLASH_TIME_OUT = 3000;
    private static final int PERMISSION_REQUEST_CODE = 200;
    static final int LOCATION_SETTINGS_REQUEST = 1;
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.splash);

        isRunning = true;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!checkPermission()) {
                        requestPermission();
                    }else {
                        doFinish();
                    }
                }else {
                    doFinish();
                }
            }
        }, SPLASH_TIME_OUT);

    }

    private synchronized void doFinish()
    {
        TelephonyManager mngr = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        GlobalState.deviceImei = mngr.getDeviceId();
        GlobalState.userObject = new ParseObject("AppUsers");
        GlobalState.userObject.put("deviceImei", GlobalState.deviceImei);

        if (isRunning)
        {
            isRunning = false;
            Intent i = new Intent(SplashScreenActivity.this, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {

        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            isRunning = false;
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /** * Method to check runtime permissions
     * @return
     */
    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION);
        int result1 = ContextCompat.checkSelfPermission(this, ACCESS_COARSE_LOCATION);
        int result2 = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
        int result3 = ContextCompat.checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);
        int result4 = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);
        int result5 = ContextCompat.checkSelfPermission(getApplicationContext(), CALL_PHONE);
        int result6 = ContextCompat.checkSelfPermission(getApplicationContext(), READ_PHONE_STATE);
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED
                && result2 == PackageManager.PERMISSION_GRANTED && result3 == PackageManager.PERMISSION_GRANTED
                && result4 == PackageManager.PERMISSION_GRANTED && result5 == PackageManager.PERMISSION_GRANTED
                && result6 == PackageManager.PERMISSION_GRANTED;
    }
    /** * Method to request for permissions
     */
    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION,
                        WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE, CAMERA, CALL_PHONE, READ_PHONE_STATE},
                PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean coarseAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean writeAccepted = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    boolean readAccepted = grantResults[3] == PackageManager.PERMISSION_GRANTED;
                    boolean cameraAccepted = grantResults[4] == PackageManager.PERMISSION_GRANTED;
                    boolean callPhoneAccepted = grantResults[5] == PackageManager.PERMISSION_GRANTED;
                    boolean phoneStateAccepted = grantResults[6] == PackageManager.PERMISSION_GRANTED;
                    /*if (locationAccepted && coarseAccepted)
                        Utils.showToast(this, "Permission Granted, Now you can access location data.");
                    else {
                        Utils.showToast(this, "Permission Denied, You cannot access location data.");
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(ACCESS_FINE_LOCATION)) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    requestPermissions(new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION},
                                            PERMISSION_REQUEST_CODE);
                                }
                                return;
                            }
                        }
                    }*/
                    if (writeAccepted && readAccepted)
                        Utils.showToast(this, "Permission Granted, Now you can access external storage.");
                    else {
                        Utils.showToast(this, "Permission Denied, You cannot access external storage.");
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE)) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE},
                                            PERMISSION_REQUEST_CODE);
                                }
                                return;
                            }
                        }
                    }
                    if (cameraAccepted)
                        Utils.showToast(this, "Permission Granted, Now you can access camera.");
                    else {
                        Utils.showToast(this, "Permission Denied, You cannot access camera.");
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(CAMERA)) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    requestPermissions(new String[]{CAMERA},
                                            PERMISSION_REQUEST_CODE);
                                }
                                return;
                            }
                        }
                    }
                    if (callPhoneAccepted)
                        Utils.showToast(this, "Permission Granted, Now you can access call.");
                    else {
                        Utils.showToast(this, "Permission Denied, You cannot access call.");
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(CALL_PHONE)) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    requestPermissions(new String[]{CALL_PHONE},
                                            PERMISSION_REQUEST_CODE);
                                }
                                return;
                            }
                        }
                    }
                    if (phoneStateAccepted)
                        Utils.showToast(this, "Permission Granted, Now you can access phone state.");
                    else {
                        Utils.showToast(this, "Permission Denied, You cannot access phone state.");
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(READ_PHONE_STATE)) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    requestPermissions(new String[]{READ_PHONE_STATE},
                                            PERMISSION_REQUEST_CODE);
                                }
                                return;
                            }
                        }
                    }
                    doFinish();
                }
                break;
        }
    }

}
