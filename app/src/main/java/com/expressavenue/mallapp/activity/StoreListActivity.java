package com.expressavenue.mallapp.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.expressavenue.mallapp.R;
import com.expressavenue.mallapp.fragments.CategoryFragment;
import com.expressavenue.mallapp.fragments.OffersByCategoryFragment;
import com.expressavenue.mallapp.fragments.StoresFragment;
import com.expressavenue.mallapp.global.GlobalState;
import com.expressavenue.mallapp.pojos.Category;
import com.expressavenue.mallapp.pojos.Store;
import com.expressavenue.mallapp.utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class StoreListActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private Dialog toolbarSearchDialog;

    private ArrayList<String> mStores;
    private ArrayList<String> mStoresForFloor;
    private StoresFragment storesFragment;

    private ArrayList<CharSequence> searchStoreArrayList;

    private ArrayList<Store> storeArrayList;
    private ArrayList<Store> sortedStoreList;
    private ArrayList<Category> categoryArrayList;
    private ArrayList<Category> sortedCategoriesList;
    private ArrayList<Category> sortedOfferCategoriesList;

    private ArrayList<String> storeWithoutFloor;

    private String storeName ="";
    private String floorName = "";

    private String categoryName = "";
    private String offerCategoryName = "";
    private ArrayList<String> mCategories;
    private ArrayList<String> mOfferCategories;
    private ArrayList<String> categoryNameList;
    private ArrayList<String> offerCategorynameList;

    private HashMap<String, Integer> categoryCountHash;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_tabs);

        getStores();
        getCategories();
        getOfferCategories();

        searchStoreArrayList = new ArrayList<CharSequence>();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbarSearchDialog = new Dialog(StoreListActivity.this, R.style.MaterialSearch);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        viewPager.setCurrentItem(0);
        GlobalState.currentFragment = "STORE";

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int pos = tab.getPosition();
                switch (pos){
                    case 0:
                        viewPager.setCurrentItem(0);
                        GlobalState.currentFragment = "STORE"; break;
                    case 1:
                        viewPager.setCurrentItem(1);
                        GlobalState.currentFragment = "CATEGORY"; break;
                    case 2:
                        viewPager.setCurrentItem(2);
                        GlobalState.currentFragment = "OFFER"; break;
                }
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        storesFragment = new StoresFragment();

        searchStoreArrayList.clear();
        searchStoreArrayList = getIntent().getCharSequenceArrayListExtra("searchStoreArrayList");

        storeWithoutFloor = new ArrayList<String>();
        categoryNameList = new ArrayList<String>();
        offerCategorynameList = new ArrayList<String>();

        categoryCountHash = new HashMap<String, Integer>();
        sortedOfferCategoriesList = new ArrayList<Category>();


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        switch (id) {
            case R.id.action_search:
                if(GlobalState.currentFragment.equals("STORE")){
                    loadToolBarSearchStores();
                }else if(GlobalState.currentFragment.equals("CATEGORY")){
                    loadToolBarSearchCategories();
                }else if(GlobalState.currentFragment.equals("OFFER"))
                    loadToolBarSearchCategoriesForOffer();
                break;
            case android.R.id.home:
                super.onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        if(toolbarSearchDialog.isShowing())
            toolbarSearchDialog.dismiss();
        else
            super.onBackPressed();
    }

    public void loadToolBarSearchCategoriesForOffer(){

        ArrayList<String> storeStored;
        final View view = StoreListActivity.this.getLayoutInflater().inflate(R.layout.activity_store_tabs, null);
        setViewForLollipop(false, view);
        final LinearLayout parentToolbarSearch = (LinearLayout) view.findViewById(R.id.parent_toolbar_search);
        LinearLayout imgToolBack = (LinearLayout) view.findViewById(R.id.layout_img_tool_back);
        final EditText edtToolSearch = (EditText) view.findViewById(R.id.edt_tool_search);
        LinearLayout imgToolClose = (LinearLayout) view.findViewById(R.id.layout_img_tool_close);
        final ListView listSearch = (ListView) view.findViewById(R.id.list_search);
        final TextView txtEmpty = (TextView) view.findViewById(R.id.txt_empty);
        parentToolbarSearch.setVisibility(View.VISIBLE);

        Utils.setListViewHeightBasedOnChildren(listSearch);

        edtToolSearch.setHint("Search Offer Category");

        toolbarSearchDialog.setContentView(view);
        toolbarSearchDialog.setCancelable(false);
        toolbarSearchDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        toolbarSearchDialog.getWindow().setGravity(Gravity.BOTTOM);
        toolbarSearchDialog.setCanceledOnTouchOutside(true);
        toolbarSearchDialog.show();

        toolbarSearchDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        storeStored = new ArrayList<String>();
        storeStored = (storeStored != null && storeStored.size() > 0) ? storeStored : new ArrayList<String>();
        final com.expressavenue.mallapp.adapters.CategorySearchAdapter categorySearchAdapter = new com.expressavenue.mallapp.adapters.CategorySearchAdapter(getApplicationContext(), storeStored, false);

        listSearch.setVisibility(View.VISIBLE);
        listSearch.setAdapter(categorySearchAdapter);

        listSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                RelativeLayout rlView = (RelativeLayout)view;
                if(rlView != null){
                    TextView txtView = (TextView)rlView.getChildAt(0);
                    String str = txtView.getText().toString();
                    String tag = (String) txtView.getTag();

                    Category category = sortedOfferCategoriesList.get(Integer.parseInt(tag));
                    GlobalState.editor.putString("categoryGlobal", category.getCategoryName());
                    GlobalState.editor.commit();
                    startActivity(new Intent(getApplicationContext(), OfferListActivity.class)
                            .putExtra("category", "category"));
                    finish();
                }
            }
        });

        if(!offerCategorynameList.isEmpty()){
            offerCategorynameList.clear();
        }

        for(int i = 0; i < sortedOfferCategoriesList.size(); i++){
            Category category = sortedOfferCategoriesList.get(i);
            offerCategoryName = category.getCategoryName();
            offerCategorynameList.add(offerCategoryName + "*" + i);
        }

        edtToolSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                String[] categoriesStored = offerCategorynameList.toArray(new String[offerCategorynameList.size()]);
                mOfferCategories = new ArrayList<String>(Arrays.asList(categoriesStored));
                listSearch.setVisibility(View.VISIBLE);
                categorySearchAdapter.updateList(mOfferCategories, true);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ArrayList<String> filterList = new ArrayList<String>();
                boolean isNodata = false;
                if (s.length() > 0) {
                    for(int j = 0; j < mOfferCategories.size(); j++){
                        if (mOfferCategories.get(j).toLowerCase().startsWith(s.toString().trim().toLowerCase())) {

                            filterList.add(mOfferCategories.get(j));

                            listSearch.setVisibility(View.VISIBLE);
                            categorySearchAdapter.updateList(filterList, true);
                            isNodata = true;
                        }

                    }
                    if (!isNodata) {
                        listSearch.setVisibility(View.GONE);
                        txtEmpty.setVisibility(View.VISIBLE);
                        txtEmpty.setText("No data found");
                    }
                } else {
                    listSearch.setVisibility(View.GONE);
                    txtEmpty.setVisibility(View.GONE);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        imgToolBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toolbarSearchDialog.dismiss();
                setViewForLollipop(true, view);
            }
        });

        imgToolClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtToolSearch.setText("");
                toolbarSearchDialog.dismiss();
                setViewForLollipop(true, view);
            }
        });
    }

    public void loadToolBarSearchCategories() {

        ArrayList<String> storeStored;
        final View view = StoreListActivity.this.getLayoutInflater().inflate(R.layout.activity_store_tabs, null);
        setViewForLollipop(false, view);
        LinearLayout parentToolbarSearch = (LinearLayout) view.findViewById(R.id.parent_toolbar_search);
        LinearLayout imgToolBack = (LinearLayout) view.findViewById(R.id.layout_img_tool_back);
        final EditText edtToolSearch = (EditText) view.findViewById(R.id.edt_tool_search);
        LinearLayout imgToolClose = (LinearLayout) view.findViewById(R.id.layout_img_tool_close);
        final ListView listSearch = (ListView) view.findViewById(R.id.list_search);
        final TextView txtEmpty = (TextView) view.findViewById(R.id.txt_empty);
        parentToolbarSearch.setVisibility(View.VISIBLE);

        Utils.setListViewHeightBasedOnChildren(listSearch);

        edtToolSearch.setHint("Search Category");

        toolbarSearchDialog.setContentView(view);
        toolbarSearchDialog.setCancelable(false);
        toolbarSearchDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        toolbarSearchDialog.getWindow().setGravity(Gravity.BOTTOM);
        toolbarSearchDialog.setCanceledOnTouchOutside(true);
        toolbarSearchDialog.show();

        toolbarSearchDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        storeStored = new ArrayList<String>();
        storeStored = (storeStored != null && storeStored.size() > 0) ? storeStored : new ArrayList<String>();
        final com.expressavenue.mallapp.adapters.CategorySearchAdapter categorySearchAdapter = new com.expressavenue.mallapp.adapters.CategorySearchAdapter(getApplicationContext(), storeStored, false);

        listSearch.setVisibility(View.VISIBLE);
        listSearch.setAdapter(categorySearchAdapter);

        listSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                RelativeLayout rlView = (RelativeLayout)view;
                if(rlView != null){
                    TextView txtView = (TextView)rlView.getChildAt(0);
                    String str = txtView.getText().toString();
                    String tag = (String) txtView.getTag();

                    Category category = sortedCategoriesList.get(Integer.parseInt(tag));
                    GlobalState.editor.putString("categoryGlobal", category.getCategoryName());
                    GlobalState.editor.commit();
                    startActivity(new Intent(getApplicationContext(), CategoryStoresListActivity.class)
                            .putExtra("category", "category"));
                    finish();
                }
            }
        });


        if(!categoryNameList.isEmpty()){
            categoryNameList.clear();
        }
        for(int i = 0; i < sortedCategoriesList.size(); i++){
            Category category = sortedCategoriesList.get(i);
            categoryName = category.getCategoryName();
            categoryNameList.add(categoryName + "*" + i);
        }

        edtToolSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                String[] categoriesStored = categoryNameList.toArray(new String[categoryNameList.size()]);
                mCategories = new ArrayList<String>(Arrays.asList(categoriesStored));
                listSearch.setVisibility(View.VISIBLE);
                categorySearchAdapter.updateList(mCategories, true);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ArrayList<String> filterList = new ArrayList<String>();
                boolean isNodata = false;
                if (s.length() > 0) {
                    for(int j = 0; j < mCategories.size(); j++){
                        if (mCategories.get(j).toLowerCase().startsWith(s.toString().trim().toLowerCase())) {

                            filterList.add(mCategories.get(j));

                            listSearch.setVisibility(View.VISIBLE);
                            categorySearchAdapter.updateList(filterList, true);
                            isNodata = true;
                        }

                    }
                    if (!isNodata) {
                        listSearch.setVisibility(View.GONE);
                        txtEmpty.setVisibility(View.VISIBLE);
                        txtEmpty.setText("No data found");
                    }
                } else {
                    listSearch.setVisibility(View.GONE);
                    txtEmpty.setVisibility(View.GONE);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        imgToolBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toolbarSearchDialog.dismiss();
                setViewForLollipop(true, view);
            }
        });

        imgToolClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                edtToolSearch.setText("");
                toolbarSearchDialog.dismiss();
                setViewForLollipop(true, view);
            }
        });

    }

    public void loadToolBarSearchStores() {

        ArrayList<String> storeStored;
        final View view = StoreListActivity.this.getLayoutInflater().inflate(R.layout.activity_store_tabs, null);
        setViewForLollipop(false, view);

        LinearLayout parentToolbarSearch = (LinearLayout) view.findViewById(R.id.parent_toolbar_search);
        LinearLayout imgToolBack = (LinearLayout) view.findViewById(R.id.layout_img_tool_back);
        final EditText edtToolSearch = (EditText) view.findViewById(R.id.edt_tool_search);
        LinearLayout imgToolClose = (LinearLayout) view.findViewById(R.id.layout_img_tool_close);
        final ListView listSearch = (ListView) view.findViewById(R.id.list_search);
        final TextView txtEmpty = (TextView) view.findViewById(R.id.txt_empty);
        parentToolbarSearch.setVisibility(View.VISIBLE);

        Utils.setListViewHeightBasedOnChildren(listSearch);

        edtToolSearch.setHint("Search Store");

        toolbarSearchDialog.setContentView(view);
        toolbarSearchDialog.setCancelable(false);
        toolbarSearchDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        toolbarSearchDialog.getWindow().setGravity(Gravity.BOTTOM);
        toolbarSearchDialog.setCanceledOnTouchOutside(true);
        toolbarSearchDialog.show();

        toolbarSearchDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        storeStored = new ArrayList<String>();
        final com.expressavenue.mallapp.adapters.SearchAdapter searchAdapter = new com.expressavenue.mallapp.adapters.SearchAdapter(getApplicationContext(), storeStored, false);

        listSearch.setVisibility(View.VISIBLE);
        listSearch.setAdapter(searchAdapter);

        listSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                RelativeLayout rlView = (RelativeLayout)view;
                if(rlView != null){
                    TextView txtView = (TextView)rlView.getChildAt(0);
                    String str = txtView.getText().toString();
                    String tag = (String) txtView.getTag();

                    Store store = sortedStoreList.get(Integer.parseInt(tag));

                    startActivity(new Intent(getApplicationContext(), LocateStoreActivity.class)
                            .putExtra("storeName", store.getStoreName())
                            .putExtra("storeNumber", store.getStoreNumber())
                            .putExtra("storeCategory", store.getStoreCategory())
                            .putExtra("storeFloor", store.getStoreFloor())
                            .putExtra("storeLatitude", store.getStoreLatitude())
                            .putExtra("storeLongitude", store.getStoreLongitude())
                            .putExtra("storeDesc", store.getStoreDesc())
                            .putExtra("storeLogo", store.getStoreLogo()));
                    finish();
                }
            }
        });

        if(!storeWithoutFloor.isEmpty()){
            storeWithoutFloor.clear();
        }
        for(int i = 0; i < sortedStoreList.size(); i++){
            Store store = sortedStoreList.get(i);
            storeName = store.getStoreName();
            floorName = store.getStoreFloor();
            storeWithoutFloor.add(storeName+"*"+floorName+"*"+i);
        }

        edtToolSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                String[] storesStored = storeWithoutFloor.toArray(new String[storeWithoutFloor.size()]);
                mStores = new ArrayList<String>(Arrays.asList(storesStored));
                listSearch.setVisibility(View.VISIBLE);
                searchAdapter.updateList(mStores, true);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ArrayList<String> filterList = new ArrayList<String>();
                boolean isNodata = false;
                if (s.length() > 0) {
                    for(int j = 0; j < mStores.size(); j++){
                        if (mStores.get(j).toLowerCase().startsWith(s.toString().trim().toLowerCase())) {

                            filterList.add(mStores.get(j));

                            listSearch.setVisibility(View.VISIBLE);
                            searchAdapter.updateList(filterList, true);
                            isNodata = true;
                        }

                    }
                    if (!isNodata) {
                        listSearch.setVisibility(View.GONE);
                        txtEmpty.setVisibility(View.VISIBLE);
                        txtEmpty.setText("No data found");
                    }
                } else {
                    listSearch.setVisibility(View.GONE);
                    txtEmpty.setVisibility(View.GONE);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        imgToolBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toolbarSearchDialog.dismiss();
                setViewForLollipop(true, view);
            }
        });

        imgToolClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                edtToolSearch.setText("");
                toolbarSearchDialog.dismiss();
                setViewForLollipop(true, view);
            }
        });

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new StoresFragment(), "STORES");
        adapter.addFragment(new CategoryFragment(), "CATEGORIES");
        adapter.addFragment(new OffersByCategoryFragment(), "OFFERS");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        public final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void getStores(){

        storeArrayList = new ArrayList<Store>();
        storeArrayList.clear();
        sortedStoreList = new ArrayList<Store>(storeArrayList);

        ParseQuery<ParseObject> query = ParseQuery.getQuery(getResources().getString(R.string.store_class_name));
        query.fromLocalDatastore();
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> storesListFromLocal, ParseException e) {
                if (e == null) {
                    for (ParseObject store : storesListFromLocal) {

                        String storeName = "";
                        String storeCategory = "";
                        String storeFloor = "";
                        String storeLatitude = "";
                        String storeLongitude = "";
                        String storeLogo = "";
                        String storeDesc = "";
                        String storeNumber = "";

                        boolean storeStatus = false;
                        boolean isStoreDeleted = true;

                        if (store.has("sFloor")) {
                            if (!store.get("sCategory").equals("GENERAL") && !store.get("sCategory").equals("RESTROOM") &&
                                    !store.get("sCategory").equals("ATM") &&
                                    !store.get("sCategory").equals("ENTERTAINMENT") && !store.get("sCategory").equals("LIFT")) {

                                if(store.has("deleted"))
                                    isStoreDeleted = store.getBoolean("deleted");
                                else
                                    isStoreDeleted = true;

                                if (store.has("sStatus"))
                                    storeStatus = store.getBoolean("sStatus");
                                else
                                    storeStatus = false;

                                if (store.has("sName"))
                                    storeName = store.getString("sName");
                                else
                                    storeName = "NA";

                                if (store.has("sCategory"))
                                    storeCategory = store.getString("sCategory");
                                else
                                    storeCategory = "NA";

                                if (store.has("sFloor"))
                                    storeFloor = store.getString("sFloor");
                                else
                                    storeFloor = "NA";

                                if (store.has("sLatitude"))
                                    storeLatitude = store.getString("sLatitude");
                                else
                                    storeLatitude = "NA";

                                if (store.has("sLongitude"))
                                    storeLongitude = store.getString("sLongitude");
                                else
                                    storeLongitude = "NA";

                                if (store.has("sLogoName")) {
                                    storeLogo = store.getString("sLogoName");
                                } else
                                    storeLogo = "NA";

                                if (store.has("sDesc"))
                                    storeDesc = store.getString("sDesc");
                                else
                                    storeDesc = "NA";

                                if (store.has("sNumber"))
                                    storeNumber = store.getString("sNumber");
                                else
                                    storeNumber = "NA";

                                if(storeStatus == true && isStoreDeleted==false){
                                    storeArrayList.add(new Store(storeName, storeCategory, storeFloor, storeLatitude,
                                            storeLongitude, storeLogo, storeDesc, storeNumber, storeStatus, isStoreDeleted));
                                }
                            }
                        }
                    }
                    Collections.sort(storeArrayList, new CustomComparatorForStores());
                    sortedStoreList.clear();
                    sortedStoreList.addAll(storeArrayList);

                } else {

                }
            }
        });
    }

    private void getOfferCategories(){

        storeArrayList = new ArrayList<Store>();
        storeArrayList.clear();

        ParseQuery<ParseObject> storeQuery = ParseQuery.getQuery(getResources().getString(R.string.store_class_name));
        storeQuery.fromLocalDatastore();
        storeQuery.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> storesListFromLocal, ParseException e) {
                if (e == null) {
                    for (ParseObject store : storesListFromLocal) {

                        String storeObjectId = "";
                        String storeCategory = "";
                        String storeName = "";

                        //Restricting RESTROOMs from store listing
                        if (store.has("sCategory")) {

                            if (!store.get("sCategory").equals("GENERAL") && !store.get("sCategory").equals("RESTROOM") && !store.get("sCategory").equals("LIFT")) {

                                storeObjectId = store.getObjectId();

                                if (store.has("sCategory"))
                                    storeCategory = store.getString("sCategory");
                                else
                                    storeCategory = "NA";

                                if(store.has("sName"))
                                    storeName = store.getString("sName");
                                else
                                    storeName = "NA";

                                storeArrayList.add(new Store(storeObjectId, storeCategory, storeName));
                            }
                        }
                    }
                } else {

                }
            }

        });

        ParseQuery<ParseObject> offerQuery = ParseQuery.getQuery("Offer");
        offerQuery.fromPin();
        offerQuery.fromLocalDatastore();
        offerQuery.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> offersList, ParseException e) {
                if (e == null) {
                    for (ParseObject offer : offersList) {

                        String storeId = "";
                        boolean isOfferDeleted = true;
                        Date offerEndDate = null;

                        if (offer.has("offerType")) {

                            if (offer.get("offerType").equals("Offer")) {

                                if (offer.has("deleted"))
                                    isOfferDeleted = offer.getBoolean("deleted");
                                else
                                    isOfferDeleted = true;

                                if (offer.has("storeId"))
                                    storeId = offer.getString("storeId");
                                else
                                    storeId = "NA";

                                if (offer.has("offerEndDate"))
                                    offerEndDate = offer.getDate("offerEndDate");
                                else
                                    offerEndDate = null;

                                if (!isOfferDeleted) {

                                    Date today = Calendar.getInstance().getTime();

                                    if ((offerEndDate.compareTo(today)) >= 0) {

                                        for (Store stores : storeArrayList) {

                                            if (storeId.equals(stores.getStoreObjectId())) {

                                                String categoryName = stores.getStoreCategory();

                                                if (categoryCountHash.containsKey(categoryName)) {
                                                    int count = categoryCountHash.get(categoryName);
                                                    categoryCountHash.remove(categoryName);
                                                    count = count + 1;
                                                    categoryCountHash.put(categoryName, count);
                                                } else {
                                                    categoryCountHash.put(categoryName, 1);
                                                }
                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }
                    categoryArrayList = new ArrayList<Category>();
                    categoryArrayList.clear();

                    ParseQuery<ParseObject> query = ParseQuery.getQuery("Categories");
                    query.fromPin();
                    query.fromLocalDatastore();
                    query.findInBackground(new FindCallback<ParseObject>() {
                        public void done(List<ParseObject> categoriesList, ParseException e) {
                            if (e == null) {
                                for (ParseObject category : categoriesList) {

                                    String categoryId;
                                    String categoryName;
                                    String categoyDesc;

                                    boolean isCategoryDeleted = false;

                                    if (category.has("deleted"))
                                        isCategoryDeleted = category.getBoolean("deleted");
                                    else
                                        isCategoryDeleted = true;

                                    if (category.has("categoryId"))
                                        categoryId = category.getString("categoryId");
                                    else
                                        categoryId = "NA";

                                    if (category.has("categoryName"))
                                        categoryName = category.getString("categoryName");
                                    else
                                        categoryName = "NA";

                                    if (category.has("categoyDesc"))
                                        categoyDesc = category.getString("categoyDesc");
                                    else
                                        categoyDesc = "NA";

                                    if (!isCategoryDeleted) {
                                        if (categoryCountHash.containsKey(categoryName))
                                            categoryArrayList.add(new Category(categoryId, categoryName, categoyDesc, isCategoryDeleted, categoryCountHash.get(categoryName)));
                                    }

                                }
                                Collections.sort(categoryArrayList, new CustomComparatorForCategories());

                                sortedOfferCategoriesList.clear();
                                sortedOfferCategoriesList.addAll(categoryArrayList);
                            } else {

                            }
                        }
                    });

                } else {
                }
            }
        });
    }

    private void getCategories(){

        categoryArrayList = new ArrayList<Category>();
        sortedCategoriesList = new ArrayList<Category>(categoryArrayList);
        categoryArrayList.clear();

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Categories");
        query.fromLocalDatastore();
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> categoriesList, ParseException e) {
                if (e == null) {
                    for (ParseObject category : categoriesList) {

                        String categoryId;
                        String categoryName;
                        String categoyDesc;

                        boolean isCategoryDeleted = false;
                        if(!category.getString("categoryName").equals("LIFT")){

                            if(category.has("deleted"))
                                isCategoryDeleted = category.getBoolean("deleted");
                            else
                                isCategoryDeleted = true;

                            if(category.has("categoryId"))
                                categoryId = category.getString("categoryId");
                            else
                                categoryId = "NA";

                            if(category.has("categoryName"))
                                categoryName = category.getString("categoryName");
                            else
                                categoryName = "NA";

                            if(category.has("categoyDesc"))
                                categoyDesc = category.getString("categoyDesc");
                            else
                                categoyDesc = "NA";

                            if(!isCategoryDeleted){
                                int count = 0;
                                for(Store store : sortedStoreList){
                                    String catName = store.getStoreCategory();
                                    if(categoryName.equals(catName)){
                                        count = 1;
                                    }
                                }
                                if(count == 1){
                                    categoryArrayList.add(new Category(categoryId, categoryName, categoyDesc, isCategoryDeleted));
                                }
                            }
                        }
                    }
                    Collections.sort(categoryArrayList, new CustomComparatorForCategories());
                    sortedCategoriesList.clear();
                    sortedCategoriesList.addAll(categoryArrayList);

                } else {

                }
            }
        });
    }

    public class CustomComparatorForStores implements Comparator<Store> {
        @Override
        public int compare(Store o1, Store o2) {
            return o1.getStoreName().compareTo(o2.getStoreName());
        }
    }

    public class CustomComparatorForCategories implements Comparator<Category> {
        @Override
        public int compare(Category o1, Category o2) {
            return o1.getCategoryName().compareTo(o2.getCategoryName());
        }
    }

    private void setViewForLollipop(boolean flag, View view){

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.LOLLIPOP){
            if(flag){
                view.findViewById(R.id.app_bar).setVisibility(View.VISIBLE);
                view.findViewById(R.id.viewpager).setVisibility(View.VISIBLE);
                view.findViewById(R.id.toolbar).setVisibility(View.VISIBLE);
            }  else {
                view.findViewById(R.id.app_bar).setVisibility(View.GONE);
                view.findViewById(R.id.viewpager).setVisibility(View.GONE);
                view.findViewById(R.id.toolbar).setVisibility(View.GONE);
            }
        }

    }
}
